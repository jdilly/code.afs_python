import logging
import os
import shutil
from contextlib import contextmanager, suppress
from pathlib import Path

import tfs
from cpymad.madx import Madx

from cpymad_lhc.general import (get_tfs, switch_magnetic_errors, match_tune, get_k_strings)
from cpymad_lhc.ir_orbit import orbit_setup
from cpymad_lhc.logging import cpymad_logging_setup
from cpymad_lhc.corrector_limits import check_corrector_limits
from irnl_rdt_correction import main as irnl_correct

LOG = logging.getLogger(__name__)  # setup in main()
LOG_LEVEL = logging.DEBUG


PATHS = {
    "db5": Path("/afs/cern.ch/eng/lhc/optics/V6.503"),
    "lhc": Path("/afs/cern.ch/eng/lhc/optics/runIII"),
    "slhc": Path("/afs/cern.ch/eng/lhc/optics/HLLHCV1.3"),
    "external": Path("/afs/cern.ch/work/j/jdilly/public/macros"),
}

TEMPDIR = Path('temp')


def pathstr(key, *args):
    return str(PATHS[key].joinpath(*args))


def make_dirs(outputdir):
    os.makedirs(TEMPDIR, exist_ok=True)
    os.makedirs(outputdir, exist_ok=True)


def clean_up():
    shutil.rmtree(TEMPDIR, ignore_errors=True)


@contextmanager
def temp_disable_errors(madx, *args):
    """ Disable all global variable args and restore their value afterwards."""
    saved = [madx.globals[arg] for arg in args]
    for arg in args:
        madx.globals[arg] = 0
    yield
    for arg, val in zip(args, saved):
        madx.globals[arg] = val


DEFAULT_COLUMNS = ['NAME', 'KEYWORD', 'S', 'LRAD', 'L', 'X', 'Y', 'BETX', 'BETY', 'DX', 'DY', 'MUX', 'MUY',] + get_k_strings()


def main(beam: int = 1, seed: int = 1, outputdir: Path = Path("Outputdata")):
    make_dirs(outputdir)
    madx = Madx(**cpymad_logging_setup(level=LOG_LEVEL,  # sets also standard loggers
                                       command_log=outputdir/'madx_commands.log',
                                       full_log=outputdir/'full_output.log'))
    mvars = madx.globals    # shorthand

    # user definitions
    local_seed = 100 + seed  # random seed for errors
    tune_x = 62.31
    tune_y = 60.32
    dispersion = 3

    # Switch which MQXF errortables to use:
    # 'True' uses the error tables for MQXFA and MQXFB
    # while 'False' the ones for body and fringe
    use_mqxf_ab_errors = False

    # Toggle correction of the errors in the dipole correctors.
    # This correction is not baseline 2020-07-07 as errors change
    # with crossing angle. Hence, this might be less reproducable
    # in reality, use with care!
    correct_mcbx_errors = False

    # apply field errors to arcs
    on_arc_errors = False

    # apply b2s errors in the MQ in the IRs (if on_b2s is active)
    # this is usually deactivated as these errors are assumed to be well corrected
    on_b2s_mq_ir = False

    # apply alignment errors
    on_alignment_errors = False

    energy = 7000            # beam energy in GeV
    emittance_norm = 2.5e-6  # normalized emittance
    n_particles = 1.0e10     # number of particles in beam
    bunch_len = 0.0755       # bunch length [m] in collision

    emittance = emittance_norm * mvars.pmass / energy
    rel_energy_spread = 4.5e-4*(450./energy)**0.5

    # define some global madx variables
    mvars.mylhcbeam = beam  # used in macros
    mvars.on_disp = 0       # correction of spurious dispersion
    mvars.nrj = energy      # used in some macros and below in beam()

    # Output helper
    def output_path(type_, output_id, dir_=outputdir):
        return dir_ / f'{type_}.hllhc.b{beam:d}.{output_id}.tfs'

    def output_twiss(output_id):
        match_tune(madx,
                   accel="HLLHC", sequence=seq_name,
                   qx=tune_x, qy=tune_y,
                   dqx=dispersion, dqy=dispersion)
        madx.twiss(sequence=seq_name)
        tfs_df = get_tfs(madx.table.twiss, columns=DEFAULT_COLUMNS, index_regex=r'BPM|M|IP')
        tfs.write(output_path('twiss', output_id), tfs_df, save_index="NAME")
        return tfs_df

    # Load Macros
    madx.call(pathstr("slhc", "toolkit", "macro.madx"))

    # Lattice Setup ---------------------------------------
    # Load Sequence
    seq_file = "lhc.seq"
    seq_name = f"lhcb{beam}"
    bv_flag = -1 if beam == 2 else 1
    if beam == 4:
        seq_file = "lhcb4.seq"
        seq_name = f"lhcb2"

    madx.call(pathstr('lhc', seq_file))  # LHC sequence
    madx.call(pathstr('slhc', 'hllhc_sequence.madx'))  # updates to HL-LHC

    # slice sequence
    madx.exec('myslice')

    madx.call(pathstr('slhc', 'errors', 'install_mqxf_fringenl.madx'))  # adding fringe place holder

    # HL-LHC V1.3
    madx.call(pathstr('slhc', 'errors', 'install_MCBXAB_errors.madx'))  # adding D1 corrector placeholders in IR1/5 (for errors)

    # HL-LLHC V1.4
    # madx.call(pathstr('slhc', 'errors', 'install_MCBXFAB_errors.madx'))  # adding D1 corrector placeholders in IR1/5 (for errors)
    # madx.call(pathstr('slhc', 'errors', 'install_MCBRD_errors.madx'))   # adding D2 corrector placeholders in IR1/5 (for errors)

    # Cycling w.r.t. to IP3 (mandatory to find closed orbit in collision in the presence of errors)
    madx.seqedit(sequence=seq_name)
    madx.flatten()
    madx.cycle(start="IP3")
    madx.endedit()

    # Define Optics and make beam
    madx.call(pathstr('slhc', 'squeeze2', 'opt_300_300_300_300_thin.madx'))  # 30cm thin round optics

    madx.beam(sequence=seq_name, bv=bv_flag,
              energy="NRJ", sige=rel_energy_spread,
              particle="proton", npart=n_particles,
              ex=emittance, ey=emittance,
              kbunch=1, sigt=bunch_len,
              )

    # Setup Orbit
    orbit_vars = orbit_setup(madx, scheme='flat')

    madx.use(sequence=seq_name)

    # Save Nominal
    df_nominal = output_twiss('nominal_thin')

    # Save nominal optics in IR+Correctors for ir nl correction
    select_pattern = r"M(QS?X|BX|BRC|C[SODT]S?X)"
    if correct_mcbx_errors:
        select_pattern += "|MCBXF"
    df_twiss_nominal_correct = get_tfs(madx.table.twiss, columns=DEFAULT_COLUMNS,
                                       index_regex=select_pattern, )
    tfs.write(output_path('twiss', 'optics_inser', dir_=TEMPDIR), df_twiss_nominal_correct, save_index="NAME")

    # Align separation magnets
    madx.call(pathstr('slhc', 'toolkit', 'align_sepdip.madx'))
    madx.exec('align_mbx15')  # HL-LHC D1
    madx.exec('align_mbrd15')  # HL-LHC D2 in IR15
    madx.exec('align_mbx28')  # V6.503 D1 in IR28
    madx.exec('align_mbrc28')  # V6.503 D2 in IR28
    madx.exec('align_mbrs')   # V6.503 D3 in IR4
    madx.exec('align_mbrb')   # V6.503 D4 in IR4
    madx.call(pathstr('slhc', 'toolkit', 'align_mbh.madx'))  # align 11T dipoles

    # Error subroutines --------------------------------------------------------
    # Error routine and measured error table for nominal LHC

    madx.call(pathstr('db5', 'measured_errors', 'Msubroutines_new.madx'))
    madx.call(pathstr('db5', 'measured_errors', 'Msubroutines_MS_MSS_MO_new.madx'))
    madx.call(pathstr('db5', 'toolkit', 'Orbit_Routines.madx'))
    madx.call(pathstr('slhc', 'errors', 'SelectLHCMonCor.madx'))
    madx.readtable(file=pathstr('db5', 'measured_errors', 'rotations_Q2_integral.tab'))

    # Error routine and error table for new IT/D1/D2/Q4/Q5
    madx.call(pathstr('slhc', 'errors', 'macro_error.madx'))  # macros for error generation in the new IT/D1's
    if use_mqxf_ab_errors:
        LOG.debug("Using MQXF AB error-tables")
        madx.call(pathstr('slhc', 'errors', 'ITa_errortable_v4'))  # target error table for the new IT
        madx.call(pathstr('slhc', 'errors', 'ITb_errortable_v5'))  # target error table for the new IT
    else:
        LOG.debug("Using MQXF body and fringe  error-tables")
        madx.call(pathstr('slhc', 'errors', 'ITbody_errortable_v5'))  # target error table for the new IT
        madx.call(pathstr('slhc', 'errors', 'ITnc_errortable_v5'))  # target error table for the new IT
        madx.call(pathstr('slhc', 'errors', 'ITcs_errortable_v5'))  # target error table for the new IT
        mvars.b6M_MQXF_col = -4  # New baseline value, if using version > v5 might already be in

    madx.call(pathstr('slhc', 'errors', 'D1_errortable_v1'))   # target error table for the new D1
    madx.call(pathstr('slhc', 'errors', 'D2_errortable_v5'))  # target error table for the new D2

    # HL-LHC v1.3
    madx.call(pathstr('slhc', 'errors', 'Q4_errortable_v2'))  # target error table for the new Q4 in IR1 and IR5

    # HL-LHC v1.4
    # madx.call(pathstr('slhc', 'errors', 'MCBXFAB_errortable_v1'))
    # madx.call(pathstr('slhc', 'errors', 'MBH_errortable_v3'))
    # madx.call(pathstr('slhc', 'errors', 'MCBRD_errortable_v1'))

    # Apply magnetic errors ----------------------------------------------------
    # Shorthand definitions ---
    def apply_measured_errors(*magnets):
        for magnet in magnets:
            madx.call(pathstr('db5', 'measured_errors', f'Efcomp_{magnet}.madx'))

    def apply_hllhc_errors(*magnets):
        for magnet in magnets:
            madx.call(pathstr('slhc', 'errors', f'Efcomp_{magnet}.madx'))

    def set_eseed(add_to_seed):
        madx.eoption(seed=local_seed+add_to_seed)

    # Set which magnetic field error orders to use
    switch_magnetic_errors(madx, AB3=True, AB4=True, AB5=True, AB6=True)

    # Read WISE
    madx.readtable(file=f"/afs/cern.ch/work/j/jdilly/wise/"
                        f"WISE-2015-LHCsqueeze-0.4_10.0_0.4_3.0-6.5TeV-emfqcs/WISE.errordef.{seed:04d}.tfs")

    # Apply Arc Errors ---
    if on_arc_errors:
        apply_measured_errors('MB')

        # Correct orbit distortion resulting from MB magnets (if present)
        if any(mvars[f'on_{field}'] for field in ('A1s', 'A1r', 'B1s' 'B1r')):
            madx.exec('initial_micado(4)')
            madx.exec('initial_micado(4)')

    # Other Errors ---
    # Nominal LHC magnets
    # Separation Dipoles
    apply_measured_errors('MBRB', 'MBRC', 'MBRS', 'MBX', 'MBW')

    # Quadrupoles
    disable = []
    if not on_b2s_mq_ir:
        disable = ['on_b2s']
    with temp_disable_errors(madx, *disable):
        apply_measured_errors('MQW', 'MQTL', 'MQMC', 'MQX',
                              'MQY', 'MQM', 'MQML', 'MQ'
                              )

    if on_alignment_errors:
        madx.call(pathstr('db5', 'measured_errors', 'Set_alignment_errors.madx'))

    # New IT/D1/D2/Q4/Q5
    set_eseed(1)
    if use_mqxf_ab_errors:
        apply_hllhc_errors('MQXFA', 'MQXFB')
    else:
        madx.call(pathstr('external', 'Efcomp_MQXFbody.madx'))  # special version
        apply_hllhc_errors('MQXFA', 'MQXFB', 'MQXFends')

    set_eseed(2)
    apply_hllhc_errors('MBXAB')  # new D1 in IR 1/5

    set_eseed(3)
    apply_hllhc_errors('MBRD')  # new D2 in IR1/5

    set_eseed(4)
    # HL-LHC v1.3
    apply_hllhc_errors('MQYY')  # new Q4 in IR1/5

    # HL-LHC v1.4
    # apply_hllhc_errors('MQY')  # ! old Q4 in IR1/5, but switched places around IP1-5

    # set_eseed(5)
    # apply_hllhc_errors('MCBXFAB')  # new triplet correctors in IR1/5

    # set_eseed(6)
    # with temp_disable_errors(madx, 'on_b2s', 'on_b2r'):  # Assumed to be well corrected? (jdilly)
    #     apply_hllhc_errors('MBH')

    # set_eseed(7)
    # apply_hllhc_errors('MCBRD')

    # Save uncorrected
    df_uncorrected = output_twiss('uncorrected')

    # Save errors to table to be used for correction ---------------------------
    # As far as I can tell `only_selected` does not work with
    # etable and there is always only the selected items in the table
    # (jdilly, cpymad 1.4.1)
    error_columns = ["NAME" + "DX", "DY"] + get_k_strings()
    madx.select(flag='error', clear=True)
    madx.select(flag='error', class_="multipole", column=error_columns)
    madx.etable(table='error')
    df_errors_correct = get_tfs(madx.table.error, index_regex=r"M[QB]X", columns=error_columns)  # can add MBRC as well
    tfs.write(output_path('errors', 'tripD1', dir_=TEMPDIR), df_errors_correct, save_index="NAME")

    # IR Nonlinear Correction: Calculate corrector powering --------------------
    correction_cmd, correction_df = irnl_correct(
        # Correction setup for HLLHC ---
        accel='hllhc',
        beams=(1,),
        optics=(df_twiss_nominal_correct,),
        errors=(df_errors_correct,),
        # rdts=,  # use default
        output=output_path('settings', 'mcx'),
        # The following are the defaults ---
        feeddown=0,  # order of feeddown to take into account
        ips=(1, 2, 5, 8),  # in which IPs to correct
        solver='lstsq',  # 'inv', 'linear' Solver to use
        update_optics=False,  # updates optics after setting corrector strength (for feeddown)
        ignore_missing_columns=False,  # True: if columns are missing, assume 0
    )

    # Apply correction
    madx.input(correction_cmd)
    with suppress(ValueError):
        check_corrector_limits(madx, accel="HLLHC", beam=beam)

    # Save corrected
    df_corrected = output_twiss('corrected')

    madx.exit()
    clean_up()


if __name__ == '__main__':
    main()
