setup1 = dict(
    rdts=["F1002", "F1011"],
    feeddown=0,
    xing=dict(),
    errors=dict(B3=True),
)
setup2 = dict(
    rdts=["F1002", "F1011"],
    feeddown=0,
    xing=dict(on_x1=-160),
    errors=dict(B3=True),
)
setup3 = dict(
    rdts=["F1002", "F1011"],
    feeddown=1,
    xing=dict(on_x1=-160),
    errors=dict(B4=True),
)
setup4 = dict(
    rdts=["F1002", "F1011"],
    feeddown=1,
    xing=dict(on_x1=-160),
    errors=dict(B3=True, B4=True),
)
setup5 = dict(
    rdts={"F1002": ["b4"], "F1011": ["b4"]},
    feeddown=0,
    xing=dict(on_x1=-160),
    errors=dict(B3=True),
)
setup6 = dict(
    rdts={"F1002": ["b4", "b3"], "F1011": ["b4", "b3"]},
    feeddown=0,
    xing=dict(on_x1=-160),
    errors=dict(B3=True, B4=True),
)
setup7 = dict(
    rdts={"F1002": ["b4", "b3"], "F1011": ["b4", "b3"]},
    feeddown=1,
    xing=dict(on_x1=-160),
    errors=dict(B3=True, B4=True),
)
setup8 = dict(
    rdts={"F1002": ["b3"], "F1011": ["b3"], "F4000": ["b4"], "F0004": ["b4"]},
    feeddown=0,
    xing=dict(on_x1=-160),
    errors=dict(B3=True, B4=True),
)
setup9 = dict(
    rdts={"F1002": ["b3"], "F1011": ["b3"], "F4000": ["b4"], "F0004": ["b4"]},
    feeddown=1,
    xing=dict(on_x1=-160),
    errors=dict(B3=True, B4=True),
)
setup10 = dict(
    rdts={"F1002": ["b3", "b4"], "F1011": ["b3", "b4"], "F4000": ["b4"], "F0004": ["b4"]},
    feeddown=0,
    xing=dict(on_x1=-160),
    errors=dict(B3=True, B4=True),
)
setup11 = dict(
    rdts={"F1002": ["b3", "b4"], "F1011": ["b3", "b4"], "F4000": ["b4"], "F0004": ["b4"]},
    feeddown=1,
    xing=dict(on_x1=-160),
    errors=dict(B3=True, B4=True),
)

setup12 = dict(
    rdts={"F1002": ["b4"], "F1011": ["b4"], "F4000": ["b4"], "F0004": ["b4"]},
    feeddown=0,
    xing=dict(on_x1=-160),
    errors=dict(B3=True, B4=True),
)

description = {
    1: "MCS, flat, b3 errors",
    2: "MCS, b3 errors",
    3: "MCS, b4 errors, with feeddown",
    4: "MCS, b3+b4 errors, with feeddown",
    5: "MCO, b3 errors",
    6: "MCS+MCO, b3+b4 errors, no feeddown",
    7: "MCS+MCO, b3+b4 errors, with feeddown",
    8: "MCS, MCO, b3+b4 errors, separate correction, no feeddown",
    9: "MCS, MCO, b3+b4 errors, separate correction, with feeddown",
    10: "MCS + MCO, b3+b4 errors, combined correction, no feeddown",
    11: "MCS + MCO, b3+b4 errors, combined correction, with feeddown",
    12: "MCS, b3+b4 errors, no feeddown",
}