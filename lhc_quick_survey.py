import logging
from pathlib import Path

import tfs
from cpymad.madx import Madx

from cpymad_lhc.general import (get_tfs, match_tune, get_k_strings, get_lhc_sequence_filename_and_bv)
from cpymad_lhc.ir_orbit import orbit_setup, log_orbit
from cpymad_lhc.logging import cpymad_logging_setup

LOG = logging.getLogger(__name__)  # setup in main()
LOG_LEVEL = logging.DEBUG

OUTDIR = Path("Outputdata")
TEMPDIR = Path('temp')


PATHS = {
    "db5": Path("/afs/cern.ch/eng/lhc/optics/V6.503"),
    "slhc": Path("/afs/cern.ch/eng/lhc/optics/SLHCV1.0"),
    "optics2016": Path("/afs/cern.ch/eng/lhc/optics/runII/2016"),
    "optics2018": Path("/afs/cern.ch/eng/lhc/optics/runII/2018"),
    "external": Path("/afs/cern.ch/work/j/jdilly/public/macros"),
}


def pathstr(key: str, *args: str):
    return str(PATHS[key].joinpath(*args))


def get_optics_path(optics: str):
    optics_map = {
        'inj': pathstr("optics2018", "PROTON", "opticsfile.1"),
        'flat6015': pathstr("optics2018", 'MDflatoptics2018', 'opticsfile_flattele60cm.21'),
        'round3030': pathstr("optics2018", "PROTON", "opticsfile.22_ctpps2")
    }
    return optics_map[optics]


def make_dirs(*dirs: Path):
    for dir_ in dirs:
        dir_.mkdir(exist_ok=True, parents=True)


def drop_allzero_columns(df: tfs.TfsDataFrame):
    return df.loc[:, (df != 0).any(axis="index")]


DEFAULT_COLUMNS = ['NAME', 'KEYWORD', 'S', 'X', 'Y', 'L', 'LRAD', 'ANGLE', 'PX', 'PY',
                   'BETX', 'BETY', 'ALFX', 'ALFY', 'DX', 'DY', 'MUX', 'MUY',
                   'R11', 'R12', 'R21', 'R22'] + get_k_strings()


def main(xing: dict, optics: str, outputdir: Path = OUTDIR, beam: int = None):
    make_dirs(outputdir)
    madx = Madx(**cpymad_logging_setup(level=LOG_LEVEL,  # sets also standard loggers
                                       command_log=outputdir/'madx_commands.log',
                                       full_log=outputdir/'full_output.log'))
    mvars = madx.globals  # shorthand

    # user definitions
    tune_x = 62.31
    tune_y = 60.32
    dispersion = 3
    emittance = 7.29767146889e-09
    n_particles = 1.0e10   # number of particles in beam

    # Output helper
    def output_path(type_, output_id, dir_=outputdir):
        return dir_ / f'{type_}.lhc.b{beam:d}.{output_id}.tfs'

    def output_twiss(output_id, index_regex=r"BPM|M|IP", dir_=outputdir, pre_match_tune=True):
        if pre_match_tune:
            match_tune(madx,
                       accel="LHC", sequence=seq_name,
                       qx=tune_x, qy=tune_y,
                       dqx=dispersion, dqy=dispersion)
        madx.twiss(sequence=seq_name)
        df_twiss = get_tfs(madx.table.twiss, columns=DEFAULT_COLUMNS, index_regex=index_regex)
        if dir_ is not None:
            tfs.write(output_path('twiss', output_id, dir_=dir_), drop_allzero_columns(df_twiss), save_index="NAME")
        return df_twiss

    def output_survey(output_id, index_regex=r"BPM|M|IP", dir_=outputdir):
        madx.survey(sequence=seq_name)
        df_survey = get_tfs(madx.table.survey, index_regex=index_regex, remove_element_numbering=False)
        if dir_ is not None:
            tfs.write(output_path('survey', output_id, dir_=dir_), drop_allzero_columns(df_survey), save_index="NAME")
        return df_survey

    # Load Macros
    madx.call(pathstr("optics2018", "toolkit", "macro.madx"))

    # Lattice Setup ---------------------------------------
    # Load Sequence
    if beam == 4:
        madx.call(pathstr("optics2018", "lhcb4_as-built.seq"))
    else:
        madx.call(pathstr("optics2018", "lhc_as-built.seq"))

    # Define Optics and make beam
    # madx.call(get_optics_path("inj"))
    madx.call(get_optics_path(optics))
    if optics == 'inj':
        mvars.NRJ = 450.000

    # Setup Orbit
    orbit_vars = orbit_setup(madx, accel='lhc', **xing)

    beams = (1, 2) if beam is None else (beam, )
    for beam in beams:
        seq_name, seq_file, bv_flag = get_lhc_sequence_filename_and_bv(beam)

        madx.beam(sequence=seq_name, bv=bv_flag,
                  energy="NRJ", particle="proton", npart=n_particles,
                  kbunch=1, ex=emittance, ey=emittance)

        madx.use(sequence=seq_name)

        # Save Nominal
        df_nominal = output_twiss('nominal')
        df_nominal = output_survey('nominal')
        log_orbit(madx, accel="lhc")

    madx.exit()