import logging
import shutil
from contextlib import contextmanager, suppress
from pathlib import Path
from typing import Sequence

import tfs
from cpymad.madx import Madx
from optics_functions.coupling import coupling_via_cmatrix, closest_tune_approach
from optics_functions.rdt import calculate_rdts
from optics_functions.utils import prepare_twiss_dataframe

from cpymad_lhc.corrector_limits import check_corrector_limits
from cpymad_lhc.coupling_correction import correct_coupling
from cpymad_lhc.general import (get_tfs, switch_magnetic_errors, match_tune, get_k_strings, amplitude_detuning_ptc, sixtrack_output, rdts_ptc,
                                get_lhc_sequence_filename_and_bv, get_kqs_for_coupling_correction)
from cpymad_lhc.ir_orbit import orbit_setup, log_orbit, check_crabbing
from cpymad_lhc.logging import cpymad_logging_setup, MADXCMD, MADXOUT
from pylhc.irnl_rdt_correction import main as irnl_correct, write_tfs, write_command, log_setup

LOG = logging.getLogger(__name__)  # setup in main()
LOG_LEVEL = logging.DEBUG

OUTDIR = Path("Outputdata")
TEMPDIR = Path('temp')

RDT_PART_MAP = {
    "A": "ABS",
    "C": "REAL",
    "S": "IMAG",
}

PATHS = {
    "db5": Path("/afs/cern.ch/eng/lhc/optics/V6.503"),
    "lhc": Path("/afs/cern.ch/eng/lhc/optics/runIII"),
    "slhc": Path("/afs/cern.ch/eng/lhc/optics/HLLHCV1.4"),
    "wise": Path("/afs/cern.ch/eng/lhc/optics/errors/0705"),
    "external": Path("/afs/cern.ch/work/j/jdilly/public/macros"),
}


def pathstr(key: str, *args: str):
    return str(PATHS[key].joinpath(*args))


def get_optics_path(optics: str):
    optics_map = {
        'inj': pathstr("slhc", "opt_inj_thin.madx"),
        'flat07530': pathstr("slhc", 'flat', 'opt_flatvh_75_300_thin.madx'),
        'round1515': pathstr('slhc', 'round', 'opt_round_150_1500_thin.madx'),
    }
    return optics_map[optics]


def reinstate_loggers(beam_data: dict, beams):
    """ Put the handlers back into the loggers"""
    if isinstance(beams, int):
        beams = [beams]
    for logger in ("", MADXCMD, MADXOUT):
        logging.getLogger(logger).handlers = [h for beam in beams for h in beam_data[beam][logger or 'logger']]


def save_loggers(beam_data: dict):
    for logger in ("", MADXCMD, MADXOUT):
        beam_data[logger or "logger"] = logging.getLogger(logger).handlers.copy()
    return beam_data


def make_dirs(*dirs: Path):
    for dir_ in dirs:
        dir_.mkdir(exist_ok=True, parents=True)


def drop_allzero_columns(df: tfs.TfsDataFrame):
    return df.loc[:, (df != 0).any(axis="index")]


def get_other_beam(beam: int):
    return 1 if beam == 4 else 4


def write_mb_optics(madx):
    madx.select(flag="twiss", clear=True)
    madx.select(flag="twiss", pattern="MB\.", class_="multipole", column="name,k0L,k1L,betx,bety,dx,mux,muy".split(","))
    madx.select(flag="twiss", pattern="MBH\.", class_="multipole", column="name,k0L,k1L,betx,bety,dx,mux,muy".split(","))
    madx.select(flag="twiss", pattern="MQT\.14", class_="multipole", column="name,k0L,k1L,betx,bety,dx,mux,muy".split(","))
    madx.select(flag="twiss", pattern="MQT\.15", class_="multipole", column="name,k0L,k1L,betx,bety,dx,mux,muy".split(","))
    madx.select(flag="twiss", pattern="MQT\.16", class_="multipole", column="name,k0L,k1L,betx,bety,dx,mux,muy".split(","))
    madx.select(flag="twiss", pattern="MQT\.17", class_="multipole", column="name,k0L,k1L,betx,bety,dx,mux,muy".split(","))
    madx.select(flag="twiss", pattern="MQT\.18", class_="multipole", column="name,k0L,k1L,betx,bety,dx,mux,muy".split(","))
    madx.select(flag="twiss", pattern="MQT\.19", class_="multipole", column="name,k0L,k1L,betx,bety,dx,mux,muy".split(","))
    madx.select(flag="twiss", pattern="MQT\.20", class_="multipole", column="name,k0L,k1L,betx,bety,dx,mux,muy".split(","))
    madx.select(flag="twiss", pattern="MQT\.21", class_="multipole", column="name,k0L,k1L,betx,bety,dx,mux,muy".split(","))
    madx.select(flag="twiss", class_="MQS", column="name,k0L,k1L,betx,bety,dx,mux,muy".split(","))
    madx.select(flag="twiss", class_="MSS", column="name,k0L,k1L,betx,bety,dx,mux,muy".split(","))
    madx.select(flag="twiss", class_="MCO", column="name,k0L,k1L,betx,bety,dx,mux,muy".split(","))
    madx.select(flag="twiss", class_="MCD", column="name,k0L,k1L,betx,bety,dx,mux,muy".split(","))
    madx.select(flag="twiss", class_="MCS", column="name,k0L,k1L,betx,bety,dx,mux,muy".split(","))
    madx.twiss(file='temp/optics0_MB.mad')


def empty_mbrd_errortable(madx, keep=()):
    for opt in ('col', 'inj'):
        for orientation in 'ab':
            for i in range(1, 16):
                order = f"{orientation}{i}"
                if order in keep:
                    continue
                madx.globals[f"{order}M_MBRD_V1_{opt}"] = 0.
                madx.globals[f"{order}M_MBRD_V2_{opt}"] = 0.
                madx.globals[f"{order}U_MBRD_{opt}"] = 0.
                madx.globals[f"{order}R_MBRD_{opt}"] = 0.


@contextmanager
def temp_disable_errors(madx, *args):
    """ Disable all global variable args and restore their value afterwards."""
    saved = [madx.globals[arg] for arg in args]
    for arg in args:
        madx.globals[arg] = 0
    yield
    for arg, val in zip(args, saved):
        madx.globals[arg] = val


DEFAULT_COLUMNS = ['NAME', 'KEYWORD', 'S', 'X', 'Y', 'L', 'LRAD',
                   'BETX', 'BETY', 'ALFX', 'ALFY', 'DX', 'DY', 'MUX', 'MUY',
                   'R11', 'R12', 'R21', 'R22'] + get_k_strings()


def main(correct_beam: str,
         rdts: Sequence,
         xing: dict,
         errors: dict,
         optics: str,
         d2b5_value: int,
         d1b5_value: int,
         limit_corrector_strength: bool,
         outputdirs: dict,
         correct_b5: str = ('d1', 'd2'),
         feeddown: int = 0,
         iterations: int = 1,
         rdts2: Sequence = None,
         seed: int = 1,
         ):
    make_dirs(*outputdirs.values(), TEMPDIR)

    # user definitions
    machine_state = 'injection' if optics == 'inj' else 'collision'
    local_seed = 100 + seed  # random seed for errors
    tune_x = 62.31
    tune_y = 60.32
    chroma = 3

    intermediate_tune_match = False  # match tunes before nominal and uncorrected output

    # Switch which MQXF errortables to use:
    # 'True' uses the error tables for MQXFA and MQXFB
    # while 'False' the ones for body and fringe
    use_mqxf_ab_errors = False

    # Toggle correction of the errors in the dipole correctors.
    # This correction is not baseline 2020-07-07 as errors change
    # with crossing angle. Hence, this might be less reproducable
    # in reality, use with care!
    correct_mcbx_errors = True

    # apply field errors to arcs
    on_arc_errors = True

    # apply b2s errors in the MQ in the IRs (if on_b2s is active)
    # this is usually deactivated as these errors are assumed to be well corrected
    on_b2s_mq = False

    # apply alignment errors
    on_alignment_errors = True

    # beam energy in GeV
    if optics == "inj":
        energy = 450.
        bunch_len = 0.0130        # bunch length [m] in collision
    else:
        energy = 7000.
        bunch_len = 0.075        # bunch length [m] in collision

    on_disp = energy > 5000      # correction of spurious dispersion
    on_qpp = False               # correction of residual Q'' by MO's

    emittance_norm = 2.5e-6  # normalized emittance
    n_particles = 2.2e11     # number of particles in beam

    rel_energy_spread = 4.5e-4*(450./energy)**0.5

    beam_data = {beam: {} for beam in outputdirs.keys()}
    for beam, outputdir in outputdirs.items():
        madx = Madx(**cpymad_logging_setup(level=LOG_LEVEL,  # sets also standard loggers
                                           command_log=outputdir/'madx_commands.log',
                                           full_log=outputdir/'full_output.log'))
        mvars = madx.globals    # shorthand

        # Define some global (madx) variables
        emittance = emittance_norm * mvars.pmass / energy
        mvars.mylhcbeam = beam  # used in macros
        mvars.nrj = energy  # used in some macros and below in beam()

        # Define Output Helper -------------------------------------------------
        def output_path(type_, output_id, dir_=None):
            if dir_ is None:
                dir_ = outputdir
            return dir_ / f'{type_}.hllhc.b{beam:d}.{output_id}.tfs'

        def get_twiss(output_id=None, index_regex=r"BPM|M|IP", dir_=None, pre_match_tune=True):
            if dir_ is None:
                dir_ = outputdir
            if pre_match_tune:
                match_tune(madx,
                           accel="HLLHC", sequence=seq_name,
                           qx=tune_x, qy=tune_y,
                           dqx=chroma, dqy=chroma)
            madx.twiss(sequence=seq_name)
            tfs_df = get_tfs(madx.table.twiss, columns=DEFAULT_COLUMNS, index_regex=index_regex)
            if output_id is not None:
                tfs.write(output_path('twiss', output_id, dir_=dir_), drop_allzero_columns(tfs_df), save_index="NAME")
            return tfs_df

        def get_errors(output_id=None, index_regex="M", dir_=None):
            if dir_ is None:
                dir_ = outputdir
            # As far as I can tell `only_selected` does not work with
            # etable and there is always only the selected items in the table
            # (jdilly, cpymad 1.4.1)
            error_columns = ["NAME", "DX", "DY"] + get_k_strings()
            madx.select(flag='error', clear=True)
            madx.select(flag='error', column=error_columns)
            madx.etable(table='error')
            df_errors = get_tfs(madx.table.error, index_regex=index_regex, columns=error_columns)
            if output_id is not None:
                tfs.write(output_path('errors', output_id, dir_=dir_), drop_allzero_columns(df_errors), save_index="NAME")
            return df_errors

        def output_ampdet(output_id):
            return amplitude_detuning_ptc(madx, ampdet=2, chroma=4, file=output_path('ampdet', output_id))

        # Load Macros ----------------------------------------------------------
        madx.call(pathstr("slhc", "toolkit", "macro.madx"))

        # Lattice Setup ---------------------------------------
        # Load Sequence
        seq_name, seq_file, bv_flag = get_lhc_sequence_filename_and_bv(beam, 'hllhc')

        madx.call(pathstr('lhc', seq_file))  # LHC sequence
        madx.call(pathstr('slhc', 'hllhc_sequence.madx'))  # updates to HL-LHC

        # slice sequence
        madx.exec('myslice')

        madx.call(pathstr('slhc', 'errors', 'install_mqxf_fringenl.madx'))  # adding fringe place holder
        madx.call(pathstr('slhc', 'errors', 'install_MCBXFAB_errors.madx'))  # adding D1 corrector placeholders in IR1/5 (for errors)
        madx.call(pathstr('slhc', 'errors', 'install_MCBRD_errors.madx'))   # adding D2 corrector placeholders in IR1/5 (for errors)

        # Cycling w.r.t. to IP3 (mandatory to find closed orbit in collision in the presence of errors)
        madx.seqedit(sequence=seq_name)
        madx.flatten()
        madx.cycle(start="IP3")
        madx.flatten()
        madx.endedit()

        # Define Optics and make beam
        madx.call(get_optics_path(optics))

        # for some reason reset in optics file
        mvars.NRJ = energy
        mvars.on_disp = int(on_disp)

        # not sure if used below, possibly in coupling correction
        mvars.on_qpp = int(on_qpp)
        mvars.qx0 = tune_x
        mvars.qx00 = float(int(tune_x))
        mvars.qy0 = tune_y
        mvars.qy00 = float(int(tune_y))
        mvars.tsplit = abs(int(tune_x) - int(tune_y))
        mvars.qprime = chroma

        # Create Beam
        madx.beam(sequence=seq_name, bv=bv_flag,
                  energy="NRJ", sige=rel_energy_spread,
                  particle="proton", npart=n_particles,
                  ex=emittance, ey=emittance,
                  kbunch=1, sigt=bunch_len,
                  )
        madx.use(sequence=seq_name)

        # Also not sure if used
        madx.exec(f"check_ip(b{seq_name[-1]})")  # also runs twiss
        madx.set(format=["10d", "18.10g", "-18s"])
        twiss_df = madx.sequence[seq_name].twiss_table.dframe()
        mvars["mux_ip15_ref"] = twiss_df.loc["ip1", "mux"] - twiss_df.loc["ip5", "mux"]
        mvars["muy_ip15_ref"] = twiss_df.loc["ip1", "muy"] - twiss_df.loc["ip5", "muy"]

        # Setup Orbit ---
        if xing['scheme'] != 'flat':
            xing['on_alice'] = 7000/energy
            xing['on_lhcb'] = 7000/energy

        # if optics.startswith("flat"):
        #     # CHECK THIS !!
        #     xing["phi_IR1"] = 90
        #     xing["phi_IR5"] = 0

        orbit_setup(madx, machine='hllhc', **xing)

        mvars.on_disp = int(on_disp)
        check_crabbing(madx, auto_set=True)

        # Save the optics ------------------------------------------------------
        madx.use(sequence=seq_name)

        # Save Nominal for output
        get_twiss('nominal', pre_match_tune=intermediate_tune_match)
        output_ampdet('nominal')

        madx.exec("crossing_save")
        log_orbit(madx, machine='hllhc')

        # Save some variables ---
        # Not sure if needed:
        mvars.n_insideD1 = 5     # default value for the number of additionnal parasitic encounters inside D1
        mvars.nho_IR1 = 11       # number of slices for head-on in IR1 (between 0 and 201)
        mvars.nho_IR2 = 11       # number of slices for head-on in IR2 (between 0 and 201)
        mvars.nho_IR5 = 11       # number of slices for head-on in IR5 (between 0 and 201)
        mvars.nho_IR8 = 11       # number of slices for head-on in IR8 (between 0 and 201)

        # SET ON_DISP to zero (reactivates itself in crossing_restore)
        mvars.on_disp = 0  # more precise angles at IPs

        # Save nominal IP positions and crossing angle
        # Needed for rematchCO later on
        madx.twiss(table="nominal")  # this table is reused later!!

        # Save for rematchCOIP
        twiss_df = madx.sequence[seq_name].twiss_table.dframe()
        for ip in (1, 2, 5, 8):
            for plane in "xy":
                mvars[f"{plane}nom{ip}"] = twiss_df.loc[f"ip{ip}", f"{plane}"]
                mvars[f"p{plane}nom{ip}"] = twiss_df.loc[f"ip{ip}", f"p{plane}"]
            mvars[f"beta.ip{ip}"] = twiss_df.loc[f"ip{ip}", "betx"]

        # Save similar to above with val_xnom1 etc ... no idea what for
        madx.call(file=pathstr("slhc", "toolkit", "get_optics_params.madx"))

        # Save nominal optics in IR+Correctors for ir nl correction
        madx.use(sequence=seq_name)
        select_pattern = r"M(QS?X|BX|BRC|BRD|C[SODT]S?X)"
        if correct_mcbx_errors:
            select_pattern += "|MCBXF"

        df_twiss_ir = get_twiss('optics_ir', index_regex=select_pattern, pre_match_tune=False)
        write_mb_optics(madx)
        madx.system(f'"cp \'temp/optics0_MB.mad\' \'{output_path("twiss", "optics_mb")}\'"')

        # Continue with disabled crossing scheme for more exact corrections
        madx.exec("crossing_disable")
        # orbit_setup(madx, scheme='flat')

        # Align separation magnets ---------------------------------------------
        madx.call(pathstr('slhc', 'toolkit', 'align_sepdip.madx'))
        madx.exec('align_mbx15')  # HL-LHC D1
        madx.exec('align_mbrd15')  # HL-LHC D2 in IR15
        madx.exec('align_mbx28')  # V6.503 D1 in IR28
        madx.exec('align_mbrc28')  # V6.503 D2 in IR28
        madx.exec('align_mbrs')   # V6.503 D3 in IR4
        madx.exec('align_mbrb')   # V6.503 D4 in IR4
        madx.call(pathstr('slhc', 'toolkit', 'align_mbh.madx'))  # align 11T dipoles

        # Error subroutines --------------------------------------------------------
        # Error routine and measured error table for nominal LHC

        madx.call(pathstr('db5', 'measured_errors', 'Msubroutines_new.madx'))
        madx.call(pathstr('db5', 'measured_errors', 'Msubroutines_MS_MSS_MO_new.madx'))
        madx.call(pathstr('db5', 'toolkit', 'Orbit_Routines.madx'))
        madx.call(pathstr('slhc', 'errors', 'SelectLHCMonCor.madx'))

        # Load the "misalignment" table that is assigned in Set_alignment_errors.madx below
        # (to be tested?). These are not really misalignments, but re-alignments of LHC magnets
        # that have changed places/been rotated in the actual machine.
        madx.readtable(file=pathstr('db5', 'measured_errors', 'rotations_Q2_integral.tab'))

        # Error routine and error table for new IT/D1/D2/Q4/Q5
        madx.call(pathstr('slhc', 'errors', 'macro_error.madx'))  # macros for error generation in the new IT/D1's
        if use_mqxf_ab_errors:
            LOG.debug("Using MQXF AB error-tables")
            madx.call(pathstr('slhc', 'errors', 'ITa_errortable_v5'))  # target error table for the new IT
            madx.call(pathstr('slhc', 'errors', 'ITb_errortable_v5'))  # target error table for the new IT
        else:
            LOG.debug("Using MQXF body and fringe  error-tables")
            madx.call(pathstr('slhc', 'errors', 'ITbody_errortable_v5'))  # target error table for the new IT
            madx.call(pathstr('slhc', 'errors', 'ITnc_errortable_v5'))  # target error table for the new IT
            madx.call(pathstr('slhc', 'errors', 'ITcs_errortable_v5'))  # target error table for the new IT
            # mvars.b6M_MQXF_col = -4  # New baseline value, if using version > v5 might already be in

        madx.call(pathstr('slhc', 'errors', 'D1_errortable_v1'))   # target error table for the new D1

        # madx.call(pathstr('slhc', 'errors', 'D2_errortable_v5'))  # target error table for the new D2
        madx.call(pathstr('slhc', 'errors', 'D2_errortable_v5_doublerandoms'))  # twice the random values, but no systematics

        # HL-LHC v1.4
        madx.call(pathstr('slhc', 'errors', 'MCBXFAB_errortable_v1_FRAS_bis'))
        madx.call(pathstr('slhc', 'errors', 'MBH_errortable_v3'))
        # madx.call(pathstr('slhc', 'errors', 'MCBRD_errortable_v1'))

        # Apply magnetic errors ----------------------------------------------------
        # Shorthand definitions ---
        def apply_measured_errors(*magnets):
            for magnet in magnets:
                madx.call(pathstr('db5', 'measured_errors', f'Efcomp_{magnet}.madx'))

        def apply_hllhc_errors(*magnets):
            for magnet in magnets:
                madx.call(pathstr('slhc', 'errors', f'Efcomp_{magnet}.madx'))

        def set_eseed(add_to_seed):
            madx.eoption(seed=local_seed+add_to_seed)

        # Set which magnetic field error orders to use
        switch_magnetic_errors(madx, **errors)

        # Read WISE
        madx.readtable(file=pathstr("wise", f"{machine_state}_errors-emfqcs-{seed}.tfs"))

        # Possibly disable b2s in MQ (see above)
        disable = []
        if not on_b2s_mq:
            disable = ['on_b2s']

        # Apply Arc Errors ---
        if on_arc_errors:
            apply_measured_errors('MB')

            # Correct orbit distortion resulting from MB magnets (if present)
            if any(mvars[f'on_{field}'] for field in ('A1s', 'A1r', 'B1s', 'B1r')):
                madx.exec('initial_micado(4)')
                madx.exec('initial_micado(4)')

        # Apply Separation Dipoles Errors ---
        apply_measured_errors('MBRB', 'MBRC', 'MBRS', 'MBX', 'MBW')

        # Apply Quadrupole Errors ---
        # Arc Quads
        if on_arc_errors:
            with temp_disable_errors(madx, *disable):
                apply_measured_errors('MQ')

        # IR Quadrupoles
        with temp_disable_errors(madx, *disable):
            apply_measured_errors('MQW', 'MQTL', 'MQMC', 'MQX',
                                  'MQY', 'MQM', 'MQML',)

        # Apply Misalignment Errors ---
        if on_alignment_errors:
            madx.call(pathstr('db5', 'measured_errors', 'Set_alignment_errors.madx'))

        # New IT/D1/D2/Q4/Q5 ---
        set_eseed(1)
        if use_mqxf_ab_errors:
            apply_hllhc_errors('MQXFA', 'MQXFB')
        else:
            madx.call(pathstr('external', 'Efcomp_MQXFbody.madx'))  # special version
            apply_hllhc_errors('MQXFA', 'MQXFB', 'MQXFends')

        # assign systematic b5 in MBX
        mvars[f"b5M_MBXAB_{machine_state[:3]}"] = d1b5_value
        set_eseed(2)
        apply_hllhc_errors('MBXAB')  # new D1 in IR 1/5

        # assign systematic MBRD values
        mvars[f"b5M_MBRD_V1_{machine_state[:3]}"] = d2b5_value
        mvars[f"b5M_MBRD_V2_{machine_state[:3]}"] = d2b5_value
        mvars.use_average_errors_MBRD = 0
        set_eseed(3)
        apply_hllhc_errors('MBRD')  # new D2 in IR1/5

        apply_hllhc_errors('MQY')  # ! old Q4 in IR1/5, but switched places around IP1-5

        set_eseed(6)
        apply_hllhc_errors('MCBXFAB')  # new triplet correctors in IR1/5

        set_eseed(7)
        with temp_disable_errors(madx, 'on_b2s', 'on_b2r'):  # Assumed to be well corrected? (jdilly)
            apply_hllhc_errors('MBH')

        # set_eseed(8)
        # apply_hllhc_errors('MCBRD')

        # Correction of field errors in MB ---
        madx.select(flag='error', clear=True)
        madx.select(flag='error', pattern=r'MB\.', class_='multipole')
        madx.select(flag='error', pattern=r'MBH\.', class_='multipole')
        madx.esave(file="temp/MB.errors")
        madx.system(f'"{pathstr("slhc", "errors", "corr_MB_ats_v4")}"')
        madx.call(file="temp/MB_corr_setting.mad")  # Defines CMRSKEW and CMISKEW !!!
        madx.system(f'"cp \'temp/MB_corr_setting.mad\' \'{output_path("settings", "mb").with_suffix(".madx")}\'"')
        madx.system(f'"cp \'temp/MB.errors\' \'{output_path("errors", "mb")}\'"')

        # Save uncorrected ---
        if rdts is None:
            df_coupling_corrected_before = coupling_via_cmatrix(get_twiss(pre_match_tune=False))
            closest_tune_approach(df_coupling_corrected_before, qx=tune_x, qy=tune_y)
            correct_coupling(madx, accel="hllhc", sequence=seq_name,
                             qx=tune_x, qy=tune_y, dqx=chroma, dqy=chroma)

        df_uncorrected = get_twiss('uncorrected', pre_match_tune=intermediate_tune_match)
        get_errors('all')
        output_ampdet('uncorrected')

        df_coupling_uncorrected = coupling_via_cmatrix(df_uncorrected)
        closest_tune_approach(df_coupling_uncorrected, qx=tune_x, qy=tune_y)

        # Possibly deactivate Errors for correction, will be reassigned after
        # re-assign systematic b5 in MBX
        if "d1" not in correct_b5:
            mvars[f"b5M_MBXAB_{machine_state[:3]}"] = 0
            madx.eoption(add=False)
            set_eseed(2)
            apply_hllhc_errors('MBXAB')  # new D1 in IR 1/5
            madx.eoption(add=True)

        # re-assign systematic MBRD values
        if "d2" not in correct_b5:
            empty_mbrd_errortable(madx)
        else:
            # empty_mbrd_errortable(madx, keep=[5])  # would be better
            if not rdts2:
                mvars.use_average_errors_MBRD = 1
        madx.eoption(add=False)
        set_eseed(3)
        apply_hllhc_errors('MBRD')
        madx.eoption(add=True)

        # Save errors to table to be used for correction -----------------------
        df_tobecorrected = get_twiss('tobecorrected', pre_match_tune=intermediate_tune_match)
        df_errors_ir = get_errors('ir', index_regex=select_pattern)

        beam_data[beam]['madx'] = madx
        beam_data[beam]['optics_df'] = df_twiss_ir
        beam_data[beam]['errors_df'] = df_errors_ir
        beam_data[beam] = save_loggers(beam_data[beam])

        # END OF SETUP PART ----------------------------------------------------
        # ----------------------------------------------------------------------

    # IR Nonlinear Correction --------------------------------------------------
    # Skipped if no rdts are given.
    if rdts is not None:
        # Calculate Corrections ------------------------------------------------
        correction_cmds, correction_dfs = {}, {}
        if rdts2 is not None:
            # correct with both optics
            reinstate_loggers(beam_data, beam_data.keys())
            correction_cmds[1], correction_dfs[1] = irnl_correct(
                # Correction setup for LHC ---
                beams=(1, 4),
                optics=[beam_data[beam]['optics_df'] for beam in beam_data.keys()],
                errors=[beam_data[beam]['errors_df'] for beam in beam_data.keys()],
                rdts=rdts,
                rdts2=rdts2,
                accel='hllhc',
                feeddown=feeddown,  # order of feeddown to take into account
                iterations=iterations,
                ignore_corrector_settings=True,  # all correctors are assumed unpowered
                ips=(1, 5),  # in which IPs to correct
                solver='lstsq',  # 'inv', 'linear' Solver to use
                ignore_missing_columns=False,  # True: if columns are missing, assume 0
            )
            correction_cmds[4] = correction_cmds[1]
            correction_dfs[4] = correction_dfs[1]
        else:
            # calculate the corrections for both beams individually
            for beam in outputdirs.keys():
                reinstate_loggers(beam_data, beam)
                correction_cmds[beam], correction_dfs[beam] = irnl_correct(
                    # Correction setup for LHC ---
                    beams=(beam,),
                    optics=(beam_data[beam]['optics_df'],),
                    errors=(beam_data[beam]['errors_df'],),
                    rdts=rdts,
                    accel='hllhc',
                    feeddown=feeddown,  # order of feeddown to take into account
                    iterations=iterations,
                    ignore_corrector_settings=True,  # all correctors are assumed unpowered
                    ips=(1, 5),  # in which IPs to correct
                    solver='lstsq',  # 'inv', 'linear' Solver to use
                    ignore_missing_columns=False,  # True: if columns are missing, assume 0
                )

        # Apply corrections ----------------------------------------------------
        for beam, outputdir in outputdirs.items():
            # Setup needed variables:
            reinstate_loggers(beam_data, beam)
            if correct_beam == "same":
                corrector_beam = beam
            elif correct_beam == "other":
                corrector_beam = get_other_beam(beam)
            else:
                corrector_beam = int(correct_beam[-1])

            correct_name = f'mcx_b{corrector_beam}'
            if rdts2 is not None:
                correct_name += f'b{get_other_beam(corrector_beam)}'

            madx = beam_data[beam]['madx']
            mvars = madx.globals
            seq_name, seq_file, bv_flag = get_lhc_sequence_filename_and_bv(beam, 'hllhc')

            # Apply correction and save to files
            madx.input(correction_cmds[corrector_beam])
            write_command(output_path('settings', correct_name).with_suffix('.madx'), correction_cmds[corrector_beam])
            write_tfs(output_path('settings', correct_name), correction_dfs[corrector_beam])

            # assign the real D[12] errors again
            madx.call(pathstr("slhc", "errors", "D1_errortable_v1"))
            madx.call(pathstr("slhc", "errors", "D2_errortable_v5_doublerandoms"))

            mvars[f"b5M_MBXAB_{machine_state[:3]}"] = d1b5_value
            mvars[f"b5M_MBRD_V1_{machine_state[:3]}"] = d2b5_value
            mvars[f"b5M_MBRD_V2_{machine_state[:3]}"] = d2b5_value

            madx.eoption(add=False)
            madx.eoption(seed=local_seed+2)
            madx.call(pathstr('slhc', 'errors', f'Efcomp_MBXAB.madx'))

            mvars.use_average_errors_MBRD = 0
            madx.eoption(seed=local_seed+3)
            madx.call(pathstr('slhc', 'errors', f'Efcomp_MBRD.madx'))
            madx.eoption(add=True)

            # Correct orbit distortion resulting from other magnets
            if any(mvars[f'on_{field}'] for field in ('A1s', 'A1r', 'B1s', 'B1r')):
                madx.exec('initial_micado(4)')
                madx.exec('initial_micado(4)')
                madx.exec('initial_micado(4)')
                madx.exec('initial_micado(4)')
                madx.exec('final_micado(0.004)')

            # restore orbit
            madx.exec("crossing_restore")

            # Correct Coupling
            df_coupling_corrected_before = coupling_via_cmatrix(get_twiss(pre_match_tune=False))
            closest_tune_approach(df_coupling_corrected_before, qx=tune_x, qy=tune_y)

            correct_coupling(madx, accel="hllhc", sequence=seq_name,
                             qx=tune_x, qy=tune_y, dqx=chroma, dqy=chroma,
                             simplex=True, match_tunes=True)

            # Limit corrector strengths
            with suppress(ValueError):
                check_corrector_limits(madx, accel="HLLHC", beam=beam, limit_to_max=limit_corrector_strength)

            # Rematch Orbit
            madx.call(pathstr('slhc', 'toolkit', 'rematchCOIP.madx'))
            if mvars['on_disp']:
                madx.call(pathstr('slhc', 'toolkit', 'rematchCOarc.madx'))

            # Limit corrector strengths again
            with suppress(ValueError):
                check_corrector_limits(madx, accel="HLLHC", beam=beam, limit_to_max=limit_corrector_strength)

            # Save corrected
            df_corrected = get_twiss('corrected')
            output_ampdet('corrected')

            df_coupling_corrected = coupling_via_cmatrix(df_corrected)
            closest_tune_approach(df_coupling_corrected, qx=tune_x, qy=tune_y)
            log_orbit(madx, accel='hllhc')

            # END OF APPLY CORRECTIONS -----------------------------------------
            # ------------------------------------------------------------------

    # Write Sixtrack output for both beams -------------------------------------
    for beam in reversed(list(outputdirs.keys())):
        reinstate_loggers(beam_data, beam)
        madx = beam_data[beam]['madx']
        sixtrack_output(madx, madx.globals.nrj)

        # manually move sixtrack output files
        # this is why other_beam needs to run first
        # (no easy way to specify output dir for them)
        for f in Path(".").glob("fc*"):
            if beam != 1:
                shutil.move(f, outputdirs[beam] / f.name)
            else:
                shutil.copy(f, outputdirs[beam] / f.name)
        madx.exit()
