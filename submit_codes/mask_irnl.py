import sys
sys.path.insert(0, '/afs/cern.ch/work/j/jdilly/public/python/')
from pathlib import Path
import re

from correct_irnl_run_helper import setup2str, correctstr2dict
from %(MACHINE)s_run_correct_irnl import main

if __name__ == '__main__':
    setup = dict(
        beam=%(BEAM)d,
        seed=%(SEED)s,
        rdts=correctstr2dict("%(RDTS)s"),
        feeddown=%(FEEDDOWN)d,
        xing={"scheme": "%(MACHINE)s_%(XING)s"} if "%(XING)s" != "flat" else {},
        errors={k: True for k in re.findall(r"[a-zA-Z]+\d+", "%(ERRORS)s")},
    )
    setup['outputdir'] = Path('%(OUTPUT)s') / "Job.%(MACHINE)s.b%(BEAM)d.xing_%(XING)s.errors_%(ERRORS)s.rdts_%(RDTS)s" / 'Seed.%(SEED)s'

    main(**setup)
