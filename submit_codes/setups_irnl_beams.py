from pathlib import Path

from omc3.utils import logging_tools
from correct_irnl_run_helper import get_beams_job_id_mask

LOG = logging_tools.get_logger(__name__, level_console=logging_tools.DEBUG, color=True)

STUDY_BEAMS = 'study.irnl_correction_multi_beams'
SIXDESKBASE = 'sixdesk_beams' \
              ''
python3_path = Path('/afs/cern.ch/work/j/jdilly/public/venvs/for_htc/bin/python')
python2_path = Path('/afs/cern.ch/work/j/jdilly/public/venvs/py2lintrack/bin')
UNLOCK = True
RESUBMIT = False
# max_stage = 'submit_sixtrack'
# max_stage = 'check_input'
MAX_STAGE = None
MASK_PATH = Path(__file__).parent / 'mask_irnl_beams.py'


def _main_setup(replace_dict, afs_work, max_stage, resubmit):
    return dict(
        working_directory=Path(f'/afs/cern.ch/work/j/{afs_work}/{SIXDESKBASE}'),
        mask=MASK_PATH,
        executable=python3_path,
        python2=python2_path,
        python3=python3_path,
        apply_mad6t_hacks=True,
        replace_dict=replace_dict,
        jobid_mask=get_beams_job_id_mask(replace_dict.keys()),
        unlock=UNLOCK,
        resubmit=resubmit,
        max_stage=max_stage,
    )


def lhc_top_same_beams(beam, afs_work='jdilly3', max_stage=MAX_STAGE, resubmit=RESUBMIT):
    replace_dict = dict(
        BEAM=beam,
        TURNS=100000,
        AMPMIN=2, AMPMAX=30, AMPSTEP=2,
        ANGLES=11,
        SEED='%SEEDRAN',
        OUTPUT=f'/afs/cern.ch/work/j/{afs_work}/{STUDY_BEAMS}/',
        FEEDDOWN=[0, 2],
        XING=["top", ],
        MACHINE=['lhc', ],
        OPTICS=["round3030", "flat6015"],
        ERRORS=['AB3AB4AB5AB6AB7AB8',],
        RDTS=["b6-b6_b4-b4_a4-a4_b3-b3_a3-a3"],
        RDTS2=["None", "b6-b6_b4-b4_a4-a4_b3-b3_a3-a3"],
        ITERATIONS=1,
        CORRECTBEAM=['same',],
    )
    return _main_setup(replace_dict, afs_work, max_stage, resubmit)


def lhc_top_other_beams(beam, afs_work='josch', max_stage=MAX_STAGE, resubmit=RESUBMIT):
    replace_dict = dict(
        BEAM=beam,
        TURNS=100000,
        AMPMIN=2, AMPMAX=30, AMPSTEP=2,
        ANGLES=11,
        SEED='%SEEDRAN',
        OUTPUT=f'/afs/cern.ch/work/j/{afs_work}/{STUDY_BEAMS}/',
        FEEDDOWN=[0, 2],
        XING=["top", ],
        MACHINE=['lhc', ],
        OPTICS=["round3030", "flat6015"],
        ERRORS=['AB3AB4AB5AB6AB7AB8', ],
        RDTS=["b6-b6_b4-b4_a4-a4_b3-b3_a3-a3"],
        RDTS2=["None", ],
        ITERATIONS=1,
        CORRECTBEAM=['other',],
    )
    return _main_setup(replace_dict, afs_work, max_stage, resubmit)


def lhc_top_same_beams_hllhc_correctors(beam, afs_work='jdilly3', max_stage=MAX_STAGE, resubmit=RESUBMIT):
    replace_dict = dict(
        BEAM=beam,
        TURNS=100000,
        AMPMIN=2, AMPMAX=30, AMPSTEP=2,
        ANGLES=11,
        SEED='%SEEDRAN',
        OUTPUT=f'/afs/cern.ch/work/j/{afs_work}/{STUDY_BEAMS}/',
        FEEDDOWN=[0, 2],
        XING=["top", ],
        MACHINE=['lhc', ],
        OPTICS=["round3030", "flat6015"],
        ERRORS=['AB3AB4AB5AB6AB7AB8',],
        RDTS=["b6-b6_a6-a6_b5-b5_a5-a5_b4-b4_a4-a4_b3-b3_a3-a3"],
        RDTS2=["None", "b6-b6_a6-a6_b5-b5_a5-a5_b4-b4_a4-a4_b3-b3_a3-a3"],
        ITERATIONS=1,
        CORRECTBEAM=['same',],
    )
    return _main_setup(replace_dict, afs_work, max_stage, resubmit)


def lhc_top_other_beams_hllhc_correctors(beam, afs_work='josch', max_stage=MAX_STAGE, resubmit=RESUBMIT):
    replace_dict = dict(
        BEAM=beam,
        TURNS=100000,
        AMPMIN=2, AMPMAX=30, AMPSTEP=2,
        ANGLES=11,
        SEED='%SEEDRAN',
        OUTPUT=f'/afs/cern.ch/work/j/{afs_work}/{STUDY_BEAMS}/',
        FEEDDOWN=[0, 2],
        XING=["top", ],
        MACHINE=['lhc', ],
        OPTICS=["round3030", "flat6015"],
        ERRORS=['AB3AB4AB5AB6AB7AB8', ],
        RDTS=["b6-b6_a6-a6_b5-b5_a5-a5_b4-b4_a4-a4_b3-b3_a3-a3"],
        RDTS2=["None", ],
        ITERATIONS=1,
        CORRECTBEAM=['other',],
    )
    return _main_setup(replace_dict, afs_work, max_stage, resubmit)


def hllhc_top_same_beams(beam, afs_work='jdilly3', max_stage=MAX_STAGE, resubmit=RESUBMIT):
    replace_dict = dict(
        BEAM=beam,
        TURNS=100000,
        AMPMIN=2, AMPMAX=30, AMPSTEP=2,
        ANGLES=11,
        SEED='%SEEDRAN',
        OUTPUT=f'/afs/cern.ch/work/j/{afs_work}/{STUDY_BEAMS}/',
        FEEDDOWN=[0, 2],
        XING=["top", ],
        MACHINE=['hllhc', ],
        OPTICS=["round1515", "flat07530"],
        ERRORS=['AB3AB4AB5AB6AB7AB8', ],
        RDTS=["b6-b6_a6-a6_b5-b5_a5-a5_b4-b4_a4-a4_b3-b3_a3-a3", ],
        RDTS2=["None", "b6-b6_a6-a6_b5-b5_a5-a5_b4-b4_a4-a4_b3-b3_a3-a3"],
        ITERATIONS=1,
        CORRECTBEAM=['same',],
    )
    return _main_setup(replace_dict, afs_work, max_stage, resubmit)


def hllhc_top_other_beams(beam, afs_work='josch', max_stage=MAX_STAGE, resubmit=RESUBMIT):
    replace_dict = dict(
        BEAM=beam,
        TURNS=100000,
        AMPMIN=2, AMPMAX=30, AMPSTEP=2,
        ANGLES=11,
        SEED='%SEEDRAN',
        OUTPUT=f'/afs/cern.ch/work/j/{afs_work}/{STUDY_BEAMS}/',
        FEEDDOWN=[0, 2],
        XING=["top", ],
        MACHINE=['hllhc', ],
        OPTICS=["round1515", "flat07530"],
        ERRORS=['AB3AB4AB5AB6AB7AB8', ],
        RDTS=["b6-b6_a6-a6_b5-b5_a5-a5_b4-b4_a4-a4_b3-b3_a3-a3", ],
        RDTS2=["None", ],
        ITERATIONS=1,
        CORRECTBEAM=['other',],
    )
    return _main_setup(replace_dict, afs_work, max_stage, resubmit)


def hllhc_top_other_beams_flatopticsonly(beam, afs_work='josch', max_stage=MAX_STAGE, resubmit=RESUBMIT):  # redo because crossing angles
    replace_dict = dict(
        BEAM=beam,
        TURNS=100000,
        AMPMIN=2, AMPMAX=30, AMPSTEP=2,
        ANGLES=11,
        SEED='%SEEDRAN',
        OUTPUT=f'/afs/cern.ch/work/j/{afs_work}/{STUDY_BEAMS}1/',
        FEEDDOWN=[0, 2],
        XING=["top", ],
        MACHINE=['hllhc', ],
        OPTICS=["flat07530"],
        ERRORS=['AB3AB4AB5AB6AB7AB8', ],
        RDTS=["b6-b6_a6-a6_b5-b5_a5-a5_b4-b4_a4-a4_b3-b3_a3-a3", ],
        RDTS2=["None", ],
        ITERATIONS=1,
        CORRECTBEAM=['other',],
    )
    return _main_setup(replace_dict, afs_work, max_stage, resubmit)


def hllhc_top_lhc_correction_same(beam, afs_work='jdilly1', max_stage=MAX_STAGE, resubmit=RESUBMIT):
    replace_dict = dict(
        BEAM=beam,
        TURNS=100000,
        AMPMIN=2, AMPMAX=30, AMPSTEP=2,
        ANGLES=11,
        SEED='%SEEDRAN',
        OUTPUT=f'/afs/cern.ch/work/j/{afs_work}/{STUDY_BEAMS}/',
        FEEDDOWN=[0, 2],
        XING=["top", ],
        MACHINE=['hllhc', ],
        OPTICS=["round1515", "flat07530"],
        ERRORS=['AB3AB4AB5AB6AB7AB8', ],
        RDTS=["b6-b6_b4-b4_a4-a4_b3-b3_a3-a3", ],
        RDTS2=["None",],
        ITERATIONS=1,
        CORRECTBEAM=['same',],
    )
    return _main_setup(replace_dict, afs_work, max_stage, resubmit)


def hllhc_top_lhc_correction_other(beam, afs_work='jdilly1', max_stage=MAX_STAGE, resubmit=RESUBMIT):
    replace_dict = dict(
        BEAM=beam,
        TURNS=100000,
        AMPMIN=2, AMPMAX=30, AMPSTEP=2,
        ANGLES=11,
        SEED='%SEEDRAN',
        OUTPUT=f'/afs/cern.ch/work/j/{afs_work}/{STUDY_BEAMS}/',
        FEEDDOWN=[0, 2],
        XING=["top", ],
        MACHINE=['hllhc', ],
        OPTICS=["round1515", "flat07530"],
        ERRORS=['AB3AB4AB5AB6AB7AB8', ],
        RDTS=["b6-b6_b4-b4_a4-a4_b3-b3_a3-a3", ],
        RDTS2=["None", "b6-b6_b4-b4_a4-a4_b3-b3_a3-a3"],
        ITERATIONS=1,
        CORRECTBEAM=['other',],
    )
    return _main_setup(replace_dict, afs_work, max_stage, resubmit)

# No Star ---


def hllhc_both_beams_NoStar(beam, afs_work='josch', max_stage=MAX_STAGE, resubmit=RESUBMIT):
    replace_dict = dict(
        BEAM=beam,
        TURNS=100000,
        AMPMIN=2, AMPMAX=30, AMPSTEP=2,
        ANGLES=11,
        SEED='%SEEDRAN',
        OUTPUT=f'/afs/cern.ch/work/j/{afs_work}/{STUDY_BEAMS}/',
        FEEDDOWN=[2],
        XING=["top", ],
        MACHINE=['hllhc', ],
        OPTICS=["round1515", "flat07530"],
        ERRORS=['AB3AB4AB5AB6AB7AB8', ],
        RDTS=["b6-b6_a6-a6_b5-b5_a5-a5_b4-b4_a4-a4_b3-b3_a3-a3", ],
        RDTS2=["b6-b6_a6-a6_b5-b5_a5-a5_b4-b4_a4-a4_b3-b3_a3-a3.NS"],
        ITERATIONS=1,
        CORRECTBEAM=['same',],
    )
    return _main_setup(replace_dict, afs_work, max_stage, resubmit)


# Correction ---

def lhc_flat_allerr_hllhc_correctors(beam, afs_work='josch', max_stage=MAX_STAGE, resubmit=RESUBMIT):
    replace_dict = dict(
        BEAM=beam,
        TURNS=100000,
        AMPMIN=2, AMPMAX=30, AMPSTEP=2,
        ANGLES=11,
        SEED='%SEEDRAN',
        OUTPUT=f'/afs/cern.ch/work/j/{afs_work}/{STUDY_BEAMS}/',
        FEEDDOWN=[0, ],
        XING=["flat", ],
        MACHINE=['lhc', ],
        OPTICS=["round3030", ],
        ERRORS=['AB3AB4AB5AB6AB7AB8', ],
        RDTS=["b6-b6_a6-a6_b5-b5_a5-a5_b4-b4_a4-a4_b3-b3_a3-a3", ],
        RDTS2=["None", ],
        ITERATIONS=1,
        CORRECTBEAM=['same',],
    )
    return _main_setup(replace_dict, afs_work, max_stage, resubmit)


def lhc_flat_allerr(beam, afs_work='jdilly2', max_stage=MAX_STAGE, resubmit=RESUBMIT):
    replace_dict = dict(
        BEAM=beam,
        TURNS=100000,
        AMPMIN=2, AMPMAX=30, AMPSTEP=2,
        ANGLES=11,
        SEED='%SEEDRAN',
        OUTPUT=f'/afs/cern.ch/work/j/{afs_work}/{STUDY_BEAMS}/',
        FEEDDOWN=[0, ],
        XING=["flat", ],
        MACHINE=['lhc', ],
        OPTICS=["round3030", ],
        ERRORS=['AB3AB4AB5AB6AB7AB8', ],
        RDTS=["b6-b6_b4-b4_a4-a4_b3-b3_a3-a3", ],
        RDTS2=["None", ],
        ITERATIONS=1,
        CORRECTBEAM=['same',],
    )
    return _main_setup(replace_dict, afs_work, max_stage, resubmit)


def lhc_allerr_nocorr(beam, afs_work='jdilly2', max_stage=MAX_STAGE, resubmit=RESUBMIT):
    replace_dict = dict(
        BEAM=beam,
        TURNS=100000,
        AMPMIN=2, AMPMAX=30, AMPSTEP=2,
        ANGLES=11,
        SEED='%SEEDRAN',
        OUTPUT=f'/afs/cern.ch/work/j/{afs_work}/{STUDY_BEAMS}/',
        FEEDDOWN=[0, ],
        XING=["flat", "top"],
        MACHINE=['lhc', ],
        OPTICS=["round3030", ],
        ERRORS=['AB3AB4AB5AB6AB7AB8', ],
        RDTS=["None", ],
        RDTS2=["None", ],
        ITERATIONS=1,
        CORRECTBEAM=['same',],
    )
    return _main_setup(replace_dict, afs_work, max_stage, resubmit)


def hllhc_flat_allerr(beam, afs_work='josch', max_stage=MAX_STAGE, resubmit=RESUBMIT):
    replace_dict = dict(
        BEAM=beam,
        TURNS=100000,
        AMPMIN=2, AMPMAX=30, AMPSTEP=2,
        ANGLES=11,
        SEED='%SEEDRAN',
        OUTPUT=f'/afs/cern.ch/work/j/{afs_work}/{STUDY_BEAMS}/',
        FEEDDOWN=[0, ],
        XING=["flat", ],
        MACHINE=['hllhc', ],
        OPTICS=["round1515", ],
        ERRORS=['AB3AB4AB5AB6AB7AB8', ],
        RDTS=["b6-b6_a6-a6_b5-b5_a5-a5_b4-b4_a4-a4_b3-b3_a3-a3", ],
        RDTS2=["None", ],
        ITERATIONS=1,
        CORRECTBEAM=['same',],
    )
    return _main_setup(replace_dict, afs_work, max_stage, resubmit)


def hllhc_allerr_nocorr(beam, afs_work='josch', max_stage=MAX_STAGE, resubmit=RESUBMIT):
    replace_dict = dict(
        BEAM=beam,
        TURNS=100000,
        AMPMIN=2, AMPMAX=30, AMPSTEP=2,
        ANGLES=11,
        SEED='%SEEDRAN',
        OUTPUT=f'/afs/cern.ch/work/j/{afs_work}/{STUDY_BEAMS}/',
        FEEDDOWN=[0, ],
        XING=["flat", "top"],
        MACHINE=['hllhc', ],
        OPTICS=["round1515", ],
        ERRORS=['AB3AB4AB5AB6AB7AB8', ],
        RDTS=["None", ],
        RDTS2=["None", ],
        ITERATIONS=1,
        CORRECTBEAM=['same',],
    )
    return _main_setup(replace_dict, afs_work, max_stage, resubmit)


# b4 correction with cross-term

def hllhc_b4cross_correction(beam, afs_work='jdilly1', max_stage=MAX_STAGE, resubmit=RESUBMIT):
    replace_dict = dict(
        BEAM=beam,
        TURNS=100000,
        AMPMIN=2, AMPMAX=30, AMPSTEP=2,
        ANGLES=11,
        SEED='%SEEDRAN',
        OUTPUT=f'/afs/cern.ch/work/j/{afs_work}/{STUDY_BEAMS}/',
        FEEDDOWN=[1,],
        XING=["top", ],
        MACHINE=['hllhc', ],
        OPTICS=["round1515",],
        ERRORS=['B4',],
        RDTS=["None", "b4-b4", "b4-b4_b4cross-b4", "b4-b4_b4cross-b5",],
        RDTS2=["None",],
        ITERATIONS=1,
        CORRECTBEAM=['same', 'other'],
    )
    return _main_setup(replace_dict, afs_work, max_stage, resubmit)


def hllhc_b4cross_correction_b5(beam, afs_work='jdilly2', max_stage=MAX_STAGE, resubmit=RESUBMIT):
    replace_dict = dict(
        BEAM=beam,
        TURNS=100000,
        AMPMIN=2, AMPMAX=30, AMPSTEP=2,
        ANGLES=11,
        SEED='%SEEDRAN',
        OUTPUT=f'/afs/cern.ch/work/j/{afs_work}/{STUDY_BEAMS}/',
        FEEDDOWN=[1,],
        XING=["top", ],
        MACHINE=['hllhc', ],
        OPTICS=["round1515",],
        ERRORS=['B4B5',],
        RDTS=["None", "b5-b5_b4-b4", "b5-b5_b4-b4_b22-b4", "b5-b5_b4-b4_b22-b5",],
        RDTS2=["None",],
        ITERATIONS=1,
        CORRECTBEAM=['same', "other"],
    )
    return _main_setup(replace_dict, afs_work, max_stage, resubmit)


# B6-B4 Correction


def lhc_b6b4_correction(beam, afs_work='jdilly3', max_stage=MAX_STAGE, resubmit=RESUBMIT):
    replace_dict = dict(
        BEAM=beam,
        TURNS=100000,
        AMPMIN=2, AMPMAX=30, AMPSTEP=2,
        ANGLES=11,
        SEED='%SEEDRAN',
        OUTPUT=f'/afs/cern.ch/work/j/{afs_work}/{STUDY_BEAMS}/',
        FEEDDOWN=[2,],
        XING=["top", ],
        MACHINE=['lhc', ],
        OPTICS=["round3030",],
        ERRORS=['B4B6', ],
        RDTS=["b6-b6_b4-b4", "b6-b6_b4-b6b4", "b4-b6", "b4-b6b4"],
        RDTS2=["None",],
        ITERATIONS=[1, 3],
        CORRECTBEAM=['same',],
    )
    return _main_setup(replace_dict, afs_work, max_stage, resubmit)
