import shutil
from pathlib import Path
import re
import importlib
import sys
import subprocess


sys.path.insert(0, '/afs/cern.ch/work/j/jdilly/public/python/')

from correct_irnl_run_helper import correctstr2dict, get_beams_output_path, get_errors
from pylhc.irnl_rdt_correction import log_setup
import logging

LOG = logging.getLogger(__name__)


if __name__ == '__main__':
    mask_vals = dict(
        machine="%(MACHINE)s",
        beam=int("%(BEAM)d"),
        correct_beam="%(CORRECT_BEAM)s",
        rdts="%(RDTS)s",
        rdts2="%(RDTS2)s",
        xing="%(XING)s",
        optics="%(OPTICS)s",
        errors="%(ERRORS)s",
        seed=int("%(SEED)s"),
        iterations=int("%(ITERATIONS)s"),
        feeddown=int("%(FEEDDOWN)s"),
        output=Path("%(OUTPUT)s"),
        d1b5_value=int("%(D1B5_VALUE)d"),
        d2b5_value=int("%(D2B5_VALUE)d"),
        correct_b5="%(CORRECT_B5)s",
        corrector_strength="%(CORRECTOR_STRENGTH)s"
    )
    # mask_vals = dict(
    #     machine="hllhc",
    #     beam=int("1"),
    #     correct_beam="same",
    #     rdts="b6-b6_a6-a6_b5-b5_a5-a5_b4-b4_a4-a4_b3-b3_a3-a3",
    #     rdts2="None",
    #     xing="top",
    #     optics="round1515",
    #     errors="AB3-15",
    #     seed=int("1"),
    #     iterations=int("1"),
    #     feeddown=int("0"),
    #     output=Path("/afs/cern.ch/work/j/jdilly1/study.correction_test/"),
    #     d1b5_value=int("0"),
    #     d2b5_value=int("0"),
    #     correct_b5="",
    #     corrector_strength="limited"
    # )

    main = importlib.import_module(f"{mask_vals['machine']}_run_correct_beams_b5sweep").main

    rdts2 = mask_vals["rdts2"]
    only_one = False
    no_star = False
    if rdts2.endswith(".NS"):
        no_star = True
        rdts2 = rdts2[:-3]
    elif rdts2 != "None":
        only_one = True

    mask_vals_no_beam = mask_vals.copy()
    mask_vals_no_beam.pop('beam')
    output_paths = {
        b: get_beams_output_path(beam=b, **mask_vals_no_beam) for b in (1, 4)
    }
    beam = mask_vals['beam']

    for b in output_paths.keys():
        output_paths[b].mkdir(exist_ok=True, parents=True)

    shutil.copy(__file__, output_paths[beam] / 'run_irnl_beams_b5sweep.py')

    sixtrackdata = list(output_paths[beam].glob("fc*"))
    if len(sixtrackdata):
        log_setup()
        LOG.info("Found pre-run Sixtrack data. Copying to local dir.")
        LOG.info("Job finished normally")  # Needed for mad6t check
        for f in sixtrackdata:
            shutil.copy(f, Path(".") / f.name)
    else:
        if beam != 1:
            raise OSError(f"No SixtrackData found in {output_paths[beam]}.")

        local_output_paths = {b: Path(f"tempout_beam{b}") for b in output_paths.keys()}

        main(
            correct_beam=mask_vals['correct_beam'],
            seed=mask_vals['seed'],
            rdts=correctstr2dict(mask_vals['rdts'], only_one=only_one, no_star=no_star),
            rdts2=correctstr2dict(rdts2, only_one=only_one, no_star=no_star),
            xing={'scheme': mask_vals['xing']},
            errors=get_errors(mask_vals['errors']),
            optics=mask_vals['optics'],
            limit_corrector_strength=mask_vals['corrector_strength'] == 'limited',
            iterations=mask_vals['iterations'],
            feeddown=mask_vals['feeddown'],
            correct_b5=re.findall(r"d\d", mask_vals['correct_b5']),
            d1b5_value=mask_vals['d1b5_value'],
            d2b5_value=mask_vals['d2b5_value'],
            outputdirs=local_output_paths,
        )

        for b in local_output_paths.keys():
            subprocess.run(["cp", "-rT", str(local_output_paths[b]), str(output_paths[b])], check=True)
