"""
Check the b6 errors and corrector values in WISE 2011, WISE 2015

"""
from pathlib import Path

from pylhc_submitter import job_submitter
from omc3.utils.logging_tools import get_logger

LOG = get_logger(__name__)

afs_work = Path("/afs/cern.ch/work/j/jdilly/")
afs_work4 = Path("/afs/cern.ch/work/j/jdilly4/")
eos_work = Path("/eos/home-j/jdilly3/work/")

afs_python = afs_work / "public" / "venvs" / "for_htc" / "bin" / "python"
MASK_PATH = Path(__file__).parent / 'mask_lhc_run_irnl_correction_wise_run_heads.py'
study_dir = afs_work4 / "study.22.wise_heads_b6_correction"

if __name__ == '__main__':
    job_submitter.main(
        mask=MASK_PATH,
        executable=afs_python,
        replace_dict=dict(
            # WISE=[2011, 2015, 2021],
            WISE=[2021],
            # SEED=list(range(1, 61)),
            SEED=list(range(1, 101)),
        ),
        jobid_mask="wise%(WISE)d_%(SEED)d",
        working_directory=study_dir,
    )