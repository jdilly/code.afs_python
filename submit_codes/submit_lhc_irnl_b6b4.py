from generic_parser import entrypoint
from pylhc_submitter import autosix
from submit_codes.setups_irnl_beams import lhc_b6b4_correction
from submit_codes.submit_helper import get_params, drop_none_value_entries
from omc3.utils import logging_tools
LOG = logging_tools.get_logger(__name__, level_console=logging_tools.DEBUG, color=True)


@entrypoint(get_params(), strict=True)
def main(opt):
    opt = drop_none_value_entries(opt)
    autosix.main(lhc_b6b4_correction(**opt))


if __name__ == '__main__':
    main()