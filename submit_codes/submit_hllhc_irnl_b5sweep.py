from generic_parser import entrypoint
from pylhc_submitter import autosix
from submit_codes.setups_irnl_beams_b5sweep import (
    hllhc_b14_lim_feeddown_d1_7_both_beams, hllhc_b50_lim_feeddown_d1_7_both_beams, hllhc_lim_d1_sweep, hllhc_lim_flat_d1_7,
    hllhc_lim_flat_d1_7_no_correction, hllhc_b50_lim_d1_7_other_beam, hllhc_b50_b14_lim_feeddown_d1_7
)
from submit_codes.submit_helper import get_params, drop_none_value_entries
from omc3.utils import logging_tools
LOG = logging_tools.get_logger(__name__, level_console=logging_tools.DEBUG, color=True)


def get_all_params():
    params = get_params()
    params.add_parameter(
            name='switch',
            type=str,
            nargs="+",
            choices=["fd", "other"],
            # choices=['b50fd', 'b14fd', 'd1sweep', 'flat', 'flat_no'],
            # choices=['b50', 'b14', 'b50d1', 'b14d1', 'b50d1fd'],
            required=True,
    )
    return params


@entrypoint(get_all_params(), strict=True)
def main(opt):
    switch = opt.pop('switch')
    opt = drop_none_value_entries(opt)
    # if 'b50fd' in switch:
    #     autosix.main(hllhc_b50_lim_feeddown_d1_7(**opt))
    # if 'b14fd' in switch:
    #     autosix.main(hllhc_b14_lim_feeddown_d1_7(**opt))
    # if 'd1sweep' in switch:
    #     autosix.main(hllhc_lim_d1_sweep(**opt))
    # if 'flat' in switch:
    #     autosix.main(hllhc_lim_flat_d1_7(**opt))
    # if 'flat_no' in switch:
    #     autosix.main(hllhc_lim_flat_d1_7_no_correction(**opt))
    if 'fd' in switch:
        autosix.main(hllhc_b50_b14_lim_feeddown_d1_7(**opt))
    if 'other' in switch:
        autosix.main(hllhc_b50_lim_d1_7_other_beam(**opt))


if __name__ == '__main__':
    main()
