from pylhc import autosix
from setups_irnl_beams import hllhc_top_same_beams_flatopticsonly, hllhc_top_other_beams_flatopticsonly
from omc3.utils import logging_tools
LOG = logging_tools.get_logger(__name__, level_console=logging_tools.DEBUG, color=True)

if __name__ == '__main__':
    # python2_path = None
    # beam = [1, ]
    # beam = [4, ]  # to be run when beam 1 is done with mad6t
    beam = [1, 4]  # after mad6t has finished

    autosix.main(hllhc_top_same_beams_flatopticsonly(beam))
    autosix.main(hllhc_top_other_beams_flatopticsonly(beam))
