from pathlib import Path

from omc3.utils import logging_tools

from analysis.analyse_ptc_output import gather_ptc_data
from correct_irnl_run_helper import get_beams_job_id_mask

LOG = logging_tools.get_logger(__name__, level_console=logging_tools.DEBUG, color=True)

STUDY_BEAMS = 'study.irnl_correction_d1d2b5sweep'
SIXDESKBASE = 'sixdeskbase_d1d2b5sweep'


EOS_STUDY = str(Path("/eos/home-j/jdilly/work_temp/") / STUDY_BEAMS)
EOS_JOSCH_STUDY = str(Path("/eos/home-j/josch/work/") / STUDY_BEAMS)
AFS_CERN_WORK = Path(f"/afs/cern.ch/work/")

python3_path = Path('/afs/cern.ch/work/j/jdilly/public/venvs/for_htc/bin/python')
python2_path = Path('/afs/cern.ch/work/j/jdilly/public/venvs/py2lintrack/bin')
sixdesk_path = Path('/afs/cern.ch/work/j/jdilly/public/SixDesk')
UNLOCK = True
RESUBMIT = False
# max_stage = 'submit_sixtrack'
# max_stage = 'check_input'
MAX_STAGE = None
MASK_PATH = Path(__file__).parent / 'mask_irnl_beams_b5sweep.py'


def _main_setup(replace_dict, afs_work, max_stage, resubmit):
    r_dict = dict(
        AMPMIN=2, AMPMAX=20, AMPSTEP=2,
        TURNS=100000,
        ANGLES=11,
        XING=["top", ],
        MACHINE=['hllhc', ],
        OPTICS=["round1515", ],
        ERRORS=['AB3-15', ],
        RDTS2=["None", ],
        FEEDDOWN=0,
        ITERATIONS=1,
        CORRECT_BEAM=['same', ],
        OUTPUT=EOS_STUDY,
        SEED='%SEEDRAN',
        RUNTYPE='col',
        ENERGY=6500_000,  # wrong, but scaled afterwards
        EMITTANCE=3.75,  # wrong, but scaled afterwards
        CORRECTOR_STRENGTH=['limited', ],
    )
    r_dict.update(replace_dict)
    return dict(
        working_directory=AFS_CERN_WORK / afs_work[0] / afs_work / SIXDESKBASE,
        mask=MASK_PATH,
        executable=python3_path,
        python2=python2_path,
        python3=python3_path,
        apply_mad6t_hacks=True,
        replace_dict=r_dict,
        jobid_mask=get_beams_job_id_mask(r_dict.keys()),
        unlock=UNLOCK,
        resubmit=resubmit,
        max_stage=max_stage,
        # max_materialize=60000,  # set in template now
        sixdesk_directory=sixdesk_path,
    )


def hllhc_b50_lim_d1_0(beam, afs_work='jdilly1', max_stage=MAX_STAGE, resubmit=RESUBMIT):
    replace_dict = dict(
        BEAM=beam,
        RDTS=["b6-b6_a6-a6_b5-b5_a5-a5_b4-b4_a4-a4_b3-b3_a3-a3", ],
        D1B5_VALUE=[0, ],
        D2B5_VALUE=[0, 5, 10, 15],
        CORRECT_B5=['no', 'd1', 'd1d2'],
    )
    return _main_setup(replace_dict, afs_work, max_stage, resubmit)


def hllhc_b50_lim_d1_7(beam, afs_work='jdilly1', max_stage=MAX_STAGE, resubmit=RESUBMIT):
    replace_dict = dict(
        BEAM=beam,
        RDTS=["b6-b6_a6-a6_b5-b5_a5-a5_b4-b4_a4-a4_b3-b3_a3-a3", ],
        D1B5_VALUE=[7, ],
        D2B5_VALUE=[0, 5, 10, 15],
        CORRECT_B5=['no', 'd1', 'd1d2'],
    )
    return _main_setup(replace_dict, afs_work, max_stage, resubmit)


def hllhc_b14_lim_d1_0(beam, afs_work='jdilly1', max_stage=MAX_STAGE, resubmit=RESUBMIT):
    replace_dict = dict(
        BEAM=beam,
        RDTS=["b6-b6_a6-a6_b14-b5_a5-a5_b4-b4_a4-a4_b3-b3_a3-a3", ],
        D1B5_VALUE=[0, ],
        D2B5_VALUE=[0, 5, 10, 15],
        CORRECT_B5=['no', 'd1', 'd1d2'],
    )
    return _main_setup(replace_dict, afs_work, max_stage, resubmit)


def hllhc_b14_lim_d1_7(beam, afs_work='jdilly1', max_stage=MAX_STAGE, resubmit=RESUBMIT):
    replace_dict = dict(
        BEAM=beam,
        RDTS=["b6-b6_a6-a6_b14-b5_a5-a5_b4-b4_a4-a4_b3-b3_a3-a3", ],
        D1B5_VALUE=[7, ],
        D2B5_VALUE=[0, 5, 10, 15],
        CORRECT_B5=['no', 'd1', 'd1d2'],
    )
    return _main_setup(replace_dict, afs_work, max_stage, resubmit)


def hllhc_lim_d1_sweep(beam, afs_work='jdilly1', max_stage=MAX_STAGE, resubmit=RESUBMIT):
    replace_dict = dict(
        BEAM=beam,
        RDTS=["b6-b6_a6-a6_b5-b5_a5-a5_b4-b4_a4-a4_b3-b3_a3-a3", "b6-b6_a6-a6_b14-b5_a5-a5_b4-b4_a4-a4_b3-b3_a3-a3"],
        D1B5_VALUE=[0, 5, 10, 15],
        D2B5_VALUE=[0, 10],
        CORRECT_B5=['no', 'd1d2'],
    )
    return _main_setup(replace_dict, afs_work, max_stage, resubmit)


def hllhc_lim_flat_d1_7(beam, afs_work='jdilly1', max_stage=MAX_STAGE, resubmit=RESUBMIT):
    replace_dict = dict(
        BEAM=beam,
        AMPMIN=2, AMPMAX=30, AMPSTEP=2,
        XING=["flat", ],
        ERRORS=['AB3-15', ],
        RDTS=["b6-b6_a6-a6_b5-b5_a5-a5_b4-b4_a4-a4_b3-b3_a3-a3", "b6-b6_a6-a6_b14-b5_a5-a5_b4-b4_a4-a4_b3-b3_a3-a3", "b6-b6_a6-a6_b32-b5_a5-a5_b4-b4_a4-a4_b3-b3_a3-a3"],
        D1B5_VALUE=[7, ],
        D2B5_VALUE=[0, 5, 10, 15],
        CORRECT_B5=['d1d2'],
    )
    return _main_setup(replace_dict, afs_work, max_stage, resubmit)


def hllhc_lim_flat_d1_7_no_correction(beam, afs_work='jdilly1', max_stage=MAX_STAGE, resubmit=RESUBMIT):
    replace_dict = dict(
        BEAM=beam,
        AMPMIN=2, AMPMAX=30, AMPSTEP=2,
        XING=["flat", ],
        ERRORS=['AB3-15', ],
        RDTS=["b6-b6_a6-a6_b5-b5_a5-a5_b4-b4_a4-a4_b3-b3_a3-a3", "b6-b6_a6-a6_b14-b5_a5-a5_b4-b4_a4-a4_b3-b3_a3-a3", "b6-b6_a6-a6_b32-b5_a5-a5_b4-b4_a4-a4_b3-b3_a3-a3"],
        CORRECT_BEAM=['same', ],
        D1B5_VALUE=[7, ],
        D2B5_VALUE=[0, 5, 10, 15],
        CORRECT_B5=['no'],
    )
    return _main_setup(replace_dict, afs_work, max_stage, resubmit)


def hllhc_b50_lim_feeddown_d1_7_both_beams(beam, afs_work='jdilly1', max_stage=MAX_STAGE, resubmit=RESUBMIT):
    replace_dict = dict(
        BEAM=beam,
        RDTS=["b6-b6_a6-a6_b5-b5_a5-a5_b4-b4_a4-a4_b3-b3_a3-a3", ],
        RDTS2=["b6-b6_a6-a6_b5-b5_a5-a5_b4-b4_a4-a4_b3-b3_a3-a3", ],
        FEEDDOWN=[0, 2],
        ITERATIONS=1,
        D1B5_VALUE=[7, ],
        D2B5_VALUE=[0, 5, 10, 15],
        CORRECT_B5=['d1d2'],
    )
    return _main_setup(replace_dict, afs_work, max_stage, resubmit)


def hllhc_b14_lim_feeddown_d1_7_both_beams(beam, afs_work='jdilly1', max_stage=MAX_STAGE, resubmit=RESUBMIT):
    replace_dict = dict(
        BEAM=beam,
        RDTS=["b6-b6_a6-a6_b14-b5_a5-a5_b4-b4_a4-a4_b3-b3_a3-a3", ],
        RDTS2=["b6-b6_a6-a6_b14-b5_a5-a5_b4-b4_a4-a4_b3-b3_a3-a3", ],
        FEEDDOWN=[0, 2],
        ITERATIONS=1,
        D1B5_VALUE=[7, ],
        D2B5_VALUE=[0, 5, 10, 15],
        CORRECT_B5=['d1d2'],
    )
    return _main_setup(replace_dict, afs_work, max_stage, resubmit)


def hllhc_b50_b14_lim_feeddown_d1_7(beam, afs_work='jdilly2', max_stage=MAX_STAGE, resubmit=RESUBMIT):
    replace_dict = dict(
        OUTPUT=EOS_JOSCH_STUDY,
        BEAM=beam,
        RDTS=["b6-b6_a6-a6_b5-b5_a5-a5_b4-b4_a4-a4_b3-b3_a3-a3", "b6-b6_a6-a6_b14-b5_a5-a5_b4-b4_a4-a4_b3-b3_a3-a3"],
        FEEDDOWN=[2],
        ITERATIONS=1,
        D1B5_VALUE=[7, ],
        D2B5_VALUE=[0, 5, 10, 15],
        CORRECT_B5=['d1d2'],
    )
    return _main_setup(replace_dict, afs_work, max_stage, resubmit)


def hllhc_b50_lim_d1_7_other_beam(beam, afs_work='jdilly2', max_stage=MAX_STAGE, resubmit=RESUBMIT):
    replace_dict = dict(
        OUTPUT=EOS_JOSCH_STUDY,
        BEAM=beam,
        RDTS=["b6-b6_a6-a6_b5-b5_a5-a5_b4-b4_a4-a4_b3-b3_a3-a3", ],
        D1B5_VALUE=[7, ],
        D2B5_VALUE=[0, 5, 10, 15],
        CORRECT_B5=['no', 'd1', 'd1d2'],
        CORRECT_BEAM=['other'],
    )
    return _main_setup(replace_dict, afs_work, max_stage, resubmit)
