from generic_parser import EntryPointParameters
from pylhc_submitter.sixdesk_tools.stages import STAGE_ORDER


def get_params():
    return EntryPointParameters(
        beam=dict(
            type=int,
            nargs="+",
            default=[1, 4],
            choices=[1, 4],
        ),
        afs_work=dict(
            type=str,
            choices=['jdilly', 'jdilly1', 'jdilly2', 'jdilly3', 'jdilly4', 'josch']
        ),
        max_stage=dict(
            type=str,
            choices=list(STAGE_ORDER.keys()),
        ),
        resubmit=dict(
            action="store_true",
        ),
    )


def drop_none_value_entries(dict_):
    return {k: v for k, v in dict_.items() if v is not None}
