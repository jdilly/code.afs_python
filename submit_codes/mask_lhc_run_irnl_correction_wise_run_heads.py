import importlib
import sys
import logging
from pathlib import Path

sys.path.insert(0, '/afs/cern.ch/work/j/jdilly/public/python/')
LOG = logging.getLogger(__name__)


if __name__ == '__main__':
    mask_vals = dict(
        year=int("%(YEAR)d"),
        wise=int("%(WISE)d"),
        seed=int("%(SEED)d"),
        # wise=int("2021"),
        # seed=int("1"),
    )
    main = importlib.import_module(f"lhc_run_irnl_correction_single_beam_heads").main

    tmp_output = Path("Outputdata")
    tmp_output.mkdir(exist_ok=True, parents=True)

    main(
        beam=1,
        outputdir=tmp_output,
        errors={"B6": True},
        seed=mask_vals['seed'],
        wise=mask_vals['wise'],
        year=mask_vals['year'],
    )



