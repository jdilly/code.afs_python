from pathlib import Path

from omc3.utils import logging_tools
from pylhc_submitter import autosix

from submit_codes.setups_irnl_beams_b5sweep import _main_setup


LOG = logging_tools.get_logger(__name__, level_console=logging_tools.DEBUG, color=True)


def hllhc_b50_lim(beam, afs_work='jdilly1', max_stage=None, resubmit=False):
    replace_dict = dict(
        BEAM=beam,
        TURNS=100000,
        AMPMIN=2, AMPMAX=20, AMPSTEP=2,
        ANGLES=11,
        FIRSTSEED=0,
        LASTSEED=0,
        SEED='11',
        OUTPUT=f'/afs/cern.ch/work/j/{afs_work}/study.test_purposes/',
        XING=["top", ],
        MACHINE=['hllhc', ],
        OPTICS=["round1515", ],
        ERRORS=['AB3-15', ],
        RDTS=["b6-b6_a6-a6_b5-b5_a5-a5_b4-b4_a4-a4_b3-b3_a3-a3", ],
        RDTS2=["None", ],
        FEEDDOWN=0,
        ITERATIONS=1,
        CORRECT_BEAM=['same', ],
        D1B5_VALUE=[0, ],
        D2B5_VALUE=[0, ],
        CORRECT_B5=['no', ],
        CORRECTOR_STRENGTH=['limited', ],
    )
    setup = _main_setup(replace_dict, afs_work, max_stage, resubmit)
    setup.update(dict(
        working_directory=Path(f'/afs/cern.ch/work/j/{afs_work}/sixdesk_testbase'),
        ssh="lxplus",
        # max_materialize=5,
    ))
    return setup


def hllhc_test_sixdesk(beam, afs_work='jdilly2', max_stage=None, resubmit=False):
    replace_dict = dict(
        BEAM=beam,
        TURNS=100000,
        AMPMIN=2, AMPMAX=20, AMPSTEP=2,
        ANGLES=11,
        FIRSTSEED=0,
        LASTSEED=0,
        SEED='11',
        OUTPUT=f'/afs/cern.ch/work/j/{afs_work}/study.test_purposes/',
        XING=["top", ],
        MACHINE=['hllhc', ],
        OPTICS=["round1515", ],
        ERRORS=['AB3-15', ],
        RDTS=["b6-b6_a6-a6_b5-b5_a5-a5_b4-b4_a4-a4_b3-b3_a3-a3", ],
        RDTS2=["None", ],
        FEEDDOWN=0,
        ITERATIONS=1,
        CORRECT_BEAM=['same', ],
        D1B5_VALUE=[0, ],
        D2B5_VALUE=[0, ],
        CORRECT_B5=['no', ],
        CORRECTOR_STRENGTH=['limited', ],
    )
    setup = _main_setup(replace_dict, afs_work, max_stage, resubmit)
    setup.update(dict(
        working_directory=Path(f'/afs/cern.ch/work/j/{afs_work}/sixdesk_testbase'),
        ssh="lxplus",
        # max_materialize=1000,
        sixdesk_directory=Path('/afs/cern.ch/work/j/jdilly/public/SixDesk/'),
        max_stage="initialize_workspace",
    ))
    return setup


if __name__ == '__main__':
    # autosix.main(hllhc_b50_lim(beam=1))
    autosix.main(hllhc_test_sixdesk(beam=1))
