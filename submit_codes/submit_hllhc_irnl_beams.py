from pylhc_submitter import autosix
from submit_codes.submit_helper import get_params, drop_none_value_entries
from submit_codes.setups_irnl_beams import (
    hllhc_top_same_beams, hllhc_top_other_beams, hllhc_flat_allerr,
    hllhc_allerr_nocorr, hllhc_top_lhc_correction_other, hllhc_top_lhc_correction_same,
    hllhc_both_beams_NoStar
)
from generic_parser import entrypoint
from omc3.utils import logging_tools
LOG = logging_tools.get_logger(__name__, level_console=logging_tools.DEBUG, color=True)


@entrypoint(get_params(), strict=True)
def main(opt):
    opt = drop_none_value_entries(opt)
    # autosix.main(hllhc_top_same_beams(**opt))
    # autosix.main(hllhc_top_other_beams(**opt))
    # autosix.main(hllhc_flat_allerr(**opt))
    # autosix.main(hllhc_allerr_nocorr(**opt))
    # autosix.main(hllhc_top_lhc_correction_other(**opt))
    # autosix.main(hllhc_top_lhc_correction_same(**opt))
    autosix.main(hllhc_both_beams_NoStar(**opt))


if __name__ == '__main__':
    main()