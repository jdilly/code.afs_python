from pylhc import autosix
from setups_irnl_beams import lhc_top_same_beams, lhc_top_other_beams, lhc_flat_allerr, lhc_allerr_nocorr
from omc3.utils import logging_tools
LOG = logging_tools.get_logger(__name__, level_console=logging_tools.DEBUG, color=True)

if __name__ == '__main__':
    # python2_path = None
    # beam = [1, ]
    # beam = [4, ]  # to be run when beam 1 is done with mad6t
    beam = [1, 4]  # after mad6t has finished

    # autosix.main(lhc_top_same_beams(beam))
    # autosix.main(lhc_top_other_beams(beam))
    autosix.main(lhc_flat_allerr(beam))
    autosix.main(lhc_allerr_nocorr(beam))
