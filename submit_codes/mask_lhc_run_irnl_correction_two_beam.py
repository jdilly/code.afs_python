import shutil
import sys
from pathlib import Path
import re
import importlib
from distutils.util import strtobool

sys.path.insert(0, '/afs/cern.ch/work/j/jdilly/public/python/')
from correct_irnl_run_helper import correctstr2dict, get_xing, get_beams_output_path
from irnl_rdt_correction.main import log_setup
import logging

LOG = logging.getLogger(__name__)


if __name__ == '__main__':
    mask_vals = dict(
        machine="%(MACHINE)s",
        beam=int("%(BEAM)d"),
        correct_beam="%(CORRECTBEAM)s",
        rdts="%(RDTS)s",
        rdts2="%(RDTS2)s",
        xing="%(XING)s",
        optics="%(OPTICS)s",
        errors="%(ERRORS)s",
        feeddown=int("%(FEEDDOWN)d"),
        iterations=int("%(ITERATIONS)d"),
        seed=int("%(SEED)s"),
        output=Path("%(OUTPUT)s"),
    )
    main = importlib.import_module(f"{mask_vals['machine']}_run_correct_beams").main

    rdts2 = mask_vals["rdts2"]
    only_one = False
    no_star = False
    if rdts2.endswith(".NS"):
        no_star = True
        rdts2 = rdts2[:-3]
    elif rdts2 != "None":
        only_one = True

    mask_vals_no_beam = mask_vals.copy()
    mask_vals_no_beam.pop('beam')
    output_paths = {
        b: get_beams_output_path(beam=b, **mask_vals_no_beam) for b in (1, 4)
    }
    beam = mask_vals['beam']

    output_paths[beam].mkdir(exist_ok=True, parents=True)
    shutil.copy(__file__, output_paths[beam] / 'run_irnl_beams.py')

    sixtrackdata = list(output_paths[beam].glob("fc*"))
    if len(sixtrackdata):
        log_setup()
        LOG.info("Found pre-run Sixtrack data. Copying to local dir.")
        LOG.info("Job finished normally")  # Needed for mad6t check
        for f in sixtrackdata:
            shutil.copy(f, Path(".") / f.name)
    else:
        if beam != 1:
            raise OSError("No SixtrackData Found.")

        main(
            correct_beam=mask_vals['correct_beam'],
            seed=mask_vals['seed'],
            rdts=correctstr2dict(mask_vals['rdts'], only_one=only_one, no_star=no_star),
            rdts2=correctstr2dict(rdts2, only_one=only_one, no_star=no_star),
            feeddown=mask_vals['feeddown'],
            iterations=mask_vals['iterations'],
            xing=get_xing(machine=mask_vals['machine'], xing=mask_vals['xing'], optics=mask_vals['optics']),
            errors={k: True for k in re.findall(r"[a-zA-Z]+\d+", mask_vals['errors'])},
            optics=mask_vals['optics'],
            outputdirs=output_paths
        )


