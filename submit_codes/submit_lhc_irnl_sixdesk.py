from pathlib import Path

from omc3.utils import logging_tools

from pylhc import autosix
LOG = logging_tools.get_logger(__name__, level_console=logging_tools.DEBUG, color=True)


def hllhc_top():
    return dict(
        working_directory=Path('/afs/cern.ch/work/j/jdilly1/sixdeskbase'),
        mask=Path('mask_irnl.py'),
        executable=python3_path,
        python2=python2_path,
        python3=python3_path,
        ignore_twissfail_check=True,
        replace_dict=dict(
            BEAM=[1, 4],
            TURNS=100000,
            AMPMIN=4, AMPMAX=30, AMPSTEP=2,
            ANGLES=11,
            SEED='%SEEDRAN',
            OUTPUT='/afs/cern.ch/work/j/jdilly1/study.irnl_correction_with_feeddown/',
            FEEDDOWN=2,
            XING="top",
            MACHINE=['hllhc'],
            ERRORS=['B4', 'B6', 'B4B6'],
            RDTS=[None, "b4-by-b4", "b6-by-b6", "b4-by-b6", "b4-by-b6b4"],
        ),
        jobid_mask="%(MACHINE)s.b%(BEAM)d.xing_%(XING)s.errors_%(ERRORS)s.rdts_%(RDTS)s",
        unlock=unlock,
        resubmit=resubmit,
    )


def hllhc_top_b6b4():
    return dict(
        working_directory=Path('/afs/cern.ch/work/j/jdilly1/sixdeskbase'),
        mask=Path('mask_irnl.py'),
        executable=python3_path,
        python2=python2_path,
        python3=python3_path,
        ignore_twissfail_check=True,
        replace_dict=dict(
            BEAM=[1, 4],
            TURNS=100000,
            AMPMIN=4, AMPMAX=30, AMPSTEP=2,
            ANGLES=11,
            SEED='%SEEDRAN',
            OUTPUT='/afs/cern.ch/work/j/jdilly1/study.irnl_correction_with_feeddown/',
            FEEDDOWN=2,
            XING="top",
            MACHINE=['hllhc'],
            ERRORS=['B6', 'B4B6'],
            RDTS=["b6-by-b6_b4-by-b4"],
        ),
        jobid_mask="%(MACHINE)s.b%(BEAM)d.xing_%(XING)s.errors_%(ERRORS)s.rdts_%(RDTS)s",
        unlock=unlock,
        resubmit=resubmit,
    )


def hllhc_flat_b4b6():
    return dict(
        working_directory=Path('/afs/cern.ch/work/j/jdilly1/sixdeskbase'),
        mask=Path('mask_irnl.py'),
        executable=python3_path,
        python2=python2_path,
        python3=python3_path,
        ignore_twissfail_check=True,
        replace_dict=dict(
            BEAM=[1, 4],
            TURNS=100000,
            AMPMIN=4, AMPMAX=30, AMPSTEP=2,
            ANGLES=11,
            SEED='%SEEDRAN',
            OUTPUT='/afs/cern.ch/work/j/jdilly1/study.irnl_correction_with_feeddown/',
            FEEDDOWN=2,
            XING="flat",
            MACHINE=['hllhc'],
            ERRORS=['B4B6'],
            RDTS=[None, "b6-by-b6_b4-by-b4"],
        ),
        jobid_mask="%(MACHINE)s.b%(BEAM)d.xing_%(XING)s.errors_%(ERRORS)s.rdts_%(RDTS)s",
        unlock=unlock,
        resubmit=resubmit,
    )


def hllhc_flat_b6():
    return dict(
        working_directory=Path('/afs/cern.ch/work/j/jdilly/sixdeskbase'),
        mask=Path('mask_irnl.py'),
        executable=python3_path,
        python2=python2_path,
        python3=python3_path,
        ignore_twissfail_check=True,
        replace_dict=dict(
            BEAM=[1, 4],
            TURNS=100000,
            AMPMIN=4, AMPMAX=30, AMPSTEP=2,
            ANGLES=11,
            SEED='%SEEDRAN',
            OUTPUT='/afs/cern.ch/work/j/jdilly/study.irnl_correction_with_feeddown/',
            FEEDDOWN=2,
            XING="flat",
            MACHINE=['hllhc'],
            ERRORS=['B6'],
            RDTS=[None, "b6-by-b6"],
        ),
        jobid_mask="%(MACHINE)s.b%(BEAM)d.xing_%(XING)s.errors_%(ERRORS)s.rdts_%(RDTS)s",
        unlock=unlock,
        resubmit=resubmit,
    )


def lhc_top():
    return dict(
        working_directory=Path('/afs/cern.ch/work/j/josch/sixdeskbase'),
        mask=Path('mask_irnl.py'),
        executable=python3_path,
        python2=python2_path,
        python3=python3_path,
        ignore_twissfail_check=True,
        replace_dict=dict(
            BEAM=[1, 4],
            TURNS=100000,
            AMPMIN=4, AMPMAX=30, AMPSTEP=2,
            ANGLES=11,
            SEED='%SEEDRAN',
            OUTPUT='/afs/cern.ch/work/j/josch/study.irnl_correction_with_feeddown/',
            FEEDDOWN=2,
            XING="top",
            MACHINE=['lhc'],
            ERRORS=['B4', 'B6', 'B4B6'],
            RDTS=[None, "b4-by-b4", "b6-by-b6", "b4-by-b6", "b4-by-b6b4"],
        ),
        jobid_mask="%(MACHINE)s.b%(BEAM)d.xing_%(XING)s.errors_%(ERRORS)s.rdts_%(RDTS)s",
        unlock=unlock,
        resubmit=resubmit,
    )


def lhc_top_b6b4():
    return dict(
        working_directory=Path('/afs/cern.ch/work/j/josch/sixdeskbase'),
        mask=Path('mask_irnl.py'),
        executable=python3_path,
        python2=python2_path,
        python3=python3_path,
        ignore_twissfail_check=True,
        replace_dict=dict(
            BEAM=[1, 4],
            TURNS=100000,
            AMPMIN=4, AMPMAX=30, AMPSTEP=2,
            ANGLES=11,
            SEED='%SEEDRAN',
            OUTPUT='/afs/cern.ch/work/j/josch/study.irnl_correction_with_feeddown/',
            FEEDDOWN=2,
            XING="top",
            MACHINE=['lhc'],
            ERRORS=['B6', 'B4B6'],
            RDTS=["b6-by-b6_b4-by-b4"],
        ),
        jobid_mask="%(MACHINE)s.b%(BEAM)d.xing_%(XING)s.errors_%(ERRORS)s.rdts_%(RDTS)s",
        unlock=unlock,
        resubmit=resubmit,
    )


def lhc_flat_b4b6():
    return dict(
        working_directory=Path('/afs/cern.ch/work/j/josch/sixdeskbase'),
        mask=Path('mask_irnl.py'),
        executable=python3_path,
        python2=python2_path,
        python3=python3_path,
        ignore_twissfail_check=True,
        replace_dict=dict(
            BEAM=[1, 4],
            TURNS=100000,
            AMPMIN=4, AMPMAX=30, AMPSTEP=2,
            ANGLES=11,
            SEED='%SEEDRAN',
            OUTPUT='/afs/cern.ch/work/j/josch/study.irnl_correction_with_feeddown/',
            FEEDDOWN=2,
            XING="flat",
            MACHINE=['lhc'],
            ERRORS=['B4B6'],
            RDTS=[None, "b6-by-b6_b4-by-b4"],
        ),
        jobid_mask="%(MACHINE)s.b%(BEAM)d.xing_%(XING)s.errors_%(ERRORS)s.rdts_%(RDTS)s",
        unlock=unlock,
        resubmit=resubmit,
    )


def lhc_flat_b6():
    return dict(
        working_directory=Path('/afs/cern.ch/work/j/josch/sixdeskbase'),
        mask=Path('mask_irnl.py'),
        executable=python3_path,
        python2=python2_path,
        python3=python3_path,
        ignore_twissfail_check=True,
        replace_dict=dict(
            BEAM=[1, 4],
            TURNS=100000,
            AMPMIN=4, AMPMAX=30, AMPSTEP=2,
            ANGLES=11,
            SEED='%SEEDRAN',
            OUTPUT='/afs/cern.ch/work/j/josch/study.irnl_correction_with_feeddown/',
            FEEDDOWN=2,
            XING="flat",
            MACHINE=['lhc'],
            ERRORS=['B6'],
            RDTS=[None, "b6-by-b6"],
        ),
        jobid_mask="%(MACHINE)s.b%(BEAM)d.xing_%(XING)s.errors_%(ERRORS)s.rdts_%(RDTS)s",
        unlock=unlock,
        resubmit=resubmit,
    )




def lhc_flat_all_errors():
    afs_work = 'jdilly'
    return dict(
        working_directory=Path(f'/afs/cern.ch/work/j/{afs_work}/sixdeskbase1'),
        mask=Path('mask_irnl_update_and_feeddown.py'),
        executable=python3_path,
        python2=python2_path,
        python3=python3_path,
        ignore_twissfail_check=True,
        replace_dict=dict(
            BEAM=[1, 4],
            TURNS=100000,
            AMPMIN=4, AMPMAX=30, AMPSTEP=2,
            ANGLES=11,
            SEED='%SEEDRAN',
            OUTPUT=f'/afs/cern.ch/work/j/{afs_work}/study.irnl_correction_with_update_and_feeddown/',
            FEEDDOWN=0,
            XING="flat",
            MACHINE=['lhc'],
            ERRORS=['AB3AB4AB5AB6'],
            UPDATE=[False],
            RDTS=['None', "b6-by-b6_b4-by-b4_a4-by-a4_b3-by-b3_a3-by-a3"],
        ),
        jobid_mask="%(MACHINE)s.b%(BEAM)d.xing_%(XING)s.errors_%(ERRORS)s.fd_%(FEEDDOWN)d.update_%(UPDATE)s.rdts_%(RDTS)s",
        unlock=unlock,
        resubmit=resubmit,
    )


def lhc_no_correct_all_errors():
    afs_work = 'jdilly'
    return dict(
        working_directory=Path(f'/afs/cern.ch/work/j/{afs_work}/sixdeskbase1'),
        mask=Path('mask_irnl_update_and_feeddown.py'),
        executable=python3_path,
        python2=python2_path,
        python3=python3_path,
        ignore_twissfail_check=True,
        replace_dict=dict(
            BEAM=[1, 4],
            TURNS=100000,
            AMPMIN=4, AMPMAX=30, AMPSTEP=2,
            ANGLES=11,
            SEED='%SEEDRAN',
            OUTPUT=f'/afs/cern.ch/work/j/{afs_work}/study.irnl_correction_with_update_and_feeddown/',
            FEEDDOWN=0,
            XING="top",
            MACHINE=['lhc'],
            ERRORS=['AB3AB4AB5AB6'],
            UPDATE=[False],
            RDTS=['None'],
        ),
        jobid_mask="%(MACHINE)s.b%(BEAM)d.xing_%(XING)s.errors_%(ERRORS)s.fd_%(FEEDDOWN)d.update_%(UPDATE)s.rdts_%(RDTS)s",
        unlock=unlock,
        resubmit=resubmit,
    )



def hllhc_flat_all_errors():
    afs_work = 'jdilly1'
    return dict(
        working_directory=Path(f'/afs/cern.ch/work/j/{afs_work}/sixdeskbase1'),
        mask=Path('mask_irnl_update_and_feeddown.py'),
        executable=python3_path,
        python2=python2_path,
        python3=python3_path,
        ignore_twissfail_check=True,
        replace_dict=dict(
            BEAM=[1, 4],
            TURNS=100000,
            AMPMIN=4, AMPMAX=30, AMPSTEP=2,
            ANGLES=11,
            SEED='%SEEDRAN',
            OUTPUT=f'/afs/cern.ch/work/j/{afs_work}/study.irnl_correction_with_update_and_feeddown/',
            FEEDDOWN=0,
            XING="flat",
            MACHINE=['hllhc'],
            ERRORS=['AB3AB4AB5AB6'],
            UPDATE=[False],
            RDTS=['None', "b6-by-b6_a6-by-a6_b5-by-b5_a5-by-a5_b4-by-b4_a4-by-a4_b3-by-b3_a3-by-a3"],
        ),
        jobid_mask="%(MACHINE)s.b%(BEAM)d.xing_%(XING)s.errors_%(ERRORS)s.fd_%(FEEDDOWN)d.update_%(UPDATE)s.rdts_%(RDTS)s",
        unlock=unlock,
        resubmit=resubmit,
    )


def hllhc_no_correct_all_errors():
    afs_work = 'jdilly1'
    return dict(
        working_directory=Path(f'/afs/cern.ch/work/j/{afs_work}/sixdeskbase1'),
        mask=Path('mask_irnl_update_and_feeddown.py'),
        executable=python3_path,
        python2=python2_path,
        python3=python3_path,
        ignore_twissfail_check=True,
        replace_dict=dict(
            BEAM=[1, 4],
            TURNS=100000,
            AMPMIN=4, AMPMAX=30, AMPSTEP=2,
            ANGLES=11,
            SEED='%SEEDRAN',
            OUTPUT=f'/afs/cern.ch/work/j/{afs_work}/study.irnl_correction_with_update_and_feeddown/',
            FEEDDOWN=0,
            XING="top",
            MACHINE=['hllhc'],
            ERRORS=['AB3AB4AB5AB6'],
            UPDATE=[False],
            RDTS=['None'],
        ),
        jobid_mask="%(MACHINE)s.b%(BEAM)d.xing_%(XING)s.errors_%(ERRORS)s.fd_%(FEEDDOWN)d.update_%(UPDATE)s.rdts_%(RDTS)s",
        unlock=unlock,
        resubmit=resubmit,
    )


def hllhc_test():
    afs_work = 'jdilly1'
    return dict(
        working_directory=Path(f'/afs/cern.ch/work/j/{afs_work}/sixdeskbase1'),
        mask=Path('mask_irnl_update_and_feeddown.py'),
        executable=python3_path,
        python2=python2_path,
        python3=python3_path,
        ignore_twissfail_check=True,
        replace_dict=dict(
            BEAM=[1],
            TURNS=100000,
            AMPMIN=4, AMPMAX=30, AMPSTEP=2,
            ANGLES=11,
            SEED='%SEEDRAN',
            OUTPUT=f'/afs/cern.ch/work/j/{afs_work}/study.irnl_correction_with_update_and_feeddown/',
            FEEDDOWN=0,
            XING="top",
            MACHINE=['hllhc'],
            ERRORS=['AB3AB4AB5AB6'],
            UPDATE=[False],
            RDTS=['None'],
        ),
        jobid_mask="%(MACHINE)s.b%(BEAM)d.xing_%(XING)s.errors_%(ERRORS)s.fd_%(FEEDDOWN)d.update_%(UPDATE)s.rdts_%(RDTS)s",
        unlock=unlock,
        resubmit=False,
    )


# Update and Feeddown ----------------------------------------------------------


def hllhc_update_and_feeddown():
    afs_work = 'jdilly1'
    return dict(
        working_directory=Path(f'/afs/cern.ch/work/j/{afs_work}/sixdeskbase1'),
        mask=Path('mask_irnl_update_and_feeddown.py'),
        executable=python3_path,
        python2=python2_path,
        python3=python3_path,
        ignore_twissfail_check=True,
        replace_dict=dict(
            BEAM=[1, 4],
            TURNS=100000,
            AMPMIN=4, AMPMAX=30, AMPSTEP=2,
            ANGLES=11,
            SEED='%SEEDRAN',
            OUTPUT=f'/afs/cern.ch/work/j/{afs_work}/study.irnl_correction_with_update_and_feeddown/',
            FEEDDOWN=[0, 2],
            XING="top",
            MACHINE=['hllhc'],
            ERRORS=['AB3AB4AB5AB6'],
            UPDATE=[True, False],
            RDTS=["b6-by-b6_a6-by-a6_b5-by-b5_a5-by-a5_b4-by-b4_a4-by-a4_b3-by-b3_a3-by-a3"],
        ),
        jobid_mask="%(MACHINE)s.b%(BEAM)d.xing_%(XING)s.errors_%(ERRORS)s.fd_%(FEEDDOWN)d.update_%(UPDATE)s.rdts_%(RDTS)s",
        unlock=unlock,
        resubmit=resubmit,
    )


def lhc_update_and_feeddown():
    afs_work = 'jdilly'
    return dict(
        working_directory=Path(f'/afs/cern.ch/work/j/{afs_work}/sixdeskbase1'),
        mask=Path('mask_irnl_update_and_feeddown.py'),
        executable=python3_path,
        python2=python2_path,
        python3=python3_path,
        ignore_twissfail_check=True,
        replace_dict=dict(
            BEAM=[1, 4],
            TURNS=100000,
            AMPMIN=4, AMPMAX=30, AMPSTEP=2,
            ANGLES=11,
            SEED='%SEEDRAN',
            OUTPUT=f'/afs/cern.ch/work/j/{afs_work}/study.irnl_correction_with_update_and_feeddown/',
            FEEDDOWN=[0, 2],
            XING="top",
            MACHINE=['lhc'],
            ERRORS=['AB3AB4AB5AB6'],
            UPDATE=[True, False],
            RDTS=["b6-by-b6_b4-by-b4_a4-by-a4_b3-by-b3_a3-by-a3"],
        ),
        jobid_mask="%(MACHINE)s.b%(BEAM)d.xing_%(XING)s.errors_%(ERRORS)s.fd_%(FEEDDOWN)d.update_%(UPDATE)s.rdts_%(RDTS)s",
        unlock=unlock,
        resubmit=resubmit,
    )


def hllhc_lhc_update_and_feeddown_b6b4():
    afs_work = 'jdilly3'
    return dict(
        working_directory=Path(f'/afs/cern.ch/work/j/{afs_work}/sixdeskbase3'),
        mask=Path('mask_irnl_update_and_feeddown.py'),
        executable=python3_path,
        python2=python2_path,
        python3=python3_path,
        ignore_twissfail_check=True,
        replace_dict=dict(
            BEAM=[1, 4],
            TURNS=100000,
            AMPMIN=4, AMPMAX=30, AMPSTEP=2,
            ANGLES=11,
            SEED='%SEEDRAN',
            OUTPUT=f'/afs/cern.ch/work/j/{afs_work}/study.irnl_correction_with_update_and_feeddown/',
            FEEDDOWN=[0, 2],
            XING="top",
            MACHINE=['lhc', 'hllhc'],
            ERRORS=['B4B6'],
            UPDATE=[True],
            RDTS=["b6-by-b6_b4-by-b4"],
        ),
        jobid_mask="%(MACHINE)s.b%(BEAM)d.xing_%(XING)s.errors_%(ERRORS)s.fd_%(FEEDDOWN)d.update_%(UPDATE)s.rdts_%(RDTS)s",
        unlock=unlock,
        resubmit=resubmit,
    )


if __name__ == '__main__':
    python3_path = Path('/afs/cern.ch/work/j/jdilly/public/venvs/for_htc/bin/python')
    python2_path = Path('/afs/cern.ch/work/j/jdilly/public/venvs/py2lintrack/bin')
    # python2_path = None
    unlock = False
    resubmit = False

    # autosix.main(lhc_top())
    # autosix.main(lhc_top_b6b4())
    # autosix.main(lhc_flat_b6())
    # autosix.main(lhc_flat_b4b6())
    # autosix.main(hllhc_top())
    # autosix.main(hllhc_top_b6b4())
    # autosix.main(hllhc_flat_b6())
    # autosix.main(hllhc_flat_b4b6())
    # autosix.main(lhc_update_and_feeddown())
    # autosix.main(lhc_no_correct_all_errors())
    # autosix.main(lhc_flat_all_errors())
    # autosix.main(hllhc_update_and_feeddown())
    # autosix.main(hllhc_no_correct_all_errors())
    # autosix.main(hllhc_flat_all_errors())
    autosix.main(hllhc_lhc_update_and_feeddown_b6b4())

