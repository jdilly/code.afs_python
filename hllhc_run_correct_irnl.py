import logging
import shutil
from contextlib import contextmanager, suppress
from pathlib import Path
from typing import Sequence

import tfs
from cpymad.madx import Madx
from optics_functions.rdt import calculate_rdts
from optics_functions.utils import prepare_twiss_dataframe

from cpymad_lhc.corrector_limits import check_corrector_limits
from cpymad_lhc.general import (get_tfs, switch_magnetic_errors, match_tune, get_k_strings, amplitude_detuning_ptc, sixtrack_output, rdts_ptc)
from cpymad_lhc.ir_orbit import orbit_setup
from cpymad_lhc.logging import cpymad_logging_setup
from pylhc.irnl_rdt_correction import main as irnl_correct

LOG = logging.getLogger(__name__)  # setup in main()
LOG_LEVEL = logging.DEBUG

OUTDIR = Path("Outputdata")
TEMPDIR = Path('temp')

RDT_PART_MAP = {
    "A": "ABS",
    "C": "REAL",
    "S": "IMAG",
}

PATHS = {
    "db5": Path("/afs/cern.ch/eng/lhc/optics/V6.503"),
    "lhc": Path("/afs/cern.ch/eng/lhc/optics/runIII"),
    "slhc": Path("/afs/cern.ch/eng/lhc/optics/HLLHCV1.3"),
    "external": Path("/afs/cern.ch/work/j/jdilly/public/macros"),
}


def pathstr(key: str, *args: str):
    return str(PATHS[key].joinpath(*args))


def get_wise_path(seed: int):
    return f"/afs/cern.ch/work/j/jdilly/wise/WISE-2015-LHCsqueeze-0.4_10.0_0.4_3.0-6.5TeV-emfqcs/WISE.errordef.{seed:04d}.tfs"


def make_dirs(*dirs: Path):
    for dir_ in dirs:
        dir_.mkdir(exist_ok=True, parents=True)


def clean_up(tempdir: Path):
    shutil.rmtree(tempdir, ignore_errors=True)


def drop_allzero_columns(df: tfs.TfsDataFrame):
    return df.loc[:, (df != 0).any(axis="index")]


@contextmanager
def temp_disable_errors(madx, *args):
    """ Disable all global variable args and restore their value afterwards."""
    saved = [madx.globals[arg] for arg in args]
    for arg in args:
        madx.globals[arg] = 0
    yield
    for arg, val in zip(args, saved):
        madx.globals[arg] = val


DEFAULT_COLUMNS = ['NAME', 'KEYWORD', 'S', 'X', 'Y', 'L', 'LRAD',
                   'BETX', 'BETY', 'ALFX', 'ALFY', 'DX', 'DY', 'MUX', 'MUY',
                   'R11', 'R12', 'R21', 'R22'] + get_k_strings()


def main(beam: int, rdts: Sequence, feeddown: int, xing: dict, errors: dict, seed: int = 1,
         update_optics: bool = False,
         outputdir: Path = OUTDIR, tempdir: Path = TEMPDIR):
    make_dirs(outputdir, tempdir)
    madx = Madx(**cpymad_logging_setup(level=LOG_LEVEL,  # sets also standard loggers
                                       command_log=outputdir/'madx_commands.log',
                                       full_log=outputdir/'full_output.log'))
    mvars = madx.globals    # shorthand

    # user definitions
    local_seed = 100 + seed  # random seed for errors
    tune_x = 62.31
    tune_y = 60.32
    dispersion = 3

    # Switch which MQXF errortables to use:
    # 'True' uses the error tables for MQXFA and MQXFB
    # while 'False' the ones for body and fringe
    use_mqxf_ab_errors = False

    # Toggle correction of the errors in the dipole correctors.
    # This correction is not baseline 2020-07-07 as errors change
    # with crossing angle. Hence, this might be less reproducable
    # in reality, use with care!
    correct_mcbx_errors = False

    # apply field errors to arcs
    on_arc_errors = False

    # apply b2s errors in the MQ in the IRs (if on_b2s is active)
    # this is usually deactivated as these errors are assumed to be well corrected
    on_b2s_mq = False

    # apply alignment errors
    on_alignment_errors = False

    energy = 7000            # beam energy in GeV
    emittance_norm = 2.5e-6  # normalized emittance
    n_particles = 1.0e10     # number of particles in beam
    bunch_len = 0.0755       # bunch length [m] in collision

    emittance = emittance_norm * mvars.pmass / energy
    rel_energy_spread = 4.5e-4*(450./energy)**0.5

    # define some global madx variables
    mvars.mylhcbeam = beam  # used in macros
    mvars.on_disp = 0       # correction of spurious dispersion
    mvars.nrj = energy      # used in some macros and below in beam()

    # Output helper
    def output_path(type_, output_id, dir_=outputdir):
        return dir_ / f'{type_}.hllhc.b{beam:d}.{output_id}.tfs'

    def output_twiss(output_id, index_regex=r"BPM|M|IP", dir_=outputdir, pre_match_tune=True):
        if pre_match_tune:
            match_tune(madx,
                       accel="HLLHC", sequence=seq_name,
                       qx=tune_x, qy=tune_y,
                       dqx=dispersion, dqy=dispersion)
        madx.twiss(sequence=seq_name)
        tfs_df = get_tfs(madx.table.twiss, columns=DEFAULT_COLUMNS, index_regex=index_regex)
        if dir_ is not None:
            tfs.write(output_path('twiss', output_id, dir_=dir_), drop_allzero_columns(tfs_df), save_index="NAME")
        return tfs_df

    def output_errors(output_id, index_regex="M", dir_=outputdir):
        # As far as I can tell `only_selected` does not work with
        # etable and there is always only the selected items in the table
        # (jdilly, cpymad 1.4.1)
        error_columns = ["NAME", "DX", "DY"] + get_k_strings()
        madx.select(flag='error', clear=True)
        madx.select(flag='error', column=error_columns)
        madx.etable(table='error')
        df_errors = get_tfs(madx.table.error, index_regex=index_regex, columns=error_columns)
        if dir_ is not None:
            tfs.write(output_path('errors', output_id, dir_=dir_), drop_allzero_columns(df_errors), save_index="NAME")
        return df_errors

    def output_analytical_rdts(output_id, index_regex="BPM|M|IP", dir_=outputdir):
        df_twiss = get_tfs(madx.table.twiss, index_regex=index_regex)
        df_errors = get_tfs(madx.table.error, index_regex=index_regex)
        df_combined = prepare_twiss_dataframe(df_twiss, df_errors, invert_signs_madx=(beam == 4))
        df_rdts_ana = calculate_rdts(df_combined, rdts=rdts, qx=tune_x, qy=tune_y,
                                     complex_columns=False, loop_phases=True, feeddown=2)
        if dir_ is not None:
            tfs.write(output_path('twiss_rdt', output_id, dir_=dir_), df_rdts_ana, save_index="NAME")
        return df_rdts_ana

    def output_ptc_rdts(output_id, index_regex="BPM|M|IP", dir_=outputdir):
        rdts_ptc(madx, order=max(sum(int(i) for i in s[1:5]) for s in rdts))
        df_rdts = get_tfs(madx.table.twissrdt, index_regex=index_regex)
        df_rdts = df_rdts.drop(columns=df_rdts.columns[~df_rdts.columns.str.match(r".*_0_0$")])  # not interested dispersion
        df_rdts = df_rdts.drop(columns=df_rdts.columns[df_rdts.columns.str.match("GNFA")])  # IMAG and REAL is enough
        df_rdts = df_rdts.rename(columns=lambda x: f"F{x[5]}{x[7]}{x[9]}{x[11]}{RDT_PART_MAP[x[3]]}")
        if dir_ is not None:
            tfs.write(output_path('ptc_rdt', output_id, dir_=dir_), df_rdts, save_index="NAME")
        return df_rdts

    def output_ampdet(output_id):
        return amplitude_detuning_ptc(madx, order=2, file=output_path('ampdet', output_id))

    # Load Macros
    madx.call(pathstr("slhc", "toolkit", "macro.madx"))

    # Lattice Setup ---------------------------------------
    # Load Sequence
    seq_file = "lhc.seq"
    seq_name = f"lhcb{beam}"
    bv_flag = -1 if beam == 2 else 1
    if beam == 4:
        seq_file = "lhcb4.seq"
        seq_name = f"lhcb2"

    madx.call(pathstr('lhc', seq_file))  # LHC sequence
    madx.call(pathstr('slhc', 'hllhc_sequence.madx'))  # updates to HL-LHC

    # slice sequence
    madx.exec('myslice')

    madx.call(pathstr('slhc', 'errors', 'install_mqxf_fringenl.madx'))  # adding fringe place holder

    # HL-LHC V1.3
    madx.call(pathstr('slhc', 'errors', 'install_MCBXAB_errors.madx'))  # adding D1 corrector placeholders in IR1/5 (for errors)

    # HL-LLHC V1.4
    # madx.call(pathstr('slhc', 'errors', 'install_MCBXFAB_errors.madx'))  # adding D1 corrector placeholders in IR1/5 (for errors)
    # madx.call(pathstr('slhc', 'errors', 'install_MCBRD_errors.madx'))   # adding D2 corrector placeholders in IR1/5 (for errors)

    # Cycling w.r.t. to IP3 (mandatory to find closed orbit in collision in the presence of errors)
    madx.seqedit(sequence=seq_name)
    madx.flatten()
    madx.cycle(start="IP3")
    madx.endedit()

    # Define Optics and make beam
    madx.call(pathstr('slhc', 'squeeze2', 'opt_300_300_300_300_thin.madx'))  # 30cm thin round optics

    madx.beam(sequence=seq_name, bv=bv_flag,
              energy="NRJ", sige=rel_energy_spread,
              particle="proton", npart=n_particles,
              ex=emittance, ey=emittance,
              kbunch=1, sigt=bunch_len,
              )

    # Setup Orbit
    orbit_vars = orbit_setup(madx, **xing)

    madx.use(sequence=seq_name)

    # Save Nominal
    df_nominal = output_twiss('nominal')
    df_ampdet_nominal = output_ampdet('nominal')

    # Save nominal optics in IR+Correctors for ir nl correction
    select_pattern = r"M(QS?X|BX|BRC|C[SODT]S?X)"
    if correct_mcbx_errors:
        select_pattern += "|MCBXF"

    df_twiss_ir = output_twiss('optics_ir', index_regex=select_pattern, dir_=outputdir, pre_match_tune=False)

    # Align separation magnets
    madx.call(pathstr('slhc', 'toolkit', 'align_sepdip.madx'))
    madx.exec('align_mbx15')  # HL-LHC D1
    madx.exec('align_mbrd15')  # HL-LHC D2 in IR15
    madx.exec('align_mbx28')  # V6.503 D1 in IR28
    madx.exec('align_mbrc28')  # V6.503 D2 in IR28
    madx.exec('align_mbrs')   # V6.503 D3 in IR4
    madx.exec('align_mbrb')   # V6.503 D4 in IR4
    madx.call(pathstr('slhc', 'toolkit', 'align_mbh.madx'))  # align 11T dipoles

    # Error subroutines --------------------------------------------------------
    # Error routine and measured error table for nominal LHC

    madx.call(pathstr('db5', 'measured_errors', 'Msubroutines_new.madx'))
    madx.call(pathstr('db5', 'measured_errors', 'Msubroutines_MS_MSS_MO_new.madx'))
    madx.call(pathstr('db5', 'toolkit', 'Orbit_Routines.madx'))
    madx.call(pathstr('slhc', 'errors', 'SelectLHCMonCor.madx'))
    madx.readtable(file=pathstr('db5', 'measured_errors', 'rotations_Q2_integral.tab'))

    # Error routine and error table for new IT/D1/D2/Q4/Q5
    madx.call(pathstr('slhc', 'errors', 'macro_error.madx'))  # macros for error generation in the new IT/D1's
    if use_mqxf_ab_errors:
        LOG.debug("Using MQXF AB error-tables")
        madx.call(pathstr('slhc', 'errors', 'ITa_errortable_v4'))  # target error table for the new IT
        madx.call(pathstr('slhc', 'errors', 'ITb_errortable_v5'))  # target error table for the new IT
    else:
        LOG.debug("Using MQXF body and fringe  error-tables")
        madx.call(pathstr('slhc', 'errors', 'ITbody_errortable_v5'))  # target error table for the new IT
        madx.call(pathstr('slhc', 'errors', 'ITnc_errortable_v5'))  # target error table for the new IT
        madx.call(pathstr('slhc', 'errors', 'ITcs_errortable_v5'))  # target error table for the new IT
        mvars.b6M_MQXF_col = -4  # New baseline value, if using version > v5 might already be in

    madx.call(pathstr('slhc', 'errors', 'D1_errortable_v1'))   # target error table for the new D1
    madx.call(pathstr('slhc', 'errors', 'D2_errortable_v5'))  # target error table for the new D2

    # HL-LHC v1.3
    madx.call(pathstr('slhc', 'errors', 'Q4_errortable_v2'))  # target error table for the new Q4 in IR1 and IR5

    # HL-LHC v1.4
    # madx.call(pathstr('slhc', 'errors', 'MCBXFAB_errortable_v1'))
    # madx.call(pathstr('slhc', 'errors', 'MBH_errortable_v3'))
    # madx.call(pathstr('slhc', 'errors', 'MCBRD_errortable_v1'))

    # Apply magnetic errors ----------------------------------------------------
    # Shorthand definitions ---
    def apply_measured_errors(*magnets):
        for magnet in magnets:
            madx.call(pathstr('db5', 'measured_errors', f'Efcomp_{magnet}.madx'))

    def apply_hllhc_errors(*magnets):
        for magnet in magnets:
            madx.call(pathstr('slhc', 'errors', f'Efcomp_{magnet}.madx'))

    def set_eseed(add_to_seed):
        madx.eoption(seed=local_seed+add_to_seed)

    # Set which magnetic field error orders to use
    switch_magnetic_errors(madx, **errors)

    # Read WISE
    madx.readtable(file=get_wise_path(seed))

    # Possibly disable b2s in MQ (see above)
    disable = []
    if not on_b2s_mq:
        disable = ['on_b2s']

    # Apply Arc Errors ---
    if on_arc_errors:
        apply_measured_errors('MB')

        # Correct orbit distortion resulting from MB magnets (if present)
        if any(mvars[f'on_{field}'] for field in ('A1s', 'A1r', 'B1s', 'B1r')):
            madx.exec('initial_micado(4)')
            madx.exec('initial_micado(4)')

        with temp_disable_errors(madx, *disable):
            apply_measured_errors('MQ')

    # Other Errors ---
    # Nominal LHC magnets
    # Separation Dipoles
    apply_measured_errors('MBRB', 'MBRC', 'MBRS', 'MBX', 'MBW')

    # IR Quadrupoles
    with temp_disable_errors(madx, *disable):
        apply_measured_errors('MQW', 'MQTL', 'MQMC', 'MQX',
                              'MQY', 'MQM', 'MQML',)

    if on_alignment_errors:
        madx.call(pathstr('db5', 'measured_errors', 'Set_alignment_errors.madx'))

    # New IT/D1/D2/Q4/Q5
    set_eseed(1)
    if use_mqxf_ab_errors:
        apply_hllhc_errors('MQXFA', 'MQXFB')
    else:
        madx.call(pathstr('external', 'Efcomp_MQXFbody.madx'))  # special version
        apply_hllhc_errors('MQXFA', 'MQXFB', 'MQXFends')

    set_eseed(2)
    apply_hllhc_errors('MBXAB')  # new D1 in IR 1/5

    set_eseed(3)
    apply_hllhc_errors('MBRD')  # new D2 in IR1/5

    set_eseed(4)
    # HL-LHC v1.3
    apply_hllhc_errors('MQYY')  # new Q4 in IR1/5

    # HL-LHC v1.4
    # apply_hllhc_errors('MQY')  # ! old Q4 in IR1/5, but switched places around IP1-5

    # set_eseed(5)
    # apply_hllhc_errors('MCBXFAB')  # new triplet correctors in IR1/5

    # set_eseed(6)
    # with temp_disable_errors(madx, 'on_b2s', 'on_b2r'):  # Assumed to be well corrected? (jdilly)
    #     apply_hllhc_errors('MBH')

    # set_eseed(7)
    # apply_hllhc_errors('MCBRD')

    # Save uncorrected
    df_uncorrected = output_twiss('uncorrected')
    df_errors = output_errors('all')
    df_ampdet_uncorrected = output_ampdet('uncorrected')

    # Save errors to table to be used for correction ---------------------------
    df_errors_ir = output_errors('ir', index_regex=r"M([QB]X|BRC)")

    # IR Nonlinear Correction: Calculate corrector powering --------------------
    if rdts is not None:
        correction_cmd, correction_df = irnl_correct(
            # Correction setup for HL-LHC ---
            beams=(beam,),
            optics=(df_twiss_ir,),
            errors=(df_errors_ir,),
            rdts=rdts,
            output=output_path('settings', 'mcx'),
            accel='hllhc',
            feeddown=feeddown,  # order of feeddown to take into account
            # The following are the defaults ---
            ips=(1, 2, 5, 8),  # in which IPs to correct
            solver='lstsq',  # 'inv', 'linear' Solver to use
            update_optics=update_optics,  # updates optics after setting corrector strength (for feeddown)
            ignore_missing_columns=False,  # True: if columns are missing, assume 0
        )

        # Apply correction
        madx.input(correction_cmd)
        with suppress(ValueError):
            check_corrector_limits(madx, accel="HLLHC", beam=beam)

        # Save corrected
        df_corrected = output_twiss('corrected')
        df_ampdet_corrected = output_ampdet('corrected')

    sixtrack_output(madx, mvars.nrj)

    madx.exit()
    clean_up(tempdir)
