import logging
from pathlib import Path

import tfs
from cpymad.madx import Madx

from cpymad_lhc.general import (get_tfs, match_tune, get_k_strings, get_lhc_sequence_filename_and_bv)
from cpymad_lhc.ir_orbit import orbit_setup, log_orbit
from cpymad_lhc.logging import cpymad_logging_setup

LOG = logging.getLogger(__name__)  # setup in main()
LOG_LEVEL = logging.DEBUG

OUTDIR = Path("Outputdata")
TEMPDIR = Path('temp')

PATHS = {
    "db5": Path("/afs/cern.ch/eng/lhc/optics/V6.503"),
    "lhc": Path("/afs/cern.ch/eng/lhc/optics/runIII"),
    "slhc": Path("/afs/cern.ch/eng/lhc/optics/HLLHCV1.3"),
    # "slhc": Path("/afs/cern.ch/eng/lhc/optics/HLLHCV1.5"),
    "external": Path("/afs/cern.ch/work/j/jdilly/public/macros"),
}


def pathstr(key: str, *args: str):
    return str(PATHS[key].joinpath(*args))


def get_optics_path(optics: str):
    optics_map = {
        'inj': pathstr("slhc", "opt_inj.madx"),
        # 'flat07530': pathstr("slhc", 'flat', 'opt_flatvh_75_300_1500.madx'),  v1.5
        'flat07530': pathstr("slhc", 'squeeze_flatvh', 'opt_075_300_300_075.madx'),
        'flat07530thin': pathstr("slhc", 'squeeze_flatvh', 'opt_075_300_300_075_thin.madx'),
        'round3030': pathstr('slhc', 'squeeze2', 'opt_300_300_300_300.madx'),
        'round3030thin': pathstr('slhc', 'squeeze2', 'opt_300_300_300_300_thin.madx'),
        'round1515': pathstr('slhc', 'squeeze2', 'opt_150_150_150_150.madx'),
        'round1515thin': pathstr('slhc', 'squeeze2', 'opt_150_150_150_150_thin.madx'),
        # 'round3030': pathstr('slhc', 'squeeze', 'opt_300_300_300_300.madx'),
        # 'round3030thin': pathstr('slhc', 'squeeze', 'opt_300_300_300_300_thin.madx'),
        # 'round1515': pathstr('slhc', 'squeeze', 'opt_150_150_150_150.madx'),
        # 'round1515thin': pathstr('slhc', 'squeeze', 'opt_150_150_150_150_thin.madx'),
    }
    return optics_map[optics]


def make_dirs(*dirs: Path):
    for dir_ in dirs:
        dir_.mkdir(exist_ok=True, parents=True)


def drop_allzero_columns(df: tfs.TfsDataFrame):
    return df.loc[:, (df != 0).any(axis="index")]


DEFAULT_COLUMNS = ['NAME', 'KEYWORD', 'S', 'X', 'Y', 'L', 'LRAD', 'ANGLE',
                   'BETX', 'BETY', 'ALFX', 'ALFY', 'DX', 'DY', 'MUX', 'MUY',
                   'R11', 'R12', 'R21', 'R22'] + get_k_strings()


def main(xing: dict, optics: str, outputdir: Path = OUTDIR, beam: int = None):
    make_dirs(outputdir)
    madx = Madx(**cpymad_logging_setup(level=LOG_LEVEL,  # sets also standard loggers
                                       command_log=outputdir/f'madx_commands.b{beam}.log',
                                       full_log=outputdir/f'full_output.b{beam}.log'))
    mvars = madx.globals    # shorthand

    # user definitions
    tune_x = 62.31
    tune_y = 60.32
    dispersion = 3

    energy = 7000            # beam energy in GeV
    emittance_norm = 2.5e-6  # normalized emittance
    n_particles = 1.0e10     # number of particles in beam
    bunch_len = 0.0755       # bunch length [m] in collision

    emittance = emittance_norm * mvars.pmass / energy
    rel_energy_spread = 4.5e-4*(450./energy)**0.5

    # define some global madx variables
    mvars.on_disp = 0       # correction of spurious dispersion
    mvars.nrj = energy      # used in some macros and below in beam()
    mvars.mylhcbeam = beam

    # Output helper
    def output_path(type_, output_id, dir_=outputdir):
        return dir_ / f'{type_}.hllhc.b{beam:d}.{output_id}.tfs'

    def output_twiss(output_id, index_regex=r"BPM|M|IP", dir_=outputdir, pre_match_tune=True):
        if pre_match_tune:
            match_tune(madx,
                       accel="HLLHC", sequence=seq_name,
                       qx=tune_x, qy=tune_y,
                       dqx=dispersion, dqy=dispersion)
        madx.twiss(sequence=seq_name)
        tfs_df = get_tfs(madx.table.twiss, columns=DEFAULT_COLUMNS, index_regex=index_regex)
        if dir_ is not None:
            tfs.write(output_path('twiss', output_id, dir_=dir_), drop_allzero_columns(tfs_df), save_index="NAME")
        return tfs_df

    def output_survey(output_id, index_regex=r"BPM|M|IP", dir_=outputdir):
        madx.survey(sequence=seq_name)
        df_survey = get_tfs(madx.table.survey, index_regex=index_regex, remove_element_numbering=False)
        if dir_ is not None:
            tfs.write(output_path('survey', output_id, dir_=dir_), drop_allzero_columns(df_survey), save_index="NAME")
        return df_survey

    # Load Macros
    madx.call(pathstr("slhc", "toolkit", "macro.madx"))

    if beam == 4:
        madx.call(pathstr('lhc', 'lhcb4.seq'))  # LHC sequence
    else:
        madx.call(pathstr('lhc', 'lhc.seq'))  # LHC sequence

    madx.call(pathstr('slhc', 'hllhc_sequence.madx'))  # updates to HL-LHC

    if optics.endswith("thin"):
        madx.exec('myslice')

    # Lattice Setup ---------------------------------------
    # Define Optics and make beam
    madx.call(get_optics_path(optics))
    if optics == 'inj':
        mvars.NRJ = 450.000

    # Setup Orbit
    orbit_vars = orbit_setup(madx, accel='hllhc', **xing)

    beams = (1, 2) if beam is None else (beam, )
    for beam in beams:
        seq_name, seq_file, bv_flag = get_lhc_sequence_filename_and_bv(beam)

        madx.beam(sequence=seq_name, bv=bv_flag,
                  energy="NRJ", sige=rel_energy_spread,
                  particle="proton", npart=n_particles,
                  ex=emittance, ey=emittance,
                  kbunch=1, sigt=bunch_len,
                  )

        madx.use(sequence=seq_name)

        # Save Nominal
        df_nominal = output_twiss('nominal')
        df_nominal_srv = output_survey('nominal')
        log_orbit(madx, accel='hllhc')

    madx.exit()
