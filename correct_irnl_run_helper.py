import re
from collections import Sequence, Iterable, defaultdict
from pathlib import Path


def correctstr2dict(correct, only_one=False, no_star=False):
    if correct is None or correct == "None":
        return None

    rdt_map = {
        'a3': ['F0003', 'F0003*'],  # correct a3 errors with F0003
        'b3': ['F1002', 'F1002*'],  # correct b3 errors with F1002
        'a4': ['F3001', 'F1003'],  # correct a4 errors with F1003 and F3001
        'b4': ['F4000', 'F0004'],  # correct b4 errors with F0004 and F4000
        'b22': ['F2020', ],     # correct b4 errors with F2020
        'a5': ['F0005', 'F0005*'],  # correct a5 errors with F0005
        'b5': ['F5000', 'F5000*'],  # correct b5 errors with F5000
        'b14': ['F1004', 'F1004*'],  # correct b5 errors with F1004
        'b32': ['F3002', 'F3002*'],  # correct b5 errors with F3002
        'a6': ['F5001', 'F1005'],  # correct a6 errors with F5001 and F1005
        'b6': ['F0006', 'F6000'],  # correct b6 errors with F6000 and F0006
    }
    correct_dict = dict()
    for pair in correct.split("_"):
        if "-by-" in pair:
            raise NameError("Old Naming scheme not supported anymore.")

        rdt_type, correctors = pair.split("-")

        for rdt in rdt_map[rdt_type]:
            if no_star and rdt.endswith("*"):
                continue
            correct_dict[rdt] = re.findall(r'[a-z]\d{1,2}', correctors)
            if only_one:
                break
    return correct_dict


def get_other_beam(beam):
    return 1 if beam == 4 else 4


def get_beams_output_path(**kwargs) -> Path:
    output: Path = kwargs['output']
    seed: int = kwargs['seed']
    jobid = get_beams_jobid(**kwargs)
    return output / f"Job.{jobid}" / f'Seed.{seed:d}'


def get_beams_job_id_mask(keys: Iterable):
    return get_beams_jobid(**{key.lower(): f'%({key.upper()})s' for key in keys})


def get_beams_jobid(**kwargs):
    machine: str = kwargs['machine']
    beam: int = kwargs['beam']
    optics: str = kwargs['optics']
    xing: str = kwargs['xing']
    errors: str = kwargs['errors']
    rdts: str = kwargs['rdts']
    rdts2: str = kwargs['rdts2']
    correct_beam: str = kwargs['correct_beam']

    feeddown: int = kwargs.get('feeddown')
    iterations: int = kwargs.get('iterations')

    correct_b5: bool = kwargs.get('correct_b5')
    d1b5_value: int = kwargs.get('d1b5_value')
    d2b5_value: int = kwargs.get('d2b5_value')
    corrector_strength: bool = kwargs.get('corrector_strength')

    if d2b5_value is None:
        # beams setup
        return f"{machine}.b{beam}.by_{correct_beam}.optics_{optics}.xing_{xing}.errors_{errors}.fd_{feeddown}.iter_{iterations}.rdts_{rdts}.rdts_{rdts2}"

    if correct_b5 is None:
        # first b5 sweep setup
        return f"{machine}.b{beam}.by_{correct_beam}.optics_{optics}.xing_{xing}.errors_{errors}.b5val_{d2b5_value}.corr_{corrector_strength}.rdts_{rdts}.rdts_{rdts2}"

    return f"{machine}.b{beam}.by_{correct_beam}.opt_{optics}.xing_{xing}.err_{errors}.fd_{feeddown}.iter_{iterations}.corrb5_{correct_b5}.d1_{d1b5_value}.d2_{d2b5_value}.{corrector_strength}.rdts_{rdts}.rdts_{rdts2}"


def get_errors(error_values):
    if error_values == 'all':
        return {f"AB{i}": True for i in range(1, 16)}
    elif '-' in error_values:
        ab = re.match(r'[aAbB]+', error_values).group(0)
        num = re.search(r'[\d-]+', error_values).group(0).split("-")
        return{f"{ab}{i}": True for i in range(int(num[0]), int(num[1])+1)}
    return {k: True for k in re.findall(r"[a-zA-Z]+\d+", error_values)}











































































































































































































































