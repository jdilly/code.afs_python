from pathlib import Path
from typing import Iterable

import matplotlib as mpl
import matplotlib.pyplot as plt
from matplotlib.figure import Figure
import numpy as np
import tfs
from omc3.plotting.utils import style as pstyle, annotations as pannot, colors as pcolors

LEGEND_BBOX = (1.02, 1)
# LEGEND_BBOX = None


# Axes Functions ---------------------------------------------------------------
def get_first_value(dict_):
    """ Return first value in dict """
    return next(iter(dict_.values()))


def axes_magnets(ax, data):
    ax = ax[0]
    ax.yaxis.set_visible(False)
    ax.xaxis.set_visible(False)
    ax.spines['top'].set_visible(False)
    ax.spines['right'].set_visible(False)
    ax.spines['bottom'].set_visible(False)
    ax.spines['left'].set_visible(False)
    plot_magnets(ax, get_first_value(data))
    ax.axhline(y=0, color='black')
    ax.axvline(x=0, linestyle='--', color='black', label='IP')
    ax.set_ylim([-1.4, 1.4])
    if LEGEND_BBOX is None:
        ax.legend(fontsize=0.8*mpl.rcParams['legend.fontsize'])
    else:
        ax.legend(loc="lower left", bbox_to_anchor=(1.02, 0), fontsize=0.8*mpl.rcParams['legend.fontsize'])


def axes_beta(axs, data, ylim):
    for ax, (beam, values) in zip(axs, data.items()):
        plot_ip_line(ax)
        plot_beta(ax, values, beam, 'X', 'red')
        plot_beta(ax, values, beam, 'Y', 'blue')

        ax.set_ylabel(r' $\beta$ [km]')
        ax.tick_params(axis='both', which='major')
        ax.set_ylim(ylim)
        ax.legend(bbox_to_anchor=LEGEND_BBOX)


def axes_beta_alternative(axs, data, ylim):
    for ax, plane in zip(axs, ("X", "Y")):
        ax.axhline(y=0, color='black')  # lw=mpl.rcParams['lines.linewidth']*.5)
        plot_ip_line(ax)
        for (name, values), color in zip(data.items(), ('red', 'blue', 'green')):
            plot_beta(ax, values, name, plane, None)
            # plot_beta(ax, values, name, plane, color)

        ax.set_ylabel(fr' $\beta_{plane}$ [km]')
        ax.tick_params(axis='both', which='major')
        ax.set_ylim(ylim)
        ax.legend(bbox_to_anchor=LEGEND_BBOX)


def axes_orbit(axs, data, ylim):
    for ax, plane in zip(axs, ("X", "Y")):
        ax.axhline(y=0, color='black') # lw=mpl.rcParams['lines.linewidth']*.5)
        plot_ip_line(ax)
        for name, values in data.items():
            plot_orbit(ax, values, name, plane)

        ax.set_ylabel(f'{plane} [mm]')
        ax.tick_params(axis='both', which='major')
        ax.set_ylim(ylim)
        ax.legend(bbox_to_anchor=LEGEND_BBOX)


# Plottings Functions ----------------------------------------------------------

def plot_magnets(ax, data):
    ax.bar(data["S"], np.sign(data['K1L']), data['L'],  color='darkblue', linewidth=3, label='Quadrupole')
    ax.bar(data["S"], np.abs(np.sign(data['ANGLE'])), data['L'], bottom=-0.5,  color='gray', linewidth=3, label='Dipole')
    # ax.axvline( x=data["S"]['MQSX.3R5'], ymin=-1.2, ymax=1.2, linestyle='-', color='limegreen', label='Coupling correctors' )
    # ax.axvline( x=data["S"]['MQSX.3L5'], ymin=-1.2, ymax=1.2, linestyle='-', color='limegreen')


def plot_beta(ax, data, beam, plane, color):
    beta_column = f'BET{plane.upper()}'
    error_column = f"ERRBET{plane}"
    if error_column in data.columns:
        mask = np.isfinite(data[plane])
        ax.errorbar(data.loc[mask, "S"], data.loc[mask, beta_column] / 1000, yerr=data.loc[mask, error_column] / 1000,
                    color=color, linewidth=3, label=f'${beam}_{plane}$', marker='.')
    else:
        ax.plot(data["S"], data[beta_column] / 1000, color=color, linewidth=3, label=f'${beam}_{plane}$')


def plot_orbit(ax, data, beam, plane):
    error_column = f"ERR{plane}"
    if error_column in data.columns:
        mask = np.isfinite(data[plane])
        ax.errorbar(data.loc[mask, "S"], data.loc[mask, plane] * 1000, yerr=data.loc[mask, error_column] * 1000, linewidth=3, label=f'${beam}_{plane}$', marker='.')
    else:
        ax.plot(data["S"], data[plane] * 1000, linewidth=3, label=f'${beam}_{plane}$')


def plot_ip_line(ax, x=0):
    ax.axvline(x=x, linestyle='--', color='black')


# Helper Functions -------------------------------------------------------------

def distance_to_ip(df, ip):
    res_df = df['S'] - df.loc[ip, 'S']
    # print(res_df)
    # res_df['S'][res_df['S'].values > (df.iloc[-1,'S']/2.)] = df['S'] - df.loc[ip ,'S'] - df.iloc[-1,'S']
    return res_df


def filter_data(data, xlim, ip):
    fdata = {}
    for beam in data.keys():
        df = data[beam]
        l = df.LENGTH
        s_diff = distance_to_ip(df, ip)
        s_diff = np.where(s_diff > l/2, s_diff - l, s_diff)
        s_diff = np.where(s_diff < -l/2, s_diff + l, s_diff)
        mask = (s_diff >= (xlim[0]*1.3)) & (s_diff <= (xlim[1]*1.3))
        res_df = df.loc[mask, :].copy()
        res_df["S"] = s_diff[mask]
        fdata[beam] = res_df.sort_values(by="S")
    return fdata


def read_data(data):
    for k, path in data.items():
        if isinstance(path, (Path, str)):
            data[k] = tfs.read(path, index="NAME")
        if isinstance(k, int):
            data[f"B{k}"] = data.pop(k)
    return data


def get_axes_distribution(components):
    hr_str = 'height_ratios'
    axes, naxes, gridspec_kw = {}, 0, {hr_str: []}
    for c in components:
        if c == 'magnets':
            axes[c] = [naxes]
            naxes += 1
            gridspec_kw[hr_str] += [1]
        else:
            axes[c] = [naxes, naxes+1]
            naxes += 2
            gridspec_kw[hr_str] += [2, 2]
    return axes, naxes, gridspec_kw


def set_plot_style(style, manual_style, naxes):
    mstyle = {u"figure.figsize": [10.24, naxes*3],
              u"markers.fillstyle": u'none',
              u"lines.marker": u'',
              u'grid.alpha': 0,
              u'legend.frameon': True,
              u'legend.fancybox': True,
              u'legend.framealpha': 0.8,
              u'legend.loc': u'upper left',
              u'savefig.format': u'pdf'}

    if manual_style is not None:
        mstyle.update(manual_style)

    pstyle.set_style(style, mstyle)

# Main


def main(data: dict, ip: str, out_name: str = None, components: Iterable[str] = ('magnets', 'orbit', 'beta'),
         xlim=(-200, 200), ylim_beta=None, ylim_orbit=None,
         style='standard', manual_style=None) -> Figure:
    axes, naxes, gridspec_kw = get_axes_distribution(components)
    set_plot_style(style, manual_style, naxes)
    fun_map = {
        'magnets': axes_magnets,
        'orbit': lambda *x: axes_orbit(*x, ylim=ylim_orbit),
        'beta': lambda *x: axes_beta(*x, ylim=ylim_beta),
        'beta_alt': lambda *x: axes_beta_alternative(*x, ylim=ylim_beta),
    }

    data = read_data(data)
    data = filter_data(data, xlim, ip)

    fig, axs = plt.subplots(nrows=naxes, ncols=1, sharex=True, gridspec_kw=gridspec_kw)
    for component, indcs in axes.items():
        try:
            fun_map[component](axs[indcs], data)
        except KeyError as e:
            pass  # allow empty axes for unknown components

    for ax in axs:
        ax.set_xlim(xlim)
    axs[-1].set_xlabel(f'Distance from {ip} [m]', fontsize=20)
    fig.tight_layout(h_pad=0.2)

    if out_name is not None:
        pannot.set_name(out_name, fig)
        fig.savefig(f"plot.{fig.canvas.get_default_filename()}")
    return fig
