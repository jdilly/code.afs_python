import logging
import shutil
from pathlib import Path
from typing import Sequence

import tfs
from cpymad.madx import Madx
from optics_functions.rdt import calculate_rdts
from optics_functions.utils import prepare_twiss_dataframe

from cpymad_lhc.general import (get_tfs, switch_magnetic_errors, match_tune, get_k_strings, amplitude_detuning_ptc, sixtrack_output, rdts_ptc)
from cpymad_lhc.ir_orbit import orbit_setup
from cpymad_lhc.logging import cpymad_logging_setup
from irnl_rdt_correction.main import irnl_rdt_correction

LOG = logging.getLogger(__name__)  # setup in main()
LOG_LEVEL = logging.DEBUG

OUTDIR = Path("Outputdata")
TEMPDIR = Path('temp')

RDT_PART_MAP = {
    "A": "ABS",
    "C": "REAL",
    "S": "IMAG",
}

PATHS = {
    "db5": Path("/afs/cern.ch/eng/lhc/optics/V6.503"),
    "slhc": Path("/afs/cern.ch/eng/lhc/optics/SLHCV1.0"),
    "optics2016": Path("/afs/cern.ch/eng/lhc/optics/runII/2016"),
    "optics2018": Path("/afs/cern.ch/eng/lhc/optics/runII/2018"),
    "external": Path("/afs/cern.ch/work/j/jdilly/public/macros"),
}


def pathstr(key: str, *args: str):
    return str(PATHS[key].joinpath(*args))


def get_wise_path(seed: int):
    return f"/afs/cern.ch/work/j/jdilly/wise/WISE-2015-LHCsqueeze-0.4_10.0_0.4_3.0-6.5TeV-emfqcs/WISE.errordef.{seed:04d}.tfs"


def apply_errors(madx: Madx, *magnets: str):
    for magnet in magnets:
        madx.call(pathstr('db5', 'measured_errors', f'Efcomp_{magnet}.madx'))


def make_dirs(*dirs: Path):
    for dir_ in dirs:
        dir_.mkdir(exist_ok=True, parents=True)


def clean_up(tempdir: Path):
    shutil.rmtree(tempdir, ignore_errors=True)


def drop_allzero_columns(df: tfs.TfsDataFrame):
    return df.loc[:, (df != 0).any(axis="index")]


DEFAULT_COLUMNS = ['NAME', 'KEYWORD', 'S', 'X', 'Y', 'L', 'LRAD',
                   'BETX', 'BETY', 'ALFX', 'ALFY', 'DX', 'DY', 'MUX', 'MUY',
                   'R11', 'R12', 'R21', 'R22'] + get_k_strings()


def main(beam: int, rdts: Sequence, feeddown: int, xing: dict, errors: dict, seed: int = 1,
         update_optics: bool = False,
         outputdir: Path = OUTDIR, tempdir: Path = TEMPDIR):
    make_dirs(outputdir, tempdir)
    madx = Madx(**cpymad_logging_setup(level=LOG_LEVEL,  # sets also standard loggers
                                       command_log=outputdir/'madx_commands.log',
                                       full_log=outputdir/'full_output.log'))
    mvars = madx.globals  # shorthand

    # user definitions
    tune_x = 62.31
    tune_y = 60.32
    dispersion = 3
    emittance = 7.29767146889e-09
    n_particles = 1.0e10   # number of particles in beam

    # apply field errors to arcs
    on_arc_errors = False

    # define some global madx variables
    mvars.mylhcbeam = beam  # used in macros

    # Define Sequence to use
    seq_file = "lhc_as-built.seq"
    seq_name = f"lhcb{beam}"
    bv_flag = -1 if beam == 2 else 1
    if beam == 4:
        seq_file = "lhcb4_as-built.seq"
        seq_name = f"lhcb2"

    # Output helper
    def output_path(type_, output_id, dir_=outputdir):
        return dir_ / f'{type_}.lhc.b{beam:d}.{output_id}.tfs'

    def output_twiss(output_id, index_regex=r"BPM|M|IP", dir_=outputdir, pre_match_tune=True):
        if pre_match_tune:
            match_tune(madx,
                       accel="LHC", sequence=seq_name,
                       qx=tune_x, qy=tune_y,
                       dqx=dispersion, dqy=dispersion)
        madx.twiss(sequence=seq_name)
        df_twiss = get_tfs(madx.table.twiss, columns=DEFAULT_COLUMNS, index_regex=index_regex)
        if dir_ is not None:
            tfs.write(output_path('twiss', output_id, dir_=dir_), drop_allzero_columns(df_twiss), save_index="NAME")
        return df_twiss

    def output_errors(output_id, index_regex="M", dir_=outputdir):
        # As far as I can tell `only_selected` does not work with
        # etable and there is always only the selected items in the table
        # (jdilly, cpymad 1.4.1)
        error_columns = ["NAME", "DX", "DY"] + get_k_strings()
        madx.select(flag='error', clear=True)
        madx.select(flag='error', column=error_columns)
        madx.etable(table='error')
        df_errors = get_tfs(madx.table.error, index_regex=index_regex, columns=error_columns)
        if dir_ is not None:
            tfs.write(output_path('errors', output_id, dir_=dir_), drop_allzero_columns(df_errors), save_index="NAME")
        return df_errors

    def output_analytical_rdts(output_id, index_regex="BPM|M|IP", dir_=outputdir):
        df_twiss = get_tfs(madx.table.twiss, index_regex=index_regex)
        df_errors = get_tfs(madx.table.error, index_regex=index_regex)
        df_combined = prepare_twiss_dataframe(df_twiss, df_errors, invert_signs_madx=(beam == 4))
        df_rdts_ana = calculate_rdts(df_combined, rdts=rdts, qx=tune_x, qy=tune_y,
                                     complex_columns=False, loop_phases=True, feeddown=2)
        if dir_ is not None:
            tfs.write(output_path('twiss_rdt', output_id, dir_=dir_), df_rdts_ana, save_index="NAME")
        return df_rdts_ana

    def output_ptc_rdts(output_id, index_regex="BPM|M|IP", dir_=outputdir):
        rdts_ptc(madx, order=max(sum(int(i) for i in s[1:5]) for s in rdts))
        df_rdts = get_tfs(madx.table.twissrdt, index_regex=index_regex)
        df_rdts = df_rdts.drop(columns=df_rdts.columns[~df_rdts.columns.str.match(r".*_0_0$")])  # not interested dispersion
        df_rdts = df_rdts.drop(columns=df_rdts.columns[df_rdts.columns.str.match("GNFA")])  # IMAG and REAL is enough
        df_rdts = df_rdts.rename(columns=lambda x: f"F{x[5]}{x[7]}{x[9]}{x[11]}{RDT_PART_MAP[x[3]]}")
        if dir_ is not None:
            tfs.write(output_path('ptc_rdt', output_id, dir_=dir_), df_rdts, save_index="NAME")
        return df_rdts

    def output_ampdet(output_id):
        return amplitude_detuning_ptc(madx, order=2, file=output_path('ampdet', output_id))

    # Load Macros
    madx.call(pathstr("optics2018", "toolkit", "macro.madx"))

    # Lattice Setup ---------------------------------------
    # Load Sequence
    madx.call(pathstr("optics2018", seq_file))

    # Slice Sequence
    mvars.slicefactor = 4
    madx.beam()
    madx.call(pathstr("optics2018", "toolkit", "myslice.madx"))
    madx.beam()
    madx.use(sequence=seq_name)
    madx.makethin(sequence=seq_name, style="teapot", makedipedge=True)

    # Cycling w.r.t. to IP3 (mandatory to find closed orbit in collision in the presence of errors)
    madx.seqedit(sequence=seq_name)
    madx.flatten()
    madx.cycle(start="IP3")
    madx.endedit()

    # Define Optics and make beam
    madx.call(pathstr("optics2018", "PROTON", "opticsfile.1"))  # Basic optics setup (injection optics)
    madx.call(pathstr("optics2018", "PROTON", "opticsfile.22_ctpps2"))  # Redefine Optics to Round 30cm collision optics

    madx.beam(sequence=seq_name, bv=bv_flag,
              energy="NRJ", particle="proton", npart=n_particles,
              kbunch=1, ex=emittance, ey=emittance)

    # Setup Orbit
    orbit_vars = orbit_setup(madx, **xing)

    madx.use(sequence=seq_name)

    # Save Nominal
    df_nominal = output_twiss('nominal')
    df_ampdet_nominal = output_ampdet('nominal')

    # Save nominal optics in IR+Correctors for ir nl correction
    df_twiss_nominal_ir = output_twiss('optics_ir', index_regex="M(QS?X|BX|BRC|C[SOT]S?X)", pre_match_tune=False)

    # Call error subroutines and measured error table for nominal LHC
    madx.call(file=pathstr('optics2016', 'measured_errors', 'Msubroutines.madx'))
    madx.readtable(file=pathstr('optics2016', 'measured_errors', 'rotations_Q2_integral.tab'))
    madx.call(file=pathstr('optics2016', 'errors', 'macro_error.madx'))
    madx.call(file=pathstr('optics2016', 'toolkit', 'Orbit_Routines.madx'))
    madx.call(file=pathstr('optics2016', 'measured_errors', 'Msubroutines_new.madx'))  # think the new subroutines are only relevant for MSS - not used pre-2017 so shouldn't make a difference compared to old Msubroutines...
    madx.call(file=pathstr('optics2016', 'measured_errors', 'Msubroutines_MS_MSS_MO_new.madx'))
    madx.call(file=pathstr('optics2016', 'toolkit', 'Orbit_Routines.madx'))
    madx.call(file=pathstr('optics2016', 'toolkit', 'SelectLHCMonCor.madx'))
    madx.readtable(file=pathstr('optics2016', 'measured_errors', 'rotations_Q2_integral.tab'))
    madx.call(file=pathstr('optics2016', 'errors', 'macro_error.madx'))  # some macros for error generation

    # Apply magnetic errors -------------------------------
    switch_magnetic_errors(madx, **errors)

    # Read WISE
    madx.readtable(file=get_wise_path(seed))

    # Apply errors to elements ---
    if on_arc_errors:
        apply_errors(madx, 'MB', 'MQ')

    apply_errors(madx,
                 # IR Dipoles
                 'MBXW',  # D1 in IP1 and IP5
                 'MBRC',  # D2
                 'MBX',  # D in IP2 and 8
                 'MBRB',  # IP4
                 'MBRS',  # IP4
                 'MBW',  # IP7 and IP3
                 # IR Quads
                 'MQX',
                 'MQY',
                 'MQM',
                 'MQMC',
                 'MQML',
                 'MQTL',
                 'MQW',
                 )

    # Save uncorrected
    df_uncorrected = output_twiss('uncorrected')
    df_ampdet_uncorrected = output_ampdet('uncorrected')
    df_errors = output_errors('all')

    # Save errors to table to be used for correction ---------------------------
    # As far as I can tell `only_selected` does not work with
    # etable and there is always only the selected items in the table
    # (jdilly, cpymad 1.4.1)
    df_errors_ir = output_errors('ir', index_regex=r"M([QB]X|BRC)")

    # IR Nonlinear Correction: Calculate corrector powering --------------------
    if rdts is not None:
        correction_cmd, correction_df = irnl_correct(
            # Correction setup for LHC ---
            beams=(beam,),
            optics=(df_twiss_nominal_ir,),
            errors=(df_errors_ir,),
            rdts=rdts,
            output=output_path('settings', 'mcx'),
            accel='lhc',
            feeddown=feeddown,  # order of feeddown to take into account
            # The following are the defaults ---
            ips=(1, 2, 5, 8),  # in which IPs to correct
            solver='lstsq',  # 'inv', 'linear' Solver to use
            update_optics=update_optics,  # updates optics after setting corrector strength (for feeddown)
            ignore_missing_columns=False,  # True: if columns are missing, assume 0
        )

        # Apply correction
        madx.input(correction_cmd)

        # Save corrected
        df_corrected = output_twiss('corrected')
        df_ampdet_corrected = output_ampdet('corrected')

    sixtrack_output(madx, mvars.nrj)

    madx.exit()
    clean_up(tempdir)
