import logging
import shutil
from contextlib import contextmanager, suppress
from pathlib import Path
from typing import Sequence

import tfs
from cpymad.madx import Madx
from optics_functions.coupling import coupling_via_cmatrix, closest_tune_approach
from optics_functions.rdt import calculate_rdts
from optics_functions.utils import prepare_twiss_dataframe

from cpymad_lhc.corrector_limits import check_corrector_limits
from cpymad_lhc.coupling_correction import correct_coupling
from cpymad_lhc.general import (get_tfs, switch_magnetic_errors, match_tune, get_k_strings, amplitude_detuning_ptc, sixtrack_output, rdts_ptc,
                                get_lhc_sequence_filename_and_bv, get_kqs_for_coupling_correction)
from cpymad_lhc.ir_orbit import orbit_setup, log_orbit
from cpymad_lhc.logging import cpymad_logging_setup
from pylhc.irnl_rdt_correction import main as irnl_correct, write_tfs, write_command, log_setup

LOG = logging.getLogger(__name__)  # setup in main()
LOG_LEVEL = logging.DEBUG

OUTDIR = Path("Outputdata")
TEMPDIR = Path('temp')

RDT_PART_MAP = {
    "A": "ABS",
    "C": "REAL",
    "S": "IMAG",
}

PATHS = {
    "db5": Path("/afs/cern.ch/eng/lhc/optics/V6.503"),
    "lhc": Path("/afs/cern.ch/eng/lhc/optics/runIII"),
    "slhc": Path("/afs/cern.ch/eng/lhc/optics/HLLHCV1.3"),
    "external": Path("/afs/cern.ch/work/j/jdilly/public/macros"),
}


def pathstr(key: str, *args: str):
    return str(PATHS[key].joinpath(*args))


def get_optics_path(optics: str):
    optics_map = {
        'inj': pathstr("slhc", "opt_inj_thin.madx"),
        # HLLHC v1.3
        'flat07530': pathstr("slhc", 'squeeze_flatvh', 'opt_075_300_300_075_thin.madx'),
        'round3030': pathstr('slhc', 'squeeze2', 'opt_300_300_300_300_thin.madx'),
        'round1515': pathstr('slhc', 'squeeze2', 'opt_150_150_150_150_thin.madx'),
        # # HLLHC v1.4
        # 'flat07530': pathstr("slhc", 'flat', 'opt_flatvh_75_300_thin.madx'),
        # 'round1515': pathstr('slhc', 'squeeze2', 'opt_round_150_1500_thin.madx'),
    }
    return optics_map[optics]


def get_wise_path(seed: int):
    return f"/afs/cern.ch/work/j/jdilly/wise/WISE-2015-LHCsqueeze-0.4_10.0_0.4_3.0-6.5TeV-emfqcs/WISE.errordef.{seed:04d}.tfs"


def make_dirs(*dirs: Path):
    for dir_ in dirs:
        dir_.mkdir(exist_ok=True, parents=True)


def drop_allzero_columns(df: tfs.TfsDataFrame):
    return df.loc[:, (df != 0).any(axis="index")]


def get_other_beam(beam: int):
    return 1 if beam == 4 else 4


def define_coupling_knobs(madx: Madx, beam: int):
    # if beam == 1:
    #     mvars["kqs.a23b1"] = "coupling.a23"
    #     mvars["kqs.r3b1"] = "coupling.a34"
    #     mvars["kqs.l4b1"] = "coupling.a34"
    #     mvars["kqs.a67b1"] = "coupling.a67"
    #     mvars["kqs.r7b1"] = "coupling.a78"
    #     mvars["kqs.l8b1"] = "coupling.a78"
    # else:
    #     mvars["kqs.r2b2"] = "coupling.a23"
    #     mvars["kqs.l3b2"] = "coupling.a23"
    #     mvars["kqs.a34b2"] = "coupling.a34"
    #     mvars["kqs.r6b2"] = "coupling.a67"
    #     mvars["kqs.l7b2"] = "coupling.a67"
    #     mvars["kqs.a78b2"] = "coupling.a78"
    # coupling_knobs = ["coupling.a23", "coupling.a34", "coupling.a67", "coupling.a78"]

    # LHC coupling knobs work equally well with HL-LHC coupling correction
    if beam == 1:
        madx.input("""
            CMRSKEW = 0;
            CMISKEW = 0;
            CMRS.b1 := CMRSKEW;
            CMIS.b1 := CMISKEW;
             KQS.R1B1   :=  ( 0.266278479040E-01) * CMRS.b1 + (-0.899733288016E-02) * CMIS.b1;
             KQS.L2B1   :=  ( 0.266278479040E-01) * CMRS.b1 + (-0.899733288016E-02) * CMIS.b1;
            KQS.A23B1   :=  ( 0.142516736842E-01) * CMRS.b1 + ( 0.848602983914E-02) * CMIS.b1;
             KQS.R3B1   :=  (-0.171205193764E-01) * CMRS.b1 + (-0.807870546221E-02) * CMIS.b1;
             KQS.L4B1   :=  (-0.171205193764E-01) * CMRS.b1 + (-0.807870546221E-02) * CMIS.b1;
            KQS.A45B1   :=  ( 0.113812285983E-01) * CMRS.b1 + ( 0.955159460427E-02) * CMIS.b1;
             KQS.R5B1   :=  ( 0.792323136002E-02) * CMRS.b1 + ( 0.100926247998E-01) * CMIS.b1;
             KQS.L6B1   :=  ( 0.792323136002E-02) * CMRS.b1 + ( 0.100926247998E-01) * CMIS.b1;
            KQS.A67B1   :=  (-0.158692136780E-01) * CMRS.b1 + ( 0.106460324212E-01) * CMIS.b1;
             KQS.R7B1   :=  (-0.739140462540E-02) * CMRS.b1 + (-0.987710657697E-02) * CMIS.b1;
             KQS.L8B1   :=  (-0.739140462540E-02) * CMRS.b1 + (-0.987710657697E-02) * CMIS.b1;
            KQS.A81B1   :=  ( 0.241324775639E-01) * CMRS.b1 + (-0.962582146500E-02) * CMIS.b1;
            """)
    else:
        madx.input("""
            CMRSKEW = 0;
            CMISKEW = 0;
            CMRS.b2 := CMRSKEW;
            CMIS.b2 := CMISKEW;
            KQS.A12B2   :=  ( 0.124458484817E-01) * CMRS.b2 + (-0.207596749726E-01) * CMIS.b2;
             KQS.R2B2   :=  ( 0.121456161967E-01) * CMRS.b2 + ( 0.397509215137E-02) * CMIS.b2;
             KQS.L3B2   :=  ( 0.121456161967E-01) * CMRS.b2 + ( 0.397509215137E-02) * CMIS.b2;
            KQS.A34B2   :=  (-0.179258964749E-01) * CMRS.b2 + ( 0.371667140033E-02) * CMIS.b2;
             KQS.R4B2   :=  ( 0.128662566995E-01) * CMRS.b2 + ( 0.360504223421E-02) * CMIS.b2;
             KQS.L5B2   :=  ( 0.128662566995E-01) * CMRS.b2 + ( 0.360504223421E-02) * CMIS.b2;
            KQS.A56B2   :=  ( 0.148124920807E-01) * CMRS.b2 + ( 0.138602241563E-02) * CMIS.b2;
             KQS.R6B2   :=  (-0.614988295119E-02) * CMRS.b2 + ( 0.179020058982E-01) * CMIS.b2;
             KQS.L7B2   :=  (-0.614988295119E-02) * CMRS.b2 + ( 0.179020058982E-01) * CMIS.b2;
            KQS.A78B2   :=  (-0.117937801373E-01) * CMRS.b2 + (-0.447830183312E-02) * CMIS.b2;
             KQS.R8B2   :=  ( 0.144218507771E-01) * CMRS.b2 + (-0.211979276194E-01) * CMIS.b2;
             KQS.L1B2   :=  ( 0.144218507771E-01) * CMRS.b2 + (-0.211979276194E-01) * CMIS.b2;
            """)


@contextmanager
def temp_disable_errors(madx, *args):
    """ Disable all global variable args and restore their value afterwards."""
    saved = [madx.globals[arg] for arg in args]
    for arg in args:
        madx.globals[arg] = 0
    yield
    for arg, val in zip(args, saved):
        madx.globals[arg] = val


DEFAULT_COLUMNS = ['NAME', 'KEYWORD', 'S', 'X', 'Y', 'L', 'LRAD',
                   'BETX', 'BETY', 'ALFX', 'ALFY', 'DX', 'DY', 'MUX', 'MUY',
                   'R11', 'R12', 'R21', 'R22'] + get_k_strings()


def main(correct_beam: str,
         rdts: Sequence, xing: dict, errors: dict, optics: str,
         feeddown: int,
         iterations: int,
         outputdirs: dict,
         rdts2: Sequence = None,
         seed: int = 1,
         ):
    make_dirs(*outputdirs.values())

    # user definitions
    local_seed = 100 + seed  # random seed for errors
    tune_x = 62.31
    tune_y = 60.32
    chroma = 3

    # Switch which MQXF errortables to use:
    # 'True' uses the error tables for MQXFA and MQXFB
    # while 'False' the ones for body and fringe
    use_mqxf_ab_errors = False

    # Toggle correction of the errors in the dipole correctors.
    # This correction is not baseline 2020-07-07 as errors change
    # with crossing angle. Hence, this might be less reproducable
    # in reality, use with care!
    correct_mcbx_errors = False

    # apply field errors to arcs
    on_arc_errors = False

    # apply b2s errors in the MQ in the IRs (if on_b2s is active)
    # this is usually deactivated as these errors are assumed to be well corrected
    on_b2s_mq = False

    # apply alignment errors
    on_alignment_errors = False

    energy = 7000            # beam energy in GeV
    emittance_norm = 2.5e-6  # normalized emittance
    n_particles = 1.0e10     # number of particles in beam
    bunch_len = 0.0755       # bunch length [m] in collision

    rel_energy_spread = 4.5e-4*(450./energy)**0.5

    madx_instances, optics_dfs, errors_dfs, loggers = {}, {}, {}, {}
    for beam, outputdir in outputdirs.items():
        madx = Madx(**cpymad_logging_setup(level=LOG_LEVEL,  # sets also standard loggers
                                           command_log=outputdir/'madx_commands.log',
                                           full_log=outputdir/'full_output.log'))
        mvars = madx.globals    # shorthand

        emittance = emittance_norm * mvars.pmass / energy

        # define some global madx variables
        mvars.mylhcbeam = beam  # used in macros
        mvars.on_disp = 0       # correction of spurious dispersion
        mvars.nrj = energy      # used in some macros and below in beam()

        # Output helper
        def output_path(type_, output_id, dir_=None):
            if dir_ is None:
                dir_ = outputdir
            return dir_ / f'{type_}.hllhc.b{beam:d}.{output_id}.tfs'

        def get_twiss(output_id=None, index_regex=r"BPM|M|IP", dir_=None, pre_match_tune=True):
            if dir_ is None:
                dir_ = outputdir
            if pre_match_tune:
                match_tune(madx,
                           accel="HLLHC", sequence=seq_name,
                           qx=tune_x, qy=tune_y,
                           dqx=chroma, dqy=chroma)
            madx.twiss(sequence=seq_name)
            tfs_df = get_tfs(madx.table.twiss, columns=DEFAULT_COLUMNS, index_regex=index_regex)
            if output_id is not None:
                tfs.write(output_path('twiss', output_id, dir_=dir_), drop_allzero_columns(tfs_df), save_index="NAME")
            return tfs_df

        def get_errors(output_id=None, index_regex="M", dir_=None):
            if dir_ is None:
                dir_ = outputdir
            # As far as I can tell `only_selected` does not work with
            # etable and there is always only the selected items in the table
            # (jdilly, cpymad 1.4.1)
            error_columns = ["NAME", "DX", "DY"] + get_k_strings()
            madx.select(flag='error', clear=True)
            madx.select(flag='error', column=error_columns)
            madx.etable(table='error')
            df_errors = get_tfs(madx.table.error, index_regex=index_regex, columns=error_columns)
            if output_id is not None:
                tfs.write(output_path('errors', output_id, dir_=dir_), drop_allzero_columns(df_errors), save_index="NAME")
            return df_errors

        def output_ampdet(output_id):
            return amplitude_detuning_ptc(madx, order=2, file=output_path('ampdet', output_id))

        # Load Macros
        madx.call(pathstr("slhc", "toolkit", "macro.madx"))

        # Lattice Setup ---------------------------------------
        # Load Sequence
        seq_name, seq_file, bv_flag = get_lhc_sequence_filename_and_bv(beam, 'hllhc')

        madx.call(pathstr('lhc', seq_file))  # LHC sequence
        madx.call(pathstr('slhc', 'hllhc_sequence.madx'))  # updates to HL-LHC

        # slice sequence
        madx.exec('myslice')

        madx.call(pathstr('slhc', 'errors', 'install_mqxf_fringenl.madx'))  # adding fringe place holder

        # HL-LHC V1.3
        madx.call(pathstr('slhc', 'errors', 'install_MCBXAB_errors.madx'))  # adding D1 corrector placeholders in IR1/5 (for errors)

        # HL-LLHC V1.4
        # madx.call(pathstr('slhc', 'errors', 'install_MCBXFAB_errors.madx'))  # adding D1 corrector placeholders in IR1/5 (for errors)
        # madx.call(pathstr('slhc', 'errors', 'install_MCBRD_errors.madx'))   # adding D2 corrector placeholders in IR1/5 (for errors)

        # Cycling w.r.t. to IP3 (mandatory to find closed orbit in collision in the presence of errors)
        madx.seqedit(sequence=seq_name)
        madx.flatten()
        madx.cycle(start="IP3")
        madx.endedit()

        # Define Optics and make beam
        madx.call(get_optics_path(optics))
        if optics == 'inj':
            mvars.NRJ = 450.000

        madx.beam(sequence=seq_name, bv=bv_flag,
                  energy="NRJ", sige=rel_energy_spread,
                  particle="proton", npart=n_particles,
                  ex=emittance, ey=emittance,
                  kbunch=1, sigt=bunch_len,
                  )

        # Setup Orbit
        orbit_vars = orbit_setup(madx, **xing)

        madx.use(sequence=seq_name)

        # Save Nominal
        df_nominal = get_twiss('nominal')
        df_ampdet_nominal = output_ampdet('nominal')
        log_orbit(madx)

        # Save nominal optics in IR+Correctors for ir nl correction
        select_pattern = r"M(QS?X|BX|BRC|C[SODT]S?X)"
        if correct_mcbx_errors:
            select_pattern += "|MCBXF"

        df_twiss_ir = get_twiss('optics_ir', index_regex=select_pattern, pre_match_tune=False)

        # Align separation magnets
        madx.call(pathstr('slhc', 'toolkit', 'align_sepdip.madx'))
        madx.exec('align_mbx15')  # HL-LHC D1
        madx.exec('align_mbrd15')  # HL-LHC D2 in IR15
        madx.exec('align_mbx28')  # V6.503 D1 in IR28
        madx.exec('align_mbrc28')  # V6.503 D2 in IR28
        madx.exec('align_mbrs')   # V6.503 D3 in IR4
        madx.exec('align_mbrb')   # V6.503 D4 in IR4
        madx.call(pathstr('slhc', 'toolkit', 'align_mbh.madx'))  # align 11T dipoles

        # Error subroutines --------------------------------------------------------
        # Error routine and measured error table for nominal LHC

        madx.call(pathstr('db5', 'measured_errors', 'Msubroutines_new.madx'))
        madx.call(pathstr('db5', 'measured_errors', 'Msubroutines_MS_MSS_MO_new.madx'))
        madx.call(pathstr('db5', 'toolkit', 'Orbit_Routines.madx'))
        madx.call(pathstr('slhc', 'errors', 'SelectLHCMonCor.madx'))
        madx.readtable(file=pathstr('db5', 'measured_errors', 'rotations_Q2_integral.tab'))

        # Error routine and error table for new IT/D1/D2/Q4/Q5
        madx.call(pathstr('slhc', 'errors', 'macro_error.madx'))  # macros for error generation in the new IT/D1's
        if use_mqxf_ab_errors:
            LOG.debug("Using MQXF AB error-tables")
            madx.call(pathstr('slhc', 'errors', 'ITa_errortable_v4'))  # target error table for the new IT
            madx.call(pathstr('slhc', 'errors', 'ITb_errortable_v5'))  # target error table for the new IT
        else:
            LOG.debug("Using MQXF body and fringe  error-tables")
            madx.call(pathstr('slhc', 'errors', 'ITbody_errortable_v5'))  # target error table for the new IT
            madx.call(pathstr('slhc', 'errors', 'ITnc_errortable_v5'))  # target error table for the new IT
            madx.call(pathstr('slhc', 'errors', 'ITcs_errortable_v5'))  # target error table for the new IT
            mvars.b6M_MQXF_col = -4  # New baseline value, if using version > v5 might already be in

        madx.call(pathstr('slhc', 'errors', 'D1_errortable_v1'))   # target error table for the new D1
        madx.call(pathstr('slhc', 'errors', 'D2_errortable_v5'))  # target error table for the new D2

        # HL-LHC v1.3
        madx.call(pathstr('slhc', 'errors', 'Q4_errortable_v2'))  # target error table for the new Q4 in IR1 and IR5

        # HL-LHC v1.4
        # madx.call(pathstr('slhc', 'errors', 'MCBXFAB_errortable_v1'))
        # madx.call(pathstr('slhc', 'errors', 'MBH_errortable_v3'))
        # madx.call(pathstr('slhc', 'errors', 'MCBRD_errortable_v1'))

        # Apply magnetic errors ----------------------------------------------------
        # Shorthand definitions ---
        def apply_measured_errors(*magnets):
            for magnet in magnets:
                madx.call(pathstr('db5', 'measured_errors', f'Efcomp_{magnet}.madx'))

        def apply_hllhc_errors(*magnets):
            for magnet in magnets:
                madx.call(pathstr('slhc', 'errors', f'Efcomp_{magnet}.madx'))

        def set_eseed(add_to_seed):
            madx.eoption(seed=local_seed+add_to_seed)

        # Set which magnetic field error orders to use
        switch_magnetic_errors(madx, **errors)

        # Read WISE
        madx.readtable(file=get_wise_path(seed))

        # Possibly disable b2s in MQ (see above)
        disable = []
        if not on_b2s_mq:
            disable = ['on_b2s']

        # Apply Arc Errors ---
        if on_arc_errors:
            apply_measured_errors('MB')

            # Correct orbit distortion resulting from MB magnets (if present)
            if any(mvars[f'on_{field}'] for field in ('A1s', 'A1r', 'B1s', 'B1r')):
                madx.exec('initial_micado(4)')
                madx.exec('initial_micado(4)')

            with temp_disable_errors(madx, *disable):
                apply_measured_errors('MQ')

        # Other Errors ---
        # Nominal LHC magnets
        # Separation Dipoles
        apply_measured_errors('MBRB', 'MBRC', 'MBRS', 'MBX', 'MBW')

        # IR Quadrupoles
        with temp_disable_errors(madx, *disable):
            apply_measured_errors('MQW', 'MQTL', 'MQMC', 'MQX',
                                  'MQY', 'MQM', 'MQML',)

        if on_alignment_errors:
            madx.call(pathstr('db5', 'measured_errors', 'Set_alignment_errors.madx'))

        # New IT/D1/D2/Q4/Q5
        set_eseed(1)
        if use_mqxf_ab_errors:
            apply_hllhc_errors('MQXFA', 'MQXFB')
        else:
            madx.call(pathstr('external', 'Efcomp_MQXFbody.madx'))  # special version
            apply_hllhc_errors('MQXFA', 'MQXFB', 'MQXFends')

        set_eseed(2)
        apply_hllhc_errors('MBXAB')  # new D1 in IR 1/5

        set_eseed(3)
        apply_hllhc_errors('MBRD')  # new D2 in IR1/5

        set_eseed(4)
        # HL-LHC v1.3
        apply_hllhc_errors('MQYY')  # new Q4 in IR1/5

        # HL-LHC v1.4
        # apply_hllhc_errors('MQY')  # ! old Q4 in IR1/5, but switched places around IP1-5

        # set_eseed(5)
        # apply_hllhc_errors('MCBXFAB')  # new triplet correctors in IR1/5

        # set_eseed(6)
        # with temp_disable_errors(madx, 'on_b2s', 'on_b2r'):  # Assumed to be well corrected? (jdilly)
        #     apply_hllhc_errors('MBH')

        # set_eseed(7)
        # apply_hllhc_errors('MCBRD')

        define_coupling_knobs(madx, beam)

        # Save uncorrected
        if rdts is None:
            df_coupling_corrected_before = coupling_via_cmatrix(get_twiss(pre_match_tune=False))
            closest_tune_approach(df_coupling_corrected_before, qx=tune_x, qy=tune_y)
            correct_coupling(madx, accel="hllhc", sequence=seq_name,
                             qx=tune_x, qy=tune_y, dqx=chroma, dqy=chroma)

        df_uncorrected = get_twiss('uncorrected')
        df_errors = get_errors('all')
        df_ampdet_uncorrected = output_ampdet('uncorrected')

        df_coupling_uncorrected = coupling_via_cmatrix(df_uncorrected)
        closest_tune_approach(df_coupling_uncorrected, qx=tune_x, qy=tune_y)

        # Save errors to table to be used for correction ---------------------------
        df_errors_ir = get_errors('ir', index_regex=r"M([QB]X|BRC)")

        madx_instances[beam] = madx
        optics_dfs[beam] = df_twiss_ir
        errors_dfs[beam] = df_errors_ir
        loggers[beam] = logging.getLogger("").handlers

    # IR Nonlinear Correction: Calculate corrector powering --------------------
    if rdts is not None:
        correction_cmds, correction_dfs = {}, {}
        if rdts2 is not None:
            # correct with both optics
            logging.getLogger("").handlers = [h for handlers in loggers.values() for h in handlers]
            correction_cmds[1], correction_dfs[1] = irnl_correct(
                # Correction setup for LHC ---
                beams=(1, 4),
                optics=list(optics_dfs.values()),
                errors=list(errors_dfs.values()),
                rdts=rdts,
                rdts2=rdts2,
                accel='hllhc',
                feeddown=feeddown,  # order of feeddown to take into account
                iterations=iterations,
                ignore_corrector_settings=True,  # all correctors are assumed unpowered
                # The following are the defaults ---
                ips=(1, 2, 5, 8),  # in which IPs to correct
                solver='lstsq',  # 'inv', 'linear' Solver to use
                ignore_missing_columns=False,  # True: if columns are missing, assume 0
            )
            correction_cmds[4] = correction_cmds[1]
            correction_dfs[4] = correction_dfs[1]
        else:
            # calculate the corrections for both beams
            for beam in outputdirs.keys():
                logging.getLogger("").handlers = loggers[beam]
                correction_cmds[beam], correction_dfs[beam] = irnl_correct(
                    # Correction setup for LHC ---
                    beams=(beam,),
                    optics=(optics_dfs[beam],),
                    errors=(errors_dfs[beam],),
                    rdts=rdts,
                    accel='hllhc',
                    feeddown=feeddown,  # order of feeddown to take into account
                    iterations=iterations,
                    ignore_corrector_settings=True,  # all correctors are assumed unpowered
                    # The following are the defaults ---
                    ips=(1, 2, 5, 8),  # in which IPs to correct
                    solver='lstsq',  # 'inv', 'linear' Solver to use
                    ignore_missing_columns=False,  # True: if columns are missing, assume 0
                )

        # Apply corrections
        for beam, outputdir in outputdirs.items():
            logging.getLogger("").handlers = loggers[beam]
            if correct_beam == "same":
                corrector_beam = beam
            elif correct_beam == "other":
                corrector_beam = get_other_beam(beam)
            else:
                corrector_beam = int(correct_beam[-1])

            correct_name = f'mcx_b{corrector_beam}'
            if rdts2 is not None:
                correct_name += f'b{get_other_beam(corrector_beam)}'

            madx = madx_instances[beam]
            seq_name, seq_file, bv_flag = get_lhc_sequence_filename_and_bv(beam, 'hllhc')
            write_command(output_path('settings', correct_name).with_suffix('.madx'), correction_cmds[corrector_beam])
            write_tfs(output_path('settings', correct_name), correction_dfs[corrector_beam])

            # Apply correction
            madx.input(correction_cmds[corrector_beam])
            with suppress(ValueError):
                check_corrector_limits(madx, accel="HLLHC", beam=beam)

            # Correct Coupling
            df_coupling_corrected_before = coupling_via_cmatrix(get_twiss(pre_match_tune=False))
            closest_tune_approach(df_coupling_corrected_before, qx=tune_x, qy=tune_y)

            correct_coupling(madx, accel="hllhc", sequence=seq_name,
                             qx=tune_x, qy=tune_y, dqx=chroma, dqy=chroma)

            # check needed variables first:
            # madx.call(pathstr('slhc', 'toolkit', 'rematchCOIP.madx'))
            # if madx.globals['on_disp']:
            #     madx.call(pathstr('slhc', 'toolkit', 'rematchCOarc.madx'))

            # Save corrected
            df_corrected = get_twiss('corrected')
            df_ampdet_corrected = output_ampdet('corrected')

            df_coupling_corrected = coupling_via_cmatrix(df_corrected)
            closest_tune_approach(df_coupling_corrected, qx=tune_x, qy=tune_y)
            log_orbit(madx)

    # Write Sixtrack output for both beams -------------------------------------
    for beam in reversed(list(outputdirs.keys())):
        logging.getLogger("").handlers = loggers[beam]
        madx = madx_instances[beam]
        sixtrack_output(madx, madx.globals.nrj)
        # manually move sixtrack output files
        # this is why other_beam needs to run first
        # (no easy way to specify output dir for them)
        for f in Path(".").glob("fc*"):
            if beam != 1:
                shutil.move(f, outputdirs[beam] / f.name)
            else:
                shutil.copy(f, outputdirs[beam] / f.name)
        madx.exit()
