import sys
from pylhc.machine_settings_info import get_info
from omc3.utils import logging_tools

LOG = logging_tools.get_logger(__name__, color=True)

KNOB_DEF = True


def all_knobs():
    return [
        "LHCBEAM/IP1-XING-H-MURAD",
        "LHCBEAM/IP1-XING-V-MURAD",
        # "LHCBEAM/IP2-XING-H-MURAD",  # does not exist
        "LHCBEAM/IP2-XING-V-MURAD",
        "LHCBEAM/IP5-XING-H-MURAD",
        "LHCBEAM/IP5-XING-V-MURAD",
        "LHCBEAM/IP8-XING-H-MURAD",
        # "LHCBEAM/IP8-XING-V-MURAD",  # does not exist

        "LHCBEAM/IP1-SEP-H-MM",
        "LHCBEAM/IP1-SEP-V-MM",
        "LHCBEAM/IP2-SEP-H-MM",
        "LHCBEAM/IP2-SEP-H-MM-RUN2",  # former IP2-SEP-H-MM
        "LHCBEAM/IP2-SEP-V-MM",
        "LHCBEAM/IP5-SEP-H-MM",
        "LHCBEAM/IP5-SEP-V-MM",
        # "LHCBEAM/IP8-SEP-H-MM",  # does not exist
        "LHCBEAM/IP8-SEP-V-MM",

        "LHCBEAM/IP1-OFFSET-H-MM",
        "LHCBEAM/IP1-OFFSET-V-MM",
        # "LHCBEAM/IP2-OFFSET-H-MM",  # does not exist
        "LHCBEAM/IP2-OFFSET-V-MM",
        "LHCBEAM/IP5-OFFSET-H-MM",
        "LHCBEAM/IP5-OFFSET-V-MM",
        "LHCBEAM/IP8-OFFSET-H-MM",
        # "LHCBEAM/IP8-OFFSET-V-MM",  # does not exist
    ]


def md3311_bump():
    # MD 3311 after with bumps (i.e. IP5 crossing)
    get_info(
        time="2018-06-16 20:10:00.000",  # 22:10 local
        knobs=all_knobs(),
        output_dir=f"tst_machine_info_{md3311_bump.__name__}",
        log=True,
        knob_definitions=KNOB_DEF,
    )


def commish_2018_04_28():
    get_info(
        time="2018-04-28 15:00:00.000",
        start_time="2018-04-28 10:00:00.000",
        knobs=all_knobs(),
        output_dir=f"tst_machine_info_{commish_2018_04_28.__name__}",
        log=True,
        knob_definitions=KNOB_DEF,
    )


def md3312():
    # After MD setup
    get_info(
        time="2018-10-30 12:50:00.000",  # 13:50 local time
        knobs=all_knobs(),
        output_dir=f"tst_machine_info_{md3312.__name__}",
        log=True,
        knob_definitions=KNOB_DEF,
    )


def beamtest_halloween():
    get_info(
        time="2021-10-31 10:46:00.000",
        knobs=all_knobs(),
        output_dir=f"tst_machine_info_{beamtest_halloween.__name__}",
        log=True,
        knob_definitions=KNOB_DEF,
    )


def fun_test():
    print("This is a Test.")


if __name__ == '__main__':
    args = sys.argv[1:]
    for arg in args:
        locals()[arg]()
