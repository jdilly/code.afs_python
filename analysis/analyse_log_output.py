"""
Gather log output
------------------

Gathers data from the logging output into dataframes to be plotted later.
The data gathered contains coupling, rdts and orbit.
"""
from pathlib import Path
from typing import Sequence, Iterable, Union

import numpy as np
import matplotlib.patches as mpatches
import pandas as pd
import regex
import tfs
from matplotlib import pyplot as plt
from omc3.utils import logging_tools
from omc3.plotting.utils import style as pstyle, annotations as pannot, colors as pcolors
from matplotlib.collections import PolyCollection

from analysis.analyse_ptc_output import DIFF_ID, RELDIFFABS_ID, TARGETDIFFABS_ID, RESIDUALDIFFABS_ID
from analysis.file_crawler import (
    dir_to_jobid, JOB_ID_COLUMN, SEED_COLUMN, SEED_DIR_MASK, get_working_dirs,
    GATHER_FOLDER, SEEDS, DELTA, get_jobdir_name, CORRECTED_ID, UNCORRECTED_ID
)
from correct_irnl_run_helper import get_beams_jobid

LOG = logging_tools.get_logger(__name__)

LOG_FILE = "full_output.log"

RDT_COLUMN = "RDT"
IP_COLUMN = "IP"
OPTICS_COLUMN = "OPTICS"
ITERATION_COLUMN = "ITERATION"
UNCORRECTED_COLUMN = "UNCORRECTED"
CORRECTED_COLUMN = "CORRECTED"
UNCORRECTED_COUPLING_COLUMN = f"{UNCORRECTED_COLUMN}_ANALYTIC"
CORRECTED_COUPLING_COLUMN = f"{CORRECTED_COLUMN}_ANALYTIC"

X, Y, PX, PY = "X", "Y", "PX", "PY"

N_IPS = 4  # number of IPs in the orbit logs

VALUE_ID, DIFFABS_ID = "value", "diffabs",


INT_COLUMNS = [IP_COLUMN, ITERATION_COLUMN, OPTICS_COLUMN, SEED_COLUMN]
FLOAT_COLUMNS = [UNCORRECTED_COUPLING_COLUMN, CORRECTED_COUPLING_COLUMN, UNCORRECTED_COLUMN, CORRECTED_COLUMN, X, Y, PX, PY]

DATA_COLUMNS = {
    'orbit': [
        JOB_ID_COLUMN, SEED_COLUMN, IP_COLUMN, X, Y, PX, PY
    ],
    'coupling': [
        JOB_ID_COLUMN, SEED_COLUMN,
        UNCORRECTED_COLUMN, CORRECTED_COLUMN,
        UNCORRECTED_COUPLING_COLUMN, CORRECTED_COUPLING_COLUMN
    ],
    'rdt': [
        JOB_ID_COLUMN, SEED_COLUMN, IP_COLUMN,
        RDT_COLUMN, OPTICS_COLUMN, ITERATION_COLUMN,
        UNCORRECTED_COLUMN, CORRECTED_COLUMN,
    ]
}


def gather_log_data(machine, beam, study_dir: Path, seeds: Sequence[int] = SEEDS, override: bool = False):
    LOG.info(f"Gathering Data from output logs for {machine}.b{beam} in {str(study_dir)}.")
    output_dir = study_dir / GATHER_FOLDER
    output_dir.mkdir(exist_ok=True)

    collected = {name: get_df(machine, beam, name, output_dir, override) for name in DATA_COLUMNS.keys()}

    for ndirs, cwd in enumerate(get_working_dirs(study_dir, beam, machine)):
        LOG.info(f"Working dir: {str(cwd.name)} -----------")
        jobid = dir_to_jobid(cwd)
        output_log = None  # reset in cwd for output check

        for seed in seeds:
            input_dir = cwd / SEED_DIR_MASK.format(seed=seed)
            LOG.info(f"  SEED dir: {str(input_dir.name)} ----------")
            output_log = None  # reset after each seed

            for name, df in collected.items():
                if not len(df.query(f'{JOB_ID_COLUMN} == "{jobid}" and {SEED_COLUMN} == {seed}')):
                    if output_log is None:  # skips reading output log if no one needs it
                        output_log = (input_dir / LOG_FILE).read_text()
                    collected[name] = globals()[f'extract_{name}'](df, jobid, seed, output_log)

        if output_log is not None:  # new data was read
            for name, df in collected.items():
                df = prepare_df(df.copy())
                tfs.write(output_dir / get_df_name(machine, beam, name), df)


# IO ---

def get_df_name(machine: str, beam: int, name: str) -> str:
    return f"{name}.{machine}.b{beam}.tfs"


def get_df(machine: str, beam: int, name: str, path: Path, override: bool) -> tfs.TfsDataFrame:
    file_path = path / get_df_name(machine, beam, name)
    if file_path.exists() and not override:
        return tfs.read_tfs(file_path)
    return tfs.TfsDataFrame(columns=DATA_COLUMNS[name])


def prepare_df(df: pd.DataFrame) -> pd.DataFrame:
    df = df.reset_index(drop=True)  # creates new range index, so it's unique

    int_cols = df.columns.intersection(INT_COLUMNS)
    df.loc[:, int_cols] = df[int_cols].astype(np.int)

    float_cols = df.columns.intersection(FLOAT_COLUMNS)
    df.loc[:, float_cols] = df[float_cols].astype(np.float)
    return df


# Extractors ---

def extract_orbit(df: pd.DataFrame, jobid: str, seed: int, log: str) -> pd.DataFrame:
    pattern = fr"\s+INFO \| IP(?P<{IP_COLUMN}>\d) (?P<plane>[XY]):.* (?<xing>[\d.eE+-]+) urad \(p[xy]\).+ (?<offset>[0-9.eE+-]+) mm"
    data = regex.finditer(pattern, log)
    df_data = pd.DataFrame(d.capturesdict() for d in data).applymap(lambda x: x[0])

    # tansform data into two tables containing offset and xing data per IP and plane and
    # merge them back together with new indices
    idx = len(df_data) - (N_IPS * 2)  # find last pairs of IPs
    df_xing = df_data.loc[idx:, :].pivot(index=IP_COLUMN, values='xing', columns='plane').add_prefix("P")
    df_ofst = df_data.loc[idx:, :].pivot(index=IP_COLUMN, values='offset', columns='plane')
    df_new = df_xing.join(df_ofst).reset_index()
    df_new.columns.name = None
    df_new[JOB_ID_COLUMN] = jobid
    df_new[SEED_COLUMN] = seed
    return df.append(df_new)


def extract_coupling(df: pd.DataFrame, jobid: str, seed: int, log: str) -> pd.DataFrame:
    methods = "|".join(["teapot", "calaga", "teapot_franchi", "franchi", "persson", "persson_alt", "hoydalsvik", "hoydalsvik_alt"])
    pattern_calaga = fr"\s+INFO \| \((:?{methods})\) \|C-\| = ([\d.eE+-]+)"
    res_calaga = regex.findall(pattern_calaga, log)[-2:]  # only the last two are needed
    if len(res_calaga) == 1:  # old version rdts None None have only final output
        res_calaga = [np.NAN] + res_calaga

    pattern = r"\s+INFO \| (?:Initial|Final) closest tune approach: ([\d.eE+-]+)"
    res = regex.findall(pattern, log)[-2:]  # only the last two are needed
    if len(res) == 1:  # Coupling correction skipped
        if not float(res[0]) <= 1e-7:
            raise IOError(f"Something wrong with coupling correction in {jobid} Seed.{seed}.")
        res = res * 2
    df_new = tfs.TfsDataFrame(
        [[jobid, seed] + res + res_calaga],
        # same columns as above, but repeated so that the order is correct here
        # even if it changes above
        columns=[
            JOB_ID_COLUMN, SEED_COLUMN,
            UNCORRECTED_COLUMN, CORRECTED_COLUMN,
            UNCORRECTED_COUPLING_COLUMN, CORRECTED_COUPLING_COLUMN
        ])
    return df.append(df_new)


def extract_rdt(df: pd.DataFrame, jobid: str, seed: int, log: str) -> pd.DataFrame:
    pattern = (
        fr"\s+INFO \| RDT change in IP(?P<{IP_COLUMN}>\d), iteration (?P<{ITERATION_COLUMN}>\d+):"
        fr"(?:\s+INFO \| Optics (?P<{OPTICS_COLUMN}>\d+), (?P<{RDT_COLUMN}>F\d+): "
        fr"(?P<{UNCORRECTED_COLUMN}>.+) -> (?P<{CORRECTED_COLUMN}>.+) .+\n)+"
    )

    found = regex.finditer(pattern, log)

    for entry in found:
        df_new = pd.DataFrame().from_dict(entry.capturesdict(), orient="index").transpose()
        df_new = df_new.replace(None)
        df_new[JOB_ID_COLUMN] = jobid
        df_new[SEED_COLUMN] = seed
        df = df.append(df_new)
    return df


# Plotting ------------------------


def plot_rdt(rdt, ip, study_dir, the_id, setups, out_label, scale=0, **kwargs):
    output_dir = study_dir / GATHER_FOLDER

    scale, scale_txt = 10**scale, "" if not scale else f" [$10^{{{-scale}}}$]"

    label = f"f^{{IR{ip}}}_{{{rdt[1:]}}}{scale_txt}"

    fig = plot_log(study_dir, the_id, setups, label, scale, rdt+ip, **kwargs)
    pannot.set_name(f'{rdt}.ip{ip}.{the_id}.{out_label}', fig)
    fig.savefig(output_dir / f"plot.{fig.canvas.get_default_filename()}")
    return fig


def plot_log(study_dir, the_id, setups, label, scale, name, **kwargs):
    # STYLE -------
    manual = {u"figure.figsize": [10.24, 5.12],
              u"markers.fillstyle": u'none', u'grid.alpha': 0,
              u'savefig.format': u'pdf'}

    ncol: np.numeric = kwargs.pop('ncol', 3)
    ylim: np.numeric = kwargs.pop('ylim', None)
    yticks: Iterable[np.numeric] = kwargs.pop('yticks', None)
    xtickrotation: Iterable[np.numeric] = kwargs.pop('xtick.rotation', None)
    plot_styles: Iterable[Union[Path, str]] = kwargs.pop('plot_styles', 'standard')

    manual.update(kwargs)
    pstyle.set_style(plot_styles, manual)

    # DELTA ------
    delta = ''
    njobs = len(setups)
    if the_id in (DIFF_ID, DIFFABS_ID, RELDIFFABS_ID, TARGETDIFFABS_ID, RESIDUALDIFFABS_ID):
        njobs = njobs - sum([(s["rdts"] is None) or (s["rdts"] == "None") for s in setups])
        delta = DELTA
        if the_id in (DIFFABS_ID, TARGETDIFFABS_ID, RESIDUALDIFFABS_ID):
            split = label.split()
            if len(split) == 1:
                split += ['']
            label = f'|{split[0]}|{split[1]}'
        if the_id == RELDIFFABS_ID:
            label = f'$\Delta_{{rel}}${label}'
            scale, scale_txt = 1, ""
        else:
            label = f'$\Delta${label}'

    bar_width = 1/(njobs + 1)
    xlim = [- bar_width/2, (len(columns) - 1) + bar_width * (njobs + 0.5)]

    fig, ax = plt.subplots()

    # plot zero line
    ax.axhline(0, color="black", lw=1, ls="-", marker="", zorder=0)

    # plot separation lines
    for idx in range(1, len(columns)):
        ax.axvline(idx-bar_width/2, ls="--", lw=1, color="black", alpha=0.2, marker="", zorder=-5)

    handles = []
    violin_idx = 0
    for job_idx, job_setup in enumerate(setups):
        job_label = job_setup.pop("label", get_beams_jobid(**job_setup))
        job_dir = study_dir / get_jobdir_name(job_setup)
        job_setup["label"] = job_label

        if job_setup["rdts"] is None or job_setup["rdts"] == "None":
            if the_id in (DIFF_ID, DIFFABS_ID):
                continue

        df_job_seeds = get_data_as_df(name, job_dir, job_setup, the_id)


        color = pcolors.get_mpl_color(job_idx)

        # plot data
        handles.append(mpatches.Patch(color=color, label=job_label))
        for idx, ad_name in enumerate(columns):
            x_pos = idx + bar_width * (violin_idx + 0.5)

            seed_data = df_job_seeds.loc[:, f'{delta}{ad_name}'] * scale
            std = seed_data.std()
            mean = seed_data.mean()
            std_min, std_max = mean-std, mean+std

            violin = ax.violinplot(dataset=seed_data, positions=[x_pos], widths=bar_width/2, showmeans=True, showextrema=True)

            for poly in violin['bodies']:
                poly.set_facecolor(color)
                poly.set_alpha(0.2)

                std_violin = PolyCollection([[v for v in poly.get_paths()[0].vertices if std_min <= v[1] <= std_max]])
                std_violin.set_facecolor(color)
                std_violin.set_alpha(0.4)
                ax.add_collection(std_violin)

            violin['cbars'].set_color(color)
            violin['cmeans'].set_color(color)

            for line_collection in ['cmaxes', 'cmins']:
                violin[line_collection].set_color(color)
                y_pos = violin[line_collection].get_segments()[0][0][1]
                violin[line_collection].set_segments([[[x_pos-bar_width/20, y_pos], [x_pos+bar_width/20, y_pos]]])

        violin_idx += 1

    ax.set_ylabel(label)
    ax.set_xticks(np.arange(len(columns)) + (bar_width*njobs)/2)
    ax.set_xticklabels([LABELS[c] for c in columns], rotation=xtickrotation)
    ax.set_xlim(xlim)

    if yticks is not None:
        ax.set_yticks(yticks)
    ax.set_ylim(ylim)

    # handles, labels = ax.get_legend_handles_labels()
    # order = [1, 0, 2, 4, 3, 5]  # don't ask
    # pannot.make_top_legend(ax,
    #                        handles=_sort_legend(handles, order), labels=_sort_legend(labels, order),
    #                        ncol=2, frame=False)
    pannot.make_top_legend(ax, handles=handles, ncol=ncol, frame=False)

    fig.tight_layout()
    return fig


def get_data_as_df(name, job_dir, job_setup, the_id):
    load_name = name
    if name.startswith('F'):
        load_name = 'rdt'

    df = tfs.read(
        job_dir / GATHER_FOLDER / get_df_name(job_setup["machine"], job_setup["beam"], load_name),
        index=SEED_COLUMN
    )


def _get_rdt_data(df, name, the_id):
    pass



# Main -------------------------


def get_other_beam(beam: int):
    return 1 if beam == 4 else 2


def get_this_beam(beam: int):
    return 1 if beam == 1 else 2

if __name__ == '__main__':
    # gather_all(gather_log_data, study_dir=Path("/eos/home-j/jdilly/work/study.irnl_correction_multi_beams"))
    # gather_log_data(study_dir=Path("/eos/home-j/jdilly/work/study.irnl_correction_multi_beams"))

    STUDY_DIR = Path("/eos/home-j/jdilly/work/study.irnl_correction_multi_beams")
    ERRORS_ALL = "AB3AB4AB5AB6AB7AB8"
    RDTS_ALL_HLLHC = "b6-b6_a6-a6_b5-b5_a5-a5_b4-b4_a4-a4_b3-b3_a3-a3"
    RDTS_ALL_LHC = "b6-b6_b4-b4_a4-a4_b3-b3_a3-a3"
    base_dir = STUDY_DIR
    machine = 'lhc'
    optics = "round3030"
    xing = "top"
    iterations = 1
    for feeddown in (0, 2):
        for beam in (1, 4):
            other_beam = get_other_beam(beam)
            this_beam = get_this_beam(beam)
            setups = [
                dict(machine=machine, beam=beam, correct_beam="same",  xing="top", feeddown=feeddown, iterations=iterations, errors=ERRORS_ALL, optics=optics, rdts=RDTS_ALL_LHC, rdts2="None", label=f"from B{this_beam}"),
                dict(machine=machine, beam=beam, correct_beam="other", xing="top", feeddown=feeddown, iterations=iterations, errors=ERRORS_ALL, optics=optics, rdts=RDTS_ALL_LHC, rdts2="None", label=f"from B{other_beam}"),
                dict(machine=machine, beam=beam, correct_beam="same", xing="top", feeddown=feeddown, iterations=iterations, errors=ERRORS_ALL, optics=optics, rdts=RDTS_ALL_LHC, rdts2=RDTS_ALL_LHC, label="combined"),
            ]
            out_label = f"{machine}.b{beam}.xing_{xing}.optics_{optics}.fd_{feeddown}.errors_all.rdts_all"
            plot_rdt(study_dir=base_dir, the_id=VALUE_ID, setups=setups, out_label=out_label, output_dir=STUDY_DIR, rdts=["F5001", "F1005", "F6000"])
    plt.show()
