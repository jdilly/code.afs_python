import logging
from collections import defaultdict
from collections.abc import Iterable

from matplotlib.ticker import MaxNLocator, MultipleLocator
from pylhc_submitter.sixdesk_tools.post_process_da import _interpolated_line, _interpolated_coords
import numpy as np
from tfs import TfsDataFrame, read_tfs
from typing import Iterable, Union
from pathlib import Path
from matplotlib import pyplot as plt
from matplotlib import rcParams, lines as mlines, patches as mpatches

from omc3.plotting.utils import style as pstyle, annotations as pannot, colors as pcolors
from matplotlib.colors import to_rgba

from pylhc_submitter.constants.autosix import (
    HEADER_NTOTAL, HEADER_INFO, HEADER_HINT,
    MEAN, STD, MIN, MAX, N,
    SEED, ANGLE, ALOST1, ALOST2, AMP
)

from pylhc_submitter.constants import autosix as autosix_const
from correct_irnl_run_helper import get_beams_jobid
from analysis.file_crawler import GATHER_FOLDER, afs_to_eos_path, SEEDS
import logging

LOG = logging.getLogger(__name__)

ALPHA_FILL = 0.02
ALPHA_FILL_STD = 0.25
ALPHA_SEED = 0.2
ALPHA_EXTREM = 0.5


def get_eos_path_if_it_exists(path_function, jobname, basedir):
    if isinstance(basedir, Path):
        path = afs_to_eos_path(path_function(jobname, basedir))
        if not path.exists():
            raise IOError(f"No data at '{path}'")
        return path
    else:
        for bdir in basedir:
            path = afs_to_eos_path(path_function(jobname, bdir))
            if path.exists():
                return path
        else:
            raise IOError(f"No data at any given basedir for job '{jobname}'")


def get_tfs_da_path(jobname, basedir):
    return get_eos_path_if_it_exists(autosix_const.get_tfs_da_path, jobname, basedir)


def get_tfs_da_seeds_stats_path(jobname, basedir):
    return get_eos_path_if_it_exists(autosix_const.get_tfs_da_seed_stats_path, jobname, basedir)


def get_tfs_da_angle_stats_path(jobname, basedir):
    return get_eos_path_if_it_exists(autosix_const.get_tfs_da_angle_stats_path, jobname, basedir)


def plot_jobs(basedir, jobs, labels, out_label, plot_std=False, **kwargs):
    rlim: np.numeric = kwargs.pop('rlim', None)
    color_idx_start: np.numeric = kwargs.pop('color_idx_start', 0)
    angle_ticks: Iterable[np.numeric] = kwargs.pop('angle_ticks', None)
    amplitude_ticks: Iterable[np.numeric] = kwargs.pop('amplitude_ticks', None)
    plot_styles: Iterable[Union[Path, str]] = kwargs.pop('plot_styles', 'standard')

    if "lines.marker" not in kwargs:
        kwargs["lines.marker"] = 'None'
    pstyle.set_style(plot_styles, kwargs)
    fig, ax = plt.subplots(nrows=1, ncols=1, subplot_kw={'projection': 'polar'})
    fig.canvas.manager.set_window_title(f"polar.{out_label}")

    handles = [None] * len(jobs)
    for idx, (jobname, name) in enumerate(zip(jobs, labels)):
        df_angles = read_tfs(get_tfs_da_angle_stats_path(jobname, basedir), index=ANGLE)
        color = pcolors.get_mpl_color(idx+color_idx_start)
        if plot_std:
            handles[idx] = plot_polar(ax, df_angles, ALOST2, color, name,
                                      df_da=None, fill=True,
                                      )
        else:
            df_da = read_tfs(get_tfs_da_path(jobname, basedir))
            handles[idx] = plot_polar(ax, df_angles, ALOST2, color, name,
                                      df_da=df_da, fill=False,
                                      )

    if angle_ticks is not None:
        ax.set_xticks(np.deg2rad(angle_ticks))

    if amplitude_ticks is not None:
        ax.set_yticks(amplitude_ticks)
        # ax.axis["left"](amplitude_ticks)

    ax.set_thetamin(0)
    ax.set_thetamax(90)
    ax.set_rlim([0, rlim])
    ax.set_xlabel(r'$\sigma_{x}~[\sigma_{nominal}]$', labelpad=15)
    ax.set_ylabel(r'$\sigma_{y}~[\sigma_{nominal}]$', labelpad=30)

    ax.tick_params(labelright=True, labelleft=True)

    # handles += [
    #     mlines.Line2D([], [], color='grey', ls='-', lw=2*rcParams["lines.linewidth"]),
    #     mlines.Line2D([], [], color='grey', ls='--', alpha=ALPHA_EXTREM),
    #     mpatches.Patch(color='grey', alpha=ALPHA_FILL_STD),
    # ]
    # labels += ['Mean DA', 'Extrema', 'Standard Dev.']
    ax.legend(
        loc='upper right',
        bbox_to_anchor=(1, .95),
        bbox_transform=fig.transFigure,
        # frameon=False,
        handles=handles,
        labels=labels,
        ncol=1,
    )

    return fig


def plot_polar(ax, df_angles, da_col, color, jobname,
               df_da=None, interpolated=True, fill=False) -> plt.Figure:
    seed_h = []
    seed_l = []
    if df_da is not None:
        # Plot individual Seed Lines
        for seed in sorted(set(df_da[SEED])):
            seed_mask = df_da[SEED] == seed
            angles = np.deg2rad(df_da.loc[seed_mask, ANGLE])
            da_data = df_da.loc[seed_mask, da_col]
            da_data.loc[da_data == 0] = np.NaN
            if interpolated:
                seed_h, _, _ = _interpolated_line(ax, angles, da_data, c=color, ls='-', label=f'_{jobname}_seed{seed:d}', alpha=ALPHA_SEED, zorder=-1)
            else:
                seed_h, = ax.plot(angles, da_data, c=color, ls='-', label=f'_{jobname}_seed{seed:d}', alpha=ALPHA_SEED, zorder=-1)
        seed_h = [seed_h]
        seed_l = ['DA per Seed']

    angles = np.deg2rad(df_angles.index)
    da_min, da_mean, da_max, da_std = (df_angles[f'{name}{da_col}'] for name in (MIN, MEAN, MAX, STD))
    if interpolated:
        _, _, ip_min = _interpolated_line(ax, angles, da_min, c=color, alpha=ALPHA_EXTREM, ls='--', zorder=-1, label=f'Minimum DA {jobname}')
        max_h, ip_x, ip_max = _interpolated_line(ax, angles, da_max, c=color, alpha=ALPHA_EXTREM, ls='--', zorder=-1, label=f'Maximum DA {jobname}')
        if fill:
            ax.fill_between(ip_x, ip_min, ip_max, color=color, alpha=ALPHA_FILL, zorder=-1)
            _, ip_std_min = _interpolated_coords(angles, da_mean-da_std)
            _, ip_std_max = _interpolated_coords(angles, da_mean+da_std)
            ax.fill_between(ip_x, ip_std_min, ip_std_max, color=color, alpha=ALPHA_FILL_STD, edgecolor="None")
        mean_h, _, _ = _interpolated_line(ax, angles, da_mean, c=color, ls='-', lw=2*rcParams["lines.linewidth"], label=f'Mean DA {jobname}')
    else:
        _, = ax.plot(angles, da_min, c=color, alpha=ALPHA_EXTREM, ls='--', zorder=-1, label=f'Minimum DA {jobname}')
        max_h, = ax.plot(angles, da_max, c=color, alpha=ALPHA_EXTREM, ls='--', zorder=-1, label=f'Maximum DA {jobname}')
        if fill:
            ax.fill_between(angles, da_min.astype(float), da_max.astype(float),  # weird conversion to obj otherwise
                            color=color, alpha=ALPHA_FILL)
            ax.fill_between(angles, (da_mean-da_std).astype(float), (da_mean+da_std).astype(float),
                            color=color, alpha=ALPHA_FILL_STD, edgecolor="None")
        mean_h, = ax.plot(angles, da_mean, c=color, ls='-', lw=2*rcParams["lines.linewidth"], label=f'Mean DA {jobname}')

    return mean_h


def plot_jobs_and_save(base_dir, output_dir, out_label, setups, **kwargs):
    outdir_path = None
    if output_dir:
        outdir_path = output_dir / GATHER_FOLDER

    labels = [s.pop("label", get_beams_jobid(**s)) for s in setups]
    jobs = [get_beams_jobid(**s) for s in setups]
    for s, l in zip(setups, labels):
        s["label"] = l  # for the next plot

    fig = plot_jobs(
        basedir=base_dir,
        labels=labels,
        jobs=jobs,
        out_label=out_label,
        **kwargs
    )

    fig.tight_layout(), fig.tight_layout()
    if outdir_path:
        fig.savefig(outdir_path / f"plot.{fig.canvas.get_default_filename()}")
    return fig


# Frederik Style Plots ---------------------------------------------------------


def plot_sweep(base_dir, output_dir, out_label, setups, da_col=ALOST2, seeds=SEEDS, **kwargs):
    outdir_path = None
    if output_dir:
        outdir_path = output_dir / GATHER_FOLDER

    manual = {u"markers.fillstyle": u'none',
              u'savefig.format': u'pdf'}

    colors: Iterable[int] = kwargs.pop('colors', None)
    plot_std: bool = kwargs.pop('plot_std', False)
    ylim: np.numeric = kwargs.pop('ylim', None)
    main_xlabel: np.numeric = kwargs.pop('xlabel', None)
    ncol: np.numeric = kwargs.pop('ncol', 3)
    extra_legend: np.numeric = kwargs.pop('extra_legend', 0)
    target: np.numeric = kwargs.pop('target', None)
    plot_styles: Iterable[Union[Path, str]] = kwargs.pop('plot_styles', 'standard')

    manual.update(kwargs)
    pstyle.set_style(plot_styles, manual)

    fig, ax = plt.subplots()
    fig.canvas.manager.set_window_title(f"da_sweep.{out_label}")

    for _ in range(extra_legend):
        ax.plot(0, 0, c='k', alpha=0., label='    ')

    if target:
        ax.axhline(target, c='k', ls='--', marker='None', label="_target")

    xlabels = [s.pop("xlabel", get_beams_jobid(**s)) for s in setups]
    labels = [s.pop("label", get_beams_jobid(**s)) for s in setups]
    jobs = [get_beams_jobid(**s) for s in setups]
    for s, l, xl in zip(setups, labels, xlabels):
        # for the next plot
        s["label"] = l
        s["xlabel"] = xl

    # Sort by label
    sorted_jobs = defaultdict(list)
    for job, xlabel, label in zip(jobs, xlabels, labels):
        sorted_jobs[label].append((xlabel, job))
        
    if colors is None:
        colors = range(len(sorted_jobs))

    for idx, (label, color_idx) in enumerate(zip(sorted_jobs.keys(), colors)):
        # Sort into DataFrame -----
        df_data = TfsDataFrame([],
                               index=range(len(sorted_jobs[label])),
                               columns=["X"] + list(seeds) + ["MIN", "MEAN", "MAX", "STD"])
        for idx_x, (xlabel, jobname) in enumerate(sorted_jobs[label]):
            df_seeds = read_tfs(get_tfs_da_seeds_stats_path(jobname, base_dir), index="SEED")

            seeds_in_df = set(s for s in seeds if s in df_seeds.index)
            seeds_not_in_df = set(seeds) - seeds_in_df
            if len(seeds_not_in_df):
                LOG.warning(f"Seeds not found in data: {', '.join(str(s) for s in seeds_not_in_df)} ({jobname})")

            df_data.loc[idx_x, "X"] = xlabel
            df_data.loc[idx_x, seeds_in_df] = df_seeds.loc[seeds_in_df, f"MEAN{da_col}"].T.to_numpy()
            for pre in ["MIN", "MEAN", "MAX", "STD"]:
                df_data.loc[idx_x, pre] = df_seeds.loc[0, f"{pre}{da_col}"]
        df_data = df_data.set_index("X")

        # Plot DataFrame -----
        color = pcolors.get_mpl_color(color_idx)
        for seed in seeds:
            ax.plot(
                df_data.index, df_data[seed],
                c=color, alpha=ALPHA_SEED,
                label=f"_{label}_seed{seed}"
            )
        ax.plot(df_data.index, df_data["MIN"], c=color, ls='--', lw=2*rcParams["lines.linewidth"], label=f'_{label}_min')
        ax.plot(df_data.index, df_data["MAX"], c=color, ls='--', lw=2*rcParams["lines.linewidth"], label=f'_{label}_max')
        ax.plot(df_data.index, df_data["MEAN"], c=color, ls='-', lw=2*rcParams["lines.linewidth"], label=label)
        if plot_std:
            ax.fill_between(
                df_data.index,
                (df_data["MEAN"] - df_data["STD"]).astype(float),
                (df_data["MEAN"] + df_data["STD"]).astype(float),
                color=color, alpha=ALPHA_FILL_STD, edgecolor="None",
            )

    ax.set_xlabel(main_xlabel)
    ax.set_xticks(xlabels)

    ax.set_ylim(ylim)
    ylim = ax.get_ylim()
    ax.get_yaxis().set_major_locator(MaxNLocator(integer=True))
    ax.get_yaxis().set_minor_locator(MultipleLocator())
    # ax.set_yticks(range(round(ylim[0]), round(ylim[1])+1), minor=True)
    ax.set_ylabel(r'Dynamic Aperture $[\sigma_{nominal}]$')

    pannot.make_top_legend(ax, ncol=ncol, frame=False)

    fig.tight_layout(), fig.tight_layout()
    if outdir_path:
        fig.savefig(outdir_path / f"plot.{fig.canvas.get_default_filename()}")
    return fig
