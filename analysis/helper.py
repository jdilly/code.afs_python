
def glue_lattice(df_lattice, slices=False):
    grouped = df_lattice.groupby(by=lambda name: name.split("..")[0], axis="index")
    df_grouped = grouped[["L", "ANGLE"] + _get_present_k_columns(df_lattice.columns)].sum()
    df_grouped["S"] = grouped["S"].max()

    slice_positions = None
    if slices:
        slice_positions = list(sorted(set(df_lattice["S"]) - set(df_grouped["S"])))
    return df_grouped, slice_positions


def _get_present_k_columns(columns):
    return columns[columns.str.match("K\d+S?L")]
