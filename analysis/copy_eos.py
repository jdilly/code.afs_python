import subprocess
import sys
from pathlib import Path

from omc3.utils import logging_tools
from pylhc_submitter.constants.autosix import get_database_path, get_autosix_results_path
from pylhc_submitter.sixdesk_tools.troubleshooting import get_all_jobs_in_base

LOG = logging_tools.get_logger(__name__, level_console=logging_tools.DEBUG)

TESTRUN = False


# Paths ------------------------------------------------------------------------

def get_afs_work(user: str):
    return Path("/afs") / "cern.ch" / "work" / user[0] / user


def get_eos(user: str):
    return Path("/eos") / f"home-{user[0]}" / user


# Copy Functions ---------------------------------------------------------------


def copy_all(basedir: Path, studydir: Path, outputdir: Path):
    # copy_studies(studydir, outputdir)
    copy_sixdeskbase(basedir, outputdir)


def copy_studies(studydir: Path, outputdir: Path):
    LOG.info(f"Copy Studies from {str(studydir)}")
    outputdir.mkdir(exist_ok=True, parents=True)
    for job_dir in studydir.glob('*'):
        if not job_dir.is_dir():
            continue
        LOG.info(f"  {job_dir.name} --> {outputdir}")
        if not TESTRUN:
            copy_dir(job_dir, outputdir)


def copy_sixdeskbase(basedir: Path, outputdir: Path):
    LOG.info(f"Copy Sixdeskbase from {str(basedir)}")
    for job in get_all_jobs_in_base(basedir):
        jobdir = outputdir / f"Job.{job}"
        db = get_database_path(job, basedir)
        autosix = get_autosix_results_path(job, basedir)
        LOG.info(f"  {db.name} --> {jobdir}")
        if not TESTRUN:
            copy_file_to_dir(db, jobdir)

        LOG.info(f"  {autosix} --> {jobdir}")
        if not TESTRUN:
            copy_dir(autosix, jobdir)


# Helper ---

def copy_dir(src: Path, dst: Path):
    dst.mkdir(exist_ok=True, parents=True)
    start_subprocess(['cp', '-rfu', str(src), str(dst)])


def copy_file_to_dir(src: Path, dst: Path):
    dst.mkdir(exist_ok=True, parents=True)
    start_subprocess(['cp', '-u', str(src), f"{str(dst)}/"])


# Commandline Call -------------------------------------------------------------

def start_subprocess(cmd, shell=False):
    cmd_str = cmd
    if not isinstance(cmd, str):
        cmd_str = ' '.join(cmd_str)
    LOG.debug(f" Command: {cmd_str}")
    proc = subprocess.Popen(cmd, stdout=subprocess.PIPE, stderr=subprocess.PIPE, shell=shell)
    for line in proc.stdout:
        decoded = line.decode("utf-8").strip()
        if decoded:
            LOG.debug(decoded)
    try:
        if proc.wait():
            raise OSError("Something failed")
    except OSError as e:
        raise IOError(e.args[0])


# Main -------------------------------------------------------------------------

if __name__ == '__main__':
    # basedir = 'sixdeskbase'
    basedir = 'sixdeskbase_d1d2b5sweep'
    # study_in = 'study.irnl_correction_multi_beams'
    # study_in = 'study.irnl_correction_beams_b5sweep'
    study_in = 'study.irnl_correction_d1d2b5sweep'
    study_out = study_in

    for arg in sys.argv[1:]:
        basedir = get_afs_work(arg) / basedir
        studydir = get_afs_work(arg) / study_in
        outputdir = get_eos('jdilly') / 'work' / study_out

        # if not studydir.exists():
        #     raise IOError(f"Study '{studydir}' does not exist")
        # copy_studies(studydir, outputdir)

        if not basedir.exists():
            raise IOError(f"Sixdeskbase '{basedir}' does not exist")
        copy_sixdeskbase(basedir, outputdir)
