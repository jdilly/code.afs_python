"""
Analyse RDTS
-------------

This script is meant to analyse RDTs as calculated by PTC.
Most of its functionality has not been implemented yet.

"""
from analysis.analyse_ptc_output import (CORRECTED_ID, UNCORRECTED_ID, _stats_columns,
    SEED_DIR_MASK, SEEDS)
from file_crawler import get_working_dirs, GATHER_FOLDER, CORRECTED_ID, UNCORRECTED_ID, SEEDS
import tfs

from omc3.utils import logging_tools
from matplotlib import pyplot as plt

LOG = logging_tools.get_logger(__name__)

AMPDET_MASK_IN = 'ampdet.{machine:s}.b{beam:d}.{id:s}.tfs'
AMPDET_MASK_SEEDS = '{order:s}ampdet.{machine:s}.b{beam:d}.{id:s}.seeds.tfs'
AMPDET_MASK_STATS = '{order:s}ampdet.{machine:s}.b{beam:d}.{id:s}.statistics.tfs'


def gather_rdt_data(beam, machine, study_dir, rdts,  seeds=SEEDS):
    working_dirs = get_working_dirs(study_dir, beam)
    output_dir = study_dir / GATHER_FOLDER
    output_dir.mkdir(exist_ok=True)


    dfs_stats = {k: tfs.TfsDataFrame(index=[cwd.name for cwd in working_dirs],
                                     columns=_stats_columns(columns))
                 for k in (CORRECTED_ID, UNCORRECTED_ID)}
    dfs_stats.update({k: tfs.TfsDataFrame(index=[cwd.name for cwd in working_dirs],
                                          columns=_stats_columns(_delta_columns(columns)))
                      for k in (DIFF_ID, DIFFABS_ID)})

    for cwd in working_dirs:
        LOG.info(f"Current Working dir: {str(cwd)}")
        cwd_output_dir = cwd / GATHER_FOLDER
        cwd_output_dir.mkdir(exist_ok=True)

        dfs_seeds = {k: tfs.TfsDataFrame(index=list(seeds), columns=columns) for k in (CORRECTED_ID, UNCORRECTED_ID, DIFF_ID, DIFFABS_ID)}
        for seed in seeds:
            input_dir = cwd / SEED_DIR_MASK.format(seed=seed)
            LOG.info(f"Current SEED dir: {str(input_dir)}")
            for id_ in (UNCORRECTED_ID, CORRECTED_ID):
                if cwd.name.endswith("None") and id_ != UNCORRECTED_ID:
                    continue

                dfs_seeds[id_].loc[seed, columns] = _get_detuning_from_ptc_output(
                    input_dir / AMPDET_MASK_IN.format(beam=beam, machine=machine, id=id_), columns).to_numpy()
        dfs_seeds[DIFF_ID] = dfs_seeds[CORRECTED_ID] - dfs_seeds[UNCORRECTED_ID]
        dfs_seeds[DIFF_ID].columns = _delta_columns(columns)

        dfs_seeds[DIFFABS_ID] = dfs_seeds[CORRECTED_ID].abs() - dfs_seeds[UNCORRECTED_ID].abs()
        dfs_seeds[DIFFABS_ID].columns = _delta_columns(columns)

        for id_, df in dfs_seeds.items():
            if cwd.name.endswith("None") and id_ != UNCORRECTED_ID:
                continue

            tfs.write(cwd_output_dir / AMPDET_MASK_SEEDS.format(order=order_text, beam=beam, machine=machine, id=id_), df, save_index=SEED_COLUMN)

            for col, (pre, np_fun) in itertools.product(df.columns, PREFIX_FUN_MAP.items()):
                dfs_stats[id_].loc[cwd.name, f"{pre}{col}"] = np_fun(df[col])

    for id_, df in dfs_stats.items():
        tfs.write(output_dir / AMPDET_MASK_STATS.format(order=order_text, beam=beam, machine=machine, id=id_), df, save_index=JOB_ID_COLUMN)


def plot_with_lattice(df_data, columns, labels, ylabel, df_lattice=None, show_slices=False, vertical_lines=None):
    if df_lattice is None:
        df_lattice = df_data.loc[:, ["S", "L", "ANGLE"] + _get_present_k_columns(df_data.columns)]
    df_lattice, slice_markers = glue_lattice(df_lattice, show_slices)

    fig, axs = plt.subplots(2, 1, gridspec_kw={'height_ratios': [1, 3]})
    plot_lattice(axs[0], df_lattice, slice_markers)
    plot_data(axs[1], df_data, columns, labels, ylabel, vertical_lines)

    return fig

