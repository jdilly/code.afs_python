"""
Some simulations were run with the wrong emittance, gamma and energy
for the HL-LHC (in fact, with the values from LHC).
This script allows you to extract the data from the database and rescale the
results by

factor = sqrt(emittance_lhc / emittance_hllhc) = sqrt( norm_emittance_lh * beta_hllhc * gamma_hllhcc / norm_emittance_hllhc / beta_lhc / gamma_lhc )
"""
import re
import sqlite3 as sql
from pathlib import Path

import numpy as np
import pandas as pd
from omc3.utils import logging_tools
from pylhc_submitter.constants.autosix import (
    get_tfs_da_path,
    get_tfs_da_seed_stats_path,
    get_tfs_da_angle_stats_path,
    MIN,
    MAX,
    SEED,
    ANGLE,
    ALOST1,
    ALOST2,
    AMP,
)
from tfs import TfsDataFrame, write_tfs

from analysis.copy_eos import start_subprocess

norm_emittance_lhc = 3.75
norm_emittance_hllhc = 2.5
gamma_lhc = 6927.6
gamma_hllhc = 7460.5
energy_lhc = 6500
energy_hllhc = 7000
pmass = 0.93827208816  # GeV / c**2
c = 299792458  # m/s

from pylhc_submitter.sixdesk_tools.post_process_da import (
_create_stats_df,
)

LOG = logging_tools.get_logger(__name__)
JOBDIR_ID = "Job."


def rewrite_all_in(base_dir: Path, filter: str = None):
    job_dirs = get_all_job_dirs(base_dir)
    for job_dir in job_dirs:
        if filter is not None and not re.match(filter, job_dir.name):
            LOG.debug(f"Filtered: {job_dir.name} ---")
            continue

        LOG.info(f"Rescaling: {job_dir.name} ---")
        hllhc_check(job_dir.name)
        rewrite_da_output(job_dir)


def get_all_job_dirs(base_dir):
    return base_dir.glob(f"{JOBDIR_ID}*")


def hllhc_check(name):
    if 'hllhc' not in name:
        raise NameError(f"'hllhc' not found in {name}")


def get_database_path(job_dir):
    databases = list(job_dir.glob("*.db"))
    if not databases:
        raise IOError(f"No databases found in {job_dir.name}")

    if len(databases) > 1:
        raise IOError(f"More than one database found in {job_dir.name}")

    return databases[0]


def extract_data_from_db(db_path: Path):
    db = sql.connect(db_path)
    df_da = pd.read_sql(
        "SELECT seed, angle, alost1, alost2, Amin, Amax FROM da_post ORDER BY seed, angle", db
    )
    df_da = df_da.rename(
        columns={
            "seed": SEED,
            "angle": ANGLE,
            "alost1": ALOST1,
            "alost2": ALOST2,
            "Amin": f"{MIN}{AMP}",
            "Amax": f"{MAX}{AMP}",
        }
    )
    return TfsDataFrame(df_da)


def rescale_data(df_da):
    scale_columns = [ALOST1, ALOST2, f"{MIN}{AMP}", f"{MAX}{AMP}"]
    df_da = df_da.copy()
    factor = analytical_ratio()
    df_da[scale_columns] *= factor
    df_da.headers["SCALE_FACTOR"] = factor
    df_da.headers["SCALE_HINT"] = "The values have been rescaled from the DB values to account for wrong emittance and energy in the simulations (LHC values were used)."
    return df_da


def rewrite_da_output(job_dir):
    db_path = get_database_path(job_dir)
    autosix_path = job_dir / 'autosix_output'
    jobname = get_jobname(job_dir)

    df_da = extract_data_from_db(db_path)
    df_da = rescale_data(df_da)
    df_angle = _create_stats_df(df_da, ANGLE)
    df_seed = _create_stats_df(df_da, SEED, global_index=0)

    for k, v in df_da.headers.items():
        df_angle.headers[k] = v
        df_seed.headers[k] = v

    tfs_da_path = autosix_path / get_tfs_da_path(jobname, Path()).name
    tfs_da_angle_stats_path = autosix_path / get_tfs_da_angle_stats_path(jobname, Path()).name
    tfs_da_seed_stats_path = autosix_path / get_tfs_da_seed_stats_path(jobname, Path()).name

    write_tfs(tfs_da_path, df_da)
    write_tfs(tfs_da_angle_stats_path, df_angle, save_index=ANGLE)
    write_tfs(tfs_da_seed_stats_path, df_seed, save_index=SEED)


def analytical_ratio():

    # LOG.info(f"Calc gamma lhc: {energy_lhc / pmass}")
    # LOG.info(f"Calc gamma hllhc: {energy_hllhc / pmass}")

    emmittance_lhc = norm_emittance_lhc / _betagamma(gamma_lhc)
    emmittance_hllhc = norm_emittance_hllhc / _betagamma(gamma_hllhc)
    ratio = np.sqrt(emmittance_lhc/emmittance_hllhc)

    # LOG.info(f"Calc ratio: {ratio}")
    return ratio


def _betagamma(gamma):
    return np.sqrt(gamma**2 - 1)


def get_jobname(job_dir):
    if not job_dir.name.startswith(JOBDIR_ID):
        raise IOError(f"Can't extract jobname from {job_dir.name}")
    return job_dir.name[len(JOBDIR_ID):]




EOS_WORK = Path("/eos/home-j/jdilly/work")
TEST_PATH = EOS_WORK / "rewrite_test"
STUDY_PATH = EOS_WORK / "study.irnl_correction_d1d2b5sweep"

def copy_testdata():
    TEST_PATH.mkdir(exist_ok=True)
    job_dirs = get_all_job_dirs(STUDY_PATH)
    for job_dir in list(job_dirs)[14:15]:
        LOG.info(f"{job_dir} ---")
        new_jobdir = TEST_PATH / job_dir.name
        new_jobdir.mkdir(exist_ok=True)

        autosix_dir = new_jobdir / "autosix_output"
        autosix_dir.mkdir(exist_ok=True)

        db_path = get_database_path(job_dir)

        start_subprocess(['cp', str(db_path.absolute()), str(new_jobdir.absolute() / db_path.name)])


if __name__ == '__main__':
    # LOG.info(analytical_ratio())
    # copy_testdata()
    # rewrite_all_in(TEST_PATH)
    # rewrite_all_in(STUDY_PATH)
    rewrite_all_in(STUDY_PATH, filter=".*hllhc.*\.rdts_b6.*\.rdts_b6.*")
