"""

Shit seeds:

Third Order Chroma:
    Job.hllhc.b4.by_same.opt_round1515.xing_top.err_AB3-15.fd_0.iter_1.corrb5_d1d2.d1_7.d2_10.limited.rdts_b6-b6_a6-a6_b14-b5_a5-a5_b4-b4_a4-a4_b3-b3_a3-a3.rdts_None
    Seed 15 Corrected


"""


import itertools
import sys

from pylhc_submitter.constants.autosix import ALOST2
from pylhc_submitter.utils.iotools import make_replace_entries_iterable

from analysis.analyse_corrector_values import plot_correctors
from pathlib import Path
from matplotlib import pyplot as plt
from analysis.analyse_ptc_output import (
    gather_ptc_data,
    plot_ptc_ampdet_data as plot_ampdet,
    plot_ptc_chroma_data as plot_chroma,
    DIFFABS_ID, TARGETDIFFABS_ID, RELDIFFABS_ID, RESIDUALDIFFABS_ID
)
from analysis.analyse_da_output import plot_jobs_and_save as plot_da, plot_sweep
from analysis.analyse_twiss_output import plot_rdt_un_corrected, gather_twiss_rdt_data, plot_rdt_un_corrected_ids

from omc3.utils import logging_tools
from omc3.plotting.utils import colors as pcolors

from analysis.file_crawler import CORRECTED_ID, UNCORRECTED_ID, NOMINAL_ID, BEFORE_CORRECTION
from correct_irnl_run_helper import get_other_beam

try:
    from mpl_editor.main import main as mpl_editor
except ImportError:
    mpl_editor = None


from pylhc.irnl_rdt_correction import DotDict

LOG = logging_tools.get_logger(__name__)

STUDY_DIR = Path("/eos/home-j/jdilly/work/study.irnl_correction_d1d2b5sweep")
STUDY_DIR_JOSCH = Path("/eos/home-j/josch/work/study.irnl_correction_d1d2b5sweep")
ERRORS_ALL = "AB3-15"

CORRECTOR_KWARGS = {
    'figure.figsize': [7.20, 6.48],
    # 'legend.handlelength': 1,
    # 'legend.handletextpad': .2,
    'legend.fontsize': 20,
    'xtick.labelsize': 20,
    'xtick.major.pad': 10,
    'ytick.labelsize': 20,
    'axes.labelsize': 20,
}

DA_KWARGS = {
    'figure.figsize': [6.4, 6.4],
    # 'legend.handlelength': 1.,
    # 'legend.handletextpad': .4,
    'legend.fontsize': 20,
    'xtick.labelsize': 20,
    'xtick.major.pad': 10,
    'ytick.labelsize': 20,
    'axes.labelsize': 20,
    'amplitude_ticks': list(range(2, 20, 2))
}

DA_D2SWEEP_KWARGS = {
    'xlabel': "D2 systematic $b_5$ [units]",
    'figure.figsize': [6.8, 6.8],
    # 'legend.handlelength': 1,
    # 'legend.handletextpad': .2,
    'legend.fontsize': 20,
    'xtick.labelsize': 20,
    'xtick.major.pad': 10,
    'ytick.labelsize': 20,
    'axes.labelsize': 20,
    'grid.alpha': 0.6,
    'axes.grid.axis': 'y',
    'axes.grid.which': 'both',
}

DA_D1SWEEP_KWARGS = DA_D2SWEEP_KWARGS.copy()
DA_D1SWEEP_KWARGS.update({
    'xlabel': "D1 systematic $b_5$ [units]",
})

AMPDET_KWARGS = {
    'figure.figsize': [7.68, 5.76],
    'legend.columnspacing': 0.5,
    'legend.handlelength': 1.,
    'legend.fontsize': 22,
    'xtick.labelsize': 24,
    'ytick.labelsize': 24,
    'axes.labelsize': 24,
}

SO_AMPDET_KWARGS = {
    'figure.figsize': [7.68, 6.4],
    'legend.columnspacing': 0.5,
    'legend.handlelength': 1.,
    'legend.fontsize': 22,
    'xtick.labelsize': 24,
    'xtick.alignment': 'center',
    'xtick.rotation': 20,
    'ytick.labelsize': 24,
    'axes.labelsize': 24,
}

SO_CHROMA_KWARGS = {
    'figure.figsize': [7.68, 6.4],
    'legend.columnspacing': 0.5,
    'legend.handlelength': 1.,
    'legend.fontsize': 22,
    'xtick.labelsize': 24,
    'xtick.alignment': 'center',
    'xtick.rotation': 20,
    'ytick.labelsize': 24,
    'axes.labelsize': 24,
}

TO_CHROMA_KWARGS = {
    'figure.figsize': [7.68, 6.4],
    'legend.columnspacing': 0.5,
    'legend.handlelength': 1.,
    'legend.fontsize': 22,
    'xtick.labelsize': 24,
    'xtick.alignment': 'center',
    'xtick.rotation': 20,
    'ytick.labelsize': 24,
    'axes.labelsize': 24,
}

FO_CHROMA_KWARGS = {
    'figure.figsize': [7.68, 6.4],
    'legend.columnspacing': 0.5,
    'legend.handlelength': 1.,
    'legend.fontsize': 22,
    'xtick.labelsize': 24,
    'xtick.alignment': 'center',
    'xtick.rotation': 20,
    'ytick.labelsize': 24,
    'axes.labelsize': 24,
}

RDT_UN_CORRECTED_KWARGS = {
    'figure.figsize': [7.68, 6.4],
    'legend.columnspacing': 0.5,
    'legend.handlelength': 1.,
    'legend.fontsize': 20,
    'xtick.labelsize': 22,
    'xtick.alignment': 'center',
    # 'xtick.rotation': 20,
    'ytick.labelsize': 22,
    'axes.labelsize': 24,
}


def setup_iterator(**kwargs):
    kwargs = make_replace_entries_iterable(kwargs)
    values_combined = itertools.product(*kwargs.values())
    for values in values_combined:
        yield DotDict(zip(kwargs.keys(), values))


def close_fig(fig):
    # mpl_editor(fig)
    plt.close(fig)
    # plt.show()
    pass


def modify_setups_rdt(setups):
    rdt_setups = [s.copy() for s in setups]
    for s in rdt_setups:
        s['label'] = f"correct \n {s['label']}"
    return rdt_setups


def rdt_label(rdt):
    return f"$f_{{{rdt[1]}00{rdt[2]}}}$" if len(rdt) == 3 else "all"


def gather_ptc_beams(update=False):
    """ update = do not redo all files but load previous results and only add new jobs. """
    for beam in (1, 4):
        gather_ptc_data(study_dir=[STUDY_DIR, STUDY_DIR_JOSCH], machine='hllhc', beam=beam, orders=(1, 2, 3, 4), update=update)
        # gather_ptc_data(study_dir=STUDY_DIR, machine='hllhc', beam=beam, orders=(4,), update=update)


def gather_twiss_beams(update=False):
    """ update = do not redo all files but load previous results and only add new jobs. """
    for beam in (1, 4):
        gather_twiss_rdt_data(
            machine='hllhc',
            beam=beam,
            rdts=['f1004', 'f5000', 'f3002'],
            study_dir=[STUDY_DIR, STUDY_DIR_JOSCH],
            update=update,
        )


def hllhc_compare_d2b5_15():
    base_dir = STUDY_DIR
    setup_def = dict(
        machine='hllhc',
        optics="round1515",
        xing="top",
        d2b5_value=15,
        errors=ERRORS_ALL,
        corrector_strength='limited',
        correct_beam="same",
        correct_b5=('no', 'd1', 'd1d2'),
        feeddown=0,
        iterations=1,
        d1b5_value=0,
        beam=(1, 4),
    )
    for s in setup_iterator(**setup_def):
        setups = [
            dict(rdts="b6-b6_a6-a6_b5-b5_a5-a5_b4-b4_a4-a4_b3-b3_a3-a3", rdts2="None", label=rdt_label("f50"), **s),
            dict(rdts="b6-b6_a6-a6_b14-b5_a5-a5_b4-b4_a4-a4_b3-b3_a3-a3", rdts2="None", label=rdt_label("f14"), **s),
        ]
        out_label = f"{s.machine}.b{s.beam}.d2b5val_{s.d2b5_value}.corr_{s.corrector_strength}"
        for ip in (1, 5):
            rdt_setups = modify_setups_rdt(setups)
            close_fig(plot_rdt_un_corrected(study_dir=base_dir, setups=rdt_setups, out_label=out_label, rdt="f5000", ip=ip, ylim=[6e4, 1.4e11], **RDT_UN_CORRECTED_KWARGS))
            close_fig(plot_rdt_un_corrected(study_dir=base_dir, setups=rdt_setups, out_label=out_label, rdt="f1004", ip=ip, ylim=[6e4, 1.4e11], **RDT_UN_CORRECTED_KWARGS))
            close_fig(plot_correctors(study_dir=base_dir, setups=setups, out_label=out_label, field_component="b5", ip=ip, ncol=2, xlim=[-6, 3], ylim=[-3, 6], **CORRECTOR_KWARGS))

        # close_fig(plot_da(base_dir=base_dir, setups=setups, out_label=out_label, output_dir=STUDY_DIR, rlim=16, plot_std=True, **DA_KWARGS))
        # for the_id in (CORRECTED_ID, UNCORRECTED_ID, NOMINAL_ID, DIFFABS_ID, TARGETDIFFABS_ID, RELDIFFABS_ID, RESIDUALDIFFABS_ID):
            # close_fig(plot_ampdet(study_dir=base_dir, the_id=the_id, order=1, setups=setups, out_label=out_label, ncol=2, ylim=[-250, 250], **AMPDET_KWARGS))
            # close_fig(plot_ampdet(study_dir=base_dir, the_id=the_id, order=2, setups=setups, out_label=out_label, ncol=2, ylim=None, **SO_AMPDET_KWARGS))
            # close_fig(plot_chroma(study_dir=base_dir, the_id=the_id, order=2, setups=setups, out_label=out_label, ncol=2, ylim=None, **SO_CHROMA_KWARGS))
            # close_fig(plot_chroma(study_dir=base_dir, the_id=the_id, order=3, setups=setups, out_label=out_label, ncol=2, ylim=None, **TO_CHROMA_KWARGS))


def hllhc_compare_d2b5_10_with_d1_sweep():
    base_dir = STUDY_DIR
    setup_def = dict(
        machine='hllhc',
        optics="round1515",
        xing="top",
        d2b5_value=10,
        errors=ERRORS_ALL,
        corrector_strength='limited',
        correct_beam="same",
        correct_b5=('no', 'd1', 'd1d2'),
        feeddown=0,
        iterations=1,
        beam=(1, 4),
        rdt=('b50',),
        # rdt=('b14',),
        # rdt=('b50', 'b14'),
    )
    for s in setup_iterator(**setup_def):
        setups = [
            dict(rdts=f"b6-b6_a6-a6_{CORR_MAP[s.rdt]}_a5-a5_b4-b4_a4-a4_b3-b3_a3-a3",
                 rdts2="None",
                 label=f"$b_5$ = {d1b5_value}",
                 d1b5_value=d1b5_value, **s)
            for d1b5_value in ('0', '7')
        ]
        out_label = f"{s.machine}.b{s.beam}.by_{s.correct_beam}.correctb5_{s.correct_b5}.rdt_{s.rdt}.corr_{s.corrector_strength}.fd_{s.feeddown}.d1_sweep.d2_{s.d2b5_value}"
        for ip in (1, 5):
        #     close_fig(plot_rdt_un_corrected(study_dir=base_dir, setups=setups, out_label=out_label, rdt="f5000", ip=ip, ylim=[6e4, 1.4e12], **RDT_UN_CORRECTED_KWARGS))
        #     close_fig(plot_rdt_un_corrected(study_dir=base_dir, setups=setups, out_label=out_label, rdt="f1004", ip=ip, ylim=[6e4, 1.4e12], **RDT_UN_CORRECTED_KWARGS))
             close_fig(plot_correctors(study_dir=base_dir, setups=setups, out_label=out_label, field_component="b5", ip=ip, ncol=2, xlim=[-6, 3], ylim=[0, 6], **CORRECTOR_KWARGS))

        # close_fig(plot_da(base_dir=base_dir, setups=setups, out_label=out_label, output_dir=STUDY_DIR, rlim=16, plot_std=True, **DA_KWARGS))
        # for the_id in (CORRECTED_ID, UNCORRECTED_ID, NOMINAL_ID, DIFFABS_ID, TARGETDIFFABS_ID, RELDIFFABS_ID, RESIDUALDIFFABS_ID):
        #     close_fig(plot_ampdet(study_dir=base_dir, the_id=the_id, order=1, setups=setups, out_label=out_label, ncol=2, ylim=[-250, 350], **AMPDET_KWARGS))
            # close_fig(plot_ampdet(study_dir=base_dir, the_id=the_id, order=2, setups=setups, out_label=out_label, ncol=2, ylim=[-50, 50], **SO_AMPDET_KWARGS))
        #     close_fig(plot_chroma(study_dir=base_dir, the_id=the_id, order=2, setups=setups, out_label=out_label, ncol=2, ylim=[-6, 6], **SO_CHROMA_KWARGS))
        #     close_fig(plot_chroma(study_dir=base_dir, the_id=the_id, order=3, setups=setups, out_label=out_label, ncol=2, ylim=[-80, 80], **TO_CHROMA_KWARGS))
        #     pass


def hllhc_corr_and_ampdet_changes_d2b5():
    base_dir = STUDY_DIR
    setup_def = dict(
        machine='hllhc',
        optics="round1515",
        xing="top",
        errors=ERRORS_ALL,
        corrector_strength='limited',
        correct_beam="same",
        correct_b5=('no', 'd1', 'd1d2'),
        feeddown=0,
        iterations=1,
        d1b5_value=(0, 7),
        beam=(1, 4),
        rdt=('b50', 'b14'),
    )
    for s in setup_iterator(**setup_def):
        setups = [
            dict(
                rdts=f"b6-b6_a6-a6_{CORR_MAP[s.rdt]}_a5-a5_b4-b4_a4-a4_b3-b3_a3-a3", rdts2="None",
                label=f"$b_5$ = {d2b5_value}",
                d2b5_value=d2b5_value,
                **s)
            for idx, d2b5_value in enumerate(('0', '5', '10', '15'))
        ]
        out_label = f"{s.machine}.b{s.beam}.by_{s.correct_beam}.correctb5_{s.correct_b5}.rdt_{s.rdt}.corr_{s.corrector_strength}.fd_{s.feeddown}.d1_{s.d1b5_value}.d2_sweep"
        for ip in (1, 5):
            # close_fig(plot_rdt_un_corrected(study_dir=base_dir, setups=setups, out_label=out_label, rdt="f5000", ip=ip, ylim=[6e4, 1.4e13], **RDT_UN_CORRECTED_KWARGS))
            # close_fig(plot_rdt_un_corrected(study_dir=base_dir, setups=setups, out_label=out_label, rdt="f1004", ip=ip, ylim=[6e4, 1.4e13], **RDT_UN_CORRECTED_KWARGS))
            close_fig(plot_correctors(study_dir=base_dir, setups=setups, out_label=out_label, field_component="b5", ip=ip, ncol=2, xlim=[-5.5, 5.5], ylim=[-5.5, 5.5], **CORRECTOR_KWARGS))

        # close_fig(plot_da(base_dir=base_dir, setups=setups, out_label=out_label, output_dir=STUDY_DIR, rlim=16, plot_std=True, **DA_KWARGS))
        # for the_id in (CORRECTED_ID, UNCORRECTED_ID, NOMINAL_ID, DIFFABS_ID, TARGETDIFFABS_ID, RELDIFFABS_ID, RESIDUALDIFFABS_ID):
        #     close_fig(plot_ampdet(study_dir=base_dir, the_id=the_id, order=1, setups=setups, out_label=out_label, ncol=2, ylim=[-250, 250], **AMPDET_KWARGS))
        #     close_fig(plot_ampdet(study_dir=base_dir, the_id=the_id, order=2, setups=setups, out_label=out_label, ncol=2, ylim=None, **SO_AMPDET_KWARGS))
        #     close_fig(plot_chroma(study_dir=base_dir, the_id=the_id, order=2, setups=setups, out_label=out_label, ncol=2, ylim=None, **SO_CHROMA_KWARGS))
        #     close_fig(plot_chroma(study_dir=base_dir, the_id=the_id, order=3, setups=setups, out_label=out_label, ncol=2, ylim=None, **TO_CHROMA_KWARGS))


def hllhc_plot_all_da_polar():
    base_dir = STUDY_DIR
    setup_def = dict(
        machine='hllhc',
        optics="round1515",
        xing="top",
        errors=ERRORS_ALL,
        corrector_strength='limited',
        correct_beam="same",
        feeddown=0,
        iterations=1,
        d1b5_value=(0, 7),
        beam=(1, 4),
        d2b5_value=(0, 5, 10, 15),
        correct_b5=('no', 'd1', 'd1d2'),
    )
    for s in setup_iterator(**setup_def):
        rdts = ("b50", "b14")
        setups = [
            dict(rdts="b6-b6_a6-a6_b5-b5_a5-a5_b4-b4_a4-a4_b3-b3_a3-a3", rdts2="None", label=rdt_label("f50"), **s),
            dict(rdts="b6-b6_a6-a6_b14-b5_a5-a5_b4-b4_a4-a4_b3-b3_a3-a3", rdts2="None", label=rdt_label("f14"), **s),
        ]
        out_label = f"{s.machine}.b{s.beam}.by_{s.correct_beam}.correctb5_{s.correct_b5}.rdt_sweep.corr_{s.corrector_strength}.fd_{s.feeddown}.d1_{s.d1b5_value}.d2_{s.d2b5_value}"
        # close_fig(plot_da(base_dir=base_dir, setups=setups, out_label=out_label, output_dir=STUDY_DIR, rlim=16, plot_std=False, **DA_KWARGS))
        # for idx, (rdt, setup) in enumerate(zip(rdts, setups)):
        #     out_label = f"{s.machine}.b{s.beam}.by_{s.correct_beam}.correctb5_{s.correct_b5}.rdt_{rdt}.corr_{s.corrector_strength}.fd_{s.feeddown}.d1_{s.d1b5_value}.d2_{s.d2b5_value}"
        #     close_fig(plot_da(base_dir=base_dir, setups=[setup], out_label=out_label, output_dir=STUDY_DIR, rlim=16, plot_std=False, color_idx_start=idx, **DA_KWARGS))

        # for ip in (1, 5):
        #     rdt_setups = modify_setups_rdt(setups)
        #     close_fig(plot_rdt_un_corrected(study_dir=base_dir, setups=rdt_setups, out_label=out_label, rdt="f5000", ip=ip, ylim=[6e4, 1.4e12], **RDT_UN_CORRECTED_KWARGS))
        #     close_fig(plot_rdt_un_corrected(study_dir=base_dir, setups=rdt_setups, out_label=out_label, rdt="f1004", ip=ip, ylim=[6e4, 1.4e12], **RDT_UN_CORRECTED_KWARGS))

        # for the_id in (CORRECTED_ID, UNCORRECTED_ID, NOMINAL_ID, DIFFABS_ID, TARGETDIFFABS_ID, RELDIFFABS_ID, RESIDUALDIFFABS_ID):
        for the_id in (CORRECTED_ID, UNCORRECTED_ID, NOMINAL_ID):
            # close_fig(plot_ampdet(study_dir=base_dir, the_id=the_id, order=1, setups=setups, out_label=out_label, ncol=2, ylim=[-250, 250], **AMPDET_KWARGS))
            # close_fig(plot_ampdet(study_dir=base_dir, the_id=the_id, order=2, setups=setups, out_label=out_label, ncol=2, ylim=[-70, 70], **SO_AMPDET_KWARGS))
            # close_fig(plot_chroma(study_dir=base_dir, the_id=the_id, order=2, setups=setups, out_label=out_label, ncol=2, ylim=[-20, 20], **SO_CHROMA_KWARGS))
            close_fig(plot_chroma(study_dir=base_dir, the_id=the_id, order=3, setups=setups, out_label=out_label, ncol=2, ylim=[-30, 30], **TO_CHROMA_KWARGS))


# Sweeps ------------------

CORR_MAP = {
    'b50': 'b5-b5',
    'b14': 'b14-b5',
    'b32': 'b32-b5',
    'b50b14b32': 'b5-b5_b14-b5_b32-b5',
}

CORRB5_MAP = {
    'no': 'not corrected',
    'd1': 'correct D1 $b_5$ ',
    'd1d2': 'correct D1 $b_5$ & D2 ',
}


def hllhc_sweep_d2b5_individual():
    setup_def = dict(
        machine='hllhc',
        optics="round1515",
        xing="top",
        errors=ERRORS_ALL,
        corrector_strength='limited',
        correct_beam="same",
        feeddown=0,
        iterations=1,
        d1b5_value=(0, 7),
        beam=(1, 4),
        correct_b5=('no', 'd1', 'd1d2'),
    )
    for s in setup_iterator(**setup_def):
        for color_idx, rdt in enumerate(["b50", "b14"]):
            setups = [
                dict(**s,
                     d2b5_value=d2b5_value,
                     rdts=f"b6-b6_a6-a6_{CORR_MAP[rdt]}_a5-a5_b4-b4_a4-a4_b3-b3_a3-a3",
                     rdts2="None",
                     label=f"{rdt_label(rdt)}, {CORRB5_MAP[s.correct_b5]}",
                     xlabel=d2b5_value
                     )
                for d2b5_value in [0, 5, 10, 15]
            ]
            out_label = f"{s.machine}.b{s.beam}.by_{s.correct_beam}.correctb5_{s.correct_b5}.rdt_{rdt}.corr_{s.corrector_strength}.fd_{s.feeddown}.d1_{s.d1b5_value}"
            close_fig(
                plot_sweep(
                    base_dir=STUDY_DIR, output_dir=STUDY_DIR, out_label=out_label, setups=setups, da_col=ALOST2,
                    ylim=[5.9, 18.2],
                    target=8.,
                    colors=[color_idx],
                    **DA_D2SWEEP_KWARGS
                )
            )


def hllhc_sweep_d2b5_individual_b50only():
    setup_def = dict(
        machine='hllhc',
        optics="round1515",
        xing="top",
        errors=ERRORS_ALL,
        corrector_strength='limited',
        correct_beam="same",
        feeddown=0,
        iterations=1,
        d1b5_value=7,
        beam=1,
        correct_b5=('no', 'd1', 'd1d2'),
    )
    for s in setup_iterator(**setup_def):
        for color_idx, rdt in enumerate(["b50",]):
            setups = [
                dict(**s,
                     d2b5_value=d2b5_value,
                     rdts=f"b6-b6_a6-a6_{CORR_MAP[rdt]}_a5-a5_b4-b4_a4-a4_b3-b3_a3-a3", rdts2="None",
                     label=f"{rdt_label(rdt)}, {CORRB5_MAP[s.correct_b5]}",
                     xlabel=d2b5_value
                     )
                for d2b5_value in [0, 5, 10, 15]
            ]
            out_label = f"{s.machine}.b{s.beam}.by_{s.correct_beam}.correctb5_{s.correct_b5}.rdt_{rdt}.corr_{s.corrector_strength}.fd_{s.feeddown}.d1_{s.d1b5_value}"
            close_fig(
                plot_sweep(
                    base_dir=STUDY_DIR, output_dir=None, out_label=out_label, setups=setups, da_col=ALOST2,
                    ylim=[5.9, 18.2],
                    target=8.,
                    colors=[color_idx],
                    **DA_D2SWEEP_KWARGS
                )
            )


def hllhc_sweep_d2b5_all_rdts():
    setup_def = dict(
        machine='hllhc',
        optics="round1515",
        xing="top",
        errors=ERRORS_ALL,
        corrector_strength='limited',
        correct_beam="same",
        feeddown=0,
        iterations=1,
        d1b5_value=(0, 7),
        beam=(1, 4),
        correct_b5=('no', 'd1', 'd1d2'),
    )
    for s in setup_iterator(**setup_def):
        setups = [
            dict(**s,
                 d2b5_value=d2b5_value,
                 rdts=f"b6-b6_a6-a6_{CORR_MAP[rdt]}_a5-a5_b4-b4_a4-a4_b3-b3_a3-a3", rdts2="None",
                 label=rdt_label(rdt),
                 xlabel=d2b5_value
                 )
            for d2b5_value in [0, 5, 10, 15]
            for rdt in ["b50", "b14"]
        ]
        out_label = f"{s.machine}.b{s.beam}.by_{s.correct_beam}.correctb5_{s.correct_b5}.rdt_sweep.corr_{s.corrector_strength}.fd_{s.feeddown}.d1_{s.d1b5_value}"
        close_fig(
            plot_sweep(
                base_dir=STUDY_DIR, output_dir=STUDY_DIR, out_label=out_label,
                setups=setups, da_col=ALOST2,
                ncol=3, ylim=[5.9, 18.2],
                target=8.,
                plot_std=True, seeds=[],
                **DA_D2SWEEP_KWARGS
            )
        )


def hllhc_sweep_d2b5_all_corrections():
    setup_def = dict(
        machine='hllhc',
        optics="round1515",
        xing="top",
        errors=ERRORS_ALL,
        corrector_strength='limited',
        correct_beam="same",
        feeddown=0,
        iterations=1,
        d1b5_value=(0, 7),
        beam=(1, 4),
        )
    for s in setup_iterator(**setup_def):
        for rdt in ("b50", "b14"):
            setups = [
                dict(**s,
                     rdts=f"b6-b6_a6-a6_{CORR_MAP[rdt]}_a5-a5_b4-b4_a4-a4_b3-b3_a3-a3", rdts2="None",
                     correct_b5=correct_b5,
                     d2b5_value=d2b5_value,
                     label=CORRB5_MAP[correct_b5],
                     xlabel=d2b5_value
                     )
                for d2b5_value in [0, 5, 10, 15]
                for correct_b5 in ['no', 'd1', 'd1d2']
            ]
            out_label = f"{s.machine}.b{s.beam}.by_{s.correct_beam}.correctb5_sweep.rdt_{rdt}.corr_{s.corrector_strength}.fd_{s.feeddown}.d1_{s.d1b5_value}"
            close_fig(
                plot_sweep(
                    base_dir=STUDY_DIR, output_dir=STUDY_DIR, out_label=out_label,
                    setups=setups, da_col=ALOST2,
                    ncol=1, ylim=[5.9, 18.2],
                    target=8.,
                    plot_std=True, seeds=[],
                    **DA_D2SWEEP_KWARGS
                )
            )


def hllhc_sweep_d2b5_all_corrections_b50only():
    setup_def = dict(
        machine='hllhc',
        optics="round1515",
        xing="top",
        errors=ERRORS_ALL,
        corrector_strength='limited',
        correct_beam="same",
        feeddown=0,
        iterations=1,
        d1b5_value=(0, 7),
        beam=(1, 4),
        rdt="b50",
        )
    for s in setup_iterator(**setup_def):
        setups = [
            dict(**s,
                 rdts=f"b6-b6_a6-a6_{CORR_MAP[s.rdt]}_a5-a5_b4-b4_a4-a4_b3-b3_a3-a3", rdts2="None",
                 correct_b5=correct_b5,
                 d2b5_value=d2b5_value,
                 label=CORRB5_MAP[correct_b5],
                 xlabel=d2b5_value
                 )
            for d2b5_value in [0, 5, 10, 15]
            for correct_b5 in ['no', 'd1', 'd1d2']
        ]
        out_label = f"{s.machine}.b{s.beam}.by_{s.correct_beam}.correctb5_sweep.rdt_{s.rdt}.corr_{s.corrector_strength}.fd_{s.feeddown}.d1_{s.d1b5_value}"
        close_fig(
            plot_sweep(
                base_dir=STUDY_DIR, output_dir=STUDY_DIR, out_label=out_label,
                setups=setups, da_col=ALOST2,
                ncol=1, ylim=[5.9, 18.2],
                target=8.,
                plot_std=True, seeds=[],
                **DA_D2SWEEP_KWARGS
            )
        )


def hllhc_sweep_d2b5_all_d1b5_b50only():
    setup_def = dict(
        machine='hllhc',
        optics="round1515",
        xing="top",
        errors=ERRORS_ALL,
        corrector_strength='limited',
        correct_beam="same",
        feeddown=0,
        iterations=1,
        beam=(1, 4),
        rdt="b50",
        correct_b5=('no', 'd1', 'd1d2'),
    )
    for s in setup_iterator(**setup_def):
        setups = [
            dict(**s,
                 rdts=f"b6-b6_a6-a6_{CORR_MAP[s.rdt]}_a5-a5_b4-b4_a4-a4_b3-b3_a3-a3", rdts2="None",
                 d1b5_value=d1b5_value,
                 d2b5_value=d2b5_value,
                 label=f"D1 systematic $b_5$ = {d1b5_value} units",
                 xlabel=d2b5_value
                 )
            for d2b5_value in [0, 5, 10, 15]
            for d1b5_value in [0, 7]
        ]
        out_label = f"{s.machine}.b{s.beam}.by_{s.correct_beam}.correctb5_{s.correct_b5}.rdt_{s.rdt}.corr_{s.corrector_strength}.fd_{s.feeddown}.d1_sweep"
        close_fig(
            plot_sweep(
                base_dir=STUDY_DIR, output_dir=STUDY_DIR, out_label=out_label,
                setups=setups, da_col=ALOST2,
                ncol=1, ylim=[5.9, 18.2],
                target=8.,
                plot_std=True, seeds=[],
                **DA_D2SWEEP_KWARGS
            )
        )


def hllhc_sweep_d2b5_all_corrections_b50only_splitcorrections():
    setup_def = dict(
        machine='hllhc',
        optics="round1515",
        xing="top",
        errors=ERRORS_ALL,
        corrector_strength='limited',
        correct_beam="same",
        feeddown=0,
        iterations=1,
        d1b5_value=(0, 7),
        beam=(1, 4),
        rdt="b50",
        correct_b5_with=[['no', 'd1d2'], ['d1', 'd1d2']]
    )
    for s in setup_iterator(**setup_def):
        setups = [
            dict(**s,
                 rdts=f"b6-b6_a6-a6_{CORR_MAP[s.rdt]}_a5-a5_b4-b4_a4-a4_b3-b3_a3-a3", rdts2="None",
                 correct_b5=correct_b5,
                 d2b5_value=d2b5_value,
                 label=CORRB5_MAP[correct_b5],
                 xlabel=d2b5_value
                 )
            for d2b5_value in [0, 5, 10, 15]
            for correct_b5 in s.correct_b5_with
        ]
        out_label = f"{s.machine}.b{s.beam}.by_{s.correct_beam}.correctb5_{''.join(s.correct_b5_with)}.rdt_{s.rdt}.corr_{s.corrector_strength}.fd_{s.feeddown}.d1_{s.d1b5_value}"
        close_fig(
            plot_sweep(
                base_dir=STUDY_DIR, output_dir=STUDY_DIR, out_label=out_label,
                setups=setups, da_col=ALOST2,
                ncol=1, ylim=[5.9, 18.2],
                target=8.,
                plot_std=True, seeds=[],
                colors=[0, 2] if 'no' in s.correct_b5_with else [1, 2],
                extra_legend=3 - len(s.correct_b5_with),
                **DA_D2SWEEP_KWARGS
            )
        )


def hllhc_corrections_sweep():
    base_dir = STUDY_DIR
    setup_def = dict(
        machine='hllhc',
        optics="round1515",
        # xing=("top", "flat"),
        xing=("top", ),
        errors=ERRORS_ALL,
        corrector_strength='limited',
        correct_beam="same",
        feeddown=0,
        iterations=1,
        d1b5_value=(0, 7),
        d2b5_value=(0, 5, 10, 15),
        beam=(1, 4),
    )
    for s in setup_iterator(**setup_def):
        for rdt in ("b50", "b14"):
            setups = [
                dict(**s,
                     rdts=f"b6-b6_a6-a6_{CORR_MAP[rdt]}_a5-a5_b4-b4_a4-a4_b3-b3_a3-a3", rdts2="None",
                     correct_b5=correct_b5,
                     label=CORRB5_MAP[correct_b5],
                     color=pcolors.get_mpl_color(idx)
                     )
                for idx, correct_b5 in enumerate(['no', 'd1', 'd1d2'])
            ]
            if s.xing == "flat":
                if s.d1b5_value == 0:
                    continue # not run
                del setups[1]  # d1 correction not run

            out_label = f"{s.machine}.b{s.beam}.by_{s.correct_beam}.xing_{s.xing}.correctb5_sweep.rdt_{rdt}.corr_{s.corrector_strength}.fd_{s.feeddown}.d1_{s.d1b5_value}.d2_{s.d2b5_value}"
            # for ip in (1, 5):
            #     close_fig(plot_rdt_un_corrected_ids(study_dir=base_dir, setups=setups, out_label=out_label, rdt="f1004", ip=ip, ylim=[9e6, 1.4e12], ncol=2, **RDT_UN_CORRECTED_KWARGS))
            #     close_fig(plot_rdt_un_corrected_ids(study_dir=base_dir, setups=setups, out_label=out_label, rdt="f5000", ip=ip, ylim=[9e6, 1.4e12], ncol=2, **RDT_UN_CORRECTED_KWARGS))

            setups = [dict(**s,
                           rdts=f"b6-b6_a6-a6_{CORR_MAP[rdt]}_a5-a5_b4-b4_a4-a4_b3-b3_a3-a3", rdts2="None",
                           correct_b5='no',  # does not matter
                           label=BEFORE_CORRECTION,  # triggers "uncorrected"
                           color="black",
                           )
                      ] + setups + [
                         dict(**s,
                              rdts=f"b6-b6_a6-a6_{CORR_MAP[rdt]}_a5-a5_b4-b4_a4-a4_b3-b3_a3-a3", rdts2="None",
                              correct_b5='no',  # does not matter
                              label=NOMINAL_ID,  # triggers "nominal"
                              color="gray",
                              )
                      ]
            # close_fig(plot_ampdet(study_dir=base_dir, the_id=CORRECTED_ID, order=1, setups=setups, out_label=out_label, ncol=2, ylim=[-250, 250], **AMPDET_KWARGS))
            # close_fig(plot_ampdet(study_dir=base_dir, the_id=CORRECTED_ID, order=2, setups=setups, out_label=out_label, ncol=2, ylim=[-70, 70], **SO_AMPDET_KWARGS))
            # close_fig(plot_chroma(study_dir=base_dir, the_id=CORRECTED_ID, order=2, setups=setups, out_label=out_label, ncol=2, ylim=[-20, 20], **SO_CHROMA_KWARGS))
            close_fig(plot_chroma(study_dir=base_dir, the_id=CORRECTED_ID, order=3, setups=setups, out_label=out_label, ncol=2, ylim=[-30, 30], **TO_CHROMA_KWARGS))
            # close_fig(plot_chroma(study_dir=base_dir, the_id=CORRECTED_ID, order=4, setups=setups, out_label=out_label, ncol=2, ylim=[-200, 200], **FO_CHROMA_KWARGS))


# Feed-Down --------------------------------------------------------------------


def hllhc_sweep_d2b5_feeddown_d1d2():
    setup_def = dict(
        machine='hllhc',
        optics="round1515",
        xing="top",
        errors=ERRORS_ALL,
        corrector_strength='limited',
        correct_beam="same",
        correct_b5='d1d2',
        iterations=1,
        d1b5_value=(7, ),
        beam=(1, 4),
        rdt=('b50', 'b14'),
    )
    for s in setup_iterator(**setup_def):
        setups = [
            dict(**s,
                 rdts=f"b6-b6_a6-a6_{CORR_MAP[s.rdt]}_a5-a5_b4-b4_a4-a4_b3-b3_a3-a3",
                 rdts2=f"b6-b6_a6-a6_{CORR_MAP[s.rdt]}_a5-a5_b4-b4_a4-a4_b3-b3_a3-a3" if feeddown else "None",
                 feeddown=feeddown,
                 d2b5_value=d2b5_value,
                 label="w/ feeddown" if feeddown else "w/o feeddown",
                 xlabel=d2b5_value
                 )
            for d2b5_value in [0, 5, 10, 15]
            for feeddown in [0, 2]
        ]
        out_label = f"{s.machine}.b{s.beam}.by_{s.correct_beam}.correctb5_{s.correct_b5}.rdt_{s.rdt}.corr_{s.corrector_strength}.fd_sweep.d1_{s.d1b5_value}"
        close_fig(
            plot_sweep(
                base_dir=STUDY_DIR, output_dir=STUDY_DIR, out_label=out_label,
                setups=setups, da_col=ALOST2,
                ncol=1, ylim=[5.9, 18.2],
                target=8.,
                plot_std=True, seeds=[],
                **DA_D2SWEEP_KWARGS
            )
        )


def hllhc_sweep_d2b5_feeddown_d1d2_ampdet():
    base_dir = STUDY_DIR
    setup_def = dict(
        machine='hllhc',
        optics="round1515",
        xing="top",
        errors=ERRORS_ALL,
        corrector_strength='limited',
        correct_beam="same",
        correct_b5='d1d2',
        iterations=1,
        d1b5_value=(7, ),
        beam=(1, 4),
        rdt=('b50', 'b14'),
        d2b5_value=(0, 5, 10, 15),
    )
    for s in setup_iterator(**setup_def):
        setups = [
            dict(**s,
                 rdts=f"b6-b6_a6-a6_{CORR_MAP[s.rdt]}_a5-a5_b4-b4_a4-a4_b3-b3_a3-a3",
                 rdts2=f"b6-b6_a6-a6_{CORR_MAP[s.rdt]}_a5-a5_b4-b4_a4-a4_b3-b3_a3-a3" if feeddown else "None",
                 feeddown=feeddown,
                 label="w/ feeddown" if feeddown else "w/o feeddown",
                 )
            for feeddown in [0, 2]
        ]
        out_label = f"{s.machine}.b{s.beam}.by_{s.correct_beam}.correctb5_{s.correct_b5}.rdt_{s.rdt}.corr_{s.corrector_strength}.fd_sweep.d1_{s.d1b5_value}.d2_{s.d2b5_value}"
        # for the_id in (CORRECTED_ID, UNCORRECTED_ID, NOMINAL_ID, DIFFABS_ID, TARGETDIFFABS_ID, RELDIFFABS_ID, RESIDUALDIFFABS_ID):
        # for the_id in (CORRECTED_ID, UNCORRECTED_ID, NOMINAL_ID):
        #     close_fig(plot_ampdet(study_dir=base_dir, the_id=the_id, order=1, setups=setups, out_label=out_label, ncol=3, ylim=[-150, 150], **AMPDET_KWARGS))
        #     close_fig(plot_ampdet(study_dir=base_dir, the_id=the_id, order=2, setups=setups, out_label=out_label, ncol=3, ylim=[-70, 70], **SO_AMPDET_KWARGS))
        #     close_fig(plot_chroma(study_dir=base_dir, the_id=the_id, order=2, setups=setups, out_label=out_label, ncol=3, ylim=[-20, 20], **SO_CHROMA_KWARGS))
        #     close_fig(plot_chroma(study_dir=base_dir, the_id=the_id, order=3, setups=setups, out_label=out_label, ncol=3, ylim=[-200, 200], **TO_CHROMA_KWARGS))

        for ip in (1, 5):
            close_fig(plot_rdt_un_corrected(study_dir=base_dir, setups=setups, out_label=out_label, rdt="f5000", ip=ip, ylim=[6e4, 1.4e12], **RDT_UN_CORRECTED_KWARGS))
            close_fig(plot_rdt_un_corrected(study_dir=base_dir, setups=setups, out_label=out_label, rdt="f1004", ip=ip, ylim=[6e4, 1.4e12], **RDT_UN_CORRECTED_KWARGS))


def hllhc_feeddown_sweep_d2b5_all_rdts():
    setup_def = dict(
        machine='hllhc',
        optics="round1515",
        xing="top",
        errors=ERRORS_ALL,
        corrector_strength='limited',
        correct_beam="same",
        feeddown=2,
        iterations=1,
        d1b5_value=(7, ),
        beam=(1, 4),
        correct_b5=('d1d2',),
    )
    for s in setup_iterator(**setup_def):
        setups = [
            dict(**s,
                 d2b5_value=d2b5_value,
                 rdts=f"b6-b6_a6-a6_{CORR_MAP[rdt]}_a5-a5_b4-b4_a4-a4_b3-b3_a3-a3",
                 rdts2=f"b6-b6_a6-a6_{CORR_MAP[rdt]}_a5-a5_b4-b4_a4-a4_b3-b3_a3-a3",
                 label=rdt_label(rdt),
                 xlabel=d2b5_value
                 )
            for d2b5_value in [0, 5, 10, 15]
            for rdt in ["b50", "b14"]
        ]
        out_label = f"{s.machine}.b{s.beam}.by_{s.correct_beam}.xing_{s.xing}.correctb5_{s.correct_b5}.rdt_sweep.corr_{s.corrector_strength}.fd_{s.feeddown}.d1_{s.d1b5_value}"
        close_fig(
            plot_sweep(
                base_dir=STUDY_DIR, output_dir=STUDY_DIR, out_label=out_label,
                setups=setups, da_col=ALOST2,
                ncol=3, ylim=[10.9, 22.2] if s.correct_b5 != 'no' else [5.9, 17.2],
                target=8.,
                plot_std=True, seeds=[],
                **DA_D2SWEEP_KWARGS
            )
        )


# Both Beams -------------------------------------------------------------------


def hllhc_sweep_d2b5_bothbeams_d1d2():
    setup_def = dict(
        machine='hllhc',
        optics="round1515",
        xing="top",
        errors=ERRORS_ALL,
        feeddown=0,
        corrector_strength='limited',
        correct_beam="same",
        correct_b5='d1d2',
        iterations=1,
        d1b5_value=(7, ),
        beam=(1, 4),
        rdt=('b50', 'b14'),
    )
    for s in setup_iterator(**setup_def):
        setups = [
            dict(**s,
                 rdts=f"b6-b6_a6-a6_{CORR_MAP[s.rdt]}_a5-a5_b4-b4_a4-a4_b3-b3_a3-a3",
                 rdts2=f"b6-b6_a6-a6_{CORR_MAP[s.rdt]}_a5-a5_b4-b4_a4-a4_b3-b3_a3-a3" if both else "None",
                 d2b5_value=d2b5_value,
                 label="both" if both else "single",
                 xlabel=d2b5_value
                 )
            for d2b5_value in [0, 5, 10, 15]
            for both in [False, True]
        ]
        out_label = f"{s.machine}.b{s.beam}.by_{s.correct_beam}.correctb5_{s.correct_b5}.rdt_{s.rdt}.corr_{s.corrector_strength}.fd_{s.feeddown}.d1_{s.d1b5_value}.d2_sweep.beams_sweep"
        close_fig(
            plot_sweep(
                base_dir=STUDY_DIR, output_dir=STUDY_DIR, out_label=out_label,
                setups=setups, da_col=ALOST2,
                ncol=1, ylim=[5.9, 18.2],
                target=8.,
                plot_std=True, seeds=[],
                **DA_D2SWEEP_KWARGS
            )
        )


def hllhc_sweep_d2b5_bothbeams_d1d2_ampdet():
    base_dir = STUDY_DIR
    setup_def = dict(
        machine='hllhc',
        optics="round1515",
        xing="top",
        errors=ERRORS_ALL,
        corrector_strength='limited',
        correct_beam="same",
        correct_b5='d1d2',
        iterations=1,
        feeddown=0,
        d1b5_value=(7, ),
        beam=(1, 4),
        rdt=('b50', 'b14'),
        d2b5_value=(0, 5, 10, 15),
    )
    for s in setup_iterator(**setup_def):
        setups = [
            dict(**s,
                 rdts=f"b6-b6_a6-a6_{CORR_MAP[s.rdt]}_a5-a5_b4-b4_a4-a4_b3-b3_a3-a3",
                 rdts2=f"b6-b6_a6-a6_{CORR_MAP[s.rdt]}_a5-a5_b4-b4_a4-a4_b3-b3_a3-a3" if both else "None",
                 label="both" if both else "single",
                 )
            for both in [False, True]
        ]
        out_label = f"{s.machine}.b{s.beam}.by_{s.correct_beam}.correctb5_{s.correct_b5}.rdt_{s.rdt}.corr_{s.corrector_strength}.fd_{s.feeddown}.d1_{s.d1b5_value}.d2_{s.d2b5_value}.beams_sweep"
        # for the_id in (CORRECTED_ID, UNCORRECTED_ID, NOMINAL_ID, DIFFABS_ID, TARGETDIFFABS_ID, RELDIFFABS_ID, RESIDUALDIFFABS_ID):
        # for the_id in (CORRECTED_ID, UNCORRECTED_ID, NOMINAL_ID):
        #     close_fig(plot_ampdet(study_dir=base_dir, the_id=the_id, order=1, setups=setups, out_label=out_label, ncol=3, ylim=[-150, 150], **AMPDET_KWARGS))
        #     close_fig(plot_ampdet(study_dir=base_dir, the_id=the_id, order=2, setups=setups, out_label=out_label, ncol=3, ylim=[-70, 70], **SO_AMPDET_KWARGS))
        #     close_fig(plot_chroma(study_dir=base_dir, the_id=the_id, order=2, setups=setups, out_label=out_label, ncol=3, ylim=[-20, 20], **SO_CHROMA_KWARGS))
        #     close_fig(plot_chroma(study_dir=base_dir, the_id=the_id, order=3, setups=setups, out_label=out_label, ncol=3, ylim=[-200, 200], **TO_CHROMA_KWARGS))

        for ip in (1, 5):
            close_fig(plot_rdt_un_corrected(study_dir=base_dir, setups=setups, out_label=out_label, rdt="f5000", ip=ip, ylim=[6e4, 1.4e12], **RDT_UN_CORRECTED_KWARGS))
            close_fig(plot_rdt_un_corrected(study_dir=base_dir, setups=setups, out_label=out_label, rdt="f1004", ip=ip, ylim=[6e4, 1.4e12], **RDT_UN_CORRECTED_KWARGS))


def hllhc_bothbeams_sweep_d2b5_all_rdts():
    setup_def = dict(
        machine='hllhc',
        optics="round1515",
        xing="top",
        errors=ERRORS_ALL,
        corrector_strength='limited',
        correct_beam="same",
        feeddown=0,
        iterations=1,
        d1b5_value=(7, ),
        beam=(1, 4),
        correct_b5=('d1d2',),
    )
    for s in setup_iterator(**setup_def):
        setups = [
            dict(**s,
                 d2b5_value=d2b5_value,
                 rdts=f"b6-b6_a6-a6_{CORR_MAP[rdt]}_a5-a5_b4-b4_a4-a4_b3-b3_a3-a3",
                 rdts2=f"b6-b6_a6-a6_{CORR_MAP[rdt]}_a5-a5_b4-b4_a4-a4_b3-b3_a3-a3",
                 label=rdt_label(rdt),
                 xlabel=d2b5_value
                 )
            for d2b5_value in [0, 5, 10, 15]
            for rdt in ["b50", "b14"]
        ]
        out_label = f"{s.machine}.b{s.beam}.by_{s.correct_beam}.xing_{s.xing}.correctb5_{s.correct_b5}.rdt_sweep.corr_{s.corrector_strength}.fd_{s.feeddown}.d1_{s.d1b5_value}.beams_both"
        close_fig(
            plot_sweep(
                base_dir=STUDY_DIR, output_dir=STUDY_DIR, out_label=out_label,
                setups=setups, da_col=ALOST2,
                ncol=3, ylim=[10.9, 22.2] if s.correct_b5 != 'no' else [5.9, 17.2],
                target=8.,
                plot_std=True, seeds=[],
                **DA_D2SWEEP_KWARGS
            )
        )

# Flat -------------------------------------------------------------------


def hllhc_flat_all_da_polar():
    base_dir = STUDY_DIR
    setup_def = dict(
        machine='hllhc',
        optics="round1515",
        xing="flat",
        errors=ERRORS_ALL,
        corrector_strength='limited',
        correct_beam="same",
        feeddown=0,
        iterations=1,
        d1b5_value=(7, ),
        beam=(1, 4),
        d2b5_value=(0, 5, 10, 15),
        correct_b5=('no', 'd1d2', ),
    )
    for s in setup_iterator(**setup_def):
        rdts = ("b50", "b14", "b32")
        setups = [
            dict(rdts=f"b6-b6_a6-a6_{CORR_MAP[rdt]}_a5-a5_b4-b4_a4-a4_b3-b3_a3-a3", rdts2="None", label=rdt_label(rdt), **s)
            for rdt in rdts
        ]
        out_label = f"{s.machine}.b{s.beam}.by_{s.correct_beam}.xing_{s.xing}.correctb5_{s.correct_b5}.rdt_sweep.corr_{s.corrector_strength}.fd_{s.feeddown}.d1_{s.d1b5_value}.d2_{s.d2b5_value}"
        close_fig(plot_da(base_dir=base_dir, setups=setups, out_label=out_label, output_dir=STUDY_DIR, rlim=16, plot_std=False, **DA_KWARGS))
        for idx, (rdt, setup) in enumerate(zip(rdts, setups)):
            out_label_rdt = f"{s.machine}.b{s.beam}.by_{s.correct_beam}.xing_{s.xing}.correctb5_{s.correct_b5}.rdt_{rdt}.corr_{s.corrector_strength}.fd_{s.feeddown}.d1_{s.d1b5_value}.d2_{s.d2b5_value}"
            close_fig(plot_da(base_dir=base_dir, setups=[setup], out_label=out_label_rdt, output_dir=STUDY_DIR, rlim=16, plot_std=False, color_idx_start=idx, **DA_KWARGS))

            for the_id in (CORRECTED_ID, UNCORRECTED_ID, NOMINAL_ID, DIFFABS_ID, TARGETDIFFABS_ID, RELDIFFABS_ID, RESIDUALDIFFABS_ID):
                close_fig(plot_ampdet(study_dir=base_dir, the_id=the_id, order=1, setups=setups, out_label=out_label_rdt, ncol=3, ylim=[-250, 250], **AMPDET_KWARGS))
                close_fig(plot_ampdet(study_dir=base_dir, the_id=the_id, order=2, setups=setups, out_label=out_label_rdt, ncol=3, ylim=[-70, 70], **SO_AMPDET_KWARGS))
                close_fig(plot_chroma(study_dir=base_dir, the_id=the_id, order=2, setups=setups, out_label=out_label_rdt, ncol=3, ylim=[-20, 20], **SO_CHROMA_KWARGS))
                close_fig(plot_chroma(study_dir=base_dir, the_id=the_id, order=3, setups=setups, out_label=out_label_rdt, ncol=3, ylim=[-200, 200], **TO_CHROMA_KWARGS))

        # for the_id in (CORRECTED_ID, UNCORRECTED_ID, NOMINAL_ID, DIFFABS_ID, TARGETDIFFABS_ID, RELDIFFABS_ID, RESIDUALDIFFABS_ID):
        for the_id in (CORRECTED_ID, UNCORRECTED_ID, NOMINAL_ID):
            close_fig(plot_ampdet(study_dir=base_dir, the_id=the_id, order=1, setups=setups, out_label=out_label, ncol=3, ylim=[-150, 150], **AMPDET_KWARGS))
            close_fig(plot_ampdet(study_dir=base_dir, the_id=the_id, order=2, setups=setups, out_label=out_label, ncol=3, ylim=[-70, 70], **SO_AMPDET_KWARGS))
            close_fig(plot_chroma(study_dir=base_dir, the_id=the_id, order=2, setups=setups, out_label=out_label, ncol=3, ylim=[-20, 20], **SO_CHROMA_KWARGS))
            close_fig(plot_chroma(study_dir=base_dir, the_id=the_id, order=3, setups=setups, out_label=out_label, ncol=3, ylim=[-200, 200], **TO_CHROMA_KWARGS))

        for ip in (1, 5):
            rdt_setups = modify_setups_rdt(setups)
            close_fig(plot_rdt_un_corrected(study_dir=base_dir, setups=rdt_setups, out_label=out_label, rdt="f5000", ip=ip, ylim=[6e4, 1.4e12], **RDT_UN_CORRECTED_KWARGS))
            close_fig(plot_rdt_un_corrected(study_dir=base_dir, setups=rdt_setups, out_label=out_label, rdt="f1004", ip=ip, ylim=[6e4, 1.4e12], **RDT_UN_CORRECTED_KWARGS))
            close_fig(plot_rdt_un_corrected(study_dir=base_dir, setups=rdt_setups, out_label=out_label, rdt="f3002", ip=ip, ylim=[6e4, 1.4e12], **RDT_UN_CORRECTED_KWARGS))


def hllhc_flat_sweep_d2b5_individual():
    setup_def = dict(
        machine='hllhc',
        optics="round1515",
        xing="flat",
        errors=ERRORS_ALL,
        corrector_strength='limited',
        correct_beam="same",
        feeddown=0,
        iterations=1,
        d1b5_value=(7, ),
        beam=(1, 4),
        correct_b5=('no', 'd1d2',),
    )
    for s in setup_iterator(**setup_def):
        for color_idx, rdt in enumerate(["b50", "b14", "b32"]):
            setups = [
                dict(**s,
                     d2b5_value=d2b5_value,
                     rdts=f"b6-b6_a6-a6_{CORR_MAP[rdt]}_a5-a5_b4-b4_a4-a4_b3-b3_a3-a3",
                     rdts2="None",
                     label=f"{rdt_label(rdt)}, {CORRB5_MAP[s.correct_b5]}",
                     xlabel=d2b5_value
                     )
                for d2b5_value in [0, 5, 10, 15]
            ]
            out_label = f"{s.machine}.b{s.beam}.by_{s.correct_beam}.xing_{s.xing}.correctb5_{s.correct_b5}.rdt_{rdt}.corr_{s.corrector_strength}.fd_{s.feeddown}.d1_{s.d1b5_value}"
            close_fig(
                plot_sweep(
                    base_dir=STUDY_DIR, output_dir=STUDY_DIR, out_label=out_label, setups=setups, da_col=ALOST2,
                    ylim=[10.9, 22.2],
                    target=8.,
                    colors=[color_idx],
                    **DA_D2SWEEP_KWARGS
                )
            )


def hllhc_flat_sweep_d2b5_all_rdts():
    setup_def = dict(
        machine='hllhc',
        optics="round1515",
        xing="flat",
        errors=ERRORS_ALL,
        corrector_strength='limited',
        correct_beam="same",
        feeddown=0,
        iterations=1,
        d1b5_value=(7, ),
        beam=(1, 4),
        correct_b5=('no', 'd1d2',),
    )
    for s in setup_iterator(**setup_def):
        setups = [
            dict(**s,
                 d2b5_value=d2b5_value,
                 rdts=f"b6-b6_a6-a6_{CORR_MAP[rdt]}_a5-a5_b4-b4_a4-a4_b3-b3_a3-a3", rdts2="None",
                 label=rdt_label(rdt),
                 xlabel=d2b5_value
                 )
            for d2b5_value in [0, 5, 10, 15]
            for rdt in ["b50", "b14", "b32"]
        ]
        out_label = f"{s.machine}.b{s.beam}.by_{s.correct_beam}.xing_{s.xing}.correctb5_{s.correct_b5}.rdt_sweep.corr_{s.corrector_strength}.fd_{s.feeddown}.d1_{s.d1b5_value}"
        close_fig(
            plot_sweep(
                base_dir=STUDY_DIR, output_dir=STUDY_DIR, out_label=out_label,
                setups=setups, da_col=ALOST2,
                ncol=3, ylim=[10.9, 22.2] if s.correct_b5 != 'no' else [5.9, 17.2],
                target=8.,
                plot_std=True, seeds=[],
                **DA_D2SWEEP_KWARGS
            )
        )


def hllhc_flat_corrections_sweep():
    base_dir = STUDY_DIR
    setup_def = dict(
        machine='hllhc',
        optics="round1515",
        xing=("flat", ),
        errors=ERRORS_ALL,
        corrector_strength='limited',
        correct_beam="same",
        feeddown=0,
        iterations=1,
        d1b5_value=(7, ),
        d2b5_value=(0, 5, 10, 15),
        beam=(1, 4),
    )
    for s in setup_iterator(**setup_def):
        for rdt in ("b50", "b14", "b32"):
            setups = [
                         dict(**s,
                              rdts=f"b6-b6_a6-a6_{CORR_MAP[rdt]}_a5-a5_b4-b4_a4-a4_b3-b3_a3-a3", rdts2="None",
                              correct_b5='no',  # does not matter
                              label=BEFORE_CORRECTION,  # triggers "uncorrected"
                              color="black",
                              )
                     ] + [
                         dict(**s,
                              rdts=f"b6-b6_a6-a6_{CORR_MAP[rdt]}_a5-a5_b4-b4_a4-a4_b3-b3_a3-a3", rdts2="None",
                              correct_b5=correct_b5,
                              label=CORRB5_MAP[correct_b5],
                              color=pcolors.get_mpl_color(idx)
                              )
                         for idx, correct_b5 in enumerate(['no', 'd1d2'])
                     ]

            out_label = f"{s.machine}.b{s.beam}.by_{s.correct_beam}.xing_{s.xing}.correctb5_sweep.rdt_{rdt}.corr_{s.corrector_strength}.fd_{s.feeddown}.d1_{s.d1b5_value}.d2_{s.d2b5_value}"
            for ip in (1, 5):
                # close_fig(plot_rdt_un_corrected_ids(study_dir=base_dir, setups=setups.copy(), out_label=out_label, rdt="f1004", ip=ip, ylim=[9e6, 1.4e12], ncol=2, **RDT_UN_CORRECTED_KWARGS))
                # close_fig(plot_rdt_un_corrected_ids(study_dir=base_dir, setups=setups.copy(), out_label=out_label, rdt="f5000", ip=ip, ylim=[9e6, 1.4e12], ncol=2, **RDT_UN_CORRECTED_KWARGS))
                close_fig(plot_rdt_un_corrected_ids(study_dir=base_dir, setups=setups.copy(), out_label=out_label, rdt="f3002", ip=ip, ylim=[9e6, 1.4e12], ncol=2, **RDT_UN_CORRECTED_KWARGS))

            # setups = setups + [
            #     dict(**s,
            #          rdts=f"b6-b6_a6-a6_{CORR_MAP[rdt]}_a5-a5_b4-b4_a4-a4_b3-b3_a3-a3", rdts2="None",
            #          correct_b5='no',  # does not matter
            #          label=NOMINAL_ID,  # triggers "nominal"
            #          color="gray",
            #          )
            # ]
            #
            # close_fig(plot_ampdet(study_dir=base_dir, the_id=CORRECTED_ID, order=1, setups=setups, out_label=out_label, ncol=2, ylim=[-250, 250], **AMPDET_KWARGS))
            # close_fig(plot_ampdet(study_dir=base_dir, the_id=CORRECTED_ID, order=2, setups=setups, out_label=out_label, ncol=2, ylim=[-70, 70], **SO_AMPDET_KWARGS))
            # close_fig(plot_chroma(study_dir=base_dir, the_id=CORRECTED_ID, order=2, setups=setups, out_label=out_label, ncol=2, ylim=[-20, 20], **SO_CHROMA_KWARGS))
            # close_fig(plot_chroma(study_dir=base_dir, the_id=CORRECTED_ID, order=3, setups=setups, out_label=out_label, ncol=2, ylim=[-30, 30], **TO_CHROMA_KWARGS))
            # # close_fig(plot_chroma(study_dir=base_dir, the_id=CORRECTED_ID, order=4, setups=setups, out_label=out_label, ncol=2, ylim=[-200, 200], **FO_CHROMA_KWARGS))



def hllhc_flat_sweep_d2b5_rdtsf50f14():
    setup_def = dict(
        machine='hllhc',
        optics="round1515",
        xing="flat",
        errors=ERRORS_ALL,
        corrector_strength='limited',
        correct_beam="same",
        feeddown=0,
        iterations=1,
        d1b5_value=(7, ),
        beam=(1, 4),
        correct_b5=('no', 'd1d2',),
    )
    for s in setup_iterator(**setup_def):
        setups = [
            dict(**s,
                 d2b5_value=d2b5_value,
                 rdts=f"b6-b6_a6-a6_{CORR_MAP[rdt]}_a5-a5_b4-b4_a4-a4_b3-b3_a3-a3", rdts2="None",
                 label=rdt_label(rdt),
                 xlabel=d2b5_value
                 )
            for d2b5_value in [0, 5, 10, 15]
            for rdt in ["b50", "b14"]
        ]
        out_label = f"{s.machine}.b{s.beam}.by_{s.correct_beam}.xing_{s.xing}.correctb5_{s.correct_b5}.rdt_f50f14.corr_{s.corrector_strength}.fd_{s.feeddown}.d1_{s.d1b5_value}"
        close_fig(
            plot_sweep(
                base_dir=STUDY_DIR, output_dir=STUDY_DIR, out_label=out_label,
                setups=setups, da_col=ALOST2,
                ncol=3, ylim=[10.9, 22.2] if s.correct_b5 != 'no' else [5.9, 17.2],
                target=8.,
                plot_std=True, seeds=[],
                **DA_D2SWEEP_KWARGS
            )
        )


def hllhc_flat_ampdet_rdtsf50f14():
    base_dir = STUDY_DIR
    setup_def = dict(
        machine='hllhc',
        optics="round1515",
        xing="flat",
        errors=ERRORS_ALL,
        corrector_strength='limited',
        correct_beam="same",
        feeddown=0,
        iterations=1,
        d1b5_value=(7, ),
        beam=(1, 4),
        d2b5_value=(0, 5, 10, 15),
        correct_b5=('no', 'd1d2', ),
    )
    for s in setup_iterator(**setup_def):
        rdts = ("b50", "b14",)
        setups = [
            dict(rdts=f"b6-b6_a6-a6_{CORR_MAP[rdt]}_a5-a5_b4-b4_a4-a4_b3-b3_a3-a3", rdts2="None", label=rdt_label(rdt), **s)
            for rdt in rdts
        ]
        out_label = f"{s.machine}.b{s.beam}.by_{s.correct_beam}.xing_{s.xing}.correctb5_{s.correct_b5}.rdt_f50f14.corr_{s.corrector_strength}.fd_{s.feeddown}.d1_{s.d1b5_value}.d2_{s.d2b5_value}"
        close_fig(plot_da(base_dir=base_dir, setups=setups, out_label=out_label, output_dir=STUDY_DIR, rlim=16, plot_std=False, **DA_KWARGS))

        # for the_id in (CORRECTED_ID, UNCORRECTED_ID, NOMINAL_ID, DIFFABS_ID, TARGETDIFFABS_ID, RELDIFFABS_ID, RESIDUALDIFFABS_ID):
        for the_id in (CORRECTED_ID, UNCORRECTED_ID, NOMINAL_ID):
            close_fig(plot_ampdet(study_dir=base_dir, the_id=the_id, order=1, setups=setups, out_label=out_label, ncol=3, ylim=[-250, 250], **AMPDET_KWARGS))
            close_fig(plot_ampdet(study_dir=base_dir, the_id=the_id, order=2, setups=setups, out_label=out_label, ncol=3, ylim=[-70, 70], **SO_AMPDET_KWARGS))
            close_fig(plot_chroma(study_dir=base_dir, the_id=the_id, order=2, setups=setups, out_label=out_label, ncol=3, ylim=[-20, 20], **SO_CHROMA_KWARGS))
            close_fig(plot_chroma(study_dir=base_dir, the_id=the_id, order=3, setups=setups, out_label=out_label, ncol=3, ylim=[-200, 200], **TO_CHROMA_KWARGS))

        # for ip in (1, 5):
        #     rdt_setups = modify_setups_rdt(setups)
        #     close_fig(plot_rdt_un_corrected(study_dir=base_dir, setups=rdt_setups, out_label=out_label, rdt="f5000", ip=ip, ylim=[6e4, 1.4e12], **RDT_UN_CORRECTED_KWARGS))
        #     close_fig(plot_rdt_un_corrected(study_dir=base_dir, setups=rdt_setups, out_label=out_label, rdt="f1004", ip=ip, ylim=[6e4, 1.4e12], **RDT_UN_CORRECTED_KWARGS))
        #     close_fig(plot_rdt_un_corrected(study_dir=base_dir, setups=rdt_setups, out_label=out_label, rdt="f3002", ip=ip, ylim=[6e4, 1.4e12], **RDT_UN_CORRECTED_KWARGS))

# D1 Sweep ---------------------------------------------------------------------


def hllhc_sweep_d1b5():
    setup_def = dict(
        machine='hllhc',
        optics="round1515",
        xing="top",
        errors=ERRORS_ALL,
        corrector_strength='limited',
        correct_beam="same",
        feeddown=0,
        iterations=1,
        d2b5_value=(0, 10),
        beam=(1, 4),

    )
    for s in setup_iterator(**setup_def):
        for rdt in ("b50", "b14"):
            setups = [
                dict(**s,
                     rdts=f"b6-b6_a6-a6_{CORR_MAP[rdt]}_a5-a5_b4-b4_a4-a4_b3-b3_a3-a3", rdts2="None",
                     correct_b5=correct_b5,
                     d1b5_value=d1b5_value,
                     label=CORRB5_MAP[correct_b5],
                     xlabel=d1b5_value
                     )
                for d1b5_value in [0, 5, 10, 15]
                for correct_b5 in ['no', 'd1d2']
            ]
            out_label = f"{s.machine}.b{s.beam}.by_{s.correct_beam}.correctb5_no_d1d2.rdt_{rdt}.corr_{s.corrector_strength}.fd_{s.feeddown}.d1_sweep.d2_{s.d2b5_value}"
            close_fig(
                plot_sweep(
                    base_dir=STUDY_DIR, output_dir=STUDY_DIR, out_label=out_label,
                    setups=setups, da_col=ALOST2,
                    ncol=1, ylim=[3.9, 18.2],
                    target=8.,
                    plot_std=True, seeds=[],
                    **DA_D1SWEEP_KWARGS
                )
            )


def hllhc_sweep_d1b5_individual():
    base_dir = STUDY_DIR
    setup_def = dict(
        machine='hllhc',
        optics="round1515",
        xing="top",
        d2b5_value=(0, 10,),
        errors=ERRORS_ALL,
        corrector_strength='limited',
        correct_beam="same",
        correct_b5=('no', 'd1d2'),
        feeddown=0,
        iterations=1,
        beam=(1, 4),
    )
    for s in setup_iterator(**setup_def):
        for color_idx, rdt in enumerate(["b50", "b14"]):
            setups = [
                dict(rdts=f"b6-b6_a6-a6_{CORR_MAP[rdt]}_a5-a5_b4-b4_a4-a4_b3-b3_a3-a3",
                     rdts2="None",
                     d1b5_value=d1b5_value,
                     label=f"{rdt_label(rdt)}",
                     xlabel=d1b5_value,
                     **s)
                for d1b5_value in [0, 5, 10, 15]
            ]
            out_label = f"{s.machine}.b{s.beam}.by_{s.correct_beam}.correctb5_{s.correct_b5}.rdt_{rdt}.corr_{s.corrector_strength}.fd_{s.feeddown}.d1_sweep.d2_{s.d2b5_value}"
            # for ip in (1, 5):
                # close_fig(plot_rdt_un_corrected(study_dir=base_dir, setups=setups, out_label=out_label, rdt="f5000", ip=ip, ylim=[6e4, 1.4e12], **RDT_UN_CORRECTED_KWARGS))
                # close_fig(plot_rdt_un_corrected(study_dir=base_dir, setups=setups, out_label=out_label, rdt="f1004", ip=ip, ylim=[6e4, 1.4e12], **RDT_UN_CORRECTED_KWARGS))
                # close_fig(plot_correctors(study_dir=base_dir, setups=setups, out_label=out_label, field_component="b5", ip=ip, ncol=2, xlim=[-6, 3], ylim=[0, 6], **CORRECTOR_KWARGS))

            # for the_id in (CORRECTED_ID, UNCORRECTED_ID, NOMINAL_ID, DIFFABS_ID, TARGETDIFFABS_ID, RELDIFFABS_ID, RESIDUALDIFFABS_ID):
            #     close_fig(plot_ampdet(study_dir=base_dir, the_id=the_id, order=1, setups=setups, out_label=out_label, ncol=2, ylim=[-250, 350], **AMPDET_KWARGS))
            #     close_fig(plot_ampdet(study_dir=base_dir, the_id=the_id, order=2, setups=setups, out_label=out_label, ncol=2, ylim=[-50, 50], **SO_AMPDET_KWARGS))
            #     close_fig(plot_chroma(study_dir=base_dir, the_id=the_id, order=2, setups=setups, out_label=out_label, ncol=2, ylim=[-6, 6], **SO_CHROMA_KWARGS))
            #     close_fig(plot_chroma(study_dir=base_dir, the_id=the_id, order=3, setups=setups, out_label=out_label, ncol=2, ylim=[-80, 80], **TO_CHROMA_KWARGS))
            #     pass
            close_fig(
                plot_sweep(
                    base_dir=base_dir, output_dir=base_dir, out_label=out_label,
                    setups=setups, da_col=ALOST2,
                    ncol=1, ylim=[5.9, 18.2],
                    target=8.,
                    plot_std=False,
                    colors=[color_idx],
                    **DA_D1SWEEP_KWARGS
                )
            )
            for setup in setups:
                out_label_setup = f"{s.machine}.b{s.beam}.by_{s.correct_beam}.correctb5_{s.correct_b5}.rdt_{rdt}.corr_{s.corrector_strength}.fd_{s.feeddown}.d1_{setup['d1b5_value']}.d2_{s.d2b5_value}"
                close_fig(
                    plot_da(base_dir=base_dir,
                            setups=[setup],
                            out_label=out_label_setup,
                            output_dir=base_dir,
                            rlim=16,
                            plot_std=False,
                            color_idx_start=color_idx,
                            **DA_KWARGS)
                )


def hllhc_sweep_d1b5_all_rdts():
    setup_def = dict(
        machine='hllhc',
        optics="round1515",
        xing="top",
        errors=ERRORS_ALL,
        corrector_strength='limited',
        correct_beam="same",
        feeddown=0,
        iterations=1,
        d2b5_value=(0, 10),
        beam=(1, 4),
        correct_b5=('no', 'd1d2'),
    )
    for s in setup_iterator(**setup_def):
        setups = [
            dict(**s,
                 d1b5_value=d1b5_value,
                 rdts=f"b6-b6_a6-a6_{CORR_MAP[rdt]}_a5-a5_b4-b4_a4-a4_b3-b3_a3-a3", rdts2="None",
                 label=rdt_label(rdt),
                 xlabel=d1b5_value
                 )
            for d1b5_value in [0, 5, 10, 15]
            for rdt in ["b50", "b14"]
        ]
        out_label = f"{s.machine}.b{s.beam}.by_{s.correct_beam}.correctb5_{s.correct_b5}.rdt_sweep.corr_{s.corrector_strength}.fd_{s.feeddown}.d1_sweep.d2_{s.d2b5_value}"
        close_fig(
            plot_sweep(
                base_dir=STUDY_DIR, output_dir=STUDY_DIR, out_label=out_label,
                setups=setups, da_col=ALOST2,
                ncol=3, ylim=[5.9, 18.2],
                target=8.,
                plot_std=True, seeds=[],
                **DA_D1SWEEP_KWARGS
            )
        )


# Both Beams ---------------------------------------------------------------------------------------


if __name__ == '__main__':
    # Gather
    # gather_ptc_beams(update=True)
    # gather_twiss_beams(update=True)
    # exit()

    # Test
    # hllhc_plot_all_da_polar_test()

    # Sweeps
    # hllhc_sweep_d2b5_individual()
    # hllhc_sweep_d2b5_all_rdts()
    # hllhc_sweep_d2b5_individual_b50only()
    # hllhc_sweep_d2b5_all_corrections()
    # hllhc_sweep_d2b5_all_corrections_b50only()
    # hllhc_sweep_d2b5_all_corrections_b50only_splitcorrections()
    # hllhc_sweep_d2b5_all_d1b5_b50only()

    # Both
    # hllhc_plot_all_da_polar()

    # Ampdets
    # hllhc_compare_d2b5_15()
    # hllhc_compare_d2b5_10_with_d1_sweep()
    # hllhc_corr_and_ampdet_changes_d2b5()
    # hllhc_sweep_d2b5_feeddown_d1d2_ampdet()
    # plt.show()

    # Flat Optics
    # hllhc_flat_all_da_polar()
    # hllhc_flat_sweep_d2b5_individual()
    # hllhc_flat_sweep_d2b5_all_rdts()
    # hllhc_flat_corrections_sweep()
    hllhc_flat_ampdet_rdtsf50f14()
    # hllhc_flat_sweep_d2b5_rdtsf50f14()

    # Feeddown
    # hllhc_sweep_d2b5_feeddown_d1d2()
    # hllhc_sweep_d2b5_feeddown_d1d2_ampdet()
    # hllhc_feeddown_sweep_d2b5_all_rdts()

    # Both Beams
    # hllhc_sweep_d2b5_bothbeams_d1d2()
    # hllhc_sweep_d2b5_bothbeams_d1d2_ampdet()
    # hllhc_bothbeams_sweep_d2b5_all_rdts()

    # D1
    # hllhc_sweep_d1b5()
    # hllhc_sweep_d1b5_individual()
    # hllhc_sweep_d1b5_all_rdts()


    # hllhc_corrections_sweep()


