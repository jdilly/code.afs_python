from pathlib import Path
from typing import List
import logging
import re

from correct_irnl_run_helper import get_beams_jobid
import tfs
import numpy as np

# config
NOMINAL_ID, CORRECTED_ID, UNCORRECTED_ID = "nominal", "corrected", "uncorrected",
SEEDS = range(1, 61)
GATHER_FOLDER = "Results"
BEFORE_CORRECTION = "before corr."

SEED_COLUMN = "SEED"
JOB_ID_COLUMN = "JOB_ID"
MIN, MAX, MEAN, STD, DELTA = "MIN", "MAX", "MEAN", "STD", "DELTA"

PREFIX_FUN_MAP = {
    MIN: np.min,
    MEAN: np.mean,
    MAX: np.max,
    STD: np.std,
}


SEED_DIR_MASK = "Seed.{seed:d}"


LOG = logging.getLogger(__name__)


def afs_to_eos_path(path: Path):
    path_str = str(path)
    if path_str.startswith("/afs"):
        return path
    return Path(path_str.replace("/sixjobs/", "/").replace("/workspace-", "/Job."))


def get_jobdir_name(setup: dict) -> str:
    return jobid_to_dir(get_beams_jobid(**setup))


def get_seeddir_name(seed: int) -> str:
    return SEED_DIR_MASK.format(seed=seed)


def get_working_dirs(main_dir: Path, beam: int, machine: str) -> List[Path]:
    job_dirs = list(main_dir.glob(f"Job.{machine}.b{beam}.*"))
    return job_dirs


def dir_to_jobid(path: Path) -> str:
    return path.name.replace("Job.", "", 1)


def jobid_to_dir(jobid: str) -> str:
    return f"Job.{jobid}"


def no_correction_name(name: str):
    matches = re.findall("rdts_([^.]+)", name)
    return all(m == "None" for m in matches)


def delta_columns(old_columns):
    return [f"{DELTA}{col}" for col in old_columns]


def stats_columns(old_columns):
    return [f"{pre}{col}" for col in old_columns for pre in PREFIX_FUN_MAP.keys()]


def seed_columns(old_columns):
    return [f"{pre}{col}" for col in old_columns for pre in ('MINSEED', 'MAXSEED')]


def gather_all(function, *args, **kwargs):
    for machine in ('lhc', 'hllhc'):
        for beam in (1, 4):
            function(*args, **kwargs, machine=machine, beam=beam)