from pathlib import Path
from typing import Sequence, Iterable, Union

import pandas as pd
import numpy as np
import tfs
from omc3.optics_measurements.constants import NAME
from omc3.plotting.utils import style as pstyle, annotations as pannot, colors as pcolors
from matplotlib.collections import PolyCollection
from matplotlib import pyplot as plt, ticker
import matplotlib.patches as mpatches

from analysis.analyse_log_output import get_df_name
from analysis.analyse_ptc_output import RESIDUALDIFFABS_ID, TARGETDIFFABS_ID, DIFFABS_ID, DIFF_ID, RELDIFFABS_ID
from analysis.file_crawler import (
    get_working_dirs, dir_to_jobid, SEED_DIR_MASK, GATHER_FOLDER, SEEDS,
    NOMINAL_ID, CORRECTED_ID, UNCORRECTED_ID, JOB_ID_COLUMN, SEED_COLUMN, gather_all, DELTA, get_jobdir_name,
    BEFORE_CORRECTION,
)
from correct_irnl_run_helper import get_beams_jobid
from irnl_rdt_correction.irnl_rdt_correction import get_elements_integral, RDT, switch_signs_for_beams, _check_dfs
from omc3.utils import logging_tools
LOG = logging_tools.get_logger(__name__)

IP = "IP"
IPS = (1, 2, 5, 8)

IDS = (NOMINAL_ID, UNCORRECTED_ID, CORRECTED_ID)  # order is important

TWISS_MASK_IN = 'twiss.{machine:s}.b{beam:d}.{id:s}.tfs'
ERRORS_MASK_IN = 'errors.{machine:s}.b{beam:d}.{id:s}.tfs'
RDT_MASK_SEEDS = '{prefix:s}twiss_rdts.{machine:s}.b{beam:d}.{id:s}.seeds.tfs'
RDT_MASK_STATS = '{prefix:s}twiss_rdts.{machine:s}.b{beam:d}.{id:s}.statistics.tfs'


def gather_twiss_rdt_data(machine, beam, rdts,  study_dir: Path, seeds: Sequence[int] = SEEDS,
                          feeddown: int = 0, update: bool = False):
    LOG.info(f"Gathering Data from twiss output {machine}.b{beam} in {str(study_dir)}.")
    if isinstance(study_dir, Path):
        working_dirs = get_working_dirs(study_dir, beam, machine)
        output_dir = study_dir / GATHER_FOLDER
    else:
        working_dirs = []
        for current_dir in study_dir:
            working_dirs.append(get_working_dirs(current_dir, beam, machine))
        output_dir = study_dir[0] / GATHER_FOLDER
    output_dir.mkdir(exist_ok=True)

    rdts = [RDT(rdt) for rdt in rdts]
    needed_orders = range(1, max(rdt.order for rdt in rdts) + feeddown+1)
    job_ids = [dir_to_jobid(cwd) for cwd in working_dirs]
    df = tfs.TfsDataFrame(index=pd.MultiIndex.from_product([job_ids, seeds, IPS], names=[JOB_ID_COLUMN, SEED_COLUMN, IP]),
                          columns=[f"{rdt.name}_{id_}" for rdt in rdts for id_ in IDS])

    df_out_name = get_df_name(machine, beam, 'twiss_rdt')

    for ndirs, cwd in enumerate(working_dirs):
        LOG.info(f"Working dir: {str(cwd.name)} -----------")
        jobid = dir_to_jobid(cwd)

        results_per_job_path = cwd / GATHER_FOLDER / df_out_name

        if results_per_job_path.exists() and update:
            LOG.info(f"Loading from file: {results_per_job_path!s}")
            df_job = tfs.read(results_per_job_path).set_index([JOB_ID_COLUMN, SEED_COLUMN, IP])

        else:
            results_per_job_path.parent.mkdir(exist_ok=True)

            df_job = tfs.TfsDataFrame(index=pd.MultiIndex.from_product([[jobid], seeds, IPS], names=[JOB_ID_COLUMN, SEED_COLUMN, IP]),
                                      columns=[f"{rdt.name}_{id_}" for rdt in rdts for id_ in IDS])

            for seed in seeds:
                input_dir = cwd / SEED_DIR_MASK.format(seed=seed)
                LOG.info(f"  SEED dir: {str(input_dir.name)} ----------")
                df_twiss_ir = get_tfs(input_dir / TWISS_MASK_IN.format(machine=machine, beam=beam, id="optics_ir"))

                for id_ in IDS:
                    LOG.info(f"  ID: {id_} ----------")
                    df_twiss = get_tfs(input_dir / TWISS_MASK_IN.format(machine=machine, beam=beam, id=id_))
                    df_twiss = df_twiss.loc[df_twiss_ir.index, :]
                    for plane in "XY":
                        if plane not in df_twiss.columns:
                            df_twiss[plane] = 0

                    if id_ == NOMINAL_ID:  # order of IDs is important!
                        # First in loop
                        df_twiss_nom = df_twiss.copy()
                        # load 'all' as 'ir' is what's used for correction, but might have elements set to 0 to ignore their errors
                        df_error_loaded = get_tfs(input_dir / ERRORS_MASK_IN.format(machine=machine, beam=beam, id="all")).loc[df_twiss_ir.index, :]
                        df_error = tfs.TfsDataFrame(0., index=df_error_loaded.index, columns=set(["DX", "DY"] + df_error_loaded.columns.to_list()))
                    elif id_ == UNCORRECTED_ID:
                        # Second in loop, afterwards df_error is set
                        df_error[df_error_loaded.columns] = df_error_loaded

                    if id_ == "updated":
                        # tfs.write("test_num.tfs", df_twiss_nom)
                        # tfs.write("test_up.tfs", df_twiss)
                        df_twiss[["BETX", "BETY"]] = df_twiss_nom[["BETX", "BETY"]]

                    _check_dfs((df_twiss,), (df_error,), (beam,), orders=needed_orders, ignore_missing_columns=True)
                    switch_signs_for_beams((df_twiss,), (df_error,), (beam,))
                    for rdt in rdts:
                        LOG.info(f"  RDT: {rdt.name}")
                        for ip in IPS:
                            value = get_elements_integral(rdt, ip, df_twiss, df_error, feeddown)  # takes all indices from df_twiss for one IP
                            LOG.info(f"  IP{ip}: {value:.2e}")

                            df_job.loc[(jobid, seed, ip), f"{rdt.name}_{id_}"] = value
            tfs.write(results_per_job_path, df_job.reset_index())
        df.loc[df_job.index, :] = df_job
    tfs.write(output_dir / df_out_name, df.reset_index())


def get_tfs(path: Path):
    return tfs.read(path, index=NAME)


def plot_rdts(study_dir, rdts, ip, the_id, setups, label, scale, name, **kwargs):
    # LOAD DATA ----------
    all_beams = set(s['beam'] for s in setups)
    all_machines = set(s['machine'] for s in setups)
    df_data = {m: {b: None for b in all_beams} for m in all_machines}
    idxSlice = pd.IndexSlice

    for machine in all_machines:
        for beam in all_beams:
            df_data[machine][beam] = get_data_rdts(study_dir, machine, beam, the_id, rdts)

    # STYLE -------
    manual = {u"figure.figsize": [10.24, 5.12],
              u"markers.fillstyle": u'none', u'grid.alpha': 0,
              u'savefig.format': u'pdf'}

    ncol: np.numeric = kwargs.pop('ncol', 3)
    ylim: np.numeric = kwargs.pop('ylim', None)
    yticks: Iterable[np.numeric] = kwargs.pop('yticks', None)
    xtickrotation: Iterable[np.numeric] = kwargs.pop('xtick.rotation', None)
    plot_styles: Iterable[Union[Path, str]] = kwargs.pop('plot_styles', 'standard')

    manual.update(kwargs)
    pstyle.set_style(plot_styles, manual)

    # DELTA ------
    delta = ''
    njobs = len(setups)
    if the_id in (DIFF_ID, DIFFABS_ID, RELDIFFABS_ID, TARGETDIFFABS_ID, RESIDUALDIFFABS_ID):
        njobs = njobs - sum([(s["rdts"] is None) or (s["rdts"] == "None") for s in setups])
        delta = DELTA
        if the_id in (DIFFABS_ID, TARGETDIFFABS_ID, RESIDUALDIFFABS_ID):
            split = label.split()
            if len(split) == 1:
                split += ['']
            label = f'|{split[0]}|{split[1]}'
        if the_id == RELDIFFABS_ID:
            label = f'$\Delta_{{rel}}${label}'
            scale, scale_txt = 1, ""
        else:
            label = f'$\Delta${label}'

    bar_width = 1/(njobs + 1)
    xlim = [- bar_width/2, (len(rdts) - 1) + bar_width * (njobs + 0.5)]

    fig, ax = plt.subplots()

    # plot zero line
    ax.axhline(0, color="black", lw=1, ls="-", marker="", zorder=0)

    # plot separation lines
    for idx in range(1, len(rdts)):
        ax.axvline(idx-bar_width/2, ls="--", lw=1, color="black", alpha=0.2, marker="", zorder=-5)

    handles = []
    violin_idx = 0
    for job_idx, job_setup in enumerate(setups):
        job_id = get_beams_jobid(**job_setup)
        job_label = job_setup.pop("label", job_id)
        job_setup["label"] = job_label

        if job_setup["rdts"] is None or job_setup["rdts"] == "None":
            if the_id in (DIFF_ID, DIFFABS_ID):
                continue

        color = pcolors.get_mpl_color(job_idx)

        # plot data
        handles.append(mpatches.Patch(color=color, label=job_label))
        for idx, rdts in enumerate(rdts):
            x_pos = idx + bar_width * (violin_idx + 0.5)

            seed_data = df_data[job_setup['machine']][job_setup['beam']].loc[idxSlice[job_id, :, ip]] * scale
            std = seed_data.std()
            mean = seed_data.mean()
            std_min, std_max = mean-std, mean+std

            violin = ax.violinplot(dataset=seed_data, positions=[x_pos], widths=bar_width/2, showmeans=True, showextrema=True)

            for poly in violin['bodies']:
                poly.set_facecolor(color)
                poly.set_alpha(0.2)

                std_violin = PolyCollection([[v for v in poly.get_paths()[0].vertices if std_min <= v[1] <= std_max]])
                std_violin.set_facecolor(color)
                std_violin.set_alpha(0.4)
                ax.add_collection(std_violin)

            violin['cbars'].set_color(color)
            violin['cmeans'].set_color(color)

            for line_collection in ['cmaxes', 'cmins']:
                violin[line_collection].set_color(color)
                y_pos = violin[line_collection].get_segments()[0][0][1]
                violin[line_collection].set_segments([[[x_pos-bar_width/20, y_pos], [x_pos+bar_width/20, y_pos]]])

        violin_idx += 1

    ax.set_ylabel(label)
    ax.set_xticks(np.arange(len(rdts)) + (bar_width*njobs)/2)
    ax.set_xticklabels([f"$f_{r[1:]}$" for r in rdts], rotation=xtickrotation)
    ax.set_xlim(xlim)

    if yticks is not None:
        ax.set_yticks(yticks)
    ax.set_ylim(ylim)

    # handles, labels = ax.get_legend_handles_labels()
    # order = [1, 0, 2, 4, 3, 5]  # don't ask
    # pannot.make_top_legend(ax,
    #                        handles=_sort_legend(handles, order), labels=_sort_legend(labels, order),
    #                        ncol=2, frame=False)
    pannot.make_top_legend(ax, handles=handles, ncol=ncol, frame=False)

    fig.tight_layout()
    return fig


def plot_rdt_un_corrected(study_dir, rdt, setups, out_label, ip, **kwargs):
    output_dir = study_dir / GATHER_FOLDER

    fig = plot_rdt_before_after(study_dir, rdt, ip, setups, **kwargs)
    pannot.set_name(f'{rdt}.un_corrected.IP{ip}.{out_label}', fig)
    fig.savefig(output_dir / f"plot.{fig.canvas.get_default_filename()}")
    return fig


def plot_rdt_before_after(study_dir, rdt, ip, setups, **kwargs):
    # LOAD DATA ----------
    all_beams = set(s['beam'] for s in setups)
    all_machines = set(s['machine'] for s in setups)
    df_data = {m: {b: None for b in all_beams} for m in all_machines}
    idxSlice = pd.IndexSlice

    for machine in all_machines:
        for beam in all_beams:
            df_data[machine][beam] = get_data_before_after(study_dir, machine, beam, rdt)

    # STYLE -------
    manual = {u"figure.figsize": [10.24, 5.12],
              u"markers.fillstyle": u'none', u'grid.alpha': 0,
              u'savefig.format': u'pdf'}

    ncol: np.numeric = kwargs.pop('ncol', 3)
    ylim: np.numeric = kwargs.pop('ylim', None)
    xtickrotation: Iterable[np.numeric] = kwargs.pop('xtick.rotation', None)
    plot_styles: Iterable[Union[Path, str]] = kwargs.pop('plot_styles', 'standard')

    manual.update(kwargs)
    pstyle.set_style(plot_styles, manual)

    # DELTA ------
    label = fr"$|f^\mathrm{{\;IP{ip}}}_{{{rdt[1:]}}}|$"
    njobs = len(setups)
    columns = [UNCORRECTED_ID, CORRECTED_ID, DIFFABS_ID]
    column_str = ["before corr.", "after corr.", "$\Delta$"]
    ncolumns = len(columns)

    bar_width = 1/(ncolumns + 1)
    xlim = [- bar_width/2, (njobs - 1) + bar_width * (ncolumns + 0.5)]

    fig, ax = plt.subplots()

    # plot zero line
    ax.axhline(0, color="black", lw=1, ls="-", marker="", zorder=0)

    # plot separation lines
    for idx in range(1, len(setups)):
        ax.axvline(idx-bar_width/2, ls="--", lw=1, color="black", alpha=0.2, marker="", zorder=-5)

    handles = [mpatches.Patch(color=pcolors.get_mpl_color(ii), label=l) for ii, l in enumerate(column_str)]
    for job_idx, job_setup in enumerate(setups):
        job_id = get_beams_jobid(**job_setup)
        job_label = job_setup.pop("label", job_id)
        job_setup["label"] = job_label

        # plot data
        for idx, the_id in enumerate(columns):
            color = pcolors.get_mpl_color(idx)
            x_pos = job_idx + bar_width * (idx + 0.5)

            seed_data = df_data[job_setup['machine']][job_setup['beam']].loc[idxSlice[job_id, :, ip], the_id]
            std = seed_data.std()
            mean = seed_data.mean()
            std_min, std_max = mean-std, mean+std

            violin = ax.violinplot(dataset=seed_data, positions=[x_pos], widths=bar_width/2, showmeans=True, showextrema=True)

            for poly in violin['bodies']:
                poly.set_facecolor(color)
                poly.set_alpha(0.2)

                std_violin = PolyCollection([[v for v in poly.get_paths()[0].vertices if std_min <= v[1] <= std_max]])
                std_violin.set_facecolor(color)
                std_violin.set_alpha(0.4)
                ax.add_collection(std_violin)

            violin['cbars'].set_color(color)
            violin['cmeans'].set_color(color)

            for line_collection in ['cmaxes', 'cmins']:
                violin[line_collection].set_color(color)
                y_pos = violin[line_collection].get_segments()[0][0][1]
                violin[line_collection].set_segments([[[x_pos-bar_width/20, y_pos], [x_pos+bar_width/20, y_pos]]])

    ax.set_ylabel(label)
    # ax.set_xticks(np.arange(2) + (bar_width*njobs)/2)
    # x_pos = job_idx + bar_width * (idx + 0.5)
    # ax.set_xticks(np.arange(njobs).repeat(ncolumns) + bar_width * (np.tile(np.arange(ncolumns), njobs) + 0.5))
    # ax.set_xticklabels(["uncorrected", "corrected", "$\Delta$"]*njobs, rotation=xtickrotation)
    ax.set_xticks(np.arange(njobs)+0.5-bar_width/2)
    ax.set_xticklabels([s['label'] for s in setups], rotation=xtickrotation)
    ax.set_xlim(xlim)


    ax.set_ylim(ylim)
    ax.set_yscale("log")

    y_minor = ticker.LogLocator(base=10.0, subs=np.arange(1.0, 10.0) * 0.1, numticks=10)
    ax.yaxis.set_minor_locator(y_minor)
    ax.yaxis.set_minor_formatter(ticker.NullFormatter())

    y_major = ticker.LogLocator(base=10.0, subs=np.arange(1, 10.0), numticks=10)
    ax.yaxis.set_major_locator(y_major)

    pannot.make_top_legend(ax, handles=handles, ncol=ncol, frame=False)

    fig.tight_layout()
    return fig


def plot_rdt_un_corrected_ids(study_dir, rdt, setups, out_label, ip, **kwargs):
    output_dir = study_dir / GATHER_FOLDER

    fig = plot_rdt_before_after_ids(study_dir, rdt, ip, setups, **kwargs)
    pannot.set_name(f'{rdt}.un_corrected_ids.IP{ip}.{out_label}', fig)
    fig.savefig(output_dir / f"plot.{fig.canvas.get_default_filename()}")
    return fig


def plot_rdt_before_after_ids(study_dir, rdt, ip, setups, **kwargs):
    # LOAD DATA ----------
    all_beams = set(s['beam'] for s in setups)
    all_machines = set(s['machine'] for s in setups)
    df_data = {m: {b: None for b in all_beams} for m in all_machines}
    idxSlice = pd.IndexSlice

    for machine in all_machines:
        for beam in all_beams:
            df_data[machine][beam] = get_data_before_after(study_dir, machine, beam, rdt)

    # STYLE -------
    manual = {u"figure.figsize": [10.24, 5.12],
              u"markers.fillstyle": u'none', u'grid.alpha': 0,
              u'savefig.format': u'pdf'}

    ncol: np.numeric = kwargs.pop('ncol', 3)
    ylim: np.numeric = kwargs.pop('ylim', None)
    xtickrotation: Iterable[np.numeric] = kwargs.pop('xtick.rotation', None)
    plot_styles: Iterable[Union[Path, str]] = kwargs.pop('plot_styles', 'standard')

    manual.update(kwargs)
    pstyle.set_style(plot_styles, manual)

    # DELTA ------
    label = fr"$|f^\mathrm{{\;IP{ip}}}_{{{rdt[1:]}}}|$"
    njobs = 1
    ncolumns = len(setups)

    bar_width = 1/(ncolumns + 1)
    xlim = [- bar_width/2, (njobs - 1) + bar_width * (ncolumns + 0.5)]

    fig, ax = plt.subplots()

    # plot zero line
    ax.axhline(0, color="black", lw=1, ls="-", marker="", zorder=0)

    for job_idx, job_setup in enumerate(setups):

        job_id = get_beams_jobid(**job_setup)
        job_setup["label"] = job_setup.get("label", job_id)
        job_setup["color"] = job_setup.get('color', pcolors.get_mpl_color(job_idx))
        color = job_setup["color"]
        the_id = CORRECTED_ID

        if job_setup["label"] == BEFORE_CORRECTION:
            the_id = UNCORRECTED_ID
        elif job_setup["label"] == NOMINAL_ID:
            the_id = NOMINAL_ID

        # plot data
        x_pos = bar_width * (job_idx + 0.5)

        seed_data = df_data[job_setup['machine']][job_setup['beam']].loc[idxSlice[job_id, :, ip], the_id]
        std = seed_data.std()
        mean = seed_data.mean()
        std_min, std_max = mean-std, mean+std

        violin = ax.violinplot(dataset=seed_data, positions=[x_pos], widths=bar_width/2, showmeans=True, showextrema=True)

        for poly in violin['bodies']:
            poly.set_facecolor(color)
            poly.set_alpha(0.2)

            std_violin = PolyCollection([[v for v in poly.get_paths()[0].vertices if std_min <= v[1] <= std_max]])
            std_violin.set_facecolor(color)
            std_violin.set_alpha(0.4)
            ax.add_collection(std_violin)

        violin['cbars'].set_color(color)
        violin['cmeans'].set_color(color)

        for line_collection in ['cmaxes', 'cmins']:
            violin[line_collection].set_color(color)
            y_pos = violin[line_collection].get_segments()[0][0][1]
            violin[line_collection].set_segments([[[x_pos-bar_width/20, y_pos], [x_pos+bar_width/20, y_pos]]])

    handles = [mpatches.Patch(color=s['color'], label=s['label']) for s in setups]
    # handles = fill_first_handle_row(handles, ncol)

    ax.set_ylabel(label)
    ax.xaxis.set_major_locator(ticker.NullLocator())
    ax.set_xlim(xlim)


    ax.set_ylim(ylim)
    ax.set_yscale("log")

    y_minor = ticker.LogLocator(base=10.0, subs=np.arange(1.0, 10.0) * 0.1, numticks=10)
    ax.yaxis.set_minor_locator(y_minor)
    ax.yaxis.set_minor_formatter(ticker.NullFormatter())

    y_major = ticker.LogLocator(base=10.0, subs=np.arange(1, 10.0), numticks=10)
    ax.yaxis.set_major_locator(y_major)

    pannot.make_top_legend(ax, handles=handles, ncol=ncol, frame=False)

    fig.tight_layout()
    return fig


def get_data_rdts(study_dir, machine, beam, the_id, rdts):
    output_dir = study_dir / GATHER_FOLDER
    df_out_name = get_df_name(machine, beam, 'twiss_rdt')
    df = tfs.read(output_dir / df_out_name)
    df = df.set_index([JOB_ID_COLUMN, SEED_COLUMN, IP])
    df_out = pd.DataFrame(index=df.index, columns=rdts)

    for rdt in rdts:
        nominal_column = f"{rdt}_{NOMINAL_ID}"
        uncorrected_column = f"{rdt}_{UNCORRECTED_ID}"
        corrected_column = f"{rdt}_{CORRECTED_ID}"

        if the_id == NOMINAL_ID:
            df_out[rdt] = df[nominal_column]

        if the_id == CORRECTED_ID:
            df_out[rdt] = df[corrected_column]

        if the_id == UNCORRECTED_ID:
            df_out[rdt] = df[uncorrected_column]

        if the_id == DIFF_ID:
            df_out[rdt] = df[corrected_column] - df[uncorrected_column]

        if the_id == DIFFABS_ID:
            df_out[rdt] = df[corrected_column].abs() - df[uncorrected_column].abs()

        if the_id == TARGETDIFFABS_ID:
            df_out[rdt] = df[nominal_column].abs() - df[uncorrected_column].abs()

        if the_id == RESIDUALDIFFABS_ID:
            df_out[rdt] = df[nominal_column].abs() - df[uncorrected_column].abs()

        if the_id == RELDIFFABS_ID:
            df_out[rdt] = ((df[corrected_column].abs() - df[uncorrected_column].abs()) /
                    (df[nominal_column].abs() - df[uncorrected_column].abs()))
    return df_out



def get_data_before_after(study_dir, machine, beam, rdt):
    output_dir = study_dir / GATHER_FOLDER
    df_out_name = get_df_name(machine, beam, 'twiss_rdt')
    df = tfs.read(output_dir / df_out_name)
    df = df.set_index([JOB_ID_COLUMN, SEED_COLUMN, IP])
    df_out = pd.DataFrame(index=df.index, columns=[UNCORRECTED_ID, CORRECTED_ID])

    uncorrected_column = f"{rdt}_{UNCORRECTED_ID}"
    corrected_column = f"{rdt}_{CORRECTED_ID}"
    nominal_column = f"{rdt}_{NOMINAL_ID}"

    df_out[CORRECTED_ID] = df[corrected_column].abs()
    df_out[UNCORRECTED_ID] = df[uncorrected_column].abs()
    df_out[DIFFABS_ID] = (df[corrected_column] - df[uncorrected_column]).abs()
    df_out[NOMINAL_ID] = df[nominal_column].abs()

    return df_out


def fill_first_handle_row(handles: list, ncol: int):
    """Fill the first row with empty entries after the first."""
    empty = mpatches.Patch(color='None', label='')
    nrow = int(np.ceil(len(handles) / ncol))
    new = handles[0:1] + [empty]*(ncol-1) + handles[1:]
    new = new + [empty] * ((ncol*nrow) - len(new))
    return np.reshape(new, [nrow, ncol]).T.flatten().tolist()


if __name__ == '__main__':
    pass
    # study_dir = Path("/home/jdilly/Work/study.20.irnl_correction_with_feeddown/code.cpymad_experiments/hl14_D2b5_15_D1b5_0_omMCBXFcorr_B1/cpymad")
    # for beam in (1, ):
    #     gather_twiss_rdt_data(
    #         machine='hllhc',
    #         beam=beam,
    #         rdts=['f1004', 'f5000'],
    #         study_dir=study_dir,
    #         seeds=[1, ]
    #     )
