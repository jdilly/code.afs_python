import sqlite3 as sql
from contextlib import contextmanager

import pandas as pd

from analysis.rescale_da import TEST_PATH, get_all_job_dirs, get_database_path, STUDY_PATH
from omc3.utils import logging_tools

LOG = logging_tools.get_logger(__name__)


@contextmanager
def connect(db_path):
    """ Contextmanager that automatically closes db but does not commit changes (as sqlite would)."""
    db = sql.connect(db_path)
    try:
        yield db
    finally:
        db.close()


def check_database(database_path):
    with connect(database_path) as db:
        try:
            db.execute('pragma integrity_check')
        except sql.DatabaseError as e:
            LOG.error(
                f"Error in '{database_path.absolute()!s}'\n"
                f"Integrity check failed with: {e!s}", exc_info=False
            )
        else:
            LOG.debug(
                f"Database looks healthy: '{database_path.absolute()!s}'"
            )


def check_database_dataonly(database_path):
    with connect(database_path) as db:
        try:
            pd.read_sql("SELECT seed, angle, alost1, alost2, Amin, Amax FROM da_post ORDER BY seed, angle", db)
        except (sql.DatabaseError, pd.io.sql.DatabaseError) as e:
            LOG.error(
                f"Error in '{database_path.absolute()!s}'\n"
                f"Integrity check failed with: {e!s}", exc_info=False
            )
        else:
            LOG.debug(
                f"Database looks healthy: '{database_path.absolute()!s}'"
            )


def check_all_db(study_dir):
    for jobdir in list(get_all_job_dirs(study_dir)):
        LOG.info(f"{jobdir} ---")
        db_path = get_database_path(jobdir)
        check_database_dataonly(db_path)


if __name__ == '__main__':
    # check_all_db(STUDY_PATH)
    check_all_db(TEST_PATH)
