import re

import tfs
from tfs import TfsDataFrame, read_tfs
from typing import Iterable, Union
from pathlib import Path
from matplotlib import pyplot as plt
from matplotlib import rcParams, lines as mlines, patches as mpatches
import numpy as np

from omc3.plotting.utils import style as pstyle, annotations as pannot, colors as pcolors
from matplotlib.colors import to_rgba

from pylhc_submitter.constants.autosix import (
    HEADER_NTOTAL, HEADER_INFO, HEADER_HINT,
    MEAN, STD, MIN, MAX, N,
    SEED, ANGLE, ALOST1, ALOST2, AMP
)

from pylhc_submitter.constants import autosix as autosix_const
from correct_irnl_run_helper import get_beams_jobid, get_other_beam
from analysis.file_crawler import GATHER_FOLDER, afs_to_eos_path, get_jobdir_name, SEEDS, get_seeddir_name


SETTINGS_FILENAME = "settings.{machine:s}.b{beam:d}.{output_id:s}.tfs"


def get_correction_filename(setup):
    beam = setup['beam']
    correct_beam = setup['correct_beam']

    if correct_beam == "same":
        corrector_beam = beam
    elif correct_beam == "other":
        corrector_beam = get_other_beam(beam)
    else:
        corrector_beam = int(correct_beam[-1])

    correct_name = f'mcx_b{corrector_beam}'
    if (setup['rdts2'] is not None) and not (setup['rdts2'] == "None"):
        correct_name += f'b{get_other_beam(corrector_beam)}'

    return SETTINGS_FILENAME.format(machine=setup['machine'], beam=beam, output_id=correct_name)


def plot_correctors(setups, out_label, field_component, ip, study_dir, seeds=SEEDS, **kwargs):
    output_dir = study_dir / GATHER_FOLDER
    output_dir.mkdir(exist_ok=True)

    manual = {u"figure.figsize": [10.24, 5.12],
              u"markers.fillstyle": u'none', u'grid.alpha': 0,
              u'savefig.format': u'pdf'}

    ylim: np.numeric = kwargs.pop('ylim', None)
    xlim: np.numeric = kwargs.pop('xlim', None)
    ncol: np.numeric = kwargs.pop('ncol', 3)
    plot_styles: Iterable[Union[Path, str]] = kwargs.pop('plot_styles', 'standard')

    manual.update(kwargs)
    pstyle.set_style(plot_styles, manual)

    fig, ax = plt.subplots()
    ax.axis("square")
    for idx, setup in enumerate(setups):
        label = setup.pop('label')

        job_dir = study_dir / get_jobdir_name(setup)
        color = pcolors.get_mpl_color(idx)
        for idx_seed, seed in enumerate(seeds):
            seed_dir = job_dir / get_seeddir_name(seed)
            try:
                data_df = tfs.read(seed_dir / get_correction_filename(setup))
            except tfs.errors.TfsFormatError as e:
                continue
                
            query_df = data_df.query(f"field_component == '{field_component}' and ip == {ip}").set_index("side")
            ax.plot(
                query_df.loc['R', 'value'],
                query_df.loc['L', 'value'],
                ls='None',
                c=color,
                marker='o',
                markersize=rcParams['lines.markersize'] * 1.3,
                label=f'{"_" if idx_seed != 0 else ""}{label}',
            )
            ax.plot(
                query_df.loc['R', 'value'],
                query_df.loc['L', 'value'],
                ls='None',
                c=color,
                marker=f"${seed:02d}$",
                label=f'_{label}',
            )
            if not idx:
                ax.set_xlabel(f"{query_df.loc['R', 'name']} ({madx_to_convention(query_df.loc['R', 'strength_component'])})")
                ax.set_ylabel(f"{query_df.loc['L', 'name']} ({madx_to_convention(query_df.loc['L', 'strength_component'])})")

        setup['label'] = label
    ax.set_xlim(xlim)
    ax.set_ylim(ylim)
    pannot.make_top_legend(ax, ncol=ncol, frame=False)

    fig.tight_layout()
    pannot.set_name(f'corrector_val.{out_label}.field_{field_component}.ip{ip}', fig)
    fig.savefig(output_dir / f"plot.{fig.canvas.get_default_filename()}")
    return fig


def madx_to_convention(strength_str: str):
    m = re.match(r"K(\d+)(S?)L", strength_str, flags=re.IGNORECASE)
    return f"{strength_str[0]}{int(m.group(1))+1}{m.group(2)}{strength_str[-1]}"