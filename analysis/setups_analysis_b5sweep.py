import sys

from pylhc_submitter.constants.autosix import ALOST2

from analysis.analyse_corrector_values import plot as corrector_plot
from pathlib import Path
from matplotlib import pyplot as plt
from analyse_ptc_output import gather_ptc_data, plot_ptc_ampdet_data as plot_ampdet, DIFFABS_ID, TARGETDIFFABS_ID, RELDIFFABS_ID, RESIDUALDIFFABS_ID
from analyse_da_output import plot_jobs_and_save as plot_da, plot_sweep

from omc3.utils import logging_tools

from analysis.analyse_ptc_output import gather_ptc_data
from analysis.file_crawler import CORRECTED_ID, UNCORRECTED_ID, NOMINAL_ID
from correct_irnl_run_helper import get_other_beam
from mpl_editor.main import main as mpl_editor

LOG = logging_tools.get_logger(__name__)

STUDY_DIR = Path("/eos/home-j/jdilly/work/study.irnl_correction_beams_b5sweep")
ERRORS_ALL = "AB3-15"

CORRECTOR_KWARGS = {
    'figure.figsize': [7.20, 6.48],
    # 'legend.handlelength': 1,
    # 'legend.handletextpad': .2,
    'legend.fontsize': 20,
    'xtick.labelsize': 20,
    'xtick.major.pad': 10,
    'ytick.labelsize': 20,
    'axes.labelsize': 20,
}

DA_KWARGS = {
    'figure.figsize': [6.4, 6.4],
    # 'legend.handlelength': 1.,
    # 'legend.handletextpad': .4,
    'legend.fontsize': 20,
    'xtick.labelsize': 20,
    'xtick.major.pad': 10,
    'ytick.labelsize': 20,
    'axes.labelsize': 20,
    'amplitude_ticks': list(range(2, 20, 2))
}

DA_SWEEP_KWARGS = {
    'xlabel': "D2 systematic $b_5$ [units]",
    'figure.figsize': [6.8, 6.8],
    # 'legend.handlelength': 1,
    # 'legend.handletextpad': .2,
    'legend.fontsize': 20,
    'xtick.labelsize': 20,
    'xtick.major.pad': 10,
    'ytick.labelsize': 20,
    'axes.labelsize': 20,
    'grid.alpha': 0.6,
    'axes.grid.axis': 'y',
    'axes.grid.which': 'both',
}

AMPDET_KWARGS = {
    'figure.figsize': [7.68, 5.76],
    'legend.columnspacing': 0.5,
    'legend.handlelength': 1.,
    'legend.fontsize': 22,
    'xtick.labelsize': 24,
    'ytick.labelsize': 24,
    'axes.labelsize': 24,
}

SO_AMPDET_KWARGS = {
    'figure.figsize': [7.68, 6.4],
    'legend.columnspacing': 0.5,
    'legend.handlelength': 1.,
    'legend.fontsize': 22,
    'xtick.labelsize': 24,
    'xtick.alignment': 'center',
    'xtick.rotation': 20,
    'ytick.labelsize': 24,
    'axes.labelsize': 24,
}

TO_AMPDET_KWARGS = {
    'figure.figsize': [7.68, 6.4],
    'legend.columnspacing': 0.5,
    'legend.handlelength': 1.,
    'legend.fontsize': 22,
    'xtick.labelsize': 24,
    'xtick.alignment': 'center',
    'xtick.rotation': 20,
    'ytick.labelsize': 24,
    'axes.labelsize': 24,
}


def close_fig(fig):
    # mpl_editor(fig)
    plt.close(fig)
    # plt.show()
    pass


def gather_ptc_beams():
    for beam in (1, 4):
        gather_ptc_data(study_dir=STUDY_DIR, machine='hllhc', beam=beam, orders=(1, 2, 3))


def hllhc_compare_d2b5_15():
    base_dir = STUDY_DIR
    machine = 'hllhc'
    optics = "round1515"
    xing = "top"
    d2b5_value = 15
    errors = ERRORS_ALL
    corrector_strength = 'limited'
    for beam in (1, 4):
        setups = [
            dict(machine=machine, beam=beam, correct_beam=f"b{beam}", d2b5_value=d2b5_value, corrector_strength=corrector_strength, xing=xing, errors=errors, optics=optics, rdts="b6-b6_a6-a6_b5-b5_a5-a5_b4-b4_a4-a4_b3-b3_a3-a3", rdts2="None", label=f"f5000"),
            dict(machine=machine, beam=beam, correct_beam=f"b{beam}", d2b5_value=d2b5_value, corrector_strength=corrector_strength, xing=xing, errors=errors, optics=optics, rdts="b6-b6_a6-a6_b14-b5_a5-a5_b4-b4_a4-a4_b3-b3_a3-a3", rdts2="None", label=f"f1004"),
            dict(machine=machine, beam=beam, correct_beam=f"b{beam}", d2b5_value=d2b5_value, corrector_strength=corrector_strength, xing=xing, errors=errors, optics=optics, rdts="b6-b6_a6-a6_b32-b5_a5-a5_b4-b4_a4-a4_b3-b3_a3-a3", rdts2="None", label="f3002"),
            dict(machine=machine, beam=beam, correct_beam=f"b{beam}", d2b5_value=d2b5_value, corrector_strength=corrector_strength, xing=xing, errors=errors, optics=optics, rdts="b6-b6_a6-a6_b5-b5_b14-b5_b32-b5_a5-a5_b4-b4_a4-a4_b3-b3_a3-a3", rdts2="None", label="all"),
        ]
        out_label = f"{machine}.b{beam}.d2b5val_{d2b5_value}.corr_{corrector_strength}"
        for ip in (1, 5):
            # close_fig(corrector_plot(setups, out_label, "b5", ip, base_dir, ncol=4, xlim=[-6, 3], ylim=[0, 6], **CORRECTOR_KWARGS))
            # close_fig(corrector_plot(setups, out_label, "a5", ip, base_dir, ncol=4, xlim=[None, None], ylim=[None, None], **CORRECTOR_KWARGS))
            # close_fig(corrector_plot(setups, out_label, "a4", ip, base_dir, ncol=4, xlim=[None, None], ylim=[None, None], **CORRECTOR_KWARGS))
            # close_fig(corrector_plot(setups, out_label, "b4", ip, base_dir, ncol=4, xlim=[None, None], ylim=[None, None], **CORRECTOR_KWARGS))
            pass

        close_fig(plot_da(base_dir=base_dir, setups=setups, out_label=out_label, output_dir=STUDY_DIR, rlim=16, plot_std=True, **DA_KWARGS))
        for the_id in (CORRECTED_ID, UNCORRECTED_ID, NOMINAL_ID, DIFFABS_ID, TARGETDIFFABS_ID, RELDIFFABS_ID, RESIDUALDIFFABS_ID):
            # close_fig(plot_ampdet(study_dir=STUDY_DIR, the_id=the_id, order=1, setups=setups, out_label=out_label, ncol=4, ylim=[-560, 250], **AMPDET_KWARGS))
            # close_fig(plot_ampdet(study_dir=STUDY_DIR, the_id=the_id, order=2, setups=setups, out_label=out_label, ncol=4, ylim=None, **SO_AMPDET_KWARGS))
            # close_fig(plot_ampdet(study_dir=STUDY_DIR, the_id=the_id, order=3, setups=setups, out_label=out_label, ncol=4, ylim=None, **TO_AMPDET_KWARGS))
            pass


def hllhc_plot_all_da_polar():
    base_dir = STUDY_DIR
    machine = 'hllhc'
    optics = "round1515"
    xing = "top"
    errors = ERRORS_ALL
    corrector_strength = 'limited'
    for d2b5_value in (0, 5, 10, 15):
        for beam in (1, 4):
            setups = [
                dict(machine=machine, beam=beam, correct_beam=f"b{beam}", d2b5_value=d2b5_value, corrector_strength=corrector_strength, xing=xing, errors=errors, optics=optics, rdts="b6-b6_a6-a6_b5-b5_a5-a5_b4-b4_a4-a4_b3-b3_a3-a3", rdts2="None", label=f"f5000"),
                dict(machine=machine, beam=beam, correct_beam=f"b{beam}", d2b5_value=d2b5_value, corrector_strength=corrector_strength, xing=xing, errors=errors, optics=optics, rdts="b6-b6_a6-a6_b14-b5_a5-a5_b4-b4_a4-a4_b3-b3_a3-a3", rdts2="None", label=f"f1004"),
                dict(machine=machine, beam=beam, correct_beam=f"b{beam}", d2b5_value=d2b5_value, corrector_strength=corrector_strength, xing=xing, errors=errors, optics=optics, rdts="b6-b6_a6-a6_b32-b5_a5-a5_b4-b4_a4-a4_b3-b3_a3-a3", rdts2="None", label="f3002"),
                dict(machine=machine, beam=beam, correct_beam=f"b{beam}", d2b5_value=d2b5_value, corrector_strength=corrector_strength, xing=xing, errors=errors, optics=optics, rdts="b6-b6_a6-a6_b5-b5_b14-b5_b32-b5_a5-a5_b4-b4_a4-a4_b3-b3_a3-a3", rdts2="None", label="all"),
            ]
            out_label = f"{machine}.b{beam}.d2b5val_{d2b5_value}.corr_{corrector_strength}"
            close_fig(plot_da(base_dir=base_dir, setups=setups, out_label=out_label, output_dir=STUDY_DIR, rlim=16, plot_std=True, **DA_KWARGS))
            for idx, setup in enumerate(setups):
                out_label = f"{machine}.b{beam}.d2b5val_{d2b5_value}.rdt_{setup['label']}.corr_{corrector_strength}"
                close_fig(plot_da(base_dir=base_dir, setups=[setup], out_label=out_label, output_dir=STUDY_DIR, rlim=16, plot_std=False, color_idx_start=idx, **DA_KWARGS))


# Sweeps ------------------

CORR_MAP = {
    'b50': 'b5-b5',
    'b14': 'b14-b5',
    'b32': 'b32-b5',
    'b50b14b32': 'b5-b5_b14-b5_b32-b5',
}


def hllhc_sweep_d2b5_individual():
    machine = 'hllhc'
    optics = "round1515"
    xing = "top"
    errors = ERRORS_ALL
    corrector_strength = 'limited'
    for beam in (1, 4):
        for correct_beam in (1, 4):
            for color_idx, rdt in enumerate(["b50", "b14", "b32", "b50b14b32"]):
                setups = [
                    dict(
                        machine=machine, beam=beam, correct_beam=f"b{correct_beam}", d2b5_value=d2b5_value,
                        corrector_strength=corrector_strength, xing=xing, errors=errors, optics=optics,
                        rdts=f"b6-b6_a6-a6_{CORR_MAP[rdt]}_a5-a5_b4-b4_a4-a4_b3-b3_a3-a3", rdts2="None",
                        label=f"f{rdt[1]}00{rdt[2]}" if len(rdt) == 3 else "all",
                        xlabel=d2b5_value
                    )
                    for d2b5_value in [-15, -10, -5, 0, 5, 10, 15]
                ]
                out_label = f"{machine}.b{beam}_by_b{correct_beam}.rdt_{rdt}.sweep.corr_{corrector_strength}"
                close_fig(
                    plot_sweep(
                        base_dir=STUDY_DIR, output_dir=STUDY_DIR, out_label=out_label, setups=setups, da_col=ALOST2,
                        ylim=[3.8, 18.2],
                        color_idx_start=color_idx,
                        **DA_SWEEP_KWARGS
                    )
                )


def hllhc_sweep_d2b5_all_rdts():
    machine = 'hllhc'
    optics = "round1515"
    xing = "top"
    errors = ERRORS_ALL
    corrector_strength = 'limited'
    for beam in (1, 4):
        for correct_beam in (1, 4):
            setups = [
                dict(
                    machine=machine, beam=beam, correct_beam=f"b{correct_beam}", d2b5_value=d2b5_value,
                    corrector_strength=corrector_strength, xing=xing, errors=errors, optics=optics,
                    rdts=f"b6-b6_a6-a6_{CORR_MAP[rdt]}_a5-a5_b4-b4_a4-a4_b3-b3_a3-a3", rdts2="None",
                    label=f"f{rdt[1]}00{rdt[2]}" if len(rdt) == 3 else "all",
                    xlabel=d2b5_value
                )
                for rdt in ["b50", "b14", "b32", "b50b14b32"]
                for d2b5_value in [-15, -10, -5, 0, 5, 10, 15]
            ]
            out_label = f"{machine}.b{beam}_by_b{correct_beam}.rdt_all.sweep.corr_{corrector_strength}"
            close_fig(
                plot_sweep(
                    base_dir=STUDY_DIR, output_dir=STUDY_DIR, out_label=out_label,
                    setups=setups, da_col=ALOST2,
                    ncol=4, ylim=[3.8, 14.2],
                    plot_std=True, seeds=[],
                    **DA_SWEEP_KWARGS
                )
            )


if __name__ == '__main__':
    # gather_ptc_beams()
    # hllhc_compare_d2b5_15()
    hllhc_plot_all_da_polar()
    # hllhc_sweep_d2b5_individual()
    # hllhc_sweep_d2b5_all_rdts()
    # plt.show()
