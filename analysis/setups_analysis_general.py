from pathlib import Path
from analyse_ptc_output import gather_ptc_data, plot_ptc_ampdet_data as plot_ampdet, DIFFABS_ID
from analyse_da_output import plot_jobs_and_save as plot_da
from matplotlib import pyplot as plt

from omc3.utils import logging_tools

LOG = logging_tools.get_logger(__name__)


STUDY_DIR = Path("/eos/home-j/jdilly/work/study.irnl_correction_multi_beams")
ERRORS_ALL = "AB3AB4AB5AB6AB7AB8"
RDTS_ALL_HLLHC = "b6-b6_a6-a6_b5-b5_a5-a5_b4-b4_a4-a4_b3-b3_a3-a3"
RDTS_ALL_LHC = "b6-b6_b4-b4_a4-a4_b3-b3_a3-a3"

DA_KWARGS = {
    'plot_std': True,
    'figure.figsize': [6.4, 6.4],
    'legend.fontsize': 20,
    'xtick.labelsize': 20,
    'xtick.major.pad': 10,
    'ytick.labelsize': 20,
    'axes.labelsize': 20,
    'amplitude_ticks': list(range(2, 20, 2))
}


def close_fig(fig):
    plt.close(fig)


def get_other_beam(beam: int):
    return 1 if beam == 4 else 2


def get_this_beam(beam: int):
    return 1 if beam == 1 else 2


def gather_ptc_beams():
    for machine in ['lhc', 'hllhc']:
    # for machine in ['hllhc']:
        for beam in (1, 4):
            for order in (1, 2):
                gather_ptc_data(study_dir=STUDY_DIR, beam=beam, machine=machine, order=order)


def hllhc_compare_round():
    base_dir = STUDY_DIR
    machine = 'hllhc'
    optics = "round1515"
    xing = "top"
    iterations = 1
    for feeddown in (0, 2):
        for beam in (1, 4):
            other_beam = get_other_beam(beam)
            this_beam = get_this_beam(beam)
            setups = [
                dict(machine=machine, beam=beam, correct_beam="same",  xing="top", feeddown=feeddown, iterations=iterations, errors=ERRORS_ALL, optics=optics, rdts=RDTS_ALL_HLLHC, rdts2="None", label=f"from B{this_beam}"),
                dict(machine=machine, beam=beam, correct_beam="other", xing="top", feeddown=feeddown, iterations=iterations, errors=ERRORS_ALL, optics=optics, rdts=RDTS_ALL_HLLHC, rdts2="None", label=f"from B{other_beam}"),
                dict(machine=machine, beam=beam, correct_beam="same", xing="top", feeddown=feeddown, iterations=iterations, errors=ERRORS_ALL, optics=optics, rdts=RDTS_ALL_HLLHC, rdts2=RDTS_ALL_HLLHC, label="combined"),
            ]
            out_label = f"{machine}.b{beam}.xing_{xing}.optics_{optics}.fd_{feeddown}.errors_all.rdts_all"
            close_fig(plot_da(base_dir=base_dir, setups=setups, out_label=out_label, output_dir=STUDY_DIR, rlim=20, **DA_KWARGS))
            for order in (1, 2):
                close_fig(plot_ampdet(study_dir=STUDY_DIR, the_id=DIFFABS_ID, order=order, setups=setups, out_label=out_label))


def hllhc_compare_flat():
    base_dir = STUDY_DIR
    machine = 'hllhc'
    optics = "flat07530"
    xing = "top"
    iterations = 1
    for feeddown in (0, 2):
        for beam in (1, 4):
            other_beam = get_other_beam(beam)
            this_beam = get_this_beam(beam)
            setups = [
                dict(machine=machine, beam=beam, correct_beam="same",  xing="top", feeddown=feeddown, iterations=iterations, errors=ERRORS_ALL, optics=optics, rdts=RDTS_ALL_HLLHC, rdts2="None", label=f"from B{this_beam}"),
                dict(machine=machine, beam=beam, correct_beam="other", xing="top", feeddown=feeddown, iterations=iterations, errors=ERRORS_ALL, optics=optics, rdts=RDTS_ALL_HLLHC, rdts2="None", label=f"from B{other_beam}"),
                dict(machine=machine, beam=beam, correct_beam="same", xing="top", feeddown=feeddown, iterations=iterations, errors=ERRORS_ALL, optics=optics, rdts=RDTS_ALL_HLLHC, rdts2=RDTS_ALL_HLLHC, label="combined"),
            ]
            out_label = f"{machine}.b{beam}.xing_{xing}.optics_{optics}.fd_{feeddown}.errors_all.rdts_all"
            close_fig(plot_da(base_dir=base_dir, setups=setups, out_label=out_label, output_dir=STUDY_DIR, rlim=20, **DA_KWARGS))
            for order in (1, 2):
                close_fig(plot_ampdet(study_dir=STUDY_DIR, the_id=DIFFABS_ID, order=order, setups=setups, out_label=out_label))


def hllhc_compare_round_lhc_correctors():
    base_dir = STUDY_DIR
    machine = 'hllhc'
    optics = "round1515"
    xing = "top"
    iterations = 1
    for feeddown in (0, 2):
        for beam in (1, 4):
            other_beam = get_other_beam(beam)
            this_beam = get_this_beam(beam)
            setups = [
                dict(machine=machine, beam=beam, correct_beam="same",  xing="top", feeddown=feeddown, iterations=iterations, errors=ERRORS_ALL, optics=optics, rdts=RDTS_ALL_LHC, rdts2="None", label=f"from B{this_beam}"),
                dict(machine=machine, beam=beam, correct_beam="other", xing="top", feeddown=feeddown, iterations=iterations, errors=ERRORS_ALL, optics=optics, rdts=RDTS_ALL_LHC, rdts2="None", label=f"from B{other_beam}"),
                dict(machine=machine, beam=beam, correct_beam="other", xing="top", feeddown=feeddown, iterations=iterations, errors=ERRORS_ALL, optics=optics, rdts=RDTS_ALL_LHC, rdts2=RDTS_ALL_LHC, label="combined"),
            ]
            out_label = f"{machine}.b{beam}.xing_{xing}.optics_{optics}.fd_{feeddown}.errors_all.rdts_lhcall"
            close_fig(plot_da(base_dir=base_dir, setups=setups, out_label=out_label, output_dir=STUDY_DIR, rlim=20, **DA_KWARGS))
            for order in (1, 2):
                close_fig(plot_ampdet(study_dir=STUDY_DIR, the_id=DIFFABS_ID, order=order, setups=setups, out_label=out_label))


def hllhc_compare_flat_lhc_correctors():
    base_dir = STUDY_DIR
    machine = 'hllhc'
    optics = "flat07530"
    xing = "top"
    iterations = 1
    for feeddown in (0, 2):
        for beam in (1, 4):
            other_beam = get_other_beam(beam)
            this_beam = get_this_beam(beam)
            setups = [
                dict(machine=machine, beam=beam, correct_beam="same",  xing="top", feeddown=feeddown, iterations=iterations, errors=ERRORS_ALL, optics=optics, rdts=RDTS_ALL_LHC, rdts2="None", label=f"from B{this_beam}"),
                dict(machine=machine, beam=beam, correct_beam="other", xing="top", feeddown=feeddown, iterations=iterations, errors=ERRORS_ALL, optics=optics, rdts=RDTS_ALL_LHC, rdts2="None", label=f"from B{other_beam}"),
                dict(machine=machine, beam=beam, correct_beam="other", xing="top", feeddown=feeddown, iterations=iterations, errors=ERRORS_ALL, optics=optics, rdts=RDTS_ALL_LHC, rdts2=RDTS_ALL_LHC, label="combined"),
            ]
            out_label = f"{machine}.b{beam}.xing_{xing}.optics_{optics}.fd_{feeddown}.errors_all.rdts_lhcall"
            close_fig(plot_da(base_dir=base_dir, setups=setups, out_label=out_label, output_dir=STUDY_DIR, rlim=20, **DA_KWARGS))
            for order in (1, 2):
                close_fig(plot_ampdet(study_dir=STUDY_DIR, the_id=DIFFABS_ID, order=order, setups=setups, out_label=out_label))


def lhc_compare_round():
    base_dir = STUDY_DIR
    machine = 'lhc'
    optics = "round3030"
    xing = "top"
    iterations = 1
    for feeddown in (0, 2):
        for beam in (1, 4):
            other_beam = get_other_beam(beam)
            this_beam = get_this_beam(beam)
            setups = [
                dict(machine=machine, beam=beam, correct_beam="same",  xing="top", feeddown=feeddown, iterations=iterations, errors=ERRORS_ALL, optics=optics, rdts=RDTS_ALL_LHC, rdts2="None", label=f"from B{this_beam}"),
                dict(machine=machine, beam=beam, correct_beam="other", xing="top", feeddown=feeddown, iterations=iterations, errors=ERRORS_ALL, optics=optics, rdts=RDTS_ALL_LHC, rdts2="None", label=f"from B{other_beam}"),
                dict(machine=machine, beam=beam, correct_beam="same", xing="top", feeddown=feeddown, iterations=iterations, errors=ERRORS_ALL, optics=optics, rdts=RDTS_ALL_LHC, rdts2=RDTS_ALL_LHC, label="combined"),
            ]
            out_label = f"{machine}.b{beam}.xing_{xing}.optics_{optics}.fd_{feeddown}.errors_all.rdts_all"
            close_fig(plot_da(base_dir=base_dir, setups=setups, out_label=out_label, output_dir=STUDY_DIR, rlim=12, **DA_KWARGS))
            for order in (1, 2):
                close_fig(plot_ampdet(study_dir=STUDY_DIR, the_id=DIFFABS_ID, order=order, setups=setups, out_label=out_label))


def lhc_compare_flat():
    base_dir = STUDY_DIR
    machine = 'lhc'
    optics = "flat6015"
    xing = "top"
    iterations = 1
    for feeddown in (0, 2):
        for beam in (1, 4):
            other_beam = get_other_beam(beam)
            this_beam = get_this_beam(beam)
            setups = [
                dict(machine=machine, beam=beam, correct_beam="same",  xing="top", feeddown=feeddown, iterations=iterations, errors=ERRORS_ALL, optics=optics, rdts=RDTS_ALL_LHC, rdts2="None", label=f"from B{this_beam}"),
                dict(machine=machine, beam=beam, correct_beam="other", xing="top", feeddown=feeddown, iterations=iterations, errors=ERRORS_ALL, optics=optics, rdts=RDTS_ALL_LHC, rdts2="None", label=f"from B{other_beam}"),
                dict(machine=machine, beam=beam, correct_beam="same", xing="top", feeddown=feeddown, iterations=iterations, errors=ERRORS_ALL, optics=optics, rdts=RDTS_ALL_LHC, rdts2=RDTS_ALL_LHC, label="combined"),
            ]
            out_label = f"{machine}.b{beam}.xing_{xing}.optics_{optics}.fd_{feeddown}.errors_all.rdts_hllhc_all"
            close_fig(plot_da(base_dir=base_dir, setups=setups, out_label=out_label, output_dir=STUDY_DIR, rlim=12, **DA_KWARGS))
            for order in (1, 2):
                close_fig(plot_ampdet(study_dir=STUDY_DIR, the_id=DIFFABS_ID, order=order, setups=setups, out_label=out_label))


def lhc_compare_round_hllhc_correctors():
    base_dir = STUDY_DIR
    machine = 'lhc'
    optics = "round3030"
    xing = "top"
    iterations = 1
    for feeddown in (0, 2):
        for beam in (1, 4):
            other_beam = get_other_beam(beam)
            this_beam = get_this_beam(beam)
            setups = [
                dict(machine=machine, beam=beam, correct_beam="same",  xing="top", feeddown=feeddown, iterations=iterations, errors=ERRORS_ALL, optics=optics, rdts=RDTS_ALL_HLLHC, rdts2="None", label=f"from B{this_beam}"),
                dict(machine=machine, beam=beam, correct_beam="other", xing="top", feeddown=feeddown, iterations=iterations, errors=ERRORS_ALL, optics=optics, rdts=RDTS_ALL_HLLHC, rdts2="None", label=f"from B{other_beam}"),
                dict(machine=machine, beam=beam, correct_beam="same", xing="top", feeddown=feeddown, iterations=iterations, errors=ERRORS_ALL, optics=optics, rdts=RDTS_ALL_HLLHC, rdts2=RDTS_ALL_HLLHC, label="combined"),
            ]
            out_label = f"{machine}.b{beam}.xing_{xing}.optics_{optics}.fd_{feeddown}.errors_all.rdts_hllhcall"
            close_fig(plot_da(base_dir=base_dir, setups=setups, out_label=out_label, output_dir=STUDY_DIR, rlim=14, **DA_KWARGS))
            for order in (1, 2):
                close_fig(plot_ampdet(study_dir=STUDY_DIR, the_id=DIFFABS_ID, order=order, setups=setups, out_label=out_label))


def lhc_compare_flat_hllhc_correctors():
    base_dir = STUDY_DIR
    machine = 'lhc'
    optics = "flat6015"
    xing = "top"
    iterations = 1
    for feeddown in (0, 2):
        for beam in (1, 4):
            other_beam = get_other_beam(beam)
            this_beam = get_this_beam(beam)
            setups = [
                dict(machine=machine, beam=beam, correct_beam="same",  xing="top", feeddown=feeddown, iterations=iterations, errors=ERRORS_ALL, optics=optics, rdts=RDTS_ALL_HLLHC, rdts2="None", label=f"from B{this_beam}"),
                dict(machine=machine, beam=beam, correct_beam="other", xing="top", feeddown=feeddown, iterations=iterations, errors=ERRORS_ALL, optics=optics, rdts=RDTS_ALL_HLLHC, rdts2="None", label=f"from B{other_beam}"),
                dict(machine=machine, beam=beam, correct_beam="same", xing="top", feeddown=feeddown, iterations=iterations, errors=ERRORS_ALL, optics=optics, rdts=RDTS_ALL_HLLHC, rdts2=RDTS_ALL_HLLHC, label="combined"),
            ]
            out_label = f"{machine}.b{beam}.xing_{xing}.optics_{optics}.fd_{feeddown}.errors_all.rdts_hllhcall"
            close_fig(plot_da(base_dir=base_dir, setups=setups, out_label=out_label, output_dir=STUDY_DIR, rlim=14, **DA_KWARGS))
            for order in (1, 2):
                close_fig(plot_ampdet(study_dir=STUDY_DIR, the_id=DIFFABS_ID, order=order, setups=setups, out_label=out_label))


if __name__ == '__main__':
    # gather_ptc_beams()
    # hllhc_compare_round()
    # hllhc_compare_flat()
    # lhc_compare_round()
    # lhc_compare_flat()
    # hllhc_compare_round_lhc_correctors()
    # hllhc_compare_flat_lhc_correctors()
    lhc_compare_round_hllhc_correctors()
    lhc_compare_flat_hllhc_correctors()
