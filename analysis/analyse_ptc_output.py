import itertools
from pathlib import Path
from typing import Iterable, Union

import matplotlib.patches as mpatches
import numpy as np
import pandas as pd
import tfs
from matplotlib import pyplot as plt, ticker
from matplotlib.collections import PolyCollection
from omc3.plotting.utils import style as pstyle, annotations as pannot, colors as pcolors
from omc3.utils import logging_tools
from analysis.file_crawler import (
    BEFORE_CORRECTION,
    JOB_ID_COLUMN, SEED_COLUMN, SEED_DIR_MASK,
    NOMINAL_ID, CORRECTED_ID, UNCORRECTED_ID, SEEDS, GATHER_FOLDER,
    get_working_dirs, get_beams_jobid, get_jobdir_name,
    delta_columns, seed_columns, stats_columns, no_correction_name, DELTA, PREFIX_FUN_MAP
)

LOG = logging_tools.get_logger(__name__)

DIFF_ID, DIFFABS_ID, RELDIFFABS_ID, TARGETDIFFABS_ID, RESIDUALDIFFABS_ID = "diff", "diffabs", "reldiffabs", "targetdiffabs", "resdiffabs"

JOB_ID_MASK_FEEDDOWN = "{MACHINE}.b{BEAM}.xing_{XING}.errors_{ERRORS}.rdts_{RDTS}"  # feed-down study
JOB_ID_MASK_UPDATE_FEEDDOWN = "{MACHINE}.b{BEAM}.xing_{XING}.errors_{ERRORS}.fd_{FEEDDOWN}.update_{UPDATE}.rdts_{RDTS}"  # feed-down and update study
JOB_ID_MASK_BEAMS = "{MACHINE}.b{BEAM}.optics_{OPTICS}.xing_{XING}.errors_{ERRORS}.fd_{FEEDDOWN}.rdts_{RDTS}.rdts_{RDTS2}"  # beams study

AMPDET_MASK_IN = 'ampdet.{machine:s}.b{beam:d}.{id:s}.tfs'
AMPDET_MASK_SEEDS = '{prefix:s}ampdet.{machine:s}.b{beam:d}.{id:s}.seeds.tfs'
AMPDET_MASK_STATS = '{prefix:s}ampdet.{machine:s}.b{beam:d}.{id:s}.statistics.tfs'


LABELS = {
    # Detuning
    "ANHX1000": r"$\partial Q_x/\partial (2 J_x)$",
    "ANHY0100": r"$\partial Q_y/\partial (2 J_y)$",
    "ANHX0100": r"$\partial Q_x/\partial (2 J_y)$",
    "ANHX2000": r"$\partial^2 Q_x/\partial (2 J_x)^2$",
    "ANHX0200": r"$\partial^2 Q_x/\partial (2 J_y)^2$",
    "ANHY2000": r"$\partial^2 Q_y/\partial (2 J_x)^2$",
    "ANHY0200": r"$\partial^2 Q_y/\partial (2 J_y)^2$",

    # Chroma
    "ANHX0010": r"$Q'_x$",
    "ANHY0010": r"$Q'_y$",
    "ANHX0020": r"$Q''_x$",
    "ANHY0020": r"$Q''_y$",
    "ANHX0030": r"$Q'''_x$",
    "ANHY0030": r"$Q'''_y$",
    "ANHX0040": r"$Q''''_x$",
    "ANHY0040": r"$Q''''_y$",
}


def get_columns(order, which=None):
    all_columns = LABELS.keys()
    if which == 'ampdet':
        all_columns = [c for c in all_columns if not int(c[6])]
    elif which == 'chroma':
        all_columns = [c for c in all_columns if int(c[6])]

    return [c for c in all_columns if sum(int(n) for n in c[4:]) == order]


def get_prefix(order):
    if order == 1:
        return ""
    elif order == 2:
        return "so_"
    elif order == 3:
        return "to_"
    elif order == 4:
        return "fo_"
    else:
        raise NotImplemented()


def gather_ptc_data(beam, machine, study_dir,  seeds=SEEDS, orders=(1, ), update=False):
    if isinstance(study_dir, Path):
        working_dirs = get_working_dirs(study_dir, beam, machine)
        output_dir = study_dir / GATHER_FOLDER
    else:
        working_dirs = []
        for current_dir in study_dir:
            working_dirs = working_dirs + get_working_dirs(current_dir, beam, machine)
        output_dir = study_dir[0] / GATHER_FOLDER

    output_dir.mkdir(exist_ok=True)

    dfs_stats = {o: _get_empty_stats_dfs(get_columns(o), working_dirs) for o in orders}
    if update:
        # Read tfs to update stats
        for order in orders:
            prefix = get_prefix(order)
            for id_ in dfs_stats[order].keys():
                try:
                    dfs_stats[order][id_] = tfs.read(output_dir / AMPDET_MASK_STATS.format(prefix=prefix, beam=beam, machine=machine, id=id_), index=JOB_ID_COLUMN)
                except FileNotFoundError:
                    pass

    for cwd in working_dirs:
        if all(cwd.name in df.index for dfs in dfs_stats.values() for df in dfs.values()):
            LOG.info(f"Working dir '{cwd.name}' already in dataframe. Skipping.")
            continue
        LOG.info(f"Current Working dir: {str(cwd)}")
        cwd_output_dir = cwd / GATHER_FOLDER
        cwd_output_dir.mkdir(exist_ok=True)

        dfs_seeds = {o: get_empty_seeds_dfs(seeds, get_columns(o)) for o in orders}

        # Read Data ---
        for seed in seeds:
            input_dir = cwd / SEED_DIR_MASK.format(seed=seed)
            LOG.info(f"Current SEED dir: {str(input_dir)}")
            for id_ in (NOMINAL_ID, UNCORRECTED_ID, CORRECTED_ID):
                if no_correction_name(cwd.name) and id_ != UNCORRECTED_ID:
                    continue

                ampdet_file = input_dir / AMPDET_MASK_IN.format(beam=beam, machine=machine, id=id_)
                try:
                    df_input = tfs.read(ampdet_file)
                except EnvironmentError as e:
                    LOG.error(f'{e!s} in {ampdet_file}')
                    continue

                for order in orders:
                    columns = get_columns(order)
                    dfs_seeds[order][id_].loc[seed, columns] = _get_detuning_from_ptc_output(df_input, columns).to_numpy()

        # Process Data ---
        for order in orders:
            prefix, columns = get_prefix(order), get_columns(order)
            dfs_seeds[order] = _calculate_diffs(dfs_seeds[order], columns)

            # Write Seeds out and calculate stats
            for id_, df_seed in dfs_seeds[order].items():
                if no_correction_name(cwd.name) and id_ != UNCORRECTED_ID:
                    continue

                tfs.write(cwd_output_dir / AMPDET_MASK_SEEDS.format(prefix=prefix, beam=beam, machine=machine, id=id_), df_seed, save_index=SEED_COLUMN)
                dfs_stats[order] = calculate_stats(dfs_stats[order], df_seed, id_, cwd)

    # Write Stats Data
    for order in orders:
        prefix, columns = get_prefix(order), get_columns(order)
        for id_, df_stats in dfs_stats[order].items():
            tfs.write(output_dir / AMPDET_MASK_STATS.format(prefix=prefix, beam=beam, machine=machine, id=id_), df_stats, save_index=JOB_ID_COLUMN)


def plot_ptc_ampdet_data(study_dir, the_id, setups, order=1, out_label=None, **kwargs):
    output_dir = study_dir / GATHER_FOLDER

    prefix, columns = get_prefix(order), get_columns(order, which='ampdet')

    # Set scale (reset for RELDIFFABS below)
    if order == 1:
        scale, scale_txt = 1e-3, "$10^3$ "
    elif order == 2:
        scale, scale_txt = 1e-12, "$10^{12}$ "
    else:
        raise NotImplemented()

    label = f'Detuning [{scale_txt}m$^{{-{order:d}}}$]'

    fig = plot_ptc(study_dir, the_id, setups, label, scale, prefix, columns, **kwargs)
    pannot.set_name(f'{prefix}ampdet.{the_id}.{out_label}', fig)
    fig.savefig(output_dir / f"plot.{fig.canvas.get_default_filename()}")
    return fig


def plot_ptc_chroma_data(study_dir, the_id, setups, order=1, out_label=None, **kwargs):
    output_dir = study_dir / GATHER_FOLDER

    prefix, columns = get_prefix(order), get_columns(order, which="chroma")
    LOG.debug(f"Plotting {prefix}chroma for {the_id} with label {out_label if out_label else ''}")

    # Set scale (reset for RELDIFFABS below)
    if order == 1:
        scale, scale_txt = 1e-0, ""
    elif order == 2:
        scale, scale_txt = 1e-3, " [$10^3$]"
    elif order == 3:
        scale, scale_txt = 1e-5, " [$10^5$]"
    elif order == 4:
        scale, scale_txt = 1e-7, " [$10^7$]"
    else:
        raise NotImplemented()

    label = "Q" + "'" * order + scale_txt

    fig = plot_ptc(study_dir, the_id, setups, label, scale, prefix, columns, **kwargs)
    pannot.set_name(f'{prefix}chroma.{the_id}.{out_label}', fig)
    fig.savefig(output_dir / f"plot.{fig.canvas.get_default_filename()}")
    return fig


def plot_ptc(study_dir, the_id, setups, label, scale, prefix, columns, **kwargs):
    # STYLE -------
    manual = {u"figure.figsize": [10.24, 5.12],
              u"markers.fillstyle": u'none', u'grid.alpha': 0,
              u'savefig.format': u'pdf'}

    ncol: np.numeric = kwargs.pop('ncol', 3)
    ylim: np.numeric = kwargs.pop('ylim', None)
    ylog: bool = kwargs.pop('ylog', False)
    yticks: Iterable[np.numeric] = kwargs.pop('yticks', None)
    xtickrotation: Iterable[np.numeric] = kwargs.pop('xtick.rotation', None)
    plot_styles: Iterable[Union[Path, str]] = kwargs.pop('plot_styles', 'standard')

    manual.update(kwargs)
    pstyle.set_style(plot_styles, manual)

    # DELTA ------
    delta = ''
    njobs = len(setups)
    if the_id in (DIFF_ID, DIFFABS_ID, RELDIFFABS_ID, TARGETDIFFABS_ID, RESIDUALDIFFABS_ID):
        njobs = njobs - sum([(s["rdts"] is None) or (s["rdts"] == "None") for s in setups])
        delta = DELTA
        if the_id in (DIFFABS_ID, TARGETDIFFABS_ID, RESIDUALDIFFABS_ID):
            split = label.split()
            if len(split) == 1:
                split += ['']
            label = f'|{split[0]}|{split[1]}'
        if the_id == RELDIFFABS_ID:
            label = f'$\Delta_{{rel}}${label}'
            scale, scale_txt = 1, ""
        else:
            label = f'$\Delta${label}'

    bar_width = 1/(njobs + 1)
    xlim = [- bar_width/2, (len(columns) - 1) + bar_width * (njobs + 0.5)]

    fig, ax = plt.subplots()

    # plot zero line
    ax.axhline(0, color="black", lw=1, ls="-", marker="", zorder=0)

    # plot separation lines
    for idx in range(1, len(columns)):
        ax.axvline(idx-bar_width/2, ls="--", lw=1, color="black", alpha=0.2, marker="", zorder=-5)

    handles = []
    violin_idx = 0
    for job_idx, job_setup in enumerate(setups):
        job_label = job_setup.pop("label", get_beams_jobid(**job_setup))
        job_dir = study_dir / get_jobdir_name(job_setup)
        job_setup["label"] = job_label

        load_id = the_id
        if job_setup["rdts"] is None or job_setup["rdts"] == "None":
            if the_id in (DIFF_ID, DIFFABS_ID):
                continue
            if the_id == CORRECTED_ID:
                load_id = UNCORRECTED_ID

        if job_label == BEFORE_CORRECTION:
            # Hack to trigger uncorrected in other context
            load_id = UNCORRECTED_ID

        if job_label == NOMINAL_ID:
            # Hack to trigger nominal in other context
            load_id = NOMINAL_ID


        df_job_seeds = tfs.read(
            job_dir / GATHER_FOLDER / AMPDET_MASK_SEEDS.format(
                prefix=prefix,
                beam=job_setup["beam"],
                machine=job_setup["machine"],
                id=load_id
            ),
            index=SEED_COLUMN
        )

        color = job_setup.get('color', pcolors.get_mpl_color(job_idx))

        # plot data
        handles.append(mpatches.Patch(color=color, label=job_label))
        for idx, ad_name in enumerate(columns):
            x_pos = idx + bar_width * (violin_idx + 0.5)

            seed_data = df_job_seeds.loc[:, f'{delta}{ad_name}'] * scale
            seed_data = check_for_outliers(seed_data, job_dir.name)
            std = seed_data.std()
            mean = seed_data.mean()
            std_min, std_max = mean-std, mean+std

            violin = ax.violinplot(dataset=seed_data, positions=[x_pos], widths=bar_width/2, showmeans=True, showextrema=True)

            for poly in violin['bodies']:
                poly.set_facecolor(color)
                poly.set_alpha(0.2)

                std_violin = PolyCollection([[v for v in poly.get_paths()[0].vertices if std_min <= v[1] <= std_max]])
                std_violin.set_facecolor(color)
                std_violin.set_alpha(0.4)
                ax.add_collection(std_violin)

            violin['cbars'].set_color(color)
            violin['cmeans'].set_color(color)

            for line_collection in ['cmaxes', 'cmins']:
                violin[line_collection].set_color(color)
                y_pos = violin[line_collection].get_segments()[0][0][1]
                violin[line_collection].set_segments([[[x_pos-bar_width/20, y_pos], [x_pos+bar_width/20, y_pos]]])

        violin_idx += 1

    ax.set_ylabel(label)
    ax.set_xticks(np.arange(len(columns)) + (bar_width*njobs)/2)
    ax.set_xticklabels([LABELS[c] for c in columns], rotation=xtickrotation)
    ax.set_xlim(xlim)

    ax.set_ylim(ylim)

    if ylog:
        ax.set_yscale("log")

        y_minor = ticker.LogLocator(base=10.0, subs=np.arange(1.0, 10.0) * 0.1, numticks=10)
        ax.yaxis.set_minor_locator(y_minor)
        ax.yaxis.set_minor_formatter(ticker.NullFormatter())

        y_major = ticker.LogLocator(base=10.0, subs=np.arange(1, 10.0), numticks=10)
        ax.yaxis.set_major_locator(y_major)

    if yticks is not None:
        ax.set_yticks(yticks)


    # handles, labels = ax.get_legend_handles_labels()
    # order = [1, 0, 2, 4, 3, 5]  # don't ask
    # pannot.make_top_legend(ax,
    #                        handles=_sort_legend(handles, order), labels=_sort_legend(labels, order),
    #                        ncol=2, frame=False)
    pannot.make_top_legend(ax, handles=handles, ncol=ncol, frame=False)

    fig.tight_layout()
    return fig


# Read Data --------------------------------------------------------------------

def _get_detuning_from_ptc_output(df, columns):
    """ Convert PTC output to DataFrame. """
    df_out = pd.DataFrame(columns=columns)
    for col in columns:
        if col[6] == '0':
            # ampdet
            df_out[col] = df.query(
                f'NAME == "{col[:4]}" and '
                f'ORDER1 == {col[4]} and ORDER2 == {col[5]} '
                f'and ORDER3 == {col[6]} and ORDER4 == {col[7]}'
            )["VALUE"].to_numpy()
        else:
            # chroma
            plane2num = {"X": 1, "Y": 2}
            df_out[col] = df.query(
                f'NAME == "DQ{plane2num[col[3]]}" and ORDER1 == {col[6]}'
            )["VALUE"].to_numpy()
    return df_out


def _get_empty_stats_dfs(columns, working_dirs):
    """ DataFrame structure (dict for IDs) for the statistics (over seeds)"""
    dfs_stats = {k: tfs.TfsDataFrame(index=[cwd.name for cwd in working_dirs],
                                     columns=stats_columns(columns) + seed_columns(columns))
                 for k in (NOMINAL_ID, CORRECTED_ID, UNCORRECTED_ID)}
    dfs_stats.update({k: tfs.TfsDataFrame(index=[cwd.name for cwd in working_dirs],
                                          columns=stats_columns(delta_columns(columns)) + seed_columns(delta_columns(columns)))
                      for k in (DIFF_ID, DIFFABS_ID, RELDIFFABS_ID, TARGETDIFFABS_ID, RESIDUALDIFFABS_ID)})
    return dfs_stats


def get_empty_seeds_dfs(seeds, columns):
    """ DataFrame structure (dict for IDs) for the results per seed."""
    return {k: tfs.TfsDataFrame(index=list(seeds), columns=columns, dtype=float) for k in (NOMINAL_ID, CORRECTED_ID, UNCORRECTED_ID, DIFF_ID, DIFFABS_ID, RELDIFFABS_ID, TARGETDIFFABS_ID, RESIDUALDIFFABS_ID)}


def _calculate_diffs(dfs_seeds, columns):
    """ Calculate the differences according to the IDs. """
    dfs_seeds[DIFF_ID] = dfs_seeds[CORRECTED_ID] - dfs_seeds[UNCORRECTED_ID]
    dfs_seeds[DIFF_ID].columns = delta_columns(columns)

    dfs_seeds[DIFFABS_ID] = dfs_seeds[CORRECTED_ID].abs() - dfs_seeds[UNCORRECTED_ID].abs()
    dfs_seeds[DIFFABS_ID].columns = delta_columns(columns)

    dfs_seeds[TARGETDIFFABS_ID] = dfs_seeds[NOMINAL_ID].abs() - dfs_seeds[UNCORRECTED_ID].abs()
    dfs_seeds[TARGETDIFFABS_ID].columns = delta_columns(columns)

    dfs_seeds[RESIDUALDIFFABS_ID] = dfs_seeds[NOMINAL_ID].abs() - dfs_seeds[CORRECTED_ID].abs()
    dfs_seeds[RESIDUALDIFFABS_ID].columns = delta_columns(columns)

    dfs_seeds[RELDIFFABS_ID] = dfs_seeds[DIFFABS_ID] / dfs_seeds[TARGETDIFFABS_ID]
    # dfs_seeds[RELDIFFABS_ID].columns = _delta_columns(columns)
    return dfs_seeds


def calculate_stats(dfs_stats, df_seed, id_, cwd):
    """ Calculate the statistics over the seeds. """
    for col, (pre, np_fun) in itertools.product(df_seed.columns, PREFIX_FUN_MAP.items()):
        dfs_stats[id_].loc[cwd.name, f"{pre}{col}"] = np_fun(df_seed[col])
        if pre == "MIN":
            dfs_stats[id_].loc[cwd.name, f"{pre}SEED{col}"] = df_seed[col].idxmin(skipna=True)
        if pre == "MAX":
            dfs_stats[id_].loc[cwd.name, f"{pre}SEED{col}"] = df_seed[col].idxmax(skipna=True)
    return dfs_stats


def check_for_outliers(data: pd.Series, name: str, filter=True):
    quartile1 = data.quantile(q=0.25)
    quartile3 = data.quantile(q=0.75)
    inter_quartile_range = quartile3 - quartile1
    cutoff = 100 * inter_quartile_range
    mask = (data < (quartile1-cutoff)) | (data > (quartile3+cutoff))
    # +-100 * data.mad()  # median absolute deviation should also work
    if any(mask):
        LOG.warning(f"{sum(mask)} Outliers found for {name}!")
        if filter:
            data = data[~mask]
    return data


if __name__ == '__main__':
    pass
