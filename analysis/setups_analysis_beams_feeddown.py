from pathlib import Path
from analyse_ptc_output import gather_ptc_data, plot_ptc_ampdet_data as plot_ampdet, DIFFABS_ID, TARGETDIFFABS_ID, RELDIFFABS_ID, RESIDUALDIFFABS_ID
from analyse_da_output import plot_jobs_and_save as plot_da
from matplotlib import pyplot as plt

from omc3.utils import logging_tools

LOG = logging_tools.get_logger(__name__)


STUDY_DIR = Path("/eos/home-j/jdilly/work/study.irnl_correction_multi_beams")
ERRORS_ALL = "AB3AB4AB5AB6AB7AB8"
RDTS_ALL_HLLHC = "b6-b6_a6-a6_b5-b5_a5-a5_b4-b4_a4-a4_b3-b3_a3-a3"
RDTS_ALL_LHC = "b6-b6_b4-b4_a4-a4_b3-b3_a3-a3"

DA_KWARGS = {
    'plot_std': True,
    'figure.figsize': [6.4, 6.4],
    'legend.handlelength': 1.,
    'legend.handletextpad': .4,
    'legend.fontsize': 20,
    'xtick.labelsize': 20,
    'xtick.major.pad': 10,
    'ytick.labelsize': 20,
    'axes.labelsize': 20,
    'amplitude_ticks': list(range(2, 20, 2))
}

AMPDET_KWARGS = {
    'figure.figsize': [7.68, 5.76],
    'legend.columnspacing': 0.5,
    'legend.handlelength': 1.,
    'legend.fontsize': 22,
    'xtick.labelsize': 24,
    'ytick.labelsize': 24,
    'axes.labelsize': 24,
}

SO_AMPDET_KWARGS = {
    'figure.figsize': [7.68, 6.4],
    'legend.columnspacing': 0.5,
    'legend.handlelength': 1.,
    'legend.fontsize': 22,
    'xtick.labelsize': 24,
    'xtick.alignment': 'center',
    'xtick.rotation': 20,
    'ytick.labelsize': 24,
    'axes.labelsize': 24,
}


def close_fig(fig):
    plt.close(fig)


def gather_ptc_beams():
    for machine in ('lhc', 'hllhc'):
        for beam in (1, 4):
            for order in (1, 2):
                gather_ptc_data(study_dir=STUDY_DIR, machine=machine, beam=beam, order=order)


def hllhc_compare_allerr():
    base_dir = STUDY_DIR
    machine = 'hllhc'
    optics = "round1515"
    iterations = 1
    for beam in (1, 4):
        setups = [
            dict(machine=machine, beam=beam, correct_beam="same",  xing="flat", feeddown=0, iterations=iterations, errors=ERRORS_ALL, optics=optics, rdts=RDTS_ALL_HLLHC, rdts2="None", label=f"flat"),
            dict(machine=machine, beam=beam, correct_beam="same", xing="top", feeddown=0, iterations=iterations, errors=ERRORS_ALL, optics=optics, rdts=RDTS_ALL_HLLHC, rdts2="None", label=f"w/o feeddown"),
            dict(machine=machine, beam=beam, correct_beam="same", xing="top", feeddown=2, iterations=iterations, errors=ERRORS_ALL, optics=optics, rdts=RDTS_ALL_HLLHC, rdts2="None", label="w/ feeddown"),
        ]
        out_label = f"{machine}.b{beam}.optics_{optics}.errors_all.rdts_all.feeddownstudy"
        close_fig(plot_da(base_dir=base_dir, setups=setups, out_label=out_label, output_dir=STUDY_DIR, rlim=20, **DA_KWARGS))
        close_fig(plot_ampdet(study_dir=STUDY_DIR, the_id=DIFFABS_ID, order=1, setups=setups, out_label=out_label, ylim=[-560, 100], **AMPDET_KWARGS))
        close_fig(plot_ampdet(study_dir=STUDY_DIR, the_id=DIFFABS_ID, order=2, setups=setups, out_label=out_label, ylim=None, **SO_AMPDET_KWARGS))
        close_fig(plot_ampdet(study_dir=STUDY_DIR, the_id=TARGETDIFFABS_ID, order=1, setups=setups, out_label=f'{TARGETDIFFABS_ID}.{out_label}', ylim=[-560, 100], **AMPDET_KWARGS))
        close_fig(plot_ampdet(study_dir=STUDY_DIR, the_id=TARGETDIFFABS_ID, order=2, setups=setups, out_label=f'{TARGETDIFFABS_ID}.{out_label}', ylim=None, **SO_AMPDET_KWARGS))
        close_fig(plot_ampdet(study_dir=STUDY_DIR, the_id=RELDIFFABS_ID, order=1, setups=setups, out_label=f'{RELDIFFABS_ID}.{out_label}', ylim=None, **AMPDET_KWARGS))
        close_fig(plot_ampdet(study_dir=STUDY_DIR, the_id=RELDIFFABS_ID, order=2, setups=setups, out_label=f'{RELDIFFABS_ID}.{out_label}', ylim=None, **SO_AMPDET_KWARGS))
        close_fig(plot_ampdet(study_dir=STUDY_DIR, the_id=RESIDUALDIFFABS_ID, order=1, setups=setups, out_label=f'{RESIDUALDIFFABS_ID}.{out_label}', ylim=[-560, 100], **AMPDET_KWARGS))
        close_fig(plot_ampdet(study_dir=STUDY_DIR, the_id=RESIDUALDIFFABS_ID, order=2, setups=setups, out_label=f'{RESIDUALDIFFABS_ID}.{out_label}', ylim=None, **SO_AMPDET_KWARGS))


def hllhc_compare_allerr_lhc_correction():
    base_dir = STUDY_DIR
    machine = 'hllhc'
    optics = "round1515"
    iterations = 1
    for beam in (1, 4):
        setups = [
            # dict(machine=machine, beam=beam, correct_beam="same",  xing="flat", feeddown=0, iterations=iterations, errors=ERRORS_ALL, optics=optics, rdts=RDTS_ALL_LHC, rdts2="None", label=f"flat"),
            dict(machine=machine, beam=beam, correct_beam="same", xing="top", feeddown=0, iterations=iterations, errors=ERRORS_ALL, optics=optics, rdts=RDTS_ALL_LHC, rdts2="None", label=f"w/o feeddown"),
            dict(machine=machine, beam=beam, correct_beam="same", xing="top", feeddown=2, iterations=iterations, errors=ERRORS_ALL, optics=optics, rdts=RDTS_ALL_LHC, rdts2="None", label="w/ feeddown"),
        ]
        out_label = f"{machine}.b{beam}.optics_{optics}.errors_all.rdts_lhcall.feeddownstudy"
        close_fig(plot_da(base_dir=base_dir, setups=setups, out_label=out_label, output_dir=STUDY_DIR, rlim=20, **DA_KWARGS))
        close_fig(plot_ampdet(study_dir=STUDY_DIR, the_id=DIFFABS_ID, order=1, setups=setups, out_label=out_label, ylim=[-560, 100], **AMPDET_KWARGS))
        close_fig(plot_ampdet(study_dir=STUDY_DIR, the_id=DIFFABS_ID, order=2, setups=setups, out_label=out_label, ylim=None, **SO_AMPDET_KWARGS))
        close_fig(plot_ampdet(study_dir=STUDY_DIR, the_id=TARGETDIFFABS_ID, order=1, setups=setups, out_label=f'{TARGETDIFFABS_ID}.{out_label}', ylim=[-560, 100], **AMPDET_KWARGS))
        close_fig(plot_ampdet(study_dir=STUDY_DIR, the_id=TARGETDIFFABS_ID, order=2, setups=setups, out_label=f'{TARGETDIFFABS_ID}.{out_label}', ylim=None, **SO_AMPDET_KWARGS))
        close_fig(plot_ampdet(study_dir=STUDY_DIR, the_id=RELDIFFABS_ID, order=1, setups=setups, out_label=f'{RELDIFFABS_ID}.{out_label}', ylim=None, **AMPDET_KWARGS))
        close_fig(plot_ampdet(study_dir=STUDY_DIR, the_id=RELDIFFABS_ID, order=2, setups=setups, out_label=f'{RELDIFFABS_ID}.{out_label}', ylim=None, **SO_AMPDET_KWARGS))
        close_fig(plot_ampdet(study_dir=STUDY_DIR, the_id=RESIDUALDIFFABS_ID, order=1, setups=setups, out_label=f'{RESIDUALDIFFABS_ID}.{out_label}', ylim=[-560, 100], **AMPDET_KWARGS))
        close_fig(plot_ampdet(study_dir=STUDY_DIR, the_id=RESIDUALDIFFABS_ID, order=2, setups=setups, out_label=f'{RESIDUALDIFFABS_ID}.{out_label}', ylim=None, **SO_AMPDET_KWARGS))


def lhc_compare_allerr():
    base_dir = STUDY_DIR
    machine = 'lhc'
    optics = "round3030"
    iterations = 1
    for beam in (1, 4):
        setups = [
            dict(machine=machine, beam=beam, correct_beam="same",  xing="flat", feeddown=0, iterations=iterations, errors=ERRORS_ALL, optics=optics, rdts=RDTS_ALL_LHC, rdts2="None", label=f"flat"),
            dict(machine=machine, beam=beam, correct_beam="same", xing="top", feeddown=0, iterations=iterations, errors=ERRORS_ALL, optics=optics, rdts=RDTS_ALL_LHC, rdts2="None", label=f"w/o feeddown"),
            dict(machine=machine, beam=beam, correct_beam="same", xing="top", feeddown=2, iterations=iterations, errors=ERRORS_ALL, optics=optics, rdts=RDTS_ALL_LHC, rdts2="None", label="w/ feeddown"),
        ]
        out_label = f"{machine}.b{beam}.optics_{optics}.errors_all.rdts_all.feeddownstudy"
        close_fig(plot_da(base_dir=base_dir, setups=setups, out_label=out_label, output_dir=STUDY_DIR, rlim=14, **DA_KWARGS))
        close_fig(plot_ampdet(study_dir=STUDY_DIR, the_id=DIFFABS_ID, order=1, setups=setups, out_label=out_label, ylim=[-170, 50], **AMPDET_KWARGS))
        close_fig(plot_ampdet(study_dir=STUDY_DIR, the_id=DIFFABS_ID, order=2, setups=setups, out_label=out_label, ylim=None, **SO_AMPDET_KWARGS))
        close_fig(plot_ampdet(study_dir=STUDY_DIR, the_id=TARGETDIFFABS_ID, order=1, setups=setups, out_label=f'{TARGETDIFFABS_ID}.{out_label}', ylim=[-170, 50], **AMPDET_KWARGS))
        close_fig(plot_ampdet(study_dir=STUDY_DIR, the_id=TARGETDIFFABS_ID, order=2, setups=setups, out_label=f'{TARGETDIFFABS_ID}.{out_label}', ylim=None, **SO_AMPDET_KWARGS))
        close_fig(plot_ampdet(study_dir=STUDY_DIR, the_id=RELDIFFABS_ID, order=1, setups=setups, out_label=f'{RELDIFFABS_ID}.{out_label}', ylim=None, **AMPDET_KWARGS))
        close_fig(plot_ampdet(study_dir=STUDY_DIR, the_id=RELDIFFABS_ID, order=2, setups=setups, out_label=f'{RELDIFFABS_ID}.{out_label}', ylim=None, **SO_AMPDET_KWARGS))
        close_fig(plot_ampdet(study_dir=STUDY_DIR, the_id=RESIDUALDIFFABS_ID, order=1, setups=setups, out_label=f'{RESIDUALDIFFABS_ID}.{out_label}', ylim=[-170, 50], **AMPDET_KWARGS))
        close_fig(plot_ampdet(study_dir=STUDY_DIR, the_id=RESIDUALDIFFABS_ID, order=2, setups=setups, out_label=f'{RESIDUALDIFFABS_ID}.{out_label}', ylim=None, **SO_AMPDET_KWARGS))


def lhc_compare_allerr_hllhc_correctors():
    base_dir = STUDY_DIR
    machine = 'lhc'
    optics = "round3030"
    iterations = 1
    for beam in (1, 4):
        setups = [
            dict(machine=machine, beam=beam, correct_beam="same",  xing="flat", feeddown=0, iterations=iterations, errors=ERRORS_ALL, optics=optics, rdts=RDTS_ALL_HLLHC, rdts2="None", label=f"flat"),
            dict(machine=machine, beam=beam, correct_beam="same", xing="top", feeddown=0, iterations=iterations, errors=ERRORS_ALL, optics=optics, rdts=RDTS_ALL_HLLHC, rdts2="None", label=f"w/o feeddown"),
            dict(machine=machine, beam=beam, correct_beam="same", xing="top", feeddown=2, iterations=iterations, errors=ERRORS_ALL, optics=optics, rdts=RDTS_ALL_HLLHC, rdts2="None", label="w/ feeddown"),
        ]
        out_label = f"{machine}.b{beam}.optics_{optics}.errors_all.rdts_hllhcall.feeddownstudy"
        close_fig(plot_da(base_dir=base_dir, setups=setups, out_label=out_label, output_dir=STUDY_DIR, rlim=14, **DA_KWARGS))
        close_fig(plot_ampdet(study_dir=STUDY_DIR, the_id=DIFFABS_ID, order=1, setups=setups, out_label=out_label, ylim=[-170, 50], **AMPDET_KWARGS))
        close_fig(plot_ampdet(study_dir=STUDY_DIR, the_id=DIFFABS_ID, order=2, setups=setups, out_label=out_label, ylim=None, **SO_AMPDET_KWARGS))
        close_fig(plot_ampdet(study_dir=STUDY_DIR, the_id=TARGETDIFFABS_ID, order=1, setups=setups, out_label=f'{TARGETDIFFABS_ID}.{out_label}', ylim=[-170, 50], **AMPDET_KWARGS))
        close_fig(plot_ampdet(study_dir=STUDY_DIR, the_id=TARGETDIFFABS_ID, order=2, setups=setups, out_label=f'{TARGETDIFFABS_ID}.{out_label}', ylim=None, **SO_AMPDET_KWARGS))
        close_fig(plot_ampdet(study_dir=STUDY_DIR, the_id=RELDIFFABS_ID, order=1, setups=setups, out_label=f'{RELDIFFABS_ID}.{out_label}', ylim=None, **AMPDET_KWARGS))
        close_fig(plot_ampdet(study_dir=STUDY_DIR, the_id=RELDIFFABS_ID, order=2, setups=setups, out_label=f'{RELDIFFABS_ID}.{out_label}', ylim=None, **SO_AMPDET_KWARGS))
        close_fig(plot_ampdet(study_dir=STUDY_DIR, the_id=RESIDUALDIFFABS_ID, order=1, setups=setups, out_label=f'{RESIDUALDIFFABS_ID}.{out_label}', ylim=[-170, 50], **AMPDET_KWARGS))
        close_fig(plot_ampdet(study_dir=STUDY_DIR, the_id=RESIDUALDIFFABS_ID, order=2, setups=setups, out_label=f'{RESIDUALDIFFABS_ID}.{out_label}', ylim=None, **SO_AMPDET_KWARGS))


if __name__ == '__main__':
    # gather_ptc_beams()
    # hllhc_compare_allerr()
    # hllhc_compare_allerr_lhc_correction()
    # lhc_compare_allerr()
    lhc_compare_allerr_hllhc_correctors()
