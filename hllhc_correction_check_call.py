from pathlib import Path

from hllhc_correction_check import main
import hllhc_check_setups
import sys


def setup2str(**setup):

    xing = "scheme_flat"
    if len(setup["xing"]):
        xing = "-".join("".join(item).replace("_", "") for item in setup["xing"].items())

    errors = 'None'
    if len(setup["errors"]):
        errors = "".join(sorted(setup["errors"].keys()))


    rdt_orders = [f"{'a' if sum(int(i) for i in s[3:5]) % 2 else 'b'}{sum(int(i) for i in s[1:5])}" for s in setup['rdts']]
    if isinstance(setup["rdts"], list):
        rdtcorr = '_'.join(sorted(set([f"{rdt}-by-{rdt}" for rdt in rdt_orders])))
    else:
        correctors = [''.join(sorted(corr)) for corr in setup['rdts'].values()]
        rdtcorr = '_'.join(sorted(set([f"{rdt}-by-{corr}" for rdt, corr in zip(rdt_orders, correctors)])))
    return f"{setup['machine']}.b{setup['beam']}.xing_{xing}.errors_{errors.lower()}.fd_{setup['feeddown']}.rdts_{rdtcorr}"


def run_main(cwd, **setup):
    name = setup2str(**setup)
    setup["outputdir"] = cwd / name
    setup["tempdir"] = cwd / f"temp_{name}"

    print(setup)
    main(**setup)


if __name__ == '__main__':
    beam = 1
    cwd = Path("/afs/cern.ch/work/j/josch/study.correction_test")
    cwd.mkdir(exist_ok=True)

    for arg in sys.argv[1:]:
        try:
            setup = getattr(hllhc_check_setups, f"setup{arg}")
        except AttributeError:
            print(f"Setup {arg} not found.")
        else:
            run_main(
                cwd=cwd,
                beam=beam,
                **setup
            )
