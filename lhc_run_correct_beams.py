import logging
import shutil
from pathlib import Path
from typing import Sequence

import tfs
from cpymad.madx import Madx
from optics_functions.rdt import calculate_rdts
from optics_functions.utils import prepare_twiss_dataframe
from optics_functions.coupling import coupling_via_cmatrix, closest_tune_approach

from cpymad_lhc.coupling_correction import correct_coupling
from cpymad_lhc.general import (get_tfs, switch_magnetic_errors, match_tune, get_k_strings, amplitude_detuning_ptc, sixtrack_output, rdts_ptc,
                                get_lhc_sequence_filename_and_bv)
from cpymad_lhc.ir_orbit import orbit_setup, log_orbit
from cpymad_lhc.logging import cpymad_logging_setup
from pylhc.irnl_rdt_correction import main as irnl_correct, write_tfs, write_command

LOG = logging.getLogger(__name__)  # setup in main()
LOG_LEVEL = logging.DEBUG

OUTDIR = Path("Outputdata")
TEMPDIR = Path('temp')

RDT_PART_MAP = {
    "A": "ABS",
    "C": "REAL",
    "S": "IMAG",
}

PATHS = {
    "db5": Path("/afs/cern.ch/eng/lhc/optics/V6.503"),
    "optics2016": Path("/afs/cern.ch/eng/lhc/optics/runII/2016"),
    "optics2018": Path("/afs/cern.ch/eng/lhc/optics/runII/2018"),
    "external": Path("/afs/cern.ch/work/j/jdilly/public/macros"),
}


def pathstr(key: str, *args: str):
    return str(PATHS[key].joinpath(*args))


def get_optics_path(optics: str):
    optics_map = {
        'inj': pathstr("optics2018", "PROTON", "opticsfile.1"),
        'flat6015': pathstr("optics2018", 'MDflatoptics2018', 'opticsfile_flattele60cm.21'),
        'round3030': pathstr("optics2018", "PROTON", "opticsfile.22_ctpps2")
    }
    return optics_map[optics]


def get_wise_path(seed: int):
    return f"/afs/cern.ch/work/j/jdilly/wise/WISE-2015-LHCsqueeze-0.4_10.0_0.4_3.0-6.5TeV-emfqcs/WISE.errordef.{seed:04d}.tfs"


def apply_errors(madx: Madx, *magnets: str):
    for magnet in magnets:
        madx.call(pathstr('db5', 'measured_errors', f'Efcomp_{magnet}.madx'))


def make_dirs(*dirs: Path):
    for dir_ in dirs:
        dir_.mkdir(exist_ok=True, parents=True)


def drop_allzero_columns(df: tfs.TfsDataFrame):
    return df.loc[:, (df != 0).any(axis="index")]


def get_other_beam(beam: int):
    return 1 if beam == 4 else 4


DEFAULT_COLUMNS = ['NAME', 'KEYWORD', 'S', 'X', 'Y', 'L', 'LRAD',
                   'BETX', 'BETY', 'ALFX', 'ALFY', 'DX', 'DY', 'MUX', 'MUY',
                   'R11', 'R12', 'R21', 'R22'] + get_k_strings()


def main(correct_beam: str,
         rdts: Sequence, xing: dict, errors: dict, optics: str,
         feeddown: int,
         iterations: int,
         outputdirs: dict,
         rdts2: Sequence = None,
         seed: int = 1,
         ):
    make_dirs(*outputdirs.values())

    # user definitions valid for both beams
    tune_x = 62.31
    tune_y = 60.32
    chroma = 3
    emittance = 7.29767146889e-09
    n_particles = 1.0e10   # number of particles in beam

    # apply field errors to arcs
    on_arc_errors = False

    madx_instances, optics_dfs, errors_dfs, loggers = {}, {}, {}, {}
    for beam, outputdir in outputdirs.items():
        madx = Madx(**cpymad_logging_setup(level=LOG_LEVEL,  # sets also standard loggers
                                           command_log=outputdir/'madx_commands.log',
                                           full_log=outputdir/'full_output.log'))
        mvars = madx.globals  # shorthand
        mvars.mylhcbeam = beam  # used in macros

        # Define Sequence to use
        seq_name, seq_file, bv_flag = get_lhc_sequence_filename_and_bv(beam)

        # Output helper - using python parameter-space magic
        def output_path(type_, output_id, dir_=None):
            if dir_ is None:
                dir_ = outputdir
            return dir_ / f'{type_}.lhc.b{beam:d}.{output_id}.tfs'

        def get_twiss(output_id=None, index_regex=r"BPM|M|IP", dir_=None, pre_match_tune=True):
            if dir_ is None:
                dir_ = outputdir
            if pre_match_tune:
                match_tune(madx,
                           accel="LHC", sequence=seq_name,
                           qx=tune_x, qy=tune_y,
                           dqx=chroma, dqy=chroma)
            madx.twiss(sequence=seq_name)
            df_twiss = get_tfs(madx.table.twiss, columns=DEFAULT_COLUMNS, index_regex=index_regex)
            if output_id is not None:
                tfs.write(output_path('twiss', output_id, dir_=dir_), drop_allzero_columns(df_twiss), save_index="NAME")
            return df_twiss

        def get_errors(output_id=None, index_regex="M", dir_=None):
            if dir_ is None:
                dir_ = outputdir
            # As far as I can tell `only_selected` does not work with
            # etable and there is always only the selected items in the table
            # (jdilly, cpymad 1.4.1)
            error_columns = ["NAME", "DX", "DY"] + get_k_strings()
            madx.select(flag='error', clear=True)
            madx.select(flag='error', column=error_columns)
            madx.etable(table='error')
            df_errors = get_tfs(madx.table.error, index_regex=index_regex, columns=error_columns)
            if output_id is not None:
                tfs.write(output_path('errors', output_id, dir_=dir_), drop_allzero_columns(df_errors), save_index="NAME")
            return df_errors

        def output_ampdet(output_id):
            return amplitude_detuning_ptc(madx, ampdet=2, chroma=4, file=output_path('ampdet', output_id))

        # Load Macros
        madx.call(pathstr("optics2018", "toolkit", "macro.madx"))

        # Lattice Setup ---------------------------------------
        # Load Sequence
        madx.call(pathstr("optics2018", seq_file))

        # Slice Sequence
        mvars.slicefactor = 4
        madx.beam()
        madx.call(pathstr("optics2018", "toolkit", "myslice.madx"))
        madx.beam()
        madx.use(sequence=seq_name)
        madx.makethin(sequence=seq_name, style="teapot", makedipedge=True)

        # Cycling w.r.t. to IP3 (mandatory to find closed orbit in collision in the presence of errors)
        madx.seqedit(sequence=seq_name)
        madx.flatten()
        madx.cycle(start="IP3")
        madx.endedit()

        # Define Optics and make beam
        madx.call(get_optics_path(optics))
        if optics == 'inj':
            mvars.NRJ = 450.000

        madx.beam(sequence=seq_name, bv=bv_flag,
                  energy="NRJ", particle="proton", npart=n_particles,
                  kbunch=1, ex=emittance, ey=emittance)

        # Setup Orbit
        orbit_vars = orbit_setup(madx, accel='lhc', **xing)

        madx.use(sequence=seq_name)

        # Save Nominal
        df_nominal = get_twiss('nominal')
        df_ampdet_nominal = output_ampdet('nominal')
        log_orbit(madx, accel='lhc')

        # Save nominal optics in IR+Correctors for ir nl correction
        df_twiss_nominal_ir = get_twiss('optics_ir', index_regex="M(QS?X|BX|BRC|C[SOT]S?X)", pre_match_tune=False)

        # Call error subroutines and measured error table for nominal LHC
        madx.call(file=pathstr('optics2016', 'measured_errors', 'Msubroutines.madx'))
        madx.readtable(file=pathstr('optics2016', 'measured_errors', 'rotations_Q2_integral.tab'))
        madx.call(file=pathstr('optics2016', 'errors', 'macro_error.madx'))
        madx.call(file=pathstr('optics2016', 'toolkit', 'Orbit_Routines.madx'))
        madx.call(file=pathstr('optics2016', 'measured_errors', 'Msubroutines_new.madx'))  # think the new subroutines are only relevant for MSS - not used pre-2017 so shouldn't make a difference compared to old Msubroutines...
        madx.call(file=pathstr('optics2016', 'measured_errors', 'Msubroutines_MS_MSS_MO_new.madx'))
        madx.call(file=pathstr('optics2016', 'toolkit', 'Orbit_Routines.madx'))
        madx.call(file=pathstr('optics2016', 'toolkit', 'SelectLHCMonCor.madx'))
        madx.readtable(file=pathstr('optics2016', 'measured_errors', 'rotations_Q2_integral.tab'))
        madx.call(file=pathstr('optics2016', 'errors', 'macro_error.madx'))  # some macros for error generation

        # Apply magnetic errors -------------------------------
        switch_magnetic_errors(madx, **errors)

        # Read WISE
        madx.readtable(file=get_wise_path(seed))

        # Apply errors to elements ---
        if on_arc_errors:
            apply_errors(madx, 'MB', 'MQ')

        apply_errors(madx,
                     # IR Dipoles
                     'MBXW',  # D1 in IP1 and IP5
                     'MBRC',  # D2
                     'MBX',  # D in IP2 and 8
                     'MBRB',  # IP4
                     'MBRS',  # IP4
                     'MBW',  # IP7 and IP3
                     # IR Quads
                     'MQX',
                     'MQY',
                     'MQM',
                     'MQMC',
                     'MQML',
                     'MQTL',
                     'MQW',
                     )

        # Save uncorrected
        if rdts is None:
            df_coupling_corrected_before = coupling_via_cmatrix(get_twiss(pre_match_tune=False))
            closest_tune_approach(df_coupling_corrected_before, qx=tune_x, qy=tune_y)
            correct_coupling(madx, accel="lhc", sequence=seq_name,
                             qx=tune_x, qy=tune_y, dqx=chroma, dqy=chroma)

        df_uncorrected = get_twiss('uncorrected')
        df_ampdet_uncorrected = output_ampdet('uncorrected')
        df_errors = get_errors('all')

        df_coupling_uncorrected = coupling_via_cmatrix(df_uncorrected)
        closest_tune_approach(df_coupling_uncorrected, qx=tune_x, qy=tune_y)

        # Save errors to table to be used for correction ---------------------------
        # As far as I can tell `only_selected` does not work with
        # etable and there is always only the selected items in the table
        # (jdilly, cpymad 1.4.1)
        df_errors_ir = get_errors('ir', index_regex=r"M([QB]X|BRC)")
        madx_instances[beam] = madx
        optics_dfs[beam] = df_twiss_nominal_ir
        errors_dfs[beam] = df_errors_ir
        loggers[beam] = logging.getLogger("").handlers

    # IR Nonlinear Correction: Calculate corrector powering --------------------
    if rdts is not None:
        correction_cmds, correction_dfs = {}, {}
        if rdts2 is not None:
            # correct with both optics
            logging.getLogger("").handlers = [h for handlers in loggers.values() for h in handlers]
            correction_cmds[1], correction_dfs[1] = irnl_correct(
                # Correction setup for LHC ---
                beams=(1, 4),
                optics=list(optics_dfs.values()),
                errors=list(errors_dfs.values()),
                rdts=rdts,
                rdts2=rdts2,
                accel='lhc',
                feeddown=feeddown,  # order of feeddown to take into account
                iterations=iterations,
                ignore_corrector_settings=True,  # all correctors are assumed unpowered
                # The following are the defaults ---
                ips=(1, 2, 5, 8),  # in which IPs to correct
                solver='lstsq',  # 'inv', 'linear' Solver to use
                ignore_missing_columns=False,  # True: if columns are missing, assume 0
            )
            correction_cmds[4] = correction_cmds[1]
            correction_dfs[4] = correction_dfs[1]
        else:
            # calculate the corrections for both beams, but assign as correction
            # for the respective other beam
            for beam in outputdirs.keys():
                logging.getLogger("").handlers = loggers[beam]
                correction_cmds[beam], correction_dfs[beam] = irnl_correct(
                    # Correction setup for LHC ---
                    beams=(beam,),
                    optics=(optics_dfs[beam],),
                    errors=(errors_dfs[beam],),
                    rdts=rdts,
                    accel='lhc',
                    feeddown=feeddown,  # order of feeddown to take into account
                    iterations=iterations,
                    ignore_corrector_settings=True,  # all correctors are assumed unpowered
                    # The following are the defaults ---
                    ips=(1, 2, 5, 8),  # in which IPs to correct
                    solver='lstsq',  # 'inv', 'linear' Solver to use
                    ignore_missing_columns=False,  # True: if columns are missing, assume 0
                )

        # Apply corrections
        for beam, outputdir in outputdirs.items():
            logging.getLogger("").handlers = loggers[beam]
            if correct_beam == "same":
                corrector_beam = beam
            elif correct_beam == "other":
                corrector_beam = get_other_beam(beam)
            else:
                corrector_beam = int(correct_beam[-1])

            correct_name = f'mcx_b{corrector_beam}'
            if rdts2 is not None:
                correct_name += f'b{get_other_beam(corrector_beam)}'

            madx = madx_instances[beam]
            seq_name, seq_file, bv_flag = get_lhc_sequence_filename_and_bv(beam)
            write_command(output_path('settings', correct_name).with_suffix('.madx'), correction_cmds[beam])
            write_tfs(output_path('settings', correct_name), correction_dfs[beam])

            # Apply correction
            madx.input(correction_cmds[corrector_beam])

            # Correct Coupling
            df_coupling_corrected_before = coupling_via_cmatrix(get_twiss(pre_match_tune=False))
            closest_tune_approach(df_coupling_corrected_before, qx=tune_x, qy=tune_y)

            correct_coupling(madx, accel="lhc", sequence=seq_name,
                             qx=tune_x, qy=tune_y, dqx=chroma, dqy=chroma)

            # Save corrected
            df_corrected = get_twiss('corrected')
            df_ampdet_corrected = output_ampdet('corrected')

            df_coupling_corrected = coupling_via_cmatrix(df_corrected)
            closest_tune_approach(df_coupling_corrected, qx=tune_x, qy=tune_y)
            log_orbit(madx, accel='lhc')

    # Write Sixtrack output for both beams -------------------------------------
    for beam in reversed(list(outputdirs.keys())):
        logging.getLogger("").handlers = loggers[beam]
        madx = madx_instances[beam]
        sixtrack_output(madx, madx.globals.nrj)
        # manually move sixtrack output files
        # this is why other_beam needs to run first
        # (no easy way to specify output dir for them)
        for f in Path(".").glob("fc*"):
            if beam != 1:
                shutil.move(f, outputdirs[beam] / f.name)
            else:
                shutil.copy(f, outputdirs[beam] / f.name)
        madx.exit()
